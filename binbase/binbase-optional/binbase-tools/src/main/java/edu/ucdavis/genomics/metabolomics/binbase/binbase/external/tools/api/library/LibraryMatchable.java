package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library;

import java.io.OutputStream;
import java.util.List;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.Writer;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.Library;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;

/**
 * defines a basic approach to match all bins against a library
 * 
 * @author wohlgemuth
 * 
 */
public interface LibraryMatchable {

	/**
	 * returns all available bins
	 * 
	 * @return
	 */
	public List<Bin> getBins();

	/**
	 * returns all available libraries
	 * 
	 * @return
	 */
	public List<Library> getLibraries();

	/**
	 * returns the retention index range
	 * 
	 * @return
	 */
	public int getRiRange();

	/**
	 * returns the minimal similarity
	 * 
	 * @return
	 */
	public double getMinimalSimilarity();

	/**
	 * runs the actual calculation
	 * 
	 * @return
	 * @throws Exception
	 */
	public DataFile calculate() throws Exception;

	/**
	 * returns the column name
	 * 
	 * @return
	 */
	public String getColumnName();

	/**
	 * calculates the result and writes it with the outputstream to the given writer
	 * @param writer
	 * @param out
	 * @throws Exception
	 */
	void calculateAndWrite(Writer writer, OutputStream out) throws Exception;

}
