package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.impl.database;

import java.util.List;
import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.ModelFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.Library;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.LibraryMassSpec;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.Ion;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.AbstractLibraryMatchable;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.DatabaseLibraryMatchable;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.DriverUtilities;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile;

/**
 * we need to provide all the properties to access the database locally
 * 
 * @author wohlgemuth
 */
public class DatabaseLibraryMatchableImplementation extends AbstractLibraryMatchable implements DatabaseLibraryMatchable {

	private IModel<Bin> binModel;

	private IModel<Library> libraryModel;

	private String databaseName;

	private String databasePassword;

	private String databaseServer;

	private final Logger logger = Logger.getLogger(getClass());

	@SuppressWarnings("unchecked")
	@Override
	protected DataFile doCalculate() throws Exception {
		logger.info("load bins...");
		final List<Bin> bins = getBins();
		logger.info("load libraries...");

		final List<Library> libraries = getLibraries();

		logger.info("define datafile...");
		final SimpleDatafile datafile = new SimpleDatafile();
		datafile.setDimension(1 + bins.size(), (libraries.size() * 2) + 2);

		datafile.setCell(0, 0, "name");
		datafile.setCell(0, 1, "id");

		int position = 2;
		int i = 0;

		for (final Library library : libraries) {
			logger.info(position);
			final String name = library.getName();

			datafile.setCell(position, 0, name);
			datafile.setCell(position + 1, 0, "unqiue mass");

			logger.info("matching: " + name);

			for (int x = 0; x < bins.size(); x++) {
				final Bin bin = bins.get(x);
				logger.info("progress: " + (x / (double) bins.size() * 100));

				datafile.setCell(0, x + 1, bin.getName());
				datafile.setCell(1, x + 1, bin.getId());

				final List<LibraryMassSpec> res = (List<LibraryMassSpec>) library.getMatchingContent(bin, getMinimalSimilarity(), getRiRange());

				// if result is empty not much todo
				if (res.isEmpty()) {
					datafile.setCell(position, x + 1, false);
					datafile.setCell(position + 1, x + 1, "");
				}
				// else get the highest similarity hit and calculate the unique
				// mass
				else {
					datafile.setCell(position, x + 1, true);

					final LibraryMassSpec spec = res.get(0);

					final int mass = bin.getUniqueMass();
					double intensity = 0;
					double baseIntensity = 0;

					for (final Ion ion : spec.getSpectra()) {
						if (ion.getMass() == mass) {
							intensity = ion.getAbsoluteIntensity();
						}
						if (ion.getMass() == spec.getBasePeak().intValue()) {
							baseIntensity = ion.getAbsoluteIntensity();
						}
					}

					final double finalIntensity = intensity / baseIntensity * 100;

					datafile.setCell(position + 1, x + 1, finalIntensity);
				}

			}

			position++;
			position++;
			i++;
		}

		return datafile;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void prepare() throws Exception {
		super.prepare();
		logger.info("preparing connection...");
		final Properties p = new Properties();
		p.setProperty(ConnectionFactory.KEY_DATABASE_PROPERTIE, getDatabaseName());
		p.setProperty(ConnectionFactory.KEY_HOST_PROPERTIE, getDatabaseServer());
		p.setProperty(ConnectionFactory.KEY_PASSWORD_PROPERTIE, getDatabasePassword());
		p.setProperty(ConnectionFactory.KEY_USERNAME_PROPERTIE, getUserName());
		p.setProperty(ConnectionFactory.KEY_TYPE_PROPERTIE, String.valueOf(DriverUtilities.POSTGRES));

		p.put("hibernate.cache.use_minimal_puts", "false");
		p.put("hibernate.cache.use_query_cache", "false");
		p.put("hibernate.cache.use_second_level_cache", "false");

		HibernateFactory.newInstance(p);

		logger.info("creating models...");
		binModel = ModelFactory.newInstance().createModel(Bin.class);
		libraryModel = ModelFactory.newInstance().createModel(Library.class);
	}

	public List<Bin> getBins() {

		binModel.setQuery("select a from Bin a");
		final Object[] result = binModel.executeQuery();
		final List<Bin> res = new Vector<Bin>(result.length);

		for (final Object element : result) {
			res.add((Bin) element);
		}
		return res;
	}

	public List<Library> getLibraries() {
		libraryModel.setQuery("select a from Library a");
		final Object[] result = libraryModel.executeQuery();
		final List<Library> res = new Vector<Library>(result.length);

		for (final Object element : result) {
			res.add((Library) element);
		}
		return res;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public String getDatabasePassword() {
		return databasePassword;
	}

	public String getDatabaseServer() {
		return databaseServer;
	}

	public final String getUserName() {
		return getColumnName();
	}

	public void setDatabaseName(final String value) {
		databaseName = value;
	}

	public void setDatabasePassword(final String value) {
		databasePassword = value;
	}

	public void setDatabaseServer(final String value) {
		databaseServer = value;
	}

	public void setUserName(final String value) {
		setColumnName(value);
	}
}
