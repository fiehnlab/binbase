package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.impl.database;

import java.util.Properties;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.DatabaseConfiguration;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;

/**
 * simplest possible confiugration
 * 
 * @author wohlgemuth
 * 
 */
public class SimpleConfiguration implements DatabaseConfiguration {

	private Logger logger = Logger.getLogger(getClass());
	
	private String databaseName;
	private String hostName;
	private String password;

	public void setDatabaseName(final String databaseName) {
		this.databaseName = databaseName;
	}

	public void setHostName(final String hostName) {
		this.hostName = hostName;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public void setType(final String type) {
		this.type = type;
	}

	public void setUserName(final String userName) {
		this.userName = userName;
	}

	private String type;
	private String userName;

	public String getDatabaseName() {
		return databaseName;
	}

	public String getHostName() {
		return hostName;
	}

	public String getPassword() {
		return password;
	}

	public String getType() {
		return type;
	}

	public String getUserName() {
		return userName;
	}

	/**
	 * generate the standard 
	 */
	public Properties generateConnectionProperties() {
		Properties p = new Properties();

		p.setProperty(ConnectionFactory.KEY_USERNAME_PROPERTIE, getUserName());
		p.setProperty(ConnectionFactory.KEY_DATABASE_PROPERTIE,
				getDatabaseName());
		p.setProperty(ConnectionFactory.KEY_HOST_PROPERTIE, getHostName());
		p.setProperty(ConnectionFactory.KEY_TYPE_PROPERTIE, getType());
		p.setProperty(ConnectionFactory.KEY_PASSWORD_PROPERTIE, getPassword());

		logger.info(p);
		return p;
	}

}
