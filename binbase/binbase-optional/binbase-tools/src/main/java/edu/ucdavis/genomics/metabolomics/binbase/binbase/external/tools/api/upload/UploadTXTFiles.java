package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.upload;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.io.Copy;

/**
 * 
 * @author wohlgemuth
 * 
 */
public class UploadTXTFiles {

	private static Logger logger = Logger.getLogger(UploadTXTFiles.class);
	
	public static void main(String[] args) {

		if (args.length < 2) {
			System.out.println("Readme:");
			System.out.println("");
			System.out
					.println("this tool is used to upload a Pegasus TXT file to the default location of the server. So that it can be used for calculations");

			System.out.println("Usage:");
			System.out.println("");
			System.out.println("arg[0] : application server");
			System.out.println("arg[1] : local folder");

			System.out.println("");

			System.exit(-1);
		}

		XMLConfigurator.getInstance().reset();
		XMLConfigurator.getInstance().addConfigurationPropertie(
				"java.naming.factory.initial",
				"org.jnp.interfaces.NamingContextFactory", true);
		XMLConfigurator.getInstance().addConfigurationPropertie(
				"java.naming.factory.url.pkgs",
				"org.jboss.naming:org.jnp.interfaces", true);
		XMLConfigurator.getInstance().addConfigurationPropertie(
				"java.naming.provider.url", args[0] + ":1099", true);

		importData(new File(args[1]));
	}

	private static void importData(File file) {
		if (file.isDirectory()) {
			for (File f : file.listFiles()) {
				importData(f);
			}
		} else if (file.isFile()) {
			if (file.getName().endsWith(".txt")
					|| file.getName().endsWith(".txt.gz")) {
				ByteArrayOutputStream out = new ByteArrayOutputStream();

				try {
					logger.info("uploading: " + file);
					Copy.copy(new FileInputStream(file), out);

					Configurator.getImportService().uploadImportFile(
							file.getName(), out.toByteArray());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}
	}
}
