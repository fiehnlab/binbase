package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.export;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.TXT;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.math.BinComparisonMatrix;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.util.HibernatePreparation;
import edu.ucdavis.genomics.metabolomics.binbase.util.locking.BinGenerationLock;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;

public class CalculateBinConparisonMatrix {

	public static void main(String[] args) throws FileNotFoundException,
			IOException, Exception {
		if (args.length < 5) {
			System.out.println("Usage:");
			System.out.println("");
			System.out.println("arg[0] : application server");
			System.out.println("arg[1] : column");
			System.out.println("arg[2] : ri-window");
			System.out.println("arg[3] : similarity");

			System.out.println("arg4] : outPutFile:");
			System.out.println("");

			System.exit(-1);
		}


		try {
			HibernatePreparation.prepare(args[0], args[1]);
			BinGenerationLock.getInstance().obtainLock(args[1],"");

			BinComparisonMatrix matrix = new BinComparisonMatrix();
			DataFile file = matrix.createMatrix(Integer.parseInt(args[2]),
					Integer.parseInt(args[3]));

			TXT txt = new TXT();
			txt.write(new FileOutputStream(new File(args[4])), file);
		} finally {

			BinGenerationLock.getInstance().releaseLock(args[1],"");
		}
	}
}
