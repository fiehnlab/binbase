package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;

/**
 * factory to create configurations for us
 * @author wohlgemuth
 *
 */
public abstract class DatabaseConfigurationFactory extends AbstractFactory {

	public final static String DEFAULT_PROPERTIE = DatabaseConfigurationFactory.class.getName();

	/**
	 * prepares the factory
	 * 
	 * @param map
	 */
	public abstract void prepare(Map<?,?> map);

	/**
	 * returnst the configured configuration
	 * 
	 * @return
	 */
	public abstract DatabaseConfiguration getConfiguration();

	/**
	 * gets the factory
	 * 
	 * @return
	 */
	public static DatabaseConfigurationFactory getFactory() {
		return getFactory(System.getProperties().getProperty(DEFAULT_PROPERTIE), System.getProperties());
	}

	/**
	 * gets the factory with the map as configuration
	 * 
	 * @param map
	 * @return
	 */
	public static DatabaseConfigurationFactory getFactory(final Map<?,?> map) {
		return getFactory(System.getProperties().getProperty(DEFAULT_PROPERTIE), map);
	}

	/**
	 * returns the factory
	 * 
	 * @param name
	 * @param configuration
	 * @return
	 */
	public static DatabaseConfigurationFactory getFactory(final String name, final Map<?,?> configuration) {
		if (name == null) {
			throw new FactoryException("sorry no default factory found!");
		}
		else {
			DatabaseConfigurationFactory factory = null;
			try {
				factory = (DatabaseConfigurationFactory) Class.forName(name).newInstance();
			}
			catch (final InstantiationException e) {
				throw new FactoryException(e.getMessage(), e);
			}
			catch (final IllegalAccessException e) {
				throw new FactoryException(e.getMessage(), e);
			}
			catch (final ClassNotFoundException e) {
				throw new FactoryException(e.getMessage(), e);
			}
			factory.prepare(configuration);
			return factory;
		}
	}
}
