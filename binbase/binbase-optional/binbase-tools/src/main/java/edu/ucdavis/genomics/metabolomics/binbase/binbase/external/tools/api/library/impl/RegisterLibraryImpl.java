package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.msp.ImportMSPFiles;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.DatabaseConfiguration;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.DatabaseConfigurationFactory;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.RegisterLibrary;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.impl.application.ApplicationServerDatabaseConfigurationFactory;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.util.HibernatePreparation;
import edu.ucdavis.genomics.metabolomics.sjp.exception.ParserException;

/**
 * registers the database
 * 
 * @author wohlgemuth
 * 
 */
public class RegisterLibraryImpl implements RegisterLibrary {

	private Logger logger = Logger.getLogger(getClass());

	private String applicationServer;

	private String column;

	public String getApplicationServer() {
		return applicationServer;
	}

	public void setApplicationServer(final String applicationServer) {
		logger.info("setting server: " + applicationServer);
		this.applicationServer = applicationServer;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(final String column) {
		logger.info("setting column: " + column);
		this.column = column;
	}

	public void register(final File file) throws FileNotFoundException,
			IOException {

		HibernatePreparation.prepare(applicationServer, column);
		ImportMSPFiles msp = new ImportMSPFiles();

		try {
			msp.importMsp(file, System.getProperties());
		} catch (ParserException e) {
			throw new IOException(e.getMessage(),e);
		} catch (InstantiationException e) {
			throw new IOException(e.getMessage(),e);
		} catch (IllegalAccessException e) {
			throw new IOException(e.getMessage(),e);
		}
	}

	public static void main(final String args[]) throws FileNotFoundException,
			IOException {
		if (args.length < 3) {
			System.out.println("Usage:");
			System.out.println("");
			System.out.println("arg[0] : application server");
			System.out.println("arg[1] : column");
			System.out.println("arg[2] : libaryFile:");
			System.out.println("");

			System.exit(-1);
		}

		final RegisterLibraryImpl impl = new RegisterLibraryImpl();
		impl.setApplicationServer(args[0]);
		impl.setColumn(args[1]);

		final File file = new File(args[2]);
		impl.register(file);
	}
}
