package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library;

/**
 * executes the matching by fetching the configurastion over the application
 * server
 * 
 * @author wohlgemuth
 * 
 */
public interface ApplicationServerLibraryMatchable extends
		DatabaseLibraryMatchable {

	/**
	 * returns the application server
	 * 
	 * @return
	 */
	public String getApplicationServer();

	/**
	 * sets the application server
	 * @param value
	 */
	public void setApplicationServer(String value);
}
