package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library;

import java.io.OutputStream;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.Writer;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;

public abstract class AbstractLibraryMatchable implements LibraryMatchable {

	private double minimalSimilarity;
	private int riRange;

	public void setMinimalSimilarity(final double minimalSimilarity) {
		this.minimalSimilarity = minimalSimilarity;
	}

	public void calculateAndWrite(final Writer writer, final OutputStream out)
			throws Exception {
		final DataFile file = calculate();
		writer.write(out, file);

		out.flush();
		out.close();
	}

	public void setRiRange(final int riRange) {
		this.riRange = riRange;
	}

	public void setColumnName(final String columnName) {
		this.columnName = columnName;
	}

	private String columnName;

	public final DataFile calculate() throws Exception {
		prepare();
		return doCalculate();
	}

	/**
	 * prepares some stuff
	 * @throws Exception 
	 */
	protected void prepare() throws Exception {
		if (getColumnName() == null) {
			throw new RuntimeException("you need to provide a column name");
		}
		if (getColumnName().length() == 0) {
			throw new RuntimeException("you need to provide a column name");
		}

		if (getMinimalSimilarity() <= 0) {
			throw new RuntimeException(
					"the attribute 'minimalSimilarity' needs to be larger 0");
		}
		if (getRiRange() <= 0) {
			throw new RuntimeException(
					"the attribute 'riRange' needs to be larger 0");
		}
	}

	/**
	 * does the actual calculation
	 * 
	 * @return
	 * @throws Exception
	 */
	protected abstract DataFile doCalculate() throws Exception;

	public String getColumnName() {
		return columnName;
	}

	public double getMinimalSimilarity() {
		return minimalSimilarity;
	}

	public int getRiRange() {
		return riRange;
	}

}
