package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.export;

import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.DelegateSetupXFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXFactory;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;

/**
 * used to upload a binbase result to the setupx/minix system or another
 * implementation
 * 
 * @author wohlgemuth
 * 
 */
public class UploadBinBaseResult {

	public static void main(String args[]) throws BinBaseException {

		if (args.length < 2) {
			System.out.println("Readme:");
			System.out.println("");
			System.out
					.println("this tool is used to attach a result file in the binbase result folder to the related study.");
			System.out
					.println("Please be aware does it not allows you to upload any file to the system! It's just a little tool incase there went something wrong during the communication phase");

			System.out.println("Usage:");
			System.out.println("");
			System.out.println("arg[0] : application server");
			System.out.println("arg[1] : experiment id");

			System.out
					.println("arg[2] : optional filename on the server to be attached, otherwise the default file is used");

			System.out.println("");

			System.exit(-1);
		}

		XMLConfigurator.getInstance().reset();
		XMLConfigurator.getInstance().addConfigurationPropertie(
				"java.naming.factory.initial",
				"org.jnp.interfaces.NamingContextFactory", true);
		XMLConfigurator.getInstance().addConfigurationPropertie(
				"java.naming.factory.url.pkgs",
				"org.jboss.naming:org.jnp.interfaces", true);
		XMLConfigurator.getInstance().addConfigurationPropertie(
				"java.naming.provider.url", args[0] + ":1099", true);

		if (args.length == 2) {
			SetupXFactory
					.newInstance(DelegateSetupXFactory.class.getName())
					.createProvider(
							XMLConfigurator.getInstance().getProperties())
					.upload(args[1], args[1]);
		} else {
			SetupXFactory
			.newInstance(DelegateSetupXFactory.class.getName())
			.createProvider(
					XMLConfigurator.getInstance().getProperties())
			.upload(args[1], args[2]);
		}

	}
}
