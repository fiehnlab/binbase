package edu.ucdavis.genomics.metabolomics.binbase.binbase.dsl;

import java.io.File;
import java.util.regex.Pattern;

public class GenerateDSLFromDatabase {

	/**
	 * main method
	 * 
	 * @param args
	 */
	public static void main(String args[]) {

		if (args.length == 3) {
			edu.ucdavis.genomics.metabolomics.binbase.dsl.generate.GenerateDSLFromDatabase generate = new edu.ucdavis.genomics.metabolomics.binbase.dsl.generate.GenerateDSLFromDatabase();
			String dsl = generate.dslGenerateForDatabase(args[1], args[0],
					true, 80, "xls");
			generate.writeDSL(new File(args[2]), dsl);
		} else if (args.length == 4) {
			edu.ucdavis.genomics.metabolomics.binbase.dsl.generate.GenerateDSLFromDatabase generate = new edu.ucdavis.genomics.metabolomics.binbase.dsl.generate.GenerateDSLFromDatabase();
			String dsl = generate.dslGenerateFromDatabaseForFilePattern(args[1],Pattern.compile(args[3]), args[0],
					true, 80, "xls");
			generate.writeDSL(new File(args[2]), dsl);

		}
		else{
			System.out.println("Usage:");
			System.out.println("");
			System.out.println("arg[0] : application server");
			System.out.println("arg[1] : column");
			System.out.println("arg[2] : outPutFile:");
			System.out.println("");

			System.out.println("optional:");
			System.out.println("");
			System.out
					.println("arg[3] : fileNamePattern - required fileName pattern to include the files into the dsl. If the sample name doesnt match, it will be skipped during the DSL generation");
			System.out.println("");

			System.exit(-1);			
		}
	}
}
