package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.impl.application;

import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.DatabaseConfiguration;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.impl.database.SimpleConfiguration;

/**
 * just a marker
 * @author wohlgemuth
 *
 */
public class ApplicationServerDatabaseConfiguration extends SimpleConfiguration implements DatabaseConfiguration {

}
