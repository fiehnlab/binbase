package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.util;

import java.util.Properties;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.DatabaseConfiguration;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.DatabaseConfigurationFactory;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.impl.application.ApplicationServerDatabaseConfigurationFactory;

/**
 * does nothing else than set the jvm wide hibernate configuartion based on the
 * application server setting
 * 
 * @author wohlgemuth
 * 
 */
public class HibernatePreparation {

	/**
	 * prepare all the hibernate settings
	 * 
	 * @param applicationServer
	 * @param column
	 */
	public static void prepare(String applicationServer, String column) {
		DatabaseConfigurationFactory factory = ApplicationServerDatabaseConfigurationFactory
				.getFactory(ApplicationServerDatabaseConfigurationFactory.class
						.getName(),
						ApplicationServerDatabaseConfigurationFactory
								.buildConfiguration(applicationServer, column));
		DatabaseConfiguration config = factory.getConfiguration();

		Properties p = new Properties();
		p = config.generateConnectionProperties();
		p.put("hibernate.cache.use_minimal_puts", "false");
		p.put("hibernate.cache.use_query_cache", "false");
		p.put("hibernate.cache.use_second_level_cache", "false");

		HibernateFactory.newInstance(p);
	}
}
