package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * registers a library in the system
 * @author wohlgemuth
 *
 */
public interface RegisterLibrary {

	/**
	 * registers the library
	 * @param name
	 * @param file
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	public void register(File file) throws FileNotFoundException, IOException;

}
