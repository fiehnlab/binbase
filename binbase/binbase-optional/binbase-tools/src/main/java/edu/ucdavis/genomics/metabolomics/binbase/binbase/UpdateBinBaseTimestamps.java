package edu.ucdavis.genomics.metabolomics.binbase.binbase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseServiceFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.DatabaseJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.ModelFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.DatabaseConfiguration;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.DatabaseConfigurationFactory;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.library.impl.application.ApplicationServerDatabaseConfigurationFactory;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.util.HibernatePreparation;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;

/**
 * updates all the binbase timestamps using the file resolver from the server
 * 
 * @author wohlgemuth
 * 
 */
public class UpdateBinBaseTimestamps {

	public UpdateBinBaseTimestamps() {
	}

	public static void main(String[] args) throws BinBaseException,
			SQLException {
		if (args.length != 2) {
			System.out.println("Usage:");
			System.out.println("");
			System.out.println("arg[0] : application server");
			System.out.println("arg[1] : column");

			System.out.println("");

			System.exit(-1);
		}

		Logger logger = Logger.getLogger(UpdateBinBaseTimestamps.class);

		DatabaseConfigurationFactory factory = ApplicationServerDatabaseConfigurationFactory
				.getFactory(ApplicationServerDatabaseConfigurationFactory.class
						.getName(),
						ApplicationServerDatabaseConfigurationFactory
								.buildConfiguration(args[0], args[1]));
		DatabaseConfiguration config = factory.getConfiguration();

		ConnectionFactory fact2 = ConnectionFactory.createFactory();

		fact2.setProperties(config.generateConnectionProperties());

		Connection c = fact2.getConnection();

		BinBaseService service = BinBaseServiceFactory.createFactory()
				.createService();

		ResultSet s = c
				.prepareStatement(
						"select distinct sample_name,date from samples order by date desc")
				.executeQuery();

		while (s.next()) {

			String name = s.getString(1);

			try {

				long timestamp = service.getTimeStampForSample(name,
						Configurator.getKeyManager().getInternalKey());
				Timestamp date = new Timestamp(timestamp);

				PreparedStatement up = c
						.prepareStatement("update samples set date = ? where sample_name = ?");

				up.setTimestamp(1, date);
				up.setString(2, name);
				int update = up.executeUpdate();

				up.close();

				logger.info("updated sample " + name + " with date " + date
						+ " applied to: " + update + " samples");
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}

	}

}
