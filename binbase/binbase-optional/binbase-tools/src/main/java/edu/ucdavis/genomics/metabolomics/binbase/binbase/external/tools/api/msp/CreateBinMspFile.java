package edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.msp;

import java.io.File;
import java.io.IOException;

import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.msp.BinToMspConverter;
import edu.ucdavis.genomics.metabolomics.binbase.binbase.external.tools.api.util.HibernatePreparation;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.ClusterException;
import edu.ucdavis.genomics.metabolomics.binbase.util.locking.BinGenerationLock;

/**
 * creates a binbase msp file containing all bins from the database
 * 
 * @author wohlgemuth
 * 
 */
public class CreateBinMspFile {

	public static void main(String[] args) throws IOException, Exception,
			ClusterException, NamingException {

		try {

			if (args.length >= 4) {
				HibernatePreparation.prepare(args[0], args[1]);
				BinGenerationLock.getInstance().obtainLock(args[1], "");

				String caseValue = args[3].toLowerCase();

				if (caseValue.equals("known")) {
					BinToMspConverter
							.convertAllKnownBinsToMsp(new File(args[2]));
				} else if (caseValue.equals("unknown")) {
					BinToMspConverter.convertAllUnKnownBinsToMsp(new File(args[2]));
				} else if (caseValue.equals("all")) {
					BinToMspConverter
							.convertAllBinsToMsp(new File(args[2]));
				} else if (caseValue.equals("id")) {
					if (args.length == 5) {
						BinToMspConverter.convertBinsToMsp(args[4], new File(
								args[2]), ",");
					} else {
						System.out
								.println("we need a total of 5 arguments in this mode!");
					}
				}

			} else {
				System.out.println("About:");
				System.out.println("");
				System.out
						.println("This tool exports all known bins as msp file, or only the specific id's. It wil not export unknowns bins!");
				System.out.println("");
				System.out.println("Usage:");
				System.out.println("");
				System.out.println("arg[0] : application server");
				System.out.println("arg[1] : column");
				System.out.println("arg[2] : outputFile:");
				System.out
						.println("arg[3] : mode [ known | unknown | all | ids ]");

				System.out
						.println("arg[4] : the id of the bin's you like to convert, seperated by a comma, if the selected mode id's");

				System.exit(-1);
			}
		} finally {
			BinGenerationLock.getInstance().releaseLock(args[1], "");
		}
	}
}
