package edu.ucdavis.genomics.metabolomics.binbase.minix.wsdl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

public class MiniXProviderTest {

	MiniXProvider toTest;
	
	String experimentId = "";
	
	@Before
	public void setUp() throws Exception {
		toTest = new MiniXProvider("http://minix.fiehnlab.ucdavis.edu/services/communications?wsdl","ipa");

	}

	@After
	public void tearDown() throws Exception {
	}
/*
	@Test
	public void testGetMetaInformationByClass() throws BinBaseException {
		String result = toTest.getMetaInformationByClass("343626");
		assertNotNull(result);
	}

	@Test
	public void testGetMetaInformationByExperiment() throws BinBaseException {
		String result = toTest.getMetaInformationByExperiment("343581");
		assertNotNull(result);
		}

	@Test
	public void testGetMetaInformationBySample() throws BinBaseException {
		MiniXProvider toTest2 = toTest;
		String result = toTest2.getMetaInformationBySample("343605");
		assertNotNull(result);	}
*/
	@Test
	public void testGetSetupXId() throws BinBaseException {
		String result = toTest.getSetupXId("110131baasa02");
		System.out.println(result);
		assertNotNull(result);
		assertTrue(result.equals("14"));

	}
	@Test
	public void testUpload() throws BinBaseException {
		toTest.upload("150994", "150994.zip");
	}
	
	@Test
	public void testGetLAbel() throws BinBaseException {
		toTest.getLabel("sdasdsa");
	}
	
	
}
