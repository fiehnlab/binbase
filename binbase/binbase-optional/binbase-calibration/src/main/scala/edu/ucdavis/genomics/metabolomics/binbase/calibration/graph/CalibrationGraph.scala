package edu.ucdavis.genomics.metabolomics.binbase.calibration.graph

import scala.collection.JavaConversions.asScalaSet
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.BinNotFoundException
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.CalibrationFile
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool.DontWriteProcessableResult
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.ObjectDetector
import edu.ucdavis.genomics.metabolomics.util.math.LinearRegression
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.FormatObject
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.GraphRenderer
import edu.ucdavis.genomics.metabolomics.util.math.Regression

/**
 * creates a calibration graph for all the selected standards
 */
class CalibrationGraph extends DontWriteProcessableResult with GraphRenderer {

  def getDescription: String = "generates a subfolder with the calibration graphs for this datafile"

  /**
   * does the actual processing
   */
  def process(datafile: ResultDataFile, configuration: Element): DataFile = {

    datafile match {
      case file: CalibrationFile =>

        file.getCalibrationStandards().toSet.foreach { standard: String =>
          try {
            val bin = file.getBinPosition(standard)
            val regression = file.getCurve(standard).asInstanceOf[Regression]

            val x: Array[Double] = regression.getXData()
            val y: Array[Double] = regression.getYData()

            val image = createXYGraph(x, y, standard, "curve", "height", "concentration", 800, 600)

            writeJPEGImage(image, standard);

          } catch {
            case e: BinNotFoundException => //nothing
          }
        }

        return file
      case _ =>
        //wrong kind of file just return it
        return datafile
    }
  }

  override def getFolder(): String = {
    "curve"
  }
}