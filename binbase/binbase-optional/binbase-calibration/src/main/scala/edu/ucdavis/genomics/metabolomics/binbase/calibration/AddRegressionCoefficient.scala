package edu.ucdavis.genomics.metabolomics.binbase.calibration

import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool.DontWriteProcessableResult
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.FormatObject
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.ObjectDetector
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.CalibrationFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.BinNotFoundException
import edu.ucdavis.genomics.metabolomics.util.math.LinearRegression


/**
 * ads a regression coefficient to the result file
 */
class AddRegressionCoefficient   extends DontWriteProcessableResult with ObjectDetector {

  def getDescription: String = "add's rows with the regression coefficient to the datafile, if the calibrated datafile used this model"

  /**
   * does the actual processing
   */
  def process(datafile: ResultDataFile, configuration: Element): DataFile = {

      datafile match {
      case file: CalibrationFile =>

        var positions: List[Int] = List.empty

        file.getCalibrationStandards().toSet.foreach { standard: String =>
          try {
            val bin = file.getBinPosition(standard)

            val formulas = file.getCurve(standard).getCoeffizent()

            while (formulas.size > positions.size) {
              val position = insertProtectedRow(datafile)
              positions = positions :+ position
              file.getCell(0, position).asInstanceOf[FormatObject[Any]].setValue("regression coefficient")
            }

            val it = positions.iterator
            formulas.foreach { reg: Double =>

              file.getCell(bin, it.next).asInstanceOf[FormatObject[Any]].setValue(reg)

            }
          } catch {
            case e: BinNotFoundException => //nothing
          }
        }

        return file
      case _ =>
        //wrong kind of file just return it
        return datafile
    }
  }
}