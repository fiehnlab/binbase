package edu.ucdavis.genomics.metabolomics.binbase.calibration

import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool.DontWriteProcessableResult
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.FormatObject
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.ObjectDetector
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.CalibrationFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile

/**
 * removes bins which are not calibrated
 */
class RemoveNotCalibratedBins extends DontWriteProcessableResult with ObjectDetector {

  def getDescription: String = "removes not calibrated bin's from the result"

  /**
   * does the actual processing
   */
  def process(datafile: ResultDataFile, configuration: Element): DataFile = {

    datafile match {
      case file: CalibrationFile =>

        file.removeNotCalibratedBins()

        return file
      case _ =>
        //wrong kind of file just return it
        return datafile
    }
  }
}