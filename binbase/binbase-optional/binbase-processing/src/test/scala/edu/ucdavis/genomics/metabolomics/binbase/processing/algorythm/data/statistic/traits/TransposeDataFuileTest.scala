package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits
import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFileFactory
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile

@RunWith(classOf[JUnitRunner])
class TransposeDataFuileTest extends FunSuite with TransposeDataFile with DataFilePrint {

  test("testing the transposing of a 2x2 datafile") {
    val file: DataFile = DataFileFactory.newInstance().createDataFile()

    file.setDimension(2, 2)
    file.setCell(0, 0, "a")
    file.setCell(0, 1, "b")
    file.setCell(1, 0, "c")
    file.setCell(1, 1, "d")

    val result: DataFile = transpose(file)
    assert(file.getCell(0, 0) == "a")
    assert(file.getCell(0, 1) == "b")
    assert(file.getCell(1, 0) == "c")
    assert(file.getCell(1, 1) == "d")

  }

  test("testing the transposing of a 2x3 datafile") {
    val file: DataFile = DataFileFactory.newInstance().createDataFile()

    file.setDimension(3, 2)
    file.setCell(0, 0, "a")
    file.setCell(0, 1, "b")
    file.setCell(0, 2, "c")

    file.setCell(1, 0, "d")
    file.setCell(1, 1, "e")
    file.setCell(1, 2, "f")

    file.setIgnoreColumn(0, true)
    val result: DataFile = transpose(file)

    assert(result.getCell(0, 0) == "a")
    assert(result.getCell(1, 0) == "b")
    assert(result.getCell(2, 0) == "c")

    assert(result.getCell(0, 1) == "d")
    assert(result.getCell(1, 1) == "e")
    assert(result.getCell(2, 1) == "f")

  }

  test("testing the transposing of a 4x10 datafile") {
    val file: DataFile = DataFileFactory.newInstance().createDataFile()

    file.setDimension(4, 10)

    for (x <- 0 until 10) {
      for (y <- 0 until 4) {
        file.setCell(x, y, x + " - " + y)
      }
    }

    file.setIgnoreColumn(0, true)
    file.setIgnoreColumn(1, true)
    file.setIgnoreColumn(4, true)
    file.setIgnoreColumn(8, true)
    
    val result: DataFile = transpose(file)

  }

}