package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.action
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.action.BasicAction
import edu.ucdavis.genomics.metabolomics.util.io.source.Source
import org.jdom.Element
import java.io.InputStream
import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling
import org.jdom.output.XMLOutputter
import org.jdom.output.Format
import java.io.ByteArrayInputStream

/**
 * writes the sop file to the result directory of this calculation
 * and can be used to inform the user how his data were processed
 */
class WriteSopFileToResult extends AttachXMLFile {

  /**
   * in which folder do we want to store this result
   */
  def getFolder: String = "sop"

  /**
   * description of the action
   */
  def getDescription: String = "this action attaches the used sop file to the result file"

  /**
   * writes the actual sop file out
   */
  def run(configuration: Element, rawdata: Source, sop: Source) = {

    def xml: Element = XmlHandling.readXml(sop)

    def outputter: XMLOutputter = new XMLOutputter(Format.getPrettyFormat())

    def result: String = outputter.outputString(xml)

    val in: InputStream = new ByteArrayInputStream(result.getBytes())

    writeXML(in, "instructions")
  }
}