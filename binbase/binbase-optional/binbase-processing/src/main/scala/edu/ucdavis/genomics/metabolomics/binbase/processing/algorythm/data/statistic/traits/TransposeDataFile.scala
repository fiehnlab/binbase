package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFileFactory
import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile

/**
 * used to transpose a datafile and just a simple example for mixins
 */
trait TransposeDataFile {

  /**
   * transposes the given datafile
   */
  def transpose(dataFile: DataFile): DataFile = {

    //required variables
    val rows: Int = dataFile.getTotalRowCount()
    val columns: Int = dataFile.getTotalColumnCount()
    val rowsIgnore: Array[Int] = dataFile.getIgnoreRows()
    val columnsIgnore: Array[Int] = dataFile.getIgnoreColumns()

    //generate a new datafile using our standard factory
    val result: DataFile = DataFileFactory.newInstance().createDataFile()

    //set the dimension
    result.setDimension(columns, rows)

    //the actual transposing
    for (i <- 0 until columns) {
      for (j <- 0 until rows) {
        val obj = dataFile.getCell(i, j)

        result.setCell(j, i, obj)
      }
    }

    //set the new ignore values
    result.setIgnoreColumns(dataFile.getIgnoreRows())
    result.setIgnoreRows(dataFile.getIgnoreColumns())
    
    //return the result
    result
  }
}