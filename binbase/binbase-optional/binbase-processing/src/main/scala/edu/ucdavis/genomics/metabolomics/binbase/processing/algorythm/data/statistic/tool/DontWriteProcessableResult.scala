package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.BasicProccessable

/**
 * simple helpfer class to not write result files to the zip file
 */
abstract class DontWriteProcessableResult extends BasicProccessable {

  /**
   * the file will be attached to the result file
   */
  override def writeResultToFile() : Boolean = false
  
  
  /**
   * returns the folder where this element should be stored
   */
   def getFolder() : String = "none"
}