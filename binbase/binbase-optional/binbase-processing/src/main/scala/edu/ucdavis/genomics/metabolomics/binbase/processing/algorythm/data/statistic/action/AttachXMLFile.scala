package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.action
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.io.source.Source
import org.jdom.output.XMLOutputter
import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling
import org.jdom.output.Format
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.action.BasicAction
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.Writer
import java.io.OutputStream
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import java.io.IOException
import edu.ucdavis.genomics.metabolomics.util.io.Copy
import java.io.ByteArrayInputStream
import java.io.InputStream

/**
 * just provides some functions to create xml files
 */
abstract class AttachXMLFile extends BasicAction {

  /**
   * writes some xml content
   */
  def writeXML(in: InputStream, name: String) = {

    writeObject(in, name, new Writer() {
      def isDatafileSupported(): Boolean = false
      def isSourceSupported(): Boolean = false
      def write(out: OutputStream, file: DataFile) = throw new IOException("not supported")
      def write(out: OutputStream, source: Source) = throw new IOException("not supported")
      def write(out: OutputStream, data: Object) = {
        Copy.copy(in, out, false)
        in.close()
      }

      //concrete implementation provides the exstension. Kind of a flaw with the design
      override def toString(): String = "xml"
    }, "")

  }
}