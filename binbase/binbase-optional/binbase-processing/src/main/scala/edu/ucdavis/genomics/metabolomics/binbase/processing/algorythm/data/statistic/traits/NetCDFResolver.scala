package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits

/**
 * used to resolve cdf files
 */
trait NetCDFResolver {

  /**
   * do we have a netcdf file
   */
  def hasNetcdf(name:String) : Boolean = {
    false
  }
}