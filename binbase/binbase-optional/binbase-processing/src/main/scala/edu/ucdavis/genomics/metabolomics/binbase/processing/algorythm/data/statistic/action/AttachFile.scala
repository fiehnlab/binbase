package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.action
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.action.ResultFileAction
import java.io.File
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.io.source.Source
import java.io.InputStream
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.Writer
import java.io.OutputStream
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import java.io.IOException
import edu.ucdavis.genomics.metabolomics.util.io.Copy
import scala.collection.mutable.MutableList
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.SampleObject
import org.apache.log4j.Logger

/**
 * basic action to attach a file to the result dataset
 */
abstract class AttachFile extends ResultFileAction {

  private val logger:Logger = Logger.getLogger(getClass())
  
  /**
   * configuration
   */
  private var configuration: Element = null

  /**
   * returns the configuration of this action
   */
  def getConfiguration(): Element = configuration

  /**
   * in which folder do we want to store this result
   */
  def getFolder: String = "data"

  /**
   * returns a file we like to attach
   */
  protected def retrieveFile(name: String): InputStream

  /**
   * lists all the file names we are interrested in. These could be based on content in the datafile
   */

  /**
   * goes over all the samples in this result and finds there name
   */
  def listFileNames(datafile: ResultDataFile): List[String] = {

    var result: MutableList[String] = new MutableList[String]()

    datafile.getSamples().toList.foreach { sample: SampleObject[String] =>
      result = sample.getValue() +: result
    }

    return result.toList
  }
  /**
   * writes a file to the result stream
   */
  def writeFile(name: String, file: InputStream) {
    if (file != null) {

      writeObject(file, name, new Writer() {
        def isDatafileSupported(): Boolean = false
        def isSourceSupported(): Boolean = false
        def write(out: OutputStream, file: DataFile) = throw new IOException("not supported")
        def write(out: OutputStream, source: Source) = throw new IOException("not supported")
        def write(out: OutputStream, data: Object) = {
          Copy.copy(file, out, false)
          file.close()
        }

        //concrete implementation provides the exstension. Kind of a flaw with the design
        override def toString(): String = getFileExtension()
      })

      file.close()

    }
  }

  /**
   * returns the extension to use for these files
   */
  protected def getFileExtension(): String

  /**
   * writes the actual sop file out
   */
  final def runDatafile(datafile: ResultDataFile, configuration: Element, rawdata: Source, sop: Source) = {

    //assign the configuration
    this.configuration = configuration

    //go over all the file names which are specified in the list
    listFileNames(datafile).foreach { name: String =>

      //retrieve the actual file
      val file: InputStream = retrieveFile(name)

      if (file == null) {
    	  logger.info("sorry file not found: " + name)
      } else {
        //write it to the storage system
        writeFile(name, file);
      }
    }

    //call it a day
  }
}