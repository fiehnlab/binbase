package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits._
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.BasicProccessable
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`._
import edu.ucdavis.genomics.metabolomics.binbase.minix.jmx.MiniXConfigurationJMXFacadeUtil._

/**
 * merges the result file with miniX metadata
 */
class MergeWithMiniXData extends DontWriteProcessableResult with MiniXSupport with ObjectDetector {

  def getDescription(): String = "integrates miniX metadata into the result datafile"

  /**
   * adds the minix metadata to the result file and returns it for further processing
   */
  def process(file: ResultDataFile, config: Element): DataFile = {

    //add columns
    val label: Int = insertProtectedColumn(file)
    val comment: Int = insertProtectedColumn(file)
    val species: Int = insertProtectedColumn(file)
    val organ: Int = insertProtectedColumn(file)
    val treatment: Int = insertProtectedColumn(file)

    //assign labels
    file.getCell(label, 0).asInstanceOf[FormatObject[Any]].setValue("label")
    file.getCell(comment, 0).asInstanceOf[FormatObject[Any]].setValue("comment")
    file.getCell(species, 0).asInstanceOf[FormatObject[Any]].setValue("species")
    file.getCell(organ, 0).asInstanceOf[FormatObject[Any]].setValue("organ")
    file.getCell(treatment, 0).asInstanceOf[FormatObject[Any]].setValue("treatment")

    //go over each sample
    file.getSamples().toList.foreach { sample =>
      val name: String = sample.getValue()
      val row: Int = file.getSamplePosition(name)

      //assign values
      file.getCell(label, row).asInstanceOf[CalculationObject[Any]].setValue(getLabel(name))
      file.getCell(comment, row).asInstanceOf[CalculationObject[Any]].setValue(getComment(name))
      file.getCell(species, row).asInstanceOf[CalculationObject[Any]].setValue(getSpecies(name))
      file.getCell(organ, row).asInstanceOf[CalculationObject[Any]].setValue(getOrgan(name))
      file.getCell(treatment, row).asInstanceOf[CalculationObject[Any]].setValue(getTreatment(name))

    }

    //returns result
    file
  }

  /**
   * access the delegate to receive the minix url
   */
  def getMiniXUrl(): String = {
    
    //access the home of the ejb and retrieves the used url
    val url: String =getHome().create().getUrl()

    //return the url 
    url

  }
}