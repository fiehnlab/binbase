package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.tool

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.BasicProccessable
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import org.jdom.Element
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.NullObject
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.ContentObject
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`._
import edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.traits.RemoteNetCDFResolver
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.resolver.ResolverBuilder
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.resolver.SimpleResolverBuilder
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.RawdataResolver
import java.io.File
import net.sf.mzmine.datastructures.RawDataAtNode
import scala.actors.Actor._
import scala.actors.Actor

/**
 * adds a row for each sample to ensure the netcdf file is actually valid and not broken. It's rather expensive since it's iterating over the whole file
 */
class NetcdfFileValid extends DontWriteProcessableResult with ObjectDetector with RemoteNetCDFResolver {

  //needed for internal configuration
  case class Result(val sample: String, val valid: Boolean)

  def getDescription: String = "adds a column to your result file, which contains the information if the netcdf file was valid and could be read"

  /**
   * adds the actual column to the datafile
   */
  def process(datafile: ResultDataFile, configuration: Element): DataFile = {

    //insert a new column
    val column = insertProtectedColumn(datafile)
    datafile.getCell(column, 0).asInstanceOf[FormatObject[Any]].setValue("netcdf file valid")

    //builds the final result file
    val submit = actor {
      receive {

        case x: Result =>
          val samplePosition: Int = datafile.getSamplePosition(x.sample)

          datafile.getCell(column, samplePosition).asInstanceOf[CalculationObject[Any]].setValue(x.valid)

        case 'stop =>
          exit();
      }
    }
    //go over all rows and check if the file exist
    datafile.getSamples().toList.par.foreach { sample =>

      //position of this sample
      val samplePosition: Int = datafile.getSamplePosition(sample.getValue())

      //query service is this sample exist
      val found: Boolean = hasNetcdf(sample.getValue())

      if (found) {

        var file: File = null

        val builder: ResolverBuilder = new SimpleResolverBuilder();
        val resolver: List[RawdataResolver] = builder.build().toList;

        resolver.foreach { res =>
          if (file != null) {
            file = res.resolveNetcdfFile(sample.getValue());

          }
        }

        try {
          val node = new RawDataAtNode(0, file);
          node.setWorkingCopy(file);

          node.preLoad();
          node.initializeScanBrowser(0, node.getNumberOfScans());

          for (i <- 0 until node.getNumberOfScans()) {
            val s = node.getNextScan();
          }

          submit ! new Result(sample.getValue(), true)

        } catch {
          case e: Exception =>
            submit ! new Result(sample.getValue(), false)
        }

      }

    }
    
    //shutting actor down
    submit ! 'stop

    //return datafile
    return datafile;
  }
}