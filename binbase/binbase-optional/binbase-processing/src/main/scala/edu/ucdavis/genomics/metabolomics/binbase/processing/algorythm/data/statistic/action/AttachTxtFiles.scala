package edu.ucdavis.genomics.metabolomics.binbase.processing.algorythm.data.statistic.action
import java.io.File
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile
import scala.collection.JavaConversions._
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.`object`.SampleObject
import java.io.InputStream
import java.io.FileInputStream
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.TXT
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseServiceFactory
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator
import java.io.ByteArrayInputStream
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.Writer
import java.io.OutputStream
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import java.io.IOException
import edu.ucdavis.genomics.metabolomics.util.io.source.Source
import scala.collection.mutable.MutableList

/**
 * attaches text files to the result set, based on all the sample names
 */
class AttachTxtFiles extends AttachFile {

  def getDescription: String = "this action attaches all cdf files to this result"

  /**
   * downloads the txt file from the server
   */
  protected def retrieveFile(name: String): InputStream = {

    val content: InputStream = new ByteArrayInputStream(Configurator.getImportService().downloadFile(name))
    return content
  }

  protected def getFileExtension(): String = {
    return "txt"
  }

  override def getFolder: String = "rawdata/txt"

}