package edu.ucdavis.genomics.metabolomics.binbase.collections;

import java.io.Serializable;

public class BerkleyKey<K> implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	K key;
	
	protected K getKey() {
		return key;
	}

	@Override
	public boolean equals(Object obj) {
		return key.equals(obj);
	}

	@Override
	public int hashCode() {
		return key.hashCode();
	}

	@Override
	public String toString() {
		return key.toString();
	}

	public BerkleyKey(K key){
		this.key = key;
	}
}
