import groovy.swing.SwingBuilder
import java.awt.FlowLayout as FL
import javax.swing.BoxLayout as BXL
import net.miginfocom.swing.MigLayout
import java.awt.Toolkit
import edu.ucdavis.genomics.metabolomics.binbase.dsl.io.GenerateConfigFile
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator
import edu.ucdavis.genomics.metabolomics.binbase.dsl.io.ConfiguratorSource
import edu.ucdavis.genomics.metabolomics.binbase.server.ejb.compounds.Configurator

onBootstrapEnd = { app ->

    //needed to calculated window position
    def tk = Toolkit.getDefaultToolkit()
    def width = 200
    int height = 100
    def x = (int) ((tk.getScreenSize().width - width) / 2)
    def y = (int) ((tk.getScreenSize().height - height) / 2)

    def s = new SwingBuilder()


    File file = new File(System.getProperty("user.home") + "/.binView.prop")

    Properties p = new Properties()
    if (file.exists()) {
        p.load(new FileReader(file))
    }

//create a dialog for the selection of the application server
    def dial = s.dialog(
            id: "applicationServer",
            title: 'ApplicationServer Configuration',
            size: [320, 120],
            location: [x, y],
            modal: true,
            layout: new MigLayout('insets 10')
    ) {
        label("server", constraints: 'split, span')
        separator(constraints: 'growx, wrap')
        textField(id: "value", constraints: 'growx', columns: 100,text:  p.getProperty("application_server"))

        button(id: "selectApplicationServerButton", constraints: 'growx,wrap', action: action(name: 'continue', closure: {

            def config = GenerateConfigFile.generateFile(value.text)

            XMLConfigurator.getInstance().destroy()
            XMLConfigurator.getInstance(new ConfiguratorSource(config))

            p.setProperty("application_server",value.text)
            p.store(new FileWriter(file),"bin view file")
            //disable the dialog

            dispose()

        }))
    }

    dial.show()

}


onUncaughtExceptionThrown = { x ->

    withMvcGroup('dialog') { m, v, c ->
        m.title = 'Sorry there was an exception!'
        m.message = """
            Oops! An unexpected error occurred and we do not
            know what to do with it. However instead of
            crashing we thought you would like to know that
            the problem was caused by a

            $x

            Also, look at the logging information printed in
            your console.

        """.stripIndent(12)
        c.show()
    }
}

onUncaughtRuntimeExceptionThrown = { x ->

    withMvcGroup('dialog') { m, v, c ->
        m.title = 'Sorry there was an exception!'
        m.message = """
            Oops! An unexpected error occurred and we do not
            know what to do with it. However instead of
            crashing we thought you would like to know that
            the problem was caused by a

            $x

            Also, look at the logging information printed in
            your console.

        """.stripIndent(12)
        c.show()
    }
}


withMvcGroup = { String type, Closure callback ->
    GriffonApplication app = ApplicationHolder.application
    try {
        callback(*app.createMVCGroup(type))
    } finally {
        app.destroyMVCGroup(type)
    }
}

eventCreateBinaryPackageEnd = { packageType ->
   // packageType may be zip, jar, applet, webstart, ...
   // 1. locate the new template file

   // 2. copy to destination and replace tokens
   // targetDistDir is already in the script binding and points to the right place
    ant.replace(dir: "${targetDistDir}/bin") {
        replacefilter(token: "@app.name@", value: GriffonNameUtils.capitalize(griffonAppName))
        replacefilter(token: "@app.version@", value: griffonAppVersion)
        replacefilter(token: "@app.java.opts@", value: javaOpts)
        replacefilter(token: "@app.main.class@", value: griffonApplicationClass)
    }
}