package render

import org.apache.log4j.Logger
import org.openscience.cdk.DefaultChemObjectBuilder
import org.openscience.cdk.Molecule
import org.openscience.cdk.aromaticity.CDKHueckelAromaticityDetector
import org.openscience.cdk.graph.ConnectivityChecker
import org.openscience.cdk.inchi.InChIToStructure
import org.openscience.cdk.interfaces.IAtomContainer
import org.openscience.cdk.layout.StructureDiagramGenerator
import org.openscience.cdk.renderer.AtomContainerRenderer
import org.openscience.cdk.renderer.font.AWTFontManager
import org.openscience.cdk.renderer.generators.BasicAtomGenerator
import org.openscience.cdk.renderer.generators.BasicBondGenerator
import org.openscience.cdk.renderer.generators.BasicSceneGenerator
import org.openscience.cdk.renderer.visitor.AWTDrawVisitor
import org.openscience.cdk.tools.CDKHydrogenAdder
import org.openscience.cdk.tools.manipulator.AtomContainerManipulator

import javax.swing.*
import java.awt.*
import java.util.List

/**
 * Created with IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 1/13/14
 * Time: 4:27 PM
 */
class StructureRenderer {

    private Logger logger = Logger.getLogger(getClass())

    /**
     * renders an inchi code on the given panel, if it's visible
     * @param inchi
     * @param panel
     * @param width
     * @param height
     */
    def renderInChICode(String inchi, JPanel panel, int width, int height, int offsetX = 0, int offsetY = 0, boolean clear = true) {
        if (panel.isShowing()) {

            panel.setOpaque(false)
            logger.debug("clearing drawing area...")
            if(clear)
                panel.getGraphics().clearRect(offsetX,offsetY, width, height)

            StructureDiagramGenerator sdg = new StructureDiagramGenerator()
            InChIToStructure convert = new InChIToStructure(inchi, DefaultChemObjectBuilder.getInstance())
            IAtomContainer mol = convert.getAtomContainer()

            AtomContainerManipulator.percieveAtomTypesAndConfigureAtoms(mol);

            CDKHueckelAromaticityDetector.detectAromaticity(mol);
            CDKHydrogenAdder.getInstance(DefaultChemObjectBuilder.getInstance()).addImplicitHydrogens mol

            // generators make the image elements
            List generators = new ArrayList()
            //generators.add(new RingGenerator())

            generators.add(new BasicAtomGenerator())
            generators.add(new BasicSceneGenerator());
            generators.add(new BasicBondGenerator());

            // the renderer needs to have a toolkit-specific font manager
            AtomContainerRenderer renderer = new AtomContainerRenderer(generators, new AWTFontManager())

            // the call to 'setup' only needs to be done on the first paint
            Rectangle drawArea = new Rectangle(offsetX,offsetY,width, height)

            if (ConnectivityChecker.isConnected(mol)) {

                logger.info("drawing -> ${inchi}")
                sdg.setMolecule(new Molecule(mol))
                sdg.generateCoordinates()
                mol = sdg.getMolecule()

                renderer.setup(mol, drawArea);

                Graphics2D g2 = (Graphics2D) panel.getGraphics();

                //clear existing content from it
                if(clear)
                    g2.clearRect(offsetX, offsetY, width, height)

                // the paint method also needs a toolkit-specific renderer
                renderer.paint(mol, new AWTDrawVisitor(g2), drawArea, true);
            } else {

                logger.info("not connected molecule, not drawing -> ${inchi}")
            }
        } else {
            logger.info("window is not showing, not drawing -> ${inchi}")
        }
    }
}
