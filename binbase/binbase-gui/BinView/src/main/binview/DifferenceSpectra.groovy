package binview

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.MassSpec
import edu.ucdavis.genomics.metabolomics.exception.NotSupportedException
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Comment
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.Ion
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.converter.SpectraConverter
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.validate.ValidateSpectra
import edu.ucdavis.genomics.metabolomics.util.math.SpectraArrayKey

/**
 * converts 2 spectra to a difference spectra, but substracting the absolute intensities
 */
class DifferenceSpectra  extends MassSpec {

    String massSpec

    public DifferenceSpectra(MassSpec a, MassSpec b){
        double[][] first = ValidateSpectra.convert(a.getMassSpec())
        double[][] second = ValidateSpectra.convert(b.getMassSpec())
        double[][] difference = new double [first.length][SpectraArrayKey.ARRAY_WIDTH]


        for(int i = 0; i < first.length; i++){
            difference[i][SpectraArrayKey.FRAGMENT_ABS_POSITION] = Math.abs(first[i][SpectraArrayKey.FRAGMENT_REL_POSITION] - second[i][SpectraArrayKey.FRAGMENT_REL_POSITION])
            difference[i][SpectraArrayKey.FRAGMENT_ION_POSITION] = first[i][SpectraArrayKey.FRAGMENT_ION_POSITION]
        }

        massSpec = ValidateSpectra.convert(difference)

    }
    Collection getComments() {
        throw new NotSupportedException("sorry not supported")
    }

    void setComments(Collection collection) {
        throw new NotSupportedException("sorry not supported")
    }

    Comment createComment() {
        throw new NotSupportedException("sorry not supported")
    }


    @Override
    String getApexSpec() {
        return "0"
    }

    @Override
    void setApexSpec(String s) {
        throw new NotSupportedException("sorry not supported")
    }

    @Override
    void setId(Integer integer) {
        throw new NotSupportedException("sorry not supported")
    }

    @Override
    Integer getId() {
        return 0
    }

    @Override
    void setRetentionIndex(Integer integer) {
        throw new NotSupportedException("sorry not supported")
    }

    @Override
    Integer getRetentionIndex() {
        throw new NotSupportedException("sorry not supported")
    }

    @Override
    void setSimilarity(Double aDouble) {
        throw new NotSupportedException("sorry not supported")
    }

    @Override
    Double getSimilarity() {
        throw new NotSupportedException("sorry not supported")
    }
}
