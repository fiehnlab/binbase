package edu.ucdavis.genomics.metabolomics.binbase.bdi.math;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.io.dest.FileDestination;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;

/**
 * simple task to generate similarity matrixes from ant
 * 
 * @author nase
 * 
 */
public class CreateBinSimilarityMatrixTask extends Task {

	private Logger logger = Logger.getLogger(getClass());
	private Vector<DatabaseConfig> config = new Vector<DatabaseConfig>();

	private boolean includeBinRefrences;

	private boolean includeBinProperties;

	private boolean createKnownVsKnownMatrix;

	private boolean createUnKnownVsUnKnownMatrix;

	private boolean createKnownVsUnKnownMatrix;

	private boolean createMspVsMsp;
	
	private String outputDirectory;

	private String libraryToCompare;

	//by default we only want two numbers after the digit
	private String similarityFormat = "#0.00";

	public void addConfig(DatabaseConfig config) {
		this.config.clear();
		this.config.add(config);
	}

	public boolean isIncludeBinRefrences() {
		return includeBinRefrences;
	}

	public void setIncludeBinRefrences(boolean includeBinRefrences) {
		this.includeBinRefrences = includeBinRefrences;
	}

	@Override
	public void execute() throws BuildException {
		try {
			log("check for properties");

			if (config.size() == 0) {
				throw new BuildException("please provide config!");
			}
			if (outputDirectory == null) {
				throw new BuildException("please provide the output directory");
			}

			log("all properties are set");

			DatabaseConfig config = this.config.get(0);

			System.setProperty(ConnectionFactory.KEY_DATABASE_PROPERTIE, config
					.getDatabase());
			System.setProperty(ConnectionFactory.KEY_HOST_PROPERTIE, config
					.getHost());
			System.setProperty(ConnectionFactory.KEY_TYPE_PROPERTIE, config
					.getType());
			System.setProperty(ConnectionFactory.KEY_USERNAME_PROPERTIE, config
					.getUser());
			System.setProperty(ConnectionFactory.KEY_PASSWORD_PROPERTIE, config
					.getPassword());

			log("create conection to database");
			configure();

			// create bin properties
			if (isIncludeBinProperties()) {
				log("create bin properties");
				DataFile file = new BinTable().createTable();
				try {
					writeFile(file, "properties.txt");
				} catch (ConfigurationException e) {
					throw new BuildException(e);
				} catch (IOException e) {
					throw new BuildException(e);
				}
			}

			// create refrences
			if (isIncludeBinRefrences()) {
				log("create bin references");
				DataFile file = new BinReferenceTable().createTable();
				try {
					writeFile(file, "references.txt");
				} catch (ConfigurationException e) {
					throw new BuildException(e);
				} catch (IOException e) {
					throw new BuildException(e);
				}
			}

			log("create matrix");
			log("bin vs lib...");

			// create similarity matrix
			if (libraryToCompare == null) {
				log("no lib provided skipping bin vs library generation");
			} else {
				try {
					BinSimilarityMatrix matrix = new BinSimilarityMatrix();
					DataFile file = matrix.createMatrix(this
							.getLibraryToCompare());
					formatSimilarity(file);
					writeFile(file, "matrix.txt");
				} catch (Exception e) {
					throw new BuildException(e);
				}
			}

			log("known bin vs known bin...");
			if (createKnownVsKnownMatrix) {
				try {
					BinSimilarityMatrix matrix = new BinSimilarityMatrix();
					DataFile file = matrix.createKnownVsKnownMatrix();
					formatSimilarity(file);
					writeFile(file, "matrix-knownVSknown.txt");
				} catch (Exception e) {
					throw new BuildException(e);
				}
			} else {
				log("skip");
			}

			log("unknown bin vs unknown bin...");
			if (createUnKnownVsUnKnownMatrix) {
				try {
					BinSimilarityMatrix matrix = new BinSimilarityMatrix();
					DataFile file = matrix.createUnknownVsUnknownMatrix();
					formatSimilarity(file);
					writeFile(file, "matrix-unknownVSunknown.txt");
				} catch (Exception e) {
					throw new BuildException(e);
				}
			} else {
				log("skip");
			}

			log("known bin vs unknown bin...");
			if (createKnownVsUnKnownMatrix) {
				try {
					BinSimilarityMatrix matrix = new BinSimilarityMatrix();
					DataFile file = matrix.createKnownVsUnknownMatrix();
					formatSimilarity(file);
					writeFile(file, "matrix-knownVSunknown.txt");
				} catch (Exception e) {
					throw new BuildException(e);
				}
			} else {
				log("skip");
			}
			
			log("msp vs msp...");
			if (createMspVsMsp) {
				try {
					BinSimilarityMatrix matrix = new BinSimilarityMatrix();
					DataFile file = matrix.createMatrixLibraryVsLibrary(libraryToCompare,libraryToCompare);
					formatSimilarity(file);
					writeFile(file, "matrix-mspVSmsp.txt");
				} catch (Exception e) {
					throw new BuildException(e);
				}
			} else {
				log("skip");
			}
			

		} catch (BuildException e) {
			throw e;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new BuildException(e);
		}
	}

	/**
	 * configures the system for us
	 * 
	 * @throws ConfigurationException
	 */
	private void configure() throws ConfigurationException {
		logger.info("looking for cfg file in config directory...");
		Source source = null;

		if (new File("config/hibernate.xml").exists() == false) {

			logger.info("looking for cfg file in current directory...");
			if (new File("hibernate.xml").exists() == false) {

				logger
						.info("looking for cfg file in resource config directory...");
				source = new ResourceSource(("/config/hibernate.xml"));
				if (source.exist() == false) {
					logger
							.info("looking for cfg file in resource directory...");
					source = new ResourceSource(("/hibernate.xml"));

					if (source.exist() == false) {
						throw new ConfigurationException(
								"sorry can't find a config!");
					}
				}
			} else {
				source = new FileSource(new File("hibernate.xml"));
			}
		} else {
			source = new FileSource(new File("config/hibernate.xml"));
		}
		Properties p = XMLConfigurator.getInstance(source).getProperties();
		p.putAll(System.getProperties());
		logger.info("configuration of hibernate is done...");
	}

	/**
	 * write the data to the output directory
	 * 
	 * @param file
	 * @param name
	 * @throws ConfigurationException
	 * @throws IOException
	 */
	private void writeFile(DataFile file, String name)
			throws ConfigurationException, IOException {
		FileDestination dest = new FileDestination(this.getOutputDirectory());
		dest.setIdentifier(name);

		java.io.OutputStream out = dest.getOutputStream();
		file.write(out);
		out.flush();
		out.close();

	}

	/**
	 * formats the similarity
	 * @param file
	 */
	private void formatSimilarity(DataFile file) {
		if (similarityFormat.length() > 0) {
			try {
				DecimalFormat format = new DecimalFormat(similarityFormat);

				for (int i = 0; i < file.getRowCount(); i++) {
					if (i > 0) {
						for (int x = 0; x < file.getColumnCount(); x++) {
							if (x > 0) {
								file.setCell(x, i, format.format(file.getCell(
										x, i)));
							}
						}
					}
				}
			} catch (Exception e) {
				log("format exception: " + e.getMessage());
			}
		}
	}

	public boolean isIncludeBinProperties() {
		return includeBinProperties;
	}

	public void setIncludeBinProperties(boolean includeBinProperties) {
		this.includeBinProperties = includeBinProperties;
	}

	public String getOutputDirectory() {
		return outputDirectory;
	}

	public void setOutputDirectory(String outputDirectory) {
		this.outputDirectory = outputDirectory;
	}

	public String getLibraryToCompare() {
		return libraryToCompare;
	}

	public void setLibraryToCompare(String libraryToCompare) {
		this.libraryToCompare = libraryToCompare;
	}

	public boolean isCreateKnownVsKnownMatrix() {
		return createKnownVsKnownMatrix;
	}

	public void setCreateKnownVsKnownMatrix(boolean createKnownVsKnownMatrix) {
		this.createKnownVsKnownMatrix = createKnownVsKnownMatrix;
	}

	public boolean isCreateUnKnownVsUnKnownMatrix() {
		return createUnKnownVsUnKnownMatrix;
	}

	public void setCreateUnKnownVsUnKnownMatrix(
			boolean createUnKnownVsUnKnownMatrix) {
		this.createUnKnownVsUnKnownMatrix = createUnKnownVsUnKnownMatrix;
	}

	public boolean isCreateKnownVsUnKnownMatrix() {
		return createKnownVsUnKnownMatrix;
	}

	public void setCreateKnownVsUnKnownMatrix(boolean createKnownVsUnKnownMatrix) {
		this.createKnownVsUnKnownMatrix = createKnownVsUnKnownMatrix;
	}

	public String getSimilarityFormat() {
		return similarityFormat;
	}

	public void setSimilarityFormat(String similarityFormat) {
		this.similarityFormat = similarityFormat;
	}

	public boolean isCreateMspVsMsp() {
		return createMspVsMsp;
	}

	public void setCreateMspVsMsp(boolean createMspVsMsp) {
		this.createMspVsMsp = createMspVsMsp;
	}

}
