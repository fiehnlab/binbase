package edu.ucdavis.genomics.metabolomics.binbase.bdi.math;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.Library;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.test.BinBaseDatabaseTest;
import edu.ucdavis.genomics.metabolomics.util.io.dest.FileDestination;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile;

/**
 * tests the similarity matrix
 * 
 * @author wohlgemuth
 * 
 */
public class BinSimilarityMatrixTest extends BinBaseDatabaseTest {

	private Logger logger = Logger.getLogger(getClass());
	private BinSimilarityMatrix matrix;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		 matrix = new BinSimilarityMatrix();
	}

	@After
	public void tearDown() throws Exception {
		logger.info("setting matrix to null...");
		matrix = null;
		logger.info("destroying hibernate factory...");
		HibernateFactory.newInstance().destroyFactory();
		logger.info("call super...");

		super.tearDown();
	}

	@Test(timeout=60000)
	public void testCreateMatrixLibraryVsLibrary() throws Exception {
		Properties p = XMLConfigurator.getInstance(
				new ResourceSource("/config/hibernate.xml")).getProperties();
		p.putAll(System.getProperties());


		Session session = HibernateFactory.newInstance(p).getSession();
		Library lib = (Library) session.createCriteria(Library.class).list()
				.get(0);
		
		
		DataFile file = matrix.createMatrixLibraryVsLibrary(lib,lib);

		// we got 10 spectra in the libray + 1 row
		assertTrue(file.getColumnCount() == 11);

		// we got 10 spectra in the libray + 1 row
		assertTrue(file.getRowCount() == 11);

		// save the generated file for user validation
		FileDestination dest = new FileDestination("target/content/result");
		dest.setIdentifier((getClass().getSimpleName() + "-result-libary-vs-libary.txt"));

		java.io.OutputStream out = dest.getOutputStream();
		file.write(out);
		out.flush();
		out.close();
		
		session.close();
		
	}
	@Test(timeout=60000)
	public void testGenerateMatrixLibrary() throws Exception {
		Properties p = XMLConfigurator.getInstance(
				new ResourceSource("/config/hibernate.xml")).getProperties();
		p.putAll(System.getProperties());


		Session session = HibernateFactory.newInstance(p).getSession();
		Library lib = (Library) session.createCriteria(Library.class).list()
				.get(0);
		
		
		DataFile file = matrix.createMatrix(lib);

		// we got 13 bins + 1 first column
		assertTrue(file.getColumnCount() == 14);

		// we got 10 spectra in the libray + 1 row
		assertTrue(file.getRowCount() == 11);

		// save the generated file for user validation
		FileDestination dest = new FileDestination("target/content/result");
		dest.setIdentifier((getClass().getSimpleName() + "-result-libary.txt"));

		java.io.OutputStream out = dest.getOutputStream();
		file.write(out);
		out.flush();
		out.close();
		
		session.close();
		
	}

	@Test(timeout=60000)
	public void testGenerateMatrixString() throws Exception {
		Properties p = XMLConfigurator.getInstance(
				new ResourceSource("/config/hibernate.xml")).getProperties();
		p.putAll(System.getProperties());

		DataFile file = matrix.createMatrix("example");

		// we got 13 bins + 1 first column
		assertTrue(file.getColumnCount() == 14);

		// we got 20 spectra in the libray + 1 row
		assertTrue(file.getRowCount() == 11);

		// save the generated file for user validation
		FileDestination dest = new FileDestination("target/content/result");
		dest.setIdentifier((getClass().getSimpleName() + "-result-string.txt"));

		java.io.OutputStream out = dest.getOutputStream();
		file.write(out);
		out.flush();
		out.close();
	}

	@Test(timeout=60000)
	public void testMain() throws Exception {

		// reset system properties
		System.setProperties(UNMODIFIED_SYSTEM_PROPERTIES);

		Properties p = new Properties();
		p.load(new ResourceSource("/test.properties").getStream());

		// add system properties in case the user specifies explicit other
		// properties
		p.putAll(System.getProperties());

		BinSimilarityMatrix.main(new String[] { p.getProperty("Binbase.host"),
				p.getProperty("Binbase.user"),
				p.getProperty("Binbase.password"),
				p.getProperty("Binbase.database"), "example",
				"target/content/result/" });

		//
		SimpleDatafile file = new SimpleDatafile();
		file.read(new FileInputStream(
				"target/content/result/example-matrix.txt"));

		// we got 13 bins + 1 first column
		assertTrue(file.getColumnCount() == 14);

		// we got 20 spectra in the libray + 1 row
		assertTrue(file.getRowCount() == 11);

		// save the generated file for user validation
		FileDestination dest = new FileDestination("target/content/result/");
		dest.setIdentifier((getClass().getSimpleName() + "-result-main.txt"));

		java.io.OutputStream out = dest.getOutputStream();
		file.write(out);
		out.flush();
		out.close();
	}

	@Override
	protected IDataSet getDataSet() {
		try {
			logger.info("looking for datafile...");
			ResourceSource source = new ResourceSource("/library-rtx5.xml");
			logger.debug("using source: " + source.getSourceName() + " exist: "
					+ source.exist());
			assertTrue(source.exist());
			return new XmlDataSet(source.getStream());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Test(timeout=60000)
	public void testCreateUnknownVsUnknownMatrix() throws ConfigurationException, IOException {
		Properties p = XMLConfigurator.getInstance(
				new ResourceSource("/config/hibernate.xml")).getProperties();
		p.putAll(System.getProperties());

		DataFile file = matrix.createUnknownVsUnknownMatrix();

		// we got 0 bins + 1 first column
		assertTrue(file.getColumnCount() == 1);

		// we got 0 bins + 1 row
		assertTrue(file.getRowCount() == 1);

		// save the generated file for user validation
		FileDestination dest = new FileDestination("target/content/result");
		dest
				.setIdentifier((getClass().getSimpleName() + "-result-knownVSknown.txt"));

		java.io.OutputStream out = dest.getOutputStream();
		file.write(out);
		out.flush();
		out.close();
	}

	@Test(timeout=60000)
	public void testCreateKnownVsKnownMatrix() throws ConfigurationException,
			IOException {
		Properties p = XMLConfigurator.getInstance(
				new ResourceSource("/config/hibernate.xml")).getProperties();
		p.putAll(System.getProperties());

		DataFile file = matrix.createKnownVsKnownMatrix();

		// we got 13 bins + 1 first column
		assertTrue(file.getColumnCount() == 14);

		// we got 20 spectra in the libray + 1 row
		assertTrue(file.getRowCount() == 14);

		// save the generated file for user validation
		FileDestination dest = new FileDestination("target/content/result");
		dest
				.setIdentifier((getClass().getSimpleName() + "-result-knownVSknown.txt"));

		java.io.OutputStream out = dest.getOutputStream();
		file.write(out);
		out.flush();
		out.close();
	}

	@Test(timeout=60000)
	public void testCreateKnownVsUnKnownMatrix() throws ConfigurationException,
			IOException {
		Properties p = XMLConfigurator.getInstance(
				new ResourceSource("/config/hibernate.xml")).getProperties();
		p.putAll(System.getProperties());

		DataFile file = matrix.createKnownVsUnknownMatrix();

		// we got 13 bins + 1 first column
		assertTrue(file.getColumnCount() == 14);

		// we got no unknown + 1 row
		assertTrue(file.getRowCount() == 1);

		// save the generated file for user validation
		FileDestination dest = new FileDestination("target/content/result");
		dest
				.setIdentifier((getClass().getSimpleName() + "-result-knownVSunknown.txt"));

		java.io.OutputStream out = dest.getOutputStream();
		file.write(out);
		out.flush();
		out.close();
	}
	
}
