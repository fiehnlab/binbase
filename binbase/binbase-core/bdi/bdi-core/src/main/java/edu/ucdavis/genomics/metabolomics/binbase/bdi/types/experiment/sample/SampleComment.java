/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Comment;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Commentable;

/**
 * @author wohlgemuth
 * @hibernate.subclass discriminator-value = "5"
 */
public class SampleComment extends Comment{

	/**
	 * 
	 */
	public SampleComment() {
		super();
	}

	/**
	 * @hibernate.many-to-one  column = "type" class = "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample" update = "true" insert = "true" not-null = "true"
	 */
	public Commentable getType() {
		return super.getType();
	}
}
