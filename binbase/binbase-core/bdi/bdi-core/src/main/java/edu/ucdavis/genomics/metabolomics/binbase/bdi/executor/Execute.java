/*
 * Created on Jan 19, 2007
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.executor;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel;

/**
 * used to allow an synchronized excution of models
 * @author wohlgemuth
 * @version Jan 19, 2007
 *
 */
public abstract class Execute {
	
	/**
	 * here we execute our model and return the data
	 * @author wohlgemuth
	 * @version Jan 19, 2007
	 * @param type
	 * @return
	 */
	public abstract void executeQuery(IModel type);
}
