package edu.ucdavis.genomics.metabolomics.binbase.bdi.math;

import java.io.OutputStream;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.Library;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.library.LibraryMassSpec;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.converter.SpectraConverter;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.DriverUtilities;
import edu.ucdavis.genomics.metabolomics.util.io.dest.FileDestination;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.math.Similarity;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile;

/**
 * generates a matrix over all bins against a library
 * 
 * @author wohlgemuth
 * 
 */
public class BinSimilarityMatrix extends AbstractBinTable {

	private Logger logger = Logger.getLogger(getClass());

	/**
	 * creates a matrix of the unknown bins vs the unknown bins
	 * 
	 * @return
	 */
	public DataFile createUnknownVsUnknownMatrix() {
		DataFile result = new SimpleDatafile();
		Collection<Bin> bins = getUnKnownBins();

		result.setDimension(bins.size() + 1, bins.size() + 1);
		result.setCell(0, 0, "bin id/bin id");
		Similarity sim = new Similarity();

		logger.info("create headers for: " + bins.size() + " bins specs");
		int column = 1;
		for (Bin bin : bins) {
			logger.info("add column for bin: " + bin.getName());
			result.setCell(column, 0, bin.getId());
			column++;
		}

		int row = 1;
		for (Bin bin : bins) {
			column = 1;
			result.setCell(0, row, bin.getId());

			for (Bin bin2 : bins) {
				logger.debug("calculate similarity for: " + bin.getName()
						+ " vs " + bin2.getName());
				sim.setLibrarySpectra(SpectraConverter.collectionToString(bin
						.getSpectra()));
				sim.setUnknownSpectra(SpectraConverter.collectionToString(bin2
						.getSpectra()));

				double value = sim.calculateSimimlarity();

				logger.debug("sim is: " + value);
				result.setCell(column, row, value);

				column++;
			}
			row++;
		}

		return result;
	}

	/**
	 * creates a matrix of the unknown bins vs the unknown bins
	 * 
	 * @return
	 */
	public DataFile createCompleteMatrix() {
		DataFile result = new SimpleDatafile();
		Collection<Bin> bins = getBins();

		result.setDimension(bins.size() + 1, bins.size() + 1);
		result.setCell(0, 0, "bin id/bin id");
		Similarity sim = new Similarity();

		logger.info("create headers for: " + bins.size() + " bins specs");
		int column = 1;
		for (Bin bin : bins) {
			logger.info("add column for bin: " + bin.getName());
			result.setCell(column, 0, bin.getId());
			column++;
		}

		int row = 1;
		for (Bin bin : bins) {
			column = 1;
			result.setCell(0, row, bin.getId());

			for (Bin bin2 : bins) {
				logger.debug("calculate similarity for: " + bin.getName()
						+ " vs " + bin2.getName());
				sim.setLibrarySpectra(SpectraConverter.collectionToString(bin
						.getSpectra()));
				sim.setUnknownSpectra(SpectraConverter.collectionToString(bin2
						.getSpectra()));

				double value = sim.calculateSimimlarity();

				logger.debug("sim is: " + value);
				result.setCell(column, row, value);

				column++;
			}
			row++;
		}

		return result;
	}

	/**
	 * generates a datafile which compares these two lists
	 * 
	 * @param binIdsA
	 * @param binIdsB
	 * @return
	 */
	public DataFile createListVsListMatrix(int[] binIdsA, int[] binIdsB) {
		Collection<Bin> bins = new Vector<Bin>();
		Collection<Bin> bins2 = new Vector<Bin>();

		for (int id : binIdsA) {
			bins.add(getBin(id));
		}
		for (int id : binIdsB) {
			bins2.add(getBin(id));
		}
		return createListVsListMatrix(bins, bins2);
	}

	public DataFile createListVsListMatrix(final Collection<Bin> bins, final Collection<Bin> bins2) {

		DataFile result = new SimpleDatafile();

		logger.info("rows:    " + bins2.size());
		logger.info("columns: " + bins.size());

		result.setDimension(bins2.size() + 1, bins.size() + 1);

		logger.info("dimensions are set to: " + result.getColumnCount() + "/"
				+ result.getRowCount());
		result.setCell(0, 0, "bin id/bin id");
		Similarity sim = new Similarity();

		logger.info("create headers for: " + bins.size() + " bins specs");
		int column = 1;
		for (Bin bin : bins) {
			logger.info("add column for bin: " + bin.getName());
			result.setCell(column, 0, bin.getId());
			column++;
		}

		logger.info("done with generating headeri," + bins.size() + " added");
		// no iteration needed since its empty anyway
		if (bins2.isEmpty() == false) {
			int row = 1;
			for (Bin bin2 : bins2) {
				column = 1;
				result.setCell(0, row, bin2.getId());

				for (Bin bin : bins) {
					logger.debug("calculate similarity for: " + bin2.getName()
							+ " vs " + bin.getName());
					sim.setLibrarySpectra(SpectraConverter
							.collectionToString(bin2.getSpectra()));
					sim.setUnknownSpectra(SpectraConverter
							.collectionToString(bin.getSpectra()));

					double value = sim.calculateSimimlarity();

					logger.debug("sim is: " + value);

					result.setCell(column, row, value);
					column++;
				}
				logger.info("done with bin: " + bin2.getName());
				row++;
			}
		}
		return result;
	}
	/**
	 * creates a matrix of know
	 * 
	 * @return
	 */
	public DataFile createKnownVsKnownMatrix() {
		DataFile result = new SimpleDatafile();
		Collection<Bin> bins = getKnownBins();

		result.setDimension(bins.size() + 1, bins.size() + 1);
		result.setCell(0, 0, "bin id/bin id");
		Similarity sim = new Similarity();

		logger.info("create headers for: " + bins.size() + " bins specs");
		int column = 1;
		for (Bin bin : bins) {
			logger.info("add column for bin: " + bin.getName());
			result.setCell(column, 0, bin.getId());
			column++;
		}

		int row = 1;
		for (Bin bin : bins) {
			column = 1;
			result.setCell(0, row, bin.getId());

			for (Bin bin2 : bins) {
				logger.debug("calculate similarity for: " + bin.getName()
						+ " vs " + bin2.getName());
				sim.setLibrarySpectra(SpectraConverter.collectionToString(bin
						.getSpectra()));
				sim.setUnknownSpectra(SpectraConverter.collectionToString(bin2
						.getSpectra()));

				double value = sim.calculateSimimlarity();

				logger.debug("sim is: " + value);
				result.setCell(column, row, value);

				column++;
			}
			row++;
		}

		return result;
	}

	/**
	 * needs a library name as input and generates a datafile containing the
	 * calculated similarities
	 * 
	 * @param libraryName
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public DataFile createMatrix(Library library) throws Exception {
		DataFile result = new SimpleDatafile();

		Collection<Bin> bins = getBins();
		Collection<LibraryMassSpec> specs = library.getSpectra();

		result.setDimension(specs.size() + 1, bins.size() + 1);
		result.setCell(0, 0, "spectra/bin id");
		Similarity sim = new Similarity();

		logger.info("create headers for: " + bins.size() + " bins specs");
		int column = 1;
		for (Bin bin : bins) {
			logger.info("add column for bin: " + bin.getName());
			result.setCell(column, 0, bin.getId());
			column++;
		}

		logger.info("generate rows for: " + specs.size()
				+ " libarary massspecs");
		int row = 1;
		for (LibraryMassSpec spec : specs) {
			column = 1;
			result.setCell(0, row, spec.getName());

			for (Bin bin : bins) {
				logger.debug("calculate similarity for: " + bin.getName()
						+ " vs " + spec.getName());
				sim.setLibrarySpectra(SpectraConverter.collectionToString(bin
						.getSpectra()));
				sim.setUnknownSpectra(SpectraConverter.collectionToString(spec
						.getSpectra()));

				double value = sim.calculateSimimlarity();

				logger.debug("sim is: " + value);
				result.setCell(column, row, value);

				column++;
			}
			row++;
		}

		return result;
	}

	public DataFile createMatrixLibraryVsLibrary(String library, String library2)
			throws Exception {

		List<Library> result = getSession().createCriteria(Library.class).add(
				Restrictions.eq("name", library)).list();
		List<Library> result2 = getSession().createCriteria(Library.class).add(
				Restrictions.eq("name", library2)).list();

		if (result.size() == 1 && result2.size() == 1) {
			return createMatrixLibraryVsLibrary(result.get(0), result2.get(0));
		} else if (result.isEmpty()) {
			throw new BinBaseException("given library was not found: "
					+ library);
		} else {
			throw new BinBaseException(
					"more than one library with the given name found: "
							+ library);
		}
	}

	/**
	 * compares library vs library
	 * 
	 * @param libraryName
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public DataFile createMatrixLibraryVsLibrary(Library library,
			Library library2) throws Exception {
		DataFile result = new SimpleDatafile();

		Collection<LibraryMassSpec> bins = library.getSpectra();
		Collection<LibraryMassSpec> specs = library2.getSpectra();

		result.setDimension(specs.size() + 1, bins.size() + 1);
		result.setCell(0, 0, "spectra/spectra");
		Similarity sim = new Similarity();

		logger.info("create headers for: " + bins.size() + " bins specs");
		int column = 1;
		for (LibraryMassSpec bin : bins) {
			logger.info("add column for spectra: " + bin.getName());
			result.setCell(column, 0, bin.getName());
			column++;
		}

		logger.info("generate rows for: " + specs.size()
				+ " libarary massspecs");
		int row = 1;
		for (LibraryMassSpec spec : specs) {
			column = 1;
			result.setCell(0, row, spec.getName());

			for (LibraryMassSpec bin : bins) {
				logger.debug("calculate similarity for: " + bin.getName()
						+ " vs " + spec.getName());
				sim.setLibrarySpectra(SpectraConverter.collectionToString(bin
						.getSpectra()));
				sim.setUnknownSpectra(SpectraConverter.collectionToString(spec
						.getSpectra()));

				double value = sim.calculateSimimlarity();

				logger.debug("sim is: " + value);
				result.setCell(column, row, value);

				column++;
			}
			row++;
		}

		return result;
	}

	/**
	 * starts the programm and generates the matrix
	 * 
	 * @param args
	 * @throws Exception
	 */
	public static void main(String args[]) throws Exception {
		if (args.length != 6) {
			System.out.println("USAGE: ");
			System.out.println("args[0]: database server");
			System.out.println("args[1]: username");
			System.out.println("args[2]: password");
			System.out.println("args[3]: database name");
			System.out.println("args[4]: library");
			System.out.println("args[5]: directory");

			System.out.println("found amount of attributes: " + args.length);
			return;
		}

		System.setProperty(ConnectionFactory.KEY_USERNAME_PROPERTIE, args[1]);
		System.setProperty(ConnectionFactory.KEY_HOST_PROPERTIE, args[0]);
		System.setProperty(ConnectionFactory.KEY_PASSWORD_PROPERTIE, args[2]);
		System.setProperty(ConnectionFactory.KEY_DATABASE_PROPERTIE, args[3]);

		if (System.getProperty(ConnectionFactory.KEY_TYPE_PROPERTIE) == null) {
			System.setProperty(ConnectionFactory.KEY_TYPE_PROPERTIE, String
					.valueOf(DriverUtilities.ORACLE));
		}
		XMLConfigurator.getInstance().addConfiguration(
				new ResourceSource("/config/hibernate.xml"));

		BinSimilarityMatrix matrix = new BinSimilarityMatrix();
		DataFile data = matrix.createMatrix(args[4]);

		FileDestination dest = new FileDestination(args[5]);
		dest.setIdentifier(args[4] + "-matrix.txt");

		OutputStream out = dest.getOutputStream();
		data.write(out);
		out.flush();
		out.close();
	}

	/**
	 * creates the matrix with a string as input parameter
	 * 
	 * @param string
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public DataFile createMatrix(String library) throws Exception {

		List<Library> result = getSession().createCriteria(Library.class).add(
				Restrictions.eq("name", library)).list();
		if (result.size() == 1) {
			return createMatrix(result.get(0));
		} else if (result.isEmpty()) {
			throw new BinBaseException("given library was not found: "
					+ library);
		} else {
			throw new BinBaseException(
					"more than one library with the given name found: "
							+ library);
		}
	}

	/**
	 * creates the known vs unknown matrix
	 * 
	 * @return
	 */
	public DataFile createKnownVsUnknownMatrix() {
		DataFile result = new SimpleDatafile();
		Collection<Bin> bins = getKnownBins();
		Collection<Bin> bins2 = getUnKnownBins();

		logger.info("rows:    " + bins2.size());
		logger.info("columns: " + bins.size());

		result.setDimension(bins2.size() + 1, bins.size() + 1);

		logger.info("dimensions are set to: " + result.getColumnCount() + "/"
				+ result.getRowCount());
		result.setCell(0, 0, "known bin id/unknown bin id");
		Similarity sim = new Similarity();

		logger.info("create headers for: " + bins.size() + " bins specs");
		int column = 1;
		for (Bin bin : bins) {
			logger.info("add column for bin: " + bin.getName());
			result.setCell(column, 0, bin.getId());
			column++;
		} 

		logger.info("done with generating headeri," + bins.size() + " added");
		// no iteration needed since its empty anyway
		if (bins2.isEmpty() == false) {
			int row = 1;
			for (Bin bin2 : bins2) {
				column = 1;
				result.setCell(0, row, bin2.getId());

				for (Bin bin : bins) {
					logger.debug("calculate similarity for: " + bin2.getName()
							+ " vs " + bin.getName());
					sim.setLibrarySpectra(SpectraConverter
							.collectionToString(bin2.getSpectra()));
					sim.setUnknownSpectra(SpectraConverter
							.collectionToString(bin.getSpectra()));

					double value = sim.calculateSimimlarity();

					logger.debug("sim is: " + value);

					result.setCell(column, row, value);
					column++;
				}
				logger.info("done with bin: " + bin2.getName());
				row++;
			}
		}
		return result;
	}
}
