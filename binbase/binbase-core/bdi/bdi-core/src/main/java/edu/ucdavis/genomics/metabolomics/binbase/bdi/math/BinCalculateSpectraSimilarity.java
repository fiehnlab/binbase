package edu.ucdavis.genomics.metabolomics.binbase.bdi.math;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.util.math.Similarity;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile;

public class BinCalculateSpectraSimilarity extends AbstractBinTable {

	private Logger logger = Logger.getLogger(getClass());

	public BinCalculateSpectraSimilarity() {
	}

	/**
	 * calculates the similairty of this spectra against all bins and export the
	 * results which have a higher similarity than the given vale
	 * 
	 * @param spectra
	 * @return
	 */
	public DataFile calculate(String spectra) {
		logger.info("received spectra: " + spectra);

		Collection<Bin> bins = getBins();

		DataFile result = new SimpleDatafile();
		
		result.addEmptyColumn("id");
		result.addEmptyColumn( "name");
		result.addEmptyColumn( "ri");
		result.addEmptyColumn( "similarity");

		Similarity sim = new Similarity();
		sim.setUnknownSpectra(spectra);

		Iterator<Bin> it = bins.iterator();

		while (it.hasNext()) {
			Bin bin = it.next();
			sim.setLibrarySpectra(bin.getMassSpec());
			List<Object> res = new Vector<Object>();
			res.add(bin.getId());
			res.add(bin.getName());
			res.add(bin.getRetentionIndex());

			res.add(sim.calculateSimimlarity());

			result.addRow(res);
		}
		return result;
	}
}
