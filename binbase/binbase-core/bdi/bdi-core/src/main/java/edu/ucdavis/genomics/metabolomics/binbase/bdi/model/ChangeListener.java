/*
 * Created on Dec 8, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.model;

/**
 * reacts on given changes of a type
 * @author wohlgemuth
 * @version Dec 8, 2005
 *
 */
public interface ChangeListener{
	/**
	 * adds a new object
	 */
	public <Type>void eventAdd(Type value);
	/**
	 * delets an object
	 */
	public <Type>void eventDelete(Type value);
	/**
	 * updates an object
	 */
	public <Type>void eventUpdate(Type value);
	/**
	 * the model tells us that an query was executed
	 */
	public <Type>void eventQuery(IModel model);
}
