/*
 * Created on 01.05.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * @swt
 * @author wohlgemuth
 * @hibernate.class table = "EXPERIMENT_CLASS"  dynamic-insert = "false" dynamic-update = "false"
 */
public class ExperimentClass implements Comparable
    {
    /**
     * Comment for <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 2L;

    /**
     * DOCUMENT ME!
     */
    private Collection samples = Collections.EMPTY_SET;

    /**
     * DOCUMENT ME!
     */
    private String name;

    /**
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.ExperimentClass#getMetaInformation()
     */
    public Map getMetaInformation() {
        //TODO
        return new HashMap();
    }

    /**
     * @param name The name to set.
     *
     * @uml.property name="name"
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @hibernate.id column = "`class`" generator-class = "native"
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.ExperimentClass#getName()
     * @swt.variable visible="true" name="Name" searchable="true"
	 * @swt.modify canModify="false"
     * @uml.property name="name"
     */
    public String getName() {
        return name;
    }

    /**
     * @param samples The samples to set.
     *
     * @uml.property name="samples"
     */
    public void setSamples(Collection samples) {
        this.samples = samples;
    }

    /**
     * @hibernate.set lazy="true" cascade = "none"
     * @hibernate.collection-key column = "`class`"
     * @hibernate.collection-one-to-many class = "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample"
     * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.ExperimentClass#getSamples()
     *
     * @uml.property name="samples"
     */
    public Collection getSamples() {
        return samples;
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public String toString() {
        return this.getName();
    }

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object arg0) {
		if(arg0 instanceof ExperimentClass ){
			ExperimentClass a = (ExperimentClass) arg0;
			a.getName().compareTo(this.getName());
		}
		return arg0.toString().compareTo(this.toString());
	}
}
