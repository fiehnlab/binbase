/*
 * Created on Dec 12, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.model;

/**
 * used to handle selections
 * @author wohlgemuth
 * @version Dec 12, 2005
 *
 * @param <Type>
 */
public interface SelectionListener<Type> {
	
	/**
	 * reatcs on a selectio of the given type and model
	 * @author wohlgemuth
	 * @version Dec 12, 2005
	 * @param type
	 * @param model
	 */
	public void selection(Type type, IModel<Type> model);
}
