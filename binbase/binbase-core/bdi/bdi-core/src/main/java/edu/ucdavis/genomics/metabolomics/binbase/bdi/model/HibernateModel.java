/*
 * Created on Dec 8, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Example;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;

/**
 * is used to work with hibernate databases as persistence layer
 * 
 * @author wohlgemuth
 * @version Dec 8, 2005
 */
public class HibernateModel<Type> implements IModel<Object> {


	private Object owner;
	
	/**
	 * contains all listener for this model
	 */
	private Set<ChangeListener> changeListeners = new HashSet<ChangeListener>();

	/**
	 * logger
	 */
	private Logger logger = Logger.getLogger(HibernateModel.class);

	private Set<ProgressListener> progressListeners = new HashSet<ProgressListener>();

	/**
	 * the used query
	 */
	private Object query = null;

	private Session session;

	private String group = this.getClass().getSimpleName();

	protected HibernateModel(HibernateFactory factory) {
		super();
		this.setHibernateSession(factory.getSession());
	}

	public void add(Object object) {


		fireTransactionBegin(ProgressListener.ADD, object);
		getHibernateSession().flush();
		Transaction t = getHibernateSession().beginTransaction();
		try {
			getHibernateSession().saveOrUpdate(object);
			t.commit();
			fireAdd(object);

		}
		catch (RuntimeException e) {
			t.rollback();
			logger.error(e);
			fireTransactionCancel(ProgressListener.ADD, e);
			throw e;
		}

		fireTransactionEnd(ProgressListener.ADD);
	}

	public void addChangeListener(ChangeListener listener) {
		if (this.changeListeners.contains(listener)) {
			logger.info("ignoring changelistener, is already assigned to this model: " + this);

		}
		else {
			changeListeners.add(listener);
		}
	}

	public void addProgressListener(ProgressListener listener) {
		if (this.progressListeners.contains(listener)) {
			logger.info("ignoring progresslistener, is already assigned to this model: " + this);

		}
		else {
			progressListeners.add(listener);
		}
	}

	public void delete(Object object) {

		fireTransactionBegin(ProgressListener.DELETE, object);
		getHibernateSession().flush();
		Transaction t = getHibernateSession().beginTransaction();

		try {
			getHibernateSession().delete(object);
			t.commit();

			fireDelete(object);

		}
		catch (RuntimeException e) {
			t.rollback();
			logger.error(e);

			fireTransactionCancel(ProgressListener.DELETE, e);
			throw e;
		}

		fireTransactionEnd(ProgressListener.DELETE);
	}

	public Object[] executeInternalQuery(Object o) {
		try {
			try {
				fireTransactionBegin(ProgressListener.QUERY, o);
				getHibernateSession().flush();

				if (o == null) {
					logger.warn("no query set");
					return new Object[0];
				}
				else if (o instanceof String) {
					logger.info("execute query: " + o);
					return getHibernateSession().createQuery(o.toString()).list().toArray();
				}
				else if (o instanceof Criteria) {
					logger.info("execute criteria query: " + o);
					return ((Criteria) o).list().toArray();
				}
				else {
					logger.info("execute query by example: " + o);
					Criteria crit = getHibernateSession().createCriteria(o.getClass()).add(Example.create(o));
					return crit.list().toArray();
				}

			}
			finally {
				fireTransactionEnd(ProgressListener.QUERY);
			}
		}
		catch (RuntimeException e) {
			fireTransactionCancel(ProgressListener.QUERY, e);
			throw e;
		}
	}

	/**
	 * executes querys
	 * 
	 * @author wohlgemuth
	 * @version Dec 14, 2005
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel#executeQuery()
	 */
	public Object[] executeQuery() {

		Object[] result = null;
		try {
			try {
				fireTransactionBegin(ProgressListener.QUERY, this.query);
				getHibernateSession().flush();

				if (this.query == null) {
					logger.warn("no query set");
					result = new Object[0];
				}
				else if (this.query instanceof String) {
					logger.info("execute query: " + this.query);
					result = getHibernateSession().createQuery(this.query.toString()).list().toArray();
				}
				else if (this.query instanceof Criteria) {
					logger.info("execute criteria query: " + this.query);
					result = ((Criteria) this.query).list().toArray();
				}
				else if (this.query instanceof NativeQuerry) {
					logger.info("execute criteria query: " + this.query);
					result = getHibernateSession().createSQLQuery(((NativeQuerry) this.query).getQuery().toString()).addEntity(
							((NativeQuerry) this.query).getEntinity()).list().toArray();
				}
				else {
					logger.info("execute query by example: " + this.query);
					Criteria crit = getHibernateSession().createCriteria(this.query.getClass()).add(Example.create(this.query));
					result = crit.list().toArray();
				}

				return result;
			}
			finally {
				fireTransactionEnd(ProgressListener.QUERY);
			}
		}
		catch (RuntimeException e) {
			logger.error(e);
			fireTransactionCancel(ProgressListener.QUERY, e);
			throw e;
		}
	}

	/**
	 * @author wohlgemuth
	 * @version Jan 3, 2007
	 * @param object
	 */
	private void fireAdd(Object object) {

		for (ChangeListener change : changeListeners) {
			logger.info("fire add - " + change);
			change.eventAdd(object);
		}
	}

	/**
	 * @author wohlgemuth
	 * @version Jan 3, 2007
	 * @param object
	 */
	private void fireDelete(Object object) {

		for (ChangeListener change : changeListeners) {
			logger.info("fire delete - " + change);
			change.eventDelete(object);
		}

	}

	/**
	 * @author wohlgemuth
	 * @version Jan 3, 2007
	 */
	private void fireSetQuery() {
		for (ChangeListener change : changeListeners) {
			logger.info("fire query - " + change);
			change.eventQuery(this);
		}
	}

	private void fireTransactionBegin(ReportEvent event, Object o) {
		for (ProgressListener progress : progressListeners) {
			logger.info("fire transaction begin - " + progress);
			progress.transactionBegin(o, event);
		}
	}

	private void fireTransactionCancel(ReportEvent event, Exception e) {
		for (ProgressListener progress : progressListeners) {
			logger.info("fire transaction cancel - " + progress);
			progress.transactionCancelled(e, event);
		}
	}

	private void fireTransactionEnd(ReportEvent event) {
		for (ProgressListener progress : progressListeners) {
			logger.info("fire transaction end - " + progress);
			progress.transactionEnd(event);
		}
	}

	/**
	 * @author wohlgemuth
	 * @version Jan 3, 2007
	 * @param type
	 */
	private void fireUpdate(Object type) {
		for (ChangeListener change : changeListeners) {
			logger.info("call listener: " + change);
			change.eventUpdate(type);
		}

	}

	/**
	 * returns all changelistener
	 * 
	 * @author wohlgemuth
	 * @version Dec 16, 2005
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IModel#getChangeListener()
	 */
	public Collection getChangeListener() {
		return changeListeners;
	}

	public Collection getProgressListener() {
		return progressListeners;
	}

	public void reload(Object type) {
		// session.reconnect();
		fireTransactionBegin(ProgressListener.RELOAD, type);
		Transaction t = getHibernateSession().beginTransaction();
		try {
			getHibernateSession().refresh(type);

			fireUpdate(type);
			t.commit();
		}
		catch (Exception e) {
			t.rollback();
			logger.error(e);
			fireTransactionCancel(ProgressListener.RELOAD, e);
			throw new RuntimeException(e);
		}
		fireTransactionEnd(ProgressListener.UPDATE);
	}


	public Object load(Class<Object> clazz,Serializable key) {
		// session.reconnect();
			Object o = getHibernateSession().load(clazz, key);
		return o;
	}

	
	public void removeChangeListener(ChangeListener listener) {
		changeListeners.remove(listener);
	}

	public void removeProgressListener(ProgressListener listener) {
		progressListeners.remove(listener);
	}

	/**
	 * @author wohlgemuth
	 * @version Dec 8, 2005
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.model.IHibernateModel#setQuery(org.hibernate.Criteria)
	 */
	public void setQuery(Object query) {
		logger.debug("set query: " + query);
		this.query = query;
		fireSetQuery();
	}

	public void update(Object object) {

		// session.clear();
		// session.reconnect();
		fireTransactionBegin(ProgressListener.UPDATE, object);
		Transaction t = getHibernateSession().beginTransaction();
		getHibernateSession().flush();
		getHibernateSession().refresh(object);

		try {
			getHibernateSession().update(object);
			t.commit();

			fireUpdate(object);

		}
		catch (RuntimeException e) {
			t.rollback();
			logger.error(e);
			fireTransactionCancel(ProgressListener.UPDATE, e);
			throw e;
		}
		fireTransactionEnd(ProgressListener.UPDATE);

	}

	public void refreshQuery() {
		logger.debug("rerun last query!");
		this.fireSetQuery();
	}


	public String getStatisticalGroup() {
		return this.group;
	}

	public void setStatisticalGroup(String group) {
		this.group = group;
	}

	protected void setHibernateSession(Session session) {
		this.session = session;
	}

	protected Session getHibernateSession() {
		return session;

	}

	public Object getQuery() {
		return query;
	}

	public Object getOwner() {
		return owner;
	}

	public void setOwner(Object owner) {
		this.owner = owner;
	}


}
