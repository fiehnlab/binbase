/*
 * Created on 01.05.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.util.Collection;
import java.util.HashSet;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.ValueAxis;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Comment;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.MassSpec;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.Ion;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.converter.SpectraConverter;
import edu.ucdavis.genomics.metabolomics.binbase.gui.swing.graph.plot.SpectraChart;
import edu.ucdavis.genomics.metabolomics.util.graph.dataset.BinBaseXYDataSet;
import edu.ucdavis.genomics.metabolomics.util.type.converter.BooleanConverter;

/**
 * @swt multi ="true"
 * @author wohlgemuth definiert einen bin
 * @hibernate.class table = "BIN" dynamic-insert = "true" dynamic-update =
 *                  "true"
 */
public class Bin extends MassSpec {

	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * DOCUMENT ME!
	 */
	private Collection anotatedSpectra;

	/**
	 * DOCUMENT ME!
	 */
	private Double apexSn;

	/**
	 * DOCUMENT ME!
	 */
	private String apexSpec;

	private Collection comments;

	private Collection compareTo;

	/**
	 * quality control properties
	 */
	private QualityControl controlProperties;

	/**
	 * DOCUMENT ME!
	 */
	private String exportString;

	/**
	 * DOCUMENT ME!
	 */
	private String generateQuantMassString;

	private BinGroup group;

	/**
	 * DOCUMENT ME!
	 */
	private Integer id;

	/**
	 * DOCUMENT ME!
	 */
	private String massSpec;

	/**
	 * DOCUMENT ME!
	 */
	private Integer minusRange;

	/**
	 * DOCUMENT ME!
	 */
	private String name;

	/**
	 * DOCUMENT ME!
	 */
	private String newBinString;

	/**
	 * DOCUMENT ME!
	 */
	private Integer plusRange;

	/**
	 * DOCUMENT ME!
	 */
	private Double purity;

	/**
	 * DOCUMENT ME!
	 */
	private Integer quantMass;

	private Collection refrences;

	/**
	 * DOCUMENT ME!
	 */
	private Integer retentionIndex;

	/**
	 * DOCUMENT ME!
	 * 
	 * @uml.property name="sample"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	private BinSample sample;

	/**
	 * DOCUMENT ME!
	 */
	private Integer spectraId;

	/**
	 * DOCUMENT ME!
	 */
	private Standard standard;

	private Collection synonyms;

	/**
	 * DOCUMENT ME!
	 */
	private Integer uniqueMass;

	/**
	 * DOCUMENT ME!
	 */
	private Collection virtuellBins;

	private Collection ionFilters;

	private Collection ratios;

	/**
	 * add a new compare
	 * 
	 * @author wohlgemuth
	 * @version May 26, 2006
	 * @param bin
	 */
	public void addCompare(BinCompare bin) {
		if (this.getCompareTo() == null) {
			this.setCompareTo(new HashSet());
		}
		bin.setBin(this);

		this.getCompareTo().add(bin);
	}

	public void addBinRatio(IonRatio ratio) {
		if (this.getRatios() == null) {
			this.setRatios(new HashSet());
		}

		ratio.setBin(this);
		this.getRatios().add(ratio);
	}

	/**
	 * ads a refrence
	 * 
	 * @param ref
	 */
	public void addRefrence(Refrence ref) {
		if (this.getRefrences() == null) {
			this.setRefrences(new HashSet());
		}
		ref.setBin(this);
		this.getRefrences().add(ref);
	}

	/**
	 * ads a synonym
	 * 
	 * @param syn
	 */
	public void addSynonym(Synonym syn) {
		if (this.getSynonyms() == null) {
			this.setSynonyms(new HashSet());
		}
		syn.setBin(this);
		this.getSynonyms().add(syn);
	}

	public Comment createComment() {
		return new BinComment();
	}

	/**
	 * @hibernate.set lazy="true" cascade = "save-update" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra"
	 * @hibernate.collection-key column = "`bin_id`"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin#getAnotatedSpectra()
	 * @uml.property name="anotatedSpectra"
	 */
	public Collection getAnotatedSpectra() {
		if (anotatedSpectra == null) {
			anotatedSpectra = new HashSet();
		}
		return anotatedSpectra;
	}

	/**
	 * @swt.variable visible="true" name="Apex Sn" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`apex_sn`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getApexSn()
	 * @uml.property name="apexSn"
	 */
	public Double getApexSn() {
		return apexSn;
	}

	/**
	 * @swt.variable visible="true" name="Sample" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.many-to-one column = "`sample_id`" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.BinSample"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getSample()
	 * @uml.property name="sample"
	 */
	public BinSample getSample() {
		return sample;
	}

	/**
	 * @hibernate.property column = "`apex`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @return Returns the apexSpec.
	 * @uml.property name="apexSpec"
	 */
	public String getApexSpec() {
		return apexSpec;
	}

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.BinComment"
	 * @hibernate.collection-key column = "type"
	 */
	public Collection getComments() {
		return this.comments;
	}

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.BinCompare"
	 * @hibernate.collection-key column = "bin_id"
	 */
	public Collection getCompareTo() {
		return compareTo;
	}

	/**
	 * @swt.variable visible="false" name="Control Properties" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.one-to-one cascade = "none" lazy="true" constrained="true"
	 * @author wohlgemuth
	 * @version Nov 14, 2005
	 * @return
	 */
	public QualityControl getControlProperties() {
		return controlProperties;
	}

	/**
	 * @hibernate.property column = "`export`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin#getExport()
	 * @uml.property name="exportString"
	 */
	public String getExportString() {
		return exportString;
	}

	/**
	 * @hibernate.property column = "`generatequantmass`" update = "true" insert
	 *                     = "true" not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin#getGenerateQuantMass()
	 * @uml.property name="generateQuantMassString"
	 */
	public String getGenerateQuantMassString() {
		return generateQuantMassString;
	}

	/**
	 * @swt.variable visible="true" name="Group" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.many-to-one cascade = "none" column = "group_id" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.BinGroup"
	 * @author wohlgemuth
	 * @version Jun 27, 2006
	 * @param group
	 */
	public BinGroup getGroup() {
		return group;
	}

	/**
	 * @swt.variable visible="true" name="Id" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.id column = "`bin_id`" generator-class = "native" not-null =
	 *               "true"
	 * @hibernate.generator-param name = "sequence" value = "BIN_ID"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getId()
	 * @uml.property name="id"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @hibernate.property column = "`spectra`" update = "true" insert = "true"
	 * @return Returns the massSpec.
	 * @uml.property name="massSpec"
	 */
	public String getMassSpec() {
		return massSpec;
	}

	/**
	 * @swt.variable visible="true" name="Minus Range" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "`minus`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin#getMinusRange()
	 * @uml.property name="minusRange"
	 */
	public Integer getMinusRange() {
		return this.minusRange;
	}

	/**
	 * @swt.variable visible="true" name="Name" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "`name`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getName()
	 * @uml.property name="name"
	 */
	public String getName() {
		return name;
	}

	/**
	 * @hibernate.property column = "`new`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin#getNewBin()
	 * @uml.property name="newBinString"
	 */
	public String getNewBinString() {
		return newBinString;
	}

	/**
	 * @swt.variable visible="true" name="Plus Range" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "`plus`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin#getPlusRange()
	 * @uml.property name="plusRange"
	 */
	public Integer getPlusRange() {
		return this.plusRange;
	}

	/**
	 * @swt.variable visible="true" name="Purity" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`purity`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getPurity()
	 * @uml.property name="purity"
	 */
	public Double getPurity() {
		return purity;
	}

	/**
	 * @swt.variable visible="true" name="Quant Mass" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "`quantmass`" update = "true" insert =
	 *                     "true" not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin#getQuantMass()
	 * @uml.property name="quantMass"
	 */
	public Integer getQuantMass() {
		return this.quantMass;
	}

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Refrence"
	 * @hibernate.collection-key column = "bin_id"
	 */
	public Collection getRefrences() {
		return this.refrences;
	}

	/**
	 * @swt.variable visible="true" name="Retention Index" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`retention_index`" update = "true" insert =
	 *                     "true" not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getRetentionIndex()
	 * @uml.property name="retentionIndex"
	 */
	public Integer getRetentionIndex() {
		return retentionIndex;
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getSimilarity()
	 * @uml.property name="similarity"
	 */
	public Double getSimilarity() {
		return new Double(1000);
	}

	/**
	 * @swt.variable visible="true" name="Spectra Id" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`spectra_id`" update = "true" insert =
	 *                     "true" not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getSpectraId()
	 * @uml.property name="spectraId"
	 */
	public Integer getSpectraId() {
		return this.spectraId;
	}

	/**
	 * @swt.variable visible="false" name="Standard" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.one-to-one cascade = "none" lazy="true" constrained="true"
	 */
	public Standard getStandard() {
		return standard;
	}

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Synonym"
	 * @hibernate.collection-key column = "bin_id"
	 */
	public Collection getSynonyms() {
		return this.synonyms;
	}

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.IonRatio"
	 * @hibernate.collection-key column = "`bin_id`"
	 */
	public Collection getRatios() {
		return this.ratios;
	}

	/**
	 * @swt.variable visible="true" name="Unique Mass" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`uniquemass`" update = "true" insert =
	 *                     "true" not-null = "true"
	 * @uml.property name="uniqueMass"
	 */
	public Integer getUniqueMass() {
		return uniqueMass;
	}

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.VirtualBin"
	 * @hibernate.collection-key column = "`parent_id`"
	 * @uml.property name="virtuellBins"
	 */
	public Collection getVirtuellBins() {
		return virtuellBins;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @swt.variable visible="true" name="Export" searchable="true"
	 * @swt.modify canModify="true"
	 * @return DOCUMENT ME!
	 */
	public Boolean isExport() {
		return new Boolean(BooleanConverter.StringtoBoolean(this
				.getExportString()));
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @swt.variable visible="true" name="Generate Quantmass" searchable="true"
	 * @swt.modify canModify="true"
	 * @return DOCUMENT ME!
	 */
	public Boolean isGenerateQuantMass() {
		return new Boolean(BooleanConverter.StringtoBoolean(this
				.getGenerateQuantMassString()));
	}

	/**
	 * @swt.variable visible="true" name="New Bin" searchable="true"
	 * @swt.modify canModify="true"
	 * @return DOCUMENT ME!
	 */
	public Boolean isNewBin() {
		return new Boolean(BooleanConverter.StringtoBoolean(this
				.getNewBinString()));
	}

	/**
	 * @param anotatedSpectra
	 *            The anotatedSpectra to set.
	 * @uml.property name="anotatedSpectra"
	 */
	public void setAnotatedSpectra(Collection anotatedSpectra) {
		this.anotatedSpectra = anotatedSpectra;
	}

	/**
	 * @param apexSn
	 *            The apexSn to set.
	 * @uml.property name="apexSn"
	 */
	public void setApexSn(Double apexSn) {
		this.apexSn = apexSn;
	}

	/**
	 * @param apexSpec
	 *            The apexSpec to set.
	 * @uml.property name="apexSpec"
	 */
	public void setApexSpec(String apexSpec) {
		this.apexSpec = apexSpec;
	}

	public void setComments(Collection comments) {
		this.comments = comments;
	}

	public void setCompareTo(Collection compare) {
		this.compareTo = compare;
	}

	public void setControlProperties(QualityControl controlProperties) {
		this.controlProperties = controlProperties;
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin#setExport(String)
	 */
	public void setExport(Boolean export) {
		this.setExportString(BooleanConverter.booleanToString(export
				.booleanValue()));
	}

	/**
	 * @param exportString
	 *            The exportString to set.
	 * @uml.property name="exportString"
	 */
	public void setExportString(String exportString) {
		this.exportString = exportString;
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin#setGenerateQuantMass(String)
	 */
	public void setGenerateQuantMass(Boolean generateQuantMass) {
		this.setGenerateQuantMassString(BooleanConverter
				.booleanToString(generateQuantMass.booleanValue()));
	}

	/**
	 * @param generateQuantMassString
	 *            The generateQuantMassString to set.
	 * @uml.property name="generateQuantMassString"
	 */
	public void setGenerateQuantMassString(String generateQuantMassString) {
		this.generateQuantMassString = generateQuantMassString;
	}

	public void setGroup(BinGroup group) {
		this.group = group;
	}

	/**
	 * @param id
	 *            The id to set.
	 * @uml.property name="id"
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param massSpec
	 *            The massSpec to set.
	 * @uml.property name="massSpec"
	 */
	public void setMassSpec(String massSpec) {
		this.massSpec = massSpec;
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin#setMinusRange(Integer)
	 * @uml.property name="minusRange"
	 */
	public void setMinusRange(Integer minusRange) {
		this.minusRange = minusRange;
	}

	/**
	 * @param name
	 *            The name to set.
	 * @uml.property name="name"
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin#setNewBin(String)
	 */
	public void setNewBin(Boolean newBin) {
		this.setNewBinString(BooleanConverter.booleanToString(newBin
				.booleanValue()));
	}

	/**
	 * @param newBinString
	 *            The newBinString to set.
	 * @uml.property name="newBinString"
	 */
	public void setNewBinString(String newBinString) {
		this.newBinString = newBinString;
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin#setPlusRange(Integer)
	 * @uml.property name="plusRange"
	 */
	public void setPlusRange(Integer plusRange) {
		this.plusRange = plusRange;
	}

	/**
	 * @param purity
	 *            The purity to set.
	 * @uml.property name="purity"
	 */
	public void setPurity(Double purity) {
		this.purity = purity;
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin#setQuantMass(Integer)
	 * @uml.property name="quantMass"
	 */
	public void setQuantMass(Integer quantMass) {
		this.quantMass = quantMass;
	}

	public void setRefrences(Collection refrences) {
		this.refrences = refrences;
	}

	/**
	 * @param retentionIndex
	 *            The retentionIndex to set.
	 * @uml.property name="retentionIndex"
	 */
	public void setRetentionIndex(Integer retentionIndex) {
		this.retentionIndex = retentionIndex;
	}

	/**
	 * @param sample
	 *            The sample to set.
	 * @uml.property name="sample"
	 */
	public void setSample(BinSample sample) {
		this.sample = sample;
	}

	/**
	 * 
	 */
	public void setSimilarity(Double similarity) {
	}

	/**
	 * @param spectraId
	 *            The spectraId to set.
	 * @uml.property name="spectraId"
	 */
	public void setSpectraId(Integer spectraId) {
		this.spectraId = spectraId;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param standard
	 *            DOCUMENT ME!
	 */
	public void setStandard(Standard standard) {
		this.standard = standard;
	}

	public void setSynonyms(Collection synonyms) {
		this.synonyms = synonyms;
	}

	/**
	 * @param uniqueMass
	 *            The uniqueMass to set.
	 * @uml.property name="uniqueMass"
	 */
	public void setUniqueMass(Integer uniqueMass) {
		this.uniqueMass = uniqueMass;
	}

	/**
	 * @param virtuellBins
	 *            The virtuellBins to set.
	 * @uml.property name="virtuellBins"
	 */
	public void setVirtuellBins(Collection virtuellBins) {
		this.virtuellBins = virtuellBins;
	}

	@Override
	public String toString() {
		return this.getName();
	}

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.BinIonFilter"
	 * @hibernate.collection-key column = "`bin_id`"
	 */
	public Collection getIonFilters() {
		return ionFilters;
	}

	public void setIonFilters(Collection ionFilters) {
		this.ionFilters = ionFilters;
	}

	public void setRatios(Collection ratios) {
		this.ratios = ratios;
	}

	/**
	 * * @swt.variable visible="true" name="Apex Sn" searchable="true"
	 * 
	 * @swt.modify canModify="false"
	 * @return
	 */
	public Integer binKey() {
		return this.getMassSpec().hashCode();
	}

	protected String generateChartTitle() {

		return  "name: " + name + " ri: " + this.getRetentionIndex() + " - "
				+ " id: " + this.getId();

	}

}
