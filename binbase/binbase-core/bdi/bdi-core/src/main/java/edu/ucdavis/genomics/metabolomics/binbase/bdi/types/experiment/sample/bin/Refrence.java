/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin;

/**
 * @swt
 * @hibernate.class table = "REFERENCE" dynamic-insert = "true" dynamic-update =
 *                  "true"
 * @author wohlgemuth defines an external or internal refrence for a bin
 */
public class Refrence {
	private Bin bin;

	private Integer id;

	private String value;

	private RefrenceClass refrenceClass;

	/**
	 * 
	 */
	public Refrence() {
		super();
	}

	/**
	 * @swt.variable visible="false" name="Bin" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.many-to-one cascade = "none" column = "bin_id" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin"
	 *                        returns the bin, belonging to this refrence
	 * @return
	 */
	public Bin getBin() {
		return this.bin;
	}

	/**
	 * @hibernate.id column = "id" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "HIBERNATE_SEQUENCE"
	 *                            returns the id belonging to this refrence
	 * @return
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * @swt.variable visible="true" name="Value" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "value" update = "true" insert = "true"
	 *                     not-null = "true" returns the refrence value
	 * @return
	 */
	public String getValue() {
		return this.value;
	}

	/**
	 * sets the bin belonging to this refrence
	 * 
	 * @param bin
	 */
	public void setBin(Bin bin) {
		this.bin = bin;
	}

	/**
	 * sets the id
	 * 
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @swt.variable visible="true" name="Refrence Class" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.many-to-one cascade = "none" column = "class_id" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.RefrenceClass"
	 *                        returns the bin, belonging to this refrence
	 * @return
	 */

	public RefrenceClass getRefrenceClass() {
		return refrenceClass;
	}

	public void setRefrenceClass(RefrenceClass refrenceClass) {
		this.refrenceClass = refrenceClass;
	}

}
