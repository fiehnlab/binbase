/*
 * Created on Dec 9, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.DriverUtilities;
import edu.ucdavis.genomics.metabolomics.util.io.Copy;

/**
 * creates hibernate sessions
 * 
 * @author wohlgemuth
 * @version Dec 9, 2005
 */
public abstract class HibernateFactory extends AbstractFactory {
	/**
	 * internal logger
	 */
	protected static Logger logger = Logger.getLogger(HibernateFactory.class);

	public static final String DEFAULT_PROPERTY_NAME = HibernateFactory.class
			.getName();

	private static HibernateFactory instance;

	public HibernateFactory() {
		super();
	}

	/**
	 * the default implementations
	 * 
	 * @author wohlgemuth
	 * @version Dec 9, 2005
	 * @return
	 */
	protected static String getDefaultFactory() {
		logger.info("using default factory");
		return SimpleHibernateFactoryImpl.class.getName();
	}

	/**
	 * creates a session
	 * 
	 * @author wohlgemuth
	 * @version Dec 9, 2005
	 * @return
	 */
	public abstract Session getSession();

	public void destroyFactory() {
		destroySession();
		instance = null;
	}

	/**
	 * initialized the factory
	 * 
	 * @author wohlgemuth
	 * @version Dec 9, 2005
	 */
	protected abstract void initialize(Properties p);

	/**
	 * creates a new instance
	 * 
	 * @author wohlgemuth
	 * @version Dec 9, 2005
	 * @return
	 */
	public static HibernateFactory newInstance(final Properties p,
			boolean forceNew) {
		return newInstance(p, findFactory(DEFAULT_PROPERTY_NAME,
				getDefaultFactory()), forceNew);
	}

	public static HibernateFactory newInstance(final Properties p) {
		return newInstance(p, false);
	}

	/**
	 * uses the system properties
	 * 
	 * @author wohlgemuth
	 * @version Dec 16, 2005
	 * @return
	 */
	public static HibernateFactory newInstance() {
		final Properties p = System.getProperties();
		p.putAll(XMLConfigurator.getInstance().getProperties());
		return newInstance(p, false);
	}

	/**
	 * @author wohlgemuth
	 * @version Dec 8, 2005
	 * @return
	 */
	public synchronized static HibernateFactory newInstance(Properties p,
			final String factoryClass, boolean forceNew) {
		logger.info("attempting to get factory of type:" + factoryClass);

		try {

			if (forceNew) {
				logger.info("forcing a new instance: "
						+ p.getProperty("Binbase.user"));
				return createInstance(p, factoryClass);
			} else {
				logger.info("returning an existing instance: "
						+ p.getProperty("Binbase.user"));

				if (instance != null) {
					if (instance.getClass().getName().equals(factoryClass) == false) {
						logger
								.info("dropping existing instance and create new instance...");
						logger.info("existing was: "
								+ instance.getClass().getName());

						instance = null;
					}
				}
				if (instance == null) {
					logger.info("create new instance using factory: "
							+ factoryClass);

					instance = createInstance(p, factoryClass);

				} else {
					logger.info("using existing factory...");
				}

			}

			return instance;
		} catch (final Exception e) {
			logger.error(e.getMessage() + p, e);
			throw new FactoryException(e);
		}
	}

	private static HibernateFactory createInstance(Properties p,
			String factoryClass) throws InstantiationException,
			IllegalAccessException, ClassNotFoundException {

		Class classObject = Class.forName(factoryClass);
		HibernateFactory instance = (HibernateFactory) classObject
				.newInstance();

		p = DriverUtilities.createConnectionProperties(p);

		instance.prepare();
		instance.initialize(p);
		return instance;
	}

	/**
	 * copies the dtd's into the right places for us
	 */
	protected void prepare() {

		final File configuration = new File("hibernate-configuration-3.0.dtd");
		final File mapping = new File("hibernate-mapping-3.0.dtd");

		if (configuration.canWrite() == false) {
			logger.warn("not able to write the configuration file!");
		}
		if (mapping.canWrite() == false) {
			logger.warn("not able to write the mapping file!");
		}

		try {
			logger.info(getClass().getResourceAsStream(
					"/hibernate-configuration-3.0.dtd").available());
			Copy.copy(getClass().getResourceAsStream(
					"/hibernate-configuration-3.0.dtd"), new FileOutputStream(
					configuration), false);

			logger.info("configuration copied to: "
					+ configuration.getAbsolutePath());

			Copy.copy(getClass().getResourceAsStream(
					"/hibernate-mapping-3.0.dtd"),
					new FileOutputStream(mapping), false);
			logger.info("mapping copied to: " + mapping.getAbsolutePath());

		} catch (final Exception e) {
			logger.error(e.getMessage(), e);
		}

	}

	public abstract SessionFactory getInternalFactory();

	/**
	 * destroys any session generated by this factory
	 * 
	 * @author wohlgemuth
	 * @version Mar 1, 2006
	 */
	public abstract void destroySession();
}
