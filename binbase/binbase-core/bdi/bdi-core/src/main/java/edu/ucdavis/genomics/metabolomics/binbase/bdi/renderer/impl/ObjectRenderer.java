/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.renderer.impl;

import java.text.DecimalFormat;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.renderer.Renderer;

/**
 * @author wohlgemuth
 * the simplest possible renderer it is just an to string presentation
 */
public class ObjectRenderer implements Renderer{

	/**
	 * 
	 */
	public ObjectRenderer() {
		super();
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.renderer.Renderer#render(java.lang.Object)
	 */
	public String render(Object o) {
		if(o == null){
			return "null";
		}
		else if( o instanceof Double){
			double value =(Double)o;
			
			if(value < 1){
				DecimalFormat format = new DecimalFormat("#0.0000");
				return format.format(value);
			}
			else{
				DecimalFormat format = new DecimalFormat("#0.00");
				return format.format(value);
			}
		}
		return o.toString();
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.renderer.Renderer#setFormat(java.lang.String)
	 */
	public void setFormat(String format) {
		//this one doesn't accept formats
	}

}
