package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.HibernateModel;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.model.ModelFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate.HibernateFactory;

/**
 * provides us with meta data about samples
 * @author wohlgemuth
 * @swt
 * @author wohlgemuth
 * @hibernate.class table = "SAMPLE_INFO" dynamic-insert = "false" dynamic-update =
 *                  "false"
 * @hibernate.cache usage = "read-only"
 */
public class SampleInfo implements Comparable {

	private Integer id;

	private Sample sample;

	private MetaKey key;

	private String value;

	public boolean equals(Object obj) {
		if (obj instanceof SampleInfo) {
			return (((SampleInfo) obj).getId().equals(this.getId()));
		}
		return false;
	}

	/**
	 * @hibernate.id column = "`id`" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "LINK_ID"
	 */
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * 	@hibernate.many-to-one column = "`sample_id`" class ="edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample"
	 * 
	 * @return
	 */
	public Sample getSample() {
		return sample;
	}

	public void setSample(Sample sample) {
		this.sample = sample;
	}

	/**
 	 * @swt.variable visible="true" name="Key" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.many-to-one column = "`key_id`" class ="edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.MetaKey"
	 * 
	 * @return
	 */
	public MetaKey getKey() {
		return key;
	}

	public void setKey(MetaKey key) {
		this.key = key;
	}

	/**
	 * @hibernate.property column = "`value`" insert = "true"
	 * 
	 * @swt.variable visible="true" name="Value" searchable="true"
	 * @swt.modify canModify="false"
	 * @return
	 */
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public int compareTo(Object o) {
		return this.getId().compareTo(((SampleInfo) o).getId());
	}

	@Override
	public String toString() {
		return getKey() + " - " + getValue();
	}
}
