/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.renderer;

/**
 * @author wohlgemuth
 * defines an renderer the job is to transform objects into strings
 */
public interface Renderer {
	
	/**
	 * returns a string representation of the object.
	 * @param o
	 * @return
	 */
	public String render(Object o);
	
	/**
	 * sets a format to format the result string
	 * @param format
	 */
	public void setFormat(String format);

}
