/*
 * Created on 01.05.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra;

import java.util.Collection;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Comment;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.util.type.converter.BooleanConverter;

/**
 * @swt multi ="true"
 * @author wohlgemuth
 * @hibernate.class table = "SPECTRA_FOUND" dynamic-insert = "true"
 *                  dynamic-update = "true" where = "`bin_id` is not null"
 */
public class Spectra extends SimpleSpectra {
	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 2L;

	private String foundAtCorrectionString;

	/**
	 * DOCUMENT ME!
	 */
	private Double apexSn;

	/**
	 * DOCUMENT ME!
	 */
	private String apexSpec;

	/**
	 * DOCUMENT ME!
	 * 
	 * @uml.property name="bin"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	private Bin bin;

	private Collection comments;

	/**
	 * DOCUMENT ME!
	 */
	private Integer id;

	/**
	 * DOCUMENT ME!
	 */
	private String massSpec;

	/**
	 * DOCUMENT ME!
	 */
	private Double purity;

	/**
	 * DOCUMENT ME!
	 */
	private Integer retentionIndex;

	/**
	 * the retention time!
	 */
	private Integer retentionTime;

	private String lecoVersion;

	/**
	 * DOCUMENT ME!
	 * 
	 * @uml.property name="sample"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	private Sample sample;

	/**
	 * DOCUMENT ME!
	 */
	private Double similarity;

	/**
	 * DOCUMENT ME!
	 */
	private Integer uniqueMass;

	/**
	 * @swt.variable visible="true" name="Height" searchable="true"
	 * @swt.modify canModify="false" wird aus dem spektrum berechnet
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getHeight()
	 * @uml.property name="height"
	 */
	public final Double getHeight() {
		return new Double(this.getProperties().calculateHeight(this.getBin().getQuantMass().intValue()));
	}

	/**
	 * @swt.variable visible="true" name="Apex Sn" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`apex_sn`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getApexSn()
	 * @uml.property name="apexSn"
	 */
	public Double getApexSn() {
		return apexSn;
	}

	/**
	 * @hibernate.property column = "`apex`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @return Returns the apexSpec.
	 * @uml.property name="apexSpec"
	 */
	public String getApexSpec() {
		return apexSpec;
	}

	/**
	 * @swt.variable visible="true" name="Bin" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.many-to-one column = "`bin_id`" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin"
	 *                        update = "true" insert = "true"
	 * @return
	 * @uml.property name="bin"
	 */
	public Bin getBin() {
		return bin;
	}

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.SpectraComment"
	 * @hibernate.collection-key column = "type"
	 */
	public Collection getComments() {
		return this.comments;
	}

	/**
	 * @swt.variable visible="true" name="id" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.id column = "`spectra_id`" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "SPECTRA_ID"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getId()
	 * @uml.property name="id"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @hibernate.property column = "`spectra`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @return Returns the massSpec.
	 * @uml.property name="massSpec"
	 */
	public String getMassSpec() {
		return massSpec;
	}

	/**
	 * @swt.variable visible="true" name="Purity" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`purity`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getPurity()
	 * @uml.property name="purity"
	 */
	public Double getPurity() {
		return purity;
	}

	/**
	 * @swt.variable visible="true" name="Retention Index" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`retention_index`" update = "true" insert =
	 *                     "true" not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getRetentionIndex()
	 * @uml.property name="retentionIndex"
	 */
	public Integer getRetentionIndex() {
		return retentionIndex;
	}

	/**
	 * @swt.variable visible="true" name="Retention Time" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`retention_time`" update = "true" insert =
	 *                     "true" not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getRetentionIndex()
	 * @uml.property name="retentionIndex"
	 */
	public Integer getRetentionTime() {
		return retentionTime;
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getRetentionIndex()
	 * @swt.variable visible="true" name="Retention Time Shift"
	 *               searchable="false"
	 * @swt.modify canModify="false"
	 */
	public Integer getShift() {
		return this.getRetentionIndex() - this.getRetentionTime();
	}

	/**
	 * @swt.variable visible="false" name="Sample" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.many-to-one column = "`sample_id`" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getSample()
	 * @uml.property name="sample"
	 */
	public Sample getSample() {
		return sample;
	}

	/**
	 * @swt.variable visible="true" name="Similarity" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`match`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getSimilarity()
	 * @uml.property name="similarity"
	 */
	public Double getSimilarity() {
		return similarity;
	}

	/**
	 * @swt.variable visible="true" name="Unique Mass" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`uniquemass`" update = "true" insert =
	 *                     "true" not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getUniqueMass()
	 * @uml.property name="uniqueMass"
	 */
	public Integer getUniqueMass() {
		return uniqueMass;
	}

	/**
	 * @swt.variable visible="true" name="UM/BP ratio" searchable="true"
	 * @swt.modify canModify="false"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getUniqueMass()
	 * @uml.property name="uniqueMass"
	 */
	public Double getUniqueBasePeakRatio() {
		return getProperties().calculateUniqueRatio();
	}

	/**
	 * @swt.variable visible="true" name="Base Peak" searchable="true"
	 * @swt.modify canModify="false"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra#getUniqueMass()
	 * @uml.property name="uniqueMass"
	 */
	public Integer getBasePeak() {
		return getProperties().calculateBasePeak();
	}

	/**
	 * @param apexSn
	 *            The apexSn to set.
	 * @uml.property name="apexSn"
	 */
	public void setApexSn(Double apexSn) {
		this.apexSn = apexSn;
	}

	/**
	 * @param apexSpec
	 *            The apexSpec to set.
	 * @uml.property name="apexSpec"
	 */
	public void setApexSpec(String apexSpec) {
		this.apexSpec = apexSpec;
	}

	/**
	 * @param bin
	 *            The bin to set.
	 * @uml.property name="bin"
	 */
	public void setBin(Bin bin) {
		this.bin = bin;
	}

	public void setComments(Collection comments) {
		this.comments = comments;
	}

	/**
	 * @param id
	 *            The id to set.
	 * @uml.property name="id"
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param massSpec
	 *            The massSpec to set.
	 * @uml.property name="massSpec"
	 */
	public void setMassSpec(String massSpec) {
		this.massSpec = massSpec;
	}

	/**
	 * @param purity
	 *            The purity to set.
	 * @uml.property name="purity"
	 */
	public void setPurity(Double purity) {
		this.purity = purity;
	}

	/**
	 * @param retentionIndex
	 *            The retentionIndex to set.
	 * @uml.property name="retentionIndex"
	 */
	public void setRetentionIndex(Integer retentionIndex) {
		this.retentionIndex = retentionIndex;
	}

	/**
	 * @param sample
	 *            The sample to set.
	 * @uml.property name="sample"
	 */
	public void setSample(Sample sample) {
		this.sample = sample;
	}

	/**
	 * @param similarity
	 *            The similarity to set.
	 * @uml.property name="similarity"
	 */
	public void setSimilarity(Double similarity) {
		this.similarity = similarity;
	}

	/**
	 * @param uniqueMass
	 *            The uniqueMass to set.
	 * @uml.property name="uniqueMass"
	 */
	public void setUniqueMass(Integer uniqueMass) {
		this.uniqueMass = uniqueMass;
	}

	public Comment createComment() {
		return new SpectraComment();
	}

	public void setRetentionTime(Integer retentionTime) {
		this.retentionTime = retentionTime;
	}

	/**
	 * @swt.variable visible="true" name="Leco Version" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`leco`" update = "true" insert = "true"
	 *                     not-null = "true"
	 * @return
	 */
	public String getLecoVersion() {
		return lecoVersion;
	}

	public void setLecoVersion(String lecoVersion) {
		this.lecoVersion = lecoVersion;
	}

	/**
	 * @swt.variable visible="true" name="Found during correction"
	 *               searchable="true"
	 * @swt.modify canModify="false"
	 * @return
	 */
	public Boolean isFoundAtCorrection() {
		return BooleanConverter.StringtoBoolean(getFoundAtCorrectionString());
	}

	/**
	 * @hibernate.property column = "`found_at_correction`" update = "true"
	 *                     insert = "true" not-null = "true"
	 * @return
	 */
	public String getFoundAtCorrectionString() {
		return foundAtCorrectionString;
	}

	public void setFoundAtCorrectionString(String foundAtCorrectionString) {
		this.foundAtCorrectionString = foundAtCorrectionString;
	}
	
	/**
	 * * @swt.variable visible="true" name="Apex Sn" searchable="true"
	 * 
	 * @swt.modify canModify="false"
	 * @return
	 */
	public Integer binKey() {
		return this.getMassSpec().hashCode();
	}
}
