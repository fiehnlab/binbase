/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Comment;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Commentable;

/**
 * @author wohlgemuth
 * @hibernate.subclass discriminator-value = "1"
 * 
 */
public class BinComment extends Comment {

	/**
	 * @hibernate.many-to-one  column = "type" class = "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin" update = "true" insert = "true" not-null = "true"
	 */
	public Commentable getType() {
		return super.getType();
	}

}
