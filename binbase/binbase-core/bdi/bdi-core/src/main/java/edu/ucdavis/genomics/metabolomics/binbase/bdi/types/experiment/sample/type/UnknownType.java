/*
 * Created on Nov 5, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.type;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Type;

/**
 * @hibernate.subclass discriminator-value = "unknown"
 * @author wohlgemuth
 *
 */
public class UnknownType extends Type{

    public UnknownType() {
        super();
    }

}
