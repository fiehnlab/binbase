/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.renderer;

import java.util.HashMap;
import java.util.Map;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.exception.RenderException;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.renderer.impl.ObjectRenderer;

/**
 * @author wohlgemuth is used to obtain renderers at runtime and to get sure that every renderer exist only ones
 */
public class RenderFactory {
	private static RenderFactory instance = null;

	/**
	 * contains all available and initialized renderers
	 */
	protected Map<String,Renderer> renderers = new HashMap<String,Renderer>();

	/**
	 * 
	 */
	public RenderFactory() {
		super();
	}

	/**
	 * creates a new factory
	 * 
	 * @return
	 */
	public static RenderFactory buildFactory() {
		if (instance == null) {
			instance = new RenderFactory();
		}
		return instance;
	}

	/**
	 * creates a renderer from the given class
	 * 
	 * @param clazz
	 * @return
	 */
	public Renderer obtainRenderer(String clazz) throws RenderException {
		Renderer o = renderers.get(clazz);

		if (o != null) {
			return o;
		}

		try {
			o = (Renderer) Class.forName(clazz).newInstance();
		} catch (Exception e) {
			throw new RenderException(e);
		}
		renderers.put(clazz, o);
		return o;
	}
	
	/**
	 * creates a default object renderer
	 * 
	 * @param clazz
	 * @return
	 */
	public Renderer obtainRenderer(){
		Renderer o = renderers.get(ObjectRenderer.class.getName());

		if (o != null) {
			return o;
		}

		o = new ObjectRenderer();
		renderers.put(o.getClass().getName(), o);
		return o;
	}
	
		
}
