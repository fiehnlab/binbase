package edu.ucdavis.genomics.metabolomics.binbase.bdi.model;

/**
 * a native query
 * @author wohlgemuth
 *
 */
public class NativeQuerry {
	
	private Class entinity;
	private Object query;
	
	public NativeQuerry(Object o,Class entinity){
		this.query = o;
		this.entinity = entinity;
	}
	
	public Object getQuery(){
		return query;
	}
	
	public Class getEntinity(){
		return entinity;
	}
}
