/*
 * Created on 09.05.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.GenericSample;
import edu.ucdavis.genomics.metabolomics.util.type.converter.BooleanConverter;

/**
 * 
 * a bin sample and only provides some basic infos
 * 
 * @swt
 * @author wohlgemuth
 * @hibernate.class table = "BIN_SAMPLES" dynamic-insert = "false" dynamic-update =
 *                  "false"
 */
public class BinSample implements Comparable, GenericSample {
	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * DOCUMENT ME!
	 */
	private String correctionFailedString;

	private String finishedString;


	/**
	 * DOCUMENT ME!
	 */
	private Integer id;

	/**
	 * DOCUMENT ME!
	 */
	private String isVisibleString;

	/**
	 * DOCUMENT ME!
	 */
	private String name;

	/**
	 * DOCUMENT ME!
	 */
	private String saturatedString;

	/**
	 * DOCUMENT ME!
	 */
	private String setupX;

	/**
	 * DOCUMENT ME!
	 */
	private Integer version;



	/**
	 * @hibernate.property column = "`correction_failed`" insert = "false"
	 *                     update = "false" not-null = "true"
	 * 
	 * 
	 * @return
	 */
	public String getCorrectionFailedString() {
		return correctionFailedString;
	}

	/**
	 * 
	 * @swt.variable visible="true" name="id" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.id column = "`sample_id`" generator-class ="assigned"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#getId()
	 * 
	 * @uml.property name="id"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @hibernate.property column = "`sample_name`" insert = "true" update =
	 *                     "true" not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#getName()
	 * 
	 * @swt.variable visible="true" name="Name" searchable="true"
	 * @swt.modify canModify="false"
	 * @uml.property name="name"
	 */
	public String getName() {
		return name;
	}


	/**
	 * @hibernate.property column = "`saturated`" insert = "false" update =
	 *                     "false" not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#isSaturated()
	 * 
	 * @uml.property name="saturatedString"
	 */
	public String getSaturatedString() {
		return saturatedString;
	}

	/**
	 * @swt.variable visible="true" name="Setup X Id" searchable="true"
	 * @swt.modify canModify="false"
	 * 
	 * @hibernate.property column = "`setupx_id`" insert = "false" update =
	 *                     "true" not-null = "true"
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#getSetupxId()
	 */
	public String getSetupxId() {
		return setupX;
	}

	/**
	 * 
	 * @swt.variable visible="true" name="Version" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`version`" insert = "false" update =
	 *                     "false"
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#version()
	 */
	public Integer getVersion() {
		return version;
	}

	/**
	 * @hibernate.property column = "`visible`" insert = "false" update =
	 *                     "false" not-null = "true"
	 * 
	 * @return
	 */
	public String getVisibleString() {
		return isVisibleString;
	}

	/**
	 * @swt.variable visible="true" name="Correction Failed" searchable="true"
	 * @swt.modify canModify="false"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#correctionFailed()
	 */
	public Boolean isCorrectionFailed() {
		return new Boolean(BooleanConverter
				.StringtoBoolean(getCorrectionFailedString()));
	}


	/**
	 * @swt.variable visible="true" name="Saturated" searchable="true"
	 * @swt.modify canModify="false"
	 * 
	 * @return DOCUMENT ME!
	 */
	public Boolean isSaturated() {
		return new Boolean(BooleanConverter.StringtoBoolean(this
				.getSaturatedString()));
	}

	/**
	 * @swt.variable visible="true" name="Visible" searchable="true"
	 * @swt.modify canModify="false"
	 * 
	 * @return DOCUMENT ME!
	 */
	public Boolean isVisible() {
		return new Boolean(BooleanConverter.StringtoBoolean(this
				.getVisibleString()));
	}

	/**
	 * @swt.variable visible="false" name="Finished" searchable="true"
	 * @swt.modify canModify="false"
	 * 
	 * @return DOCUMENT ME!
	 */
	public Boolean isFinished() {
		return new Boolean(BooleanConverter.StringtoBoolean(this
				.getFinishedString()));
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param correctionFailedAsString
	 *            DOCUMENT ME!
	 */
	public void setCorrectionFailedString(String correctionFailedAsString) {
		this.correctionFailedString = correctionFailedAsString;
	}

	/**
	 * @param id
	 *            The id to set.
	 * 
	 * @uml.property name="id"
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param name
	 *            The name to set.
	 * 
	 * @uml.property name="name"
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @param saturated
	 *            The saturated to set.
	 */
	public void setSaturated(Boolean value) {
		this.setSaturatedString(BooleanConverter.booleanToString(value
				.booleanValue()));
	}

	/**
	 * @param saturatedString
	 *            The saturatedString to set.
	 * 
	 * @uml.property name="saturatedString"
	 */
	public void setSaturatedString(String saturatedString) {
		this.saturatedString = saturatedString;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param setupX
	 *            DOCUMENT ME!
	 */
	public void setSetupxId(String setupX) {
		this.setupX = setupX;
	}
	
	/**
	 * DOCUMENT ME!
	 * 
	 * @param version
	 *            DOCUMENT ME!
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param b
	 *            DOCUMENT ME!
	 */
	public void setVisible(Boolean b) {
		this.setVisibleString(BooleanConverter
				.booleanToString(b.booleanValue()));
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param isVisibleString
	 *            DOCUMENT ME!
	 */
	public void setVisibleString(String isVisibleString) {
		this.isVisibleString = isVisibleString;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 */
	public String toString() {
		return this.getName();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object arg0) {
		if (arg0 instanceof BinSample) {
			return ((BinSample) arg0).getName().compareTo(this.getName());
		}
		return arg0.toString().compareTo(this.toString());
	}

	/**
	 * @hibernate.property column = "`finished`" insert = "false" update =
	 *                     "false" not-null = "true"
	 * 
	 * 
	 * @return
	 */
	public String getFinishedString() {
		return finishedString;
	}

	public void setFinishedString(String finishedString) {
		this.finishedString = finishedString;
	}

}
