/*
 * Created on 09.05.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Comment;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.Commentable;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.util.type.converter.BooleanConverter;

/**
 * @swt
 * @author wohlgemuth
 * @hibernate.class table = "SAMPLES" dynamic-insert = "true"  where = "finished='TRUE'" dynamic-update =
 *                  "true"
 */
public class Sample implements Commentable, Comparable, GenericSample {
	/**
	 * Comment for <code>serialVersionUID</code>
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * DOCUMENT ME!
	 */
	private Collection bins = Collections.EMPTY_SET;

	private Collection comments;

	/**
	 * DOCUMENT ME!
	 */
	private Configuration configuration;

	/**
	 * DOCUMENT ME!
	 */
	private String correctionFailedString;

	private String finishedString;

	/**
	 * DOCUMENT ME!
	 * 
	 * @uml.property name="experiment"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	private ExperimentClass experiment;

	/**
	 * DOCUMENT ME!
	 */
	private Integer id;

	/**
	 * DOCUMENT ME!
	 */
	private String isVisibleString;

	/**
	 * DOCUMENT ME!
	 */
	private String name;

	/**
	 * DOCUMENT ME!
	 */
	private Collection notMatchedSpectra;

	/**
	 * DOCUMENT ME!
	 */
	private String saturatedString;

	/**
	 * DOCUMENT ME!
	 */
	private String setupX;

	/**
	 * DOCUMENT ME!
	 */
	private Collection spectra = Collections.EMPTY_SET;

	private String gcMethod;

	private String msMethod;

	private String asMethod;

	private String qcMethod;

	private String dpMethod;

	private String operator;

	private Integer tray;

	private String machine;

	private Date dateOfImport;
	
	private Date dateOfMeasurment;
	
	/**
	 * DOCUMENT ME!
	 */
	private Integer version;

	private Collection resultLinks;

	private Collection metaData;

	private SampleResult result;

	private Type type;

	public void addComment(Comment comment) {
		if (this.getComments() == null) {
			this.setComments(new HashSet());
		}
		comment.setType(this);
		this.getComments().add(comment);
	}

	public void addResultLink(ResultLink link) {
		if (this.getResultLinks() == null) {
			this.setResultLinks(new HashSet());
		}
		link.setSample(this);
		this.getResultLinks().add(link);
	}

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin"
	 * @hibernate.collection-key column = "`sample_id`"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#getBins()
	 * 
	 * @uml.property name="bins"
	 */
	public Collection getBins() {
		return bins;
	}

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.SampleComment"
	 * @hibernate.collection-key column = "type"
	 */
	public Collection getComments() {
		return this.comments;
	}

	/**
	 * *
	 * 
	 * @hibernate.many-to-one cascade = "none" column = "`configuration_id`"
	 *                        class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Configuration"
	 *                        insert="false" update="false"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#getConfiguration()
	 */
	public Configuration getConfiguration() {
		return configuration;
	}

	/**
	 * *
	 * 
	 * @hibernate.many-to-one cascade = "none" column = "`type`" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Type"
	 *                        insert="false" update="false"
	 * 
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @hibernate.property column = "`correction_failed`" insert = "false"
	 *                     update = "false" not-null = "true"
	 * 
	 * 
	 * @return
	 */
	public String getCorrectionFailedString() {
		return correctionFailedString;
	}

	/**
	 * @swt.variable visible="false" name="Experiment Class" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.many-to-one column="`class`" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.ExperimentClass"
	 * @return Returns the experiment.
	 * 
	 * @uml.property name="experiment"
	 */
	public ExperimentClass getExperiment() {
		return experiment;
	}

	/**
	 * 
	 * @swt.variable visible="true" name="id" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.id column = "`sample_id`" generator-class ="assigned"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#getId()
	 * 
	 * @uml.property name="id"
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @hibernate.property column = "`sample_name`" insert = "true" update =
	 *                     "true" not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#getName()
	 * 
	 * @swt.variable visible="true" name="Name" searchable="true"
	 * @swt.modify canModify="false"
	 * @uml.property name="name"
	 */
	public String getName() {
		return name;
	}

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.NotFoundSpectra"
	 * @hibernate.collection-key column = "`sample_id`"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#getSpectra()
	 * 
	 * @uml.property name="spectra"
	 */
	public Collection getNotMatchedSpectra() {
		return notMatchedSpectra;
	}

	/**
	 * @hibernate.property column = "`saturated`" insert = "false" update =
	 *                     "false" not-null = "true"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#isSaturated()
	 * 
	 * @uml.property name="saturatedString"
	 */
	public String getSaturatedString() {
		return saturatedString;
	}

	/**
	 * @swt.variable visible="true" name="Setup X Id" searchable="true"
	 * @swt.modify canModify="false"
	 * 
	 * @hibernate.property column = "`setupx_id`" insert = "false" update =
	 *                     "true" not-null = "true"
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#getSetupxId()
	 */
	public String getSetupxId() {
		return setupX;
	}

	/**
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.spectra.Spectra"
	 * @hibernate.collection-key column = "`sample_id`"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#getSpectra()
	 * 
	 * @uml.property name="spectra"
	 */
	public Collection getSpectra() {
		return spectra;
	}

	/**
	 * 
	 * @swt.variable visible="true" name="Version" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.property column = "`version`" insert = "false" update =
	 *                     "false"
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#version()
	 */
	public Integer getVersion() {
		return version;
	}

	/**
	 * @hibernate.property column = "`visible`" insert = "false" update =
	 *                     "false" not-null = "true"
	 * 
	 * @return
	 */
	public String getVisibleString() {
		return isVisibleString;
	}

	/**
	 * @swt.variable visible="true" name="Correction Failed" searchable="true"
	 * @swt.modify canModify="false"
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#correctionFailed()
	 */
	public Boolean isCorrectionFailed() {
		return new Boolean(BooleanConverter.StringtoBoolean(getCorrectionFailedString()));
	}

	/**
	 * @swt.variable visible="true" name="Saturated" searchable="true"
	 * @swt.modify canModify="false"
	 * 
	 * @return DOCUMENT ME!
	 */
	public Boolean isSaturated() {
		return new Boolean(BooleanConverter.StringtoBoolean(this.getSaturatedString()));
	}

	/**
	 * @swt.variable visible="true" name="Visible" searchable="true"
	 * @swt.modify canModify="false"
	 * 
	 * @return DOCUMENT ME!
	 */
	public Boolean isVisible() {
		return new Boolean(BooleanConverter.StringtoBoolean(this.getVisibleString()));
	}

	/**
	 * @swt.variable visible="true" name="Finished" searchable="true"
	 * @swt.modify canModify="false"
	 * 
	 * @return DOCUMENT ME!
	 */
	public Boolean isFinished() {
		return new Boolean(BooleanConverter.StringtoBoolean(this.getFinishedString()));
	}

	/**
	 * @param bins
	 *            The bins to set.
	 * 
	 * @uml.property name="bins"
	 */
	public void setBins(Collection bins) {
		this.bins = bins;
	}

	public void setComments(Collection comments) {
		this.comments = comments;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param configuration
	 *            DOCUMENT ME!
	 */
	public void setConfiguration(Configuration configuration) {
		this.configuration = configuration;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param correctionFailedAsString
	 *            DOCUMENT ME!
	 */
	public void setCorrectionFailedString(String correctionFailedAsString) {
		this.correctionFailedString = correctionFailedAsString;
	}

	/**
	 * @param experiment
	 *            The experiment to set.
	 * 
	 * @uml.property name="experiment"
	 */
	public void setExperiment(ExperimentClass experiment) {
		this.experiment = experiment;
	}

	/**
	 * @param id
	 *            The id to set.
	 * 
	 * @uml.property name="id"
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param name
	 *            The name to set.
	 * 
	 * @uml.property name="name"
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param notMatchedSpectra
	 *            DOCUMENT ME!
	 */
	public void setNotMatchedSpectra(Collection notMatchedSpectra) {
		this.notMatchedSpectra = notMatchedSpectra;
	}

	/**
	 * @param saturated
	 *            The saturated to set.
	 */
	public void setSaturated(Boolean value) {
		this.setSaturatedString(BooleanConverter.booleanToString(value.booleanValue()));
	}

	/**
	 * @param saturatedString
	 *            The saturatedString to set.
	 * 
	 * @uml.property name="saturatedString"
	 */
	public void setSaturatedString(String saturatedString) {
		this.saturatedString = saturatedString;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param setupX
	 *            DOCUMENT ME!
	 */
	public void setSetupxId(String setupX) {
		this.setupX = setupX;
	}

	/**
	 * @param spectra
	 *            The spectra to set.
	 * 
	 * @uml.property name="spectra"
	 */
	public void setSpectra(Collection spectra) {
		this.spectra = spectra;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param version
	 *            DOCUMENT ME!
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param b
	 *            DOCUMENT ME!
	 */
	public void setVisible(Boolean b) {
		this.setVisibleString(BooleanConverter.booleanToString(b.booleanValue()));
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param isVisibleString
	 *            DOCUMENT ME!
	 */
	public void setVisibleString(String isVisibleString) {
		this.isVisibleString = isVisibleString;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 */
	public String toString() {
		return this.getName();
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param result
	 *            DOCUMENT ME!
	 */
	public void setResult(SampleResult result) {
		this.result = result;
	}

	/**
	 * @hibernate.one-to-one class =
	 *                       "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.SampleResult" lazy="true" constrained="true"
	 */
	public SampleResult getResult() {
		return result;
	}

	/**
	 * @param resultLinks
	 *            The resultLinks to set.
	 */
	public void setResultLinks(Collection resultLinks) {
		this.resultLinks = resultLinks;
	}

	/**
	 * 
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.ResultLink"
	 * @hibernate.collection-key column = "`sample_id`"
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.Sample#getResultLinks()
	 */
	public Collection getResultLinks() {
		return resultLinks;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(Object arg0) {
		if (arg0 instanceof Sample) {
			return ((Sample) arg0).getName().compareTo(this.getName());
		}
		return arg0.toString().compareTo(this.toString());
	}

	public Comment createComment() {
		return new SampleComment();
	}

	public void setType(Type type) {
		this.type = type;
	}

	/**
	 * @hibernate.property column = "`finished`" insert = "false" update =
	 *                     "false" not-null = "true"
	 * 
	 * 
	 * @return
	 */
	public String getFinishedString() {
		return finishedString;
	}

	public void setFinishedString(String finishedString) {
		this.finishedString = finishedString;
	}


	/**
	 * @hibernate.property column = "`gcmethod`" insert = "true" update =
	 *                     "true" not-null = "true"
	 * 
	 * @swt.variable visible="true" name="GC Method" searchable="true"
	 * @swt.modify canModify="false"
	 * @return
	 */
	public String getGcMethod() {
		return gcMethod;
	}

	public void setGcMethod(String gcMethod) {
		this.gcMethod = gcMethod;
	}


	/**
	 * @hibernate.property column = "`msmethod`" insert = "true" update =
	 *                     "true" not-null = "true"
	 * 
	 * @swt.variable visible="true" name="MS Method" searchable="true"
	 * @swt.modify canModify="false"
	 * @return
	 */
	public String getMsMethod() {
		return msMethod;
	}

	public void setMsMethod(String msMethod) {
		this.msMethod = msMethod;
	}


	/**
	 * @hibernate.property column = "`asmethod`" insert = "true" update =
	 *                     "true" not-null = "true"
	 * 
	 * @swt.variable visible="true" name="AS Method" searchable="true"
	 * @swt.modify canModify="false"
	 * @return
	 */
	public String getAsMethod() {
		return asMethod;
	}

	public void setAsMethod(String asMethod) {
		this.asMethod = asMethod;
	}


	/**
	 * @hibernate.property column = "`qcmethod`" insert = "true" update =
	 *                     "true" not-null = "true"
	 * 
	 * @swt.variable visible="true" name="QC Method" searchable="true"
	 * @swt.modify canModify="false"
	 * @return
	 */
	public String getQcMethod() {
		return qcMethod;
	}

	public void setQcMethod(String qcMethod) {
		this.qcMethod = qcMethod;
	}


	/**
	 * @hibernate.property column = "`dpmethod`" insert = "true" update =
	 *                     "true" not-null = "true"
	 * 
	 * @swt.variable visible="true" name="DP Method" searchable="true"
	 * @swt.modify canModify="false"
	 * @return
	 */
	public String getDpMethod() {
		return dpMethod;
	}

	public void setDpMethod(String dpMethod) {
		this.dpMethod = dpMethod;
	}


	/**
	 * @hibernate.property column = "`operator`" insert = "true" update =
	 *                     "true" not-null = "true"
	 * 
	 * @swt.variable visible="true" name="Operator" searchable="true"
	 * @swt.modify canModify="false"
	 * @return
	 */
	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}


	/**
	 * @hibernate.property column = "`tray`" insert = "true" update =
	 *                     "true" not-null = "true"
	 * 
	 * @swt.variable visible="true" name="Tray" searchable="true"
	 * @swt.modify canModify="false"
	 * @return
	 */
	public Integer getTray() {
		return tray;
	}

	public void setTray(Integer tray) {
		this.tray = tray;
	}

	/**
	 * @hibernate.property column = "`machine`" insert = "true" update =
	 *                     "true" not-null = "true"
	 * 
	 * @swt.variable visible="true" name="Machine" searchable="true"
	 * @swt.modify canModify="false"
	 * @return
	 */
	public String getMachine() {
		return machine;
	}

	public void setMachine(String machine) {
		this.machine = machine;
	}

	/**
	 * 
	 * @hibernate.set lazy="true" cascade = "none" inverse = "true"
	 * @hibernate.collection-one-to-many class =
	 *                                   "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.SampleInfo"
	 * @hibernate.collection-key column = "`sample_id`"
	 * 
	 */
	public Collection getMetaData() {
		return metaData;
	}

	public void setMetaData(Collection metaData) {
		this.metaData = metaData;
	}

	/**
	 * @hibernate.property column = "`date_of_import`" insert = "true" update =
	 *                     "true" not-null = "true"
	 * 
	 * @swt.variable visible="true" name="Import Date" searchable="true"
	 * @swt.modify canModify="false"
	 * @return
	 */
	public Date getDateOfImport() {
		return dateOfImport;
	}

	
	public void setDateOfImport(Date dateOfImport) {
		this.dateOfImport = dateOfImport;
	}

	/**
	 * @hibernate.property column = "`date`" insert = "true" update =
	 *                     "true" not-null = "true"
	 * 
	 * @swt.variable visible="true" name="Creation Date" searchable="true"
	 * @swt.modify canModify="false"
	 * @return
	 */
	public Date getDateOfMeasurment() {
		return dateOfMeasurment;
	}

	public void setDateOfMeasurment(Date dateOfMeasurment) {
		this.dateOfMeasurment = dateOfMeasurment;
	}

}
