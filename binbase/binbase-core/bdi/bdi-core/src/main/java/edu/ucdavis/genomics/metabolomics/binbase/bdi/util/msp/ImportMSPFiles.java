/*
 * Created on 16.02.2004
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.util.msp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import edu.ucdavis.genomics.metabolomics.sjp.Parser;
import edu.ucdavis.genomics.metabolomics.sjp.ParserHandler;
import edu.ucdavis.genomics.metabolomics.sjp.exception.ParserException;
import edu.ucdavis.genomics.metabolomics.sjp.parser.msp.MSPParser;
import edu.ucdavis.genomics.metabolomics.util.BasicObject;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.io.FileUtil;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;

/**
 * importiert die angegebene datei oder das verzeichnis in die datenbank
 * 
 * @author wohlgemuth
 */
public class ImportMSPFiles extends BasicObject {
	/**
	 * importiert das angegebene verzeichnis/datei in die datenbank
	 * 
	 * @param args
	 * 
	 * args[0] das verzeichnis args[1] der user args[2] das password
	 */
	public static void main(String[] args) throws InstantiationException, IllegalAccessException, FileNotFoundException, ParserException, IOException {
		if (args.length != 1) {
			System.err.println("Sorry need argument");
			System.err.println("arg[0] = file or dir");

			return;
		}

		new ImportMSPFiles().importMsp(new File(args[0]));

	}

	/**
	 * imports the given file/diretory as library
	 * 
	 * @param f
	 * @param p
	 * @throws ParserException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void importMsp(File f, Properties p) throws ParserException, InstantiationException, IllegalAccessException, FileNotFoundException, IOException {
		ParserHandler handler = new HibernateDabaseResultHandler();

		if (f.isDirectory()) {
			File[] files = f.listFiles();

			for (int i = 0; i < files.length; i++) {
				p.put(HibernateDabaseResultHandler.LIBRARY, FileUtil.cleanFileName(files[i].getAbsolutePath()));

				Parser parser = Parser.create(MSPParser.class);
				parser.setProperties(p);
				handler.setProperties(p);
				parser.parse(files[i], handler);
			}
		} else if (f.isFile()) {
			p.put(HibernateDabaseResultHandler.LIBRARY, FileUtil.cleanFileName(f.getName()));

			Parser parser = Parser.create(MSPParser.class);
			parser.setProperties(p);
			handler.setProperties(p);
			parser.parse(f, handler);
		} else {
			System.err.println("something is wrong with this file: " + f);
		}
	}

	/**
	 * imports the given file/diretory as library
	 * 
	 * @param file
	 * @throws FileNotFoundException
	 * @throws ParserException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IOException
	 */
	public void importMsp(File file) throws FileNotFoundException, ParserException, InstantiationException, IllegalAccessException, IOException {
		importMsp(file, System.getProperties());
	}

}
