/*
 * Created on Dec 11, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Properties;

import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.EntityMode;
import org.hibernate.Filter;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.ReplicationMode;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.stat.SessionStatistics;

import edu.ucdavis.genomics.metabolomics.exception.NotSupportedException;

/**
 * returns a statless sessionfactory
 * @author wohlgemuth
 * @version Dec 11, 2006
 *
 */
public class HibernateStatelessSessionFactory extends HibernateFactory{

	private SessionFactory factory;
	
	private Session session;
	
	@Override
	public void destroyFactory() {
		destroySession();
	}

	@Override
	public void destroySession() {
		
	}

	@Override
	public Session getSession() {
		if(session == null){
			StatelessSession ses = factory.openStatelessSession();
			
			session = new InternalSession(ses);
		}
		return session;
	}

	@Override
	protected void initialize(Properties p) {
		
	}

	private class InternalSession implements Session{
		private StatelessSession session;

		private static final long serialVersionUID = 1L;

		public InternalSession(StatelessSession ses) {
			session = ses;
		}

		public Transaction beginTransaction() {
			return session.beginTransaction();
		}

		public Connection close() {
			session.close();
			
			return null;
		}

		public Connection connection() {
			return session.connection();
		}

		public Criteria createCriteria(Class arg0, String arg1) {
			return session.createCriteria(arg0, arg1);
		}

		public Criteria createCriteria(Class arg0) {
			return session.createCriteria(arg0);
		}

		public Criteria createCriteria(String arg0, String arg1) {
			return session.createCriteria(arg0, arg1);
		}

		public Criteria createCriteria(String arg0) {
			return session.createCriteria(arg0);
		}

		public Query createQuery(String arg0) {
			return session.createQuery(arg0);
		}

		public SQLQuery createSQLQuery(String arg0) throws HibernateException {
			return session.createSQLQuery(arg0);
		}

		public void delete(Object arg0) {
			session.delete(arg0);
		}

		public void delete(String arg0, Object arg1) {
			session.delete(arg0, arg1);
		}

		public Object get(Class arg0, Serializable arg1, LockMode arg2) {
			return session.get(arg0, arg1, arg2);
		}

		public Object get(Class arg0, Serializable arg1) {
			return session.get(arg0, arg1);
		}

		public Object get(String arg0, Serializable arg1, LockMode arg2) {
			return session.get(arg0, arg1, arg2);
		}

		public Object get(String arg0, Serializable arg1) {
			return session.get(arg0, arg1);
		}

		public Query getNamedQuery(String arg0) {
			return session.getNamedQuery(arg0);
		}

		public Transaction getTransaction() {
			return session.getTransaction();
		}

		public Serializable insert(Object arg0) {
			return session.insert(arg0);
		}

		public Serializable insert(String arg0, Object arg1) {
			return session.insert(arg0, arg1);
		}

		public void update(Object arg0) {
			session.update(arg0);
		}

		public void update(String arg0, Object arg1) {
			session.update(arg0, arg1);
		}

		public void cancelQuery() throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void clear() {
			throw new NotSupportedException("not supported method");
		}

		public boolean contains(Object arg0) {
			throw new NotSupportedException("not supported method");
		}

		public Query createFilter(Object arg0, String arg1) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void disableFilter(String arg0) {
			throw new NotSupportedException("not supported method");
		}

		public Connection disconnect() throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public Filter enableFilter(String arg0) {
			throw new NotSupportedException("not supported method");
		}

		public void evict(Object arg0) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void flush() throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public CacheMode getCacheMode() {
			throw new NotSupportedException("not supported method");
		}

		public LockMode getCurrentLockMode(Object arg0) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public Filter getEnabledFilter(String arg0) {
			throw new NotSupportedException("not supported method");
		}

		public EntityMode getEntityMode() {
			throw new NotSupportedException("not supported method");
		}

		public String getEntityName(Object arg0) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public FlushMode getFlushMode() {
			throw new NotSupportedException("not supported method");
		}

		public Serializable getIdentifier(Object arg0) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public Session getSession(EntityMode arg0) {
			throw new NotSupportedException("not supported method");
		}

		public SessionFactory getSessionFactory() {
			throw new NotSupportedException("not supported method");
		}

		public SessionStatistics getStatistics() {
			throw new NotSupportedException("not supported method");
		}

		public boolean isConnected() {
			throw new NotSupportedException("not supported method");
		}

		public boolean isDirty() throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public boolean isOpen() {
			throw new NotSupportedException("not supported method");
		}

		public Object load(Class arg0, Serializable arg1) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public Object load(String arg0, Serializable arg1) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void load(Object arg0, Serializable arg1) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public Object load(Class arg0, Serializable arg1, LockMode arg2) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public Object load(String arg0, Serializable arg1, LockMode arg2) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void lock(Object arg0, LockMode arg1) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void lock(String arg0, Object arg1, LockMode arg2) throws HibernateException {
			throw new NotSupportedException("not supported method");			
		}

		public Object merge(Object arg0) throws HibernateException {
throw new NotSupportedException("not supported method");
		}

		public Object merge(String arg0, Object arg1) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void persist(Object arg0) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void persist(String arg0, Object arg1) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void reconnect() throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void reconnect(Connection arg0) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void refresh(Object arg0) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void refresh(Object arg0, LockMode arg1) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void replicate(Object arg0, ReplicationMode arg1) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void replicate(String arg0, Object arg1, ReplicationMode arg2) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public Serializable save(Object arg0) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void save(Object arg0, Serializable arg1) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public Serializable save(String arg0, Object arg1) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void save(String arg0, Object arg1, Serializable arg2) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void saveOrUpdate(Object arg0) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void saveOrUpdate(String arg0, Object arg1) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void setCacheMode(CacheMode arg0) {
			throw new NotSupportedException("not supported method");
		}

		public void setFlushMode(FlushMode arg0) {
			throw new NotSupportedException("not supported method");
		}

		public void setReadOnly(Object arg0, boolean arg1) {
			throw new NotSupportedException("not supported method");
		}

		public void update(Object arg0, Serializable arg1) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}

		public void update(String arg0, Object arg1, Serializable arg2) throws HibernateException {
			throw new NotSupportedException("not supported method");
		}
	}

	@Override
	public SessionFactory getInternalFactory() {
		getSession();
		return factory;
	}
}
