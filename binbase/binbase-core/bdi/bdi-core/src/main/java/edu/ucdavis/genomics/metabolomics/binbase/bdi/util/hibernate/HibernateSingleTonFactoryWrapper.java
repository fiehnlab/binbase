/*
 * Created on Dec 9, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.util.hibernate;

import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * a wrapper to use singleton based variant of the hibernate factory
 * 
 * @author wohlgemuth
 * @version Dec 9, 2005
 * 
 */
public class HibernateSingleTonFactoryWrapper extends HibernateFactory {
	private static HibernateFactory factory;

	public HibernateSingleTonFactoryWrapper() {
		super();
	}

	@Override
	public Session getSession() {
		return factory.getSession();
	}

	@Override
	protected void initialize(Properties p) {
		if (factory == null) {
			String fac = p.getProperty(HibernateSingleTonFactoryWrapper.class.getName());
			if(fac == null){
				System.err.println("no implementation for the singleton provided, use default");
				fac = getDefaultFactory();
			}
			factory = HibernateFactory.newInstance(p, fac,false);
		}
	}

	@Override
	public void destroySession() {
		factory.destroySession();
	}

	@Override
	public void destroyFactory() {
		factory.destroyFactory();
		factory = null;
	}

	@Override
	public SessionFactory getInternalFactory() {
		return factory.getInternalFactory();
	}
}
