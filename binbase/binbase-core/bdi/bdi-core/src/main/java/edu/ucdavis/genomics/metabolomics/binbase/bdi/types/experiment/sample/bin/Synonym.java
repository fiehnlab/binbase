/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin;

/**
 * @author wohlgemuth defines a synonym
 * @hibernate.class table = "SYNONYME"
 */
public class Synonym {
	private Integer id;

	private String name;

	/**
	 * 
	 */
	private Bin bin;

	/**
	 * 
	 */
	public Synonym() {
		super();
		
	}

	/**
	 * @hibernate.id column = "id" generator-class = "native"
	 * @hibernate.generator-param name = "sequence" value = "HIBERNATE_SEQUENCE"
	 * @return
	 */
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @swt.variable visible="true" name="Name" searchable="true"
	 * @swt.modify canModify="true"
	 * @hibernate.property column = "name" insert = "true" update = "true"
	 * @return
	 */
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @swt.variable visible="true" name="Bin" searchable="true"
	 * @swt.modify canModify="false"
	 * @hibernate.many-to-one cascade = "none" column = "bin_id" class =
	 *                        "edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin"
	 */
	public Bin getBin() {
		return this.bin;
	}

	public void setBin(Bin bin) {
		this.bin = bin;
	}
}
