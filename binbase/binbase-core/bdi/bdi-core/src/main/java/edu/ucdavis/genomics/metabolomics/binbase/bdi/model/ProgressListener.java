/*
 * Created on Jan 3, 2007
 */
package edu.ucdavis.genomics.metabolomics.binbase.bdi.model;

import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;

/**
 * helps to monitor transactions
 * 
 * @author wohlgemuth
 * @version Jan 3, 2007
 * 
 */
public interface ProgressListener {

	/**
	 * begins a transaction for the given object
	 * 
	 * @author wohlgemuth
	 * @version Jan 3, 2007
	 * @param o
	 */
	public void transactionBegin(Object o, ReportEvent event);

	/**
	 * ends the current transaction
	 * 
	 * @author wohlgemuth
	 * @version Jan 3, 2007
	 */
	public void transactionEnd(ReportEvent event);

	/**
	 * cancel the current transaction
	 * 
	 * @author wohlgemuth
	 * @version Jan 3, 2007
	 * @param e
	 */
	public void transactionCancelled(Exception e, ReportEvent event);

	ReportEvent UPDATE = new ReportEvent("update", "update");

	ReportEvent ADD = new ReportEvent("add", "add");

	ReportEvent DELETE = new ReportEvent("delete", "delete");

	ReportEvent QUERY = new ReportEvent("query", "query");

	ReportEvent RELOAD = new ReportEvent("reload", "reload");

}
