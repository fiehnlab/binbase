/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.xdoclet.task.swt;

import xdoclet.TemplateSubTask;

/**
 * @author wohlgemuth
 * generate viewer sorter
 * @ant.element   display-name="Viewer Sorter" name="sorter" parent="edu.ucdavis.genomics.metabolomics.xdoclet.task.swt.SWTTask"
 */
public class ViewerSorterSubTask extends TemplateSubTask{	

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	/**
	 * the fild for this task
	 */
	private static String DEFAULT_TEMPLATE_FILE = "resources/swt-sorter.xdt";

	public ViewerSorterSubTask(){
		setDestinationFile("{0}Sorter.java");
		setHavingClassTag("swt");
		setTemplateURL(getClass().getResource(DEFAULT_TEMPLATE_FILE));
		setAcceptAbstractClasses(true);
		setAcceptInterfaces(true);
	}
}
