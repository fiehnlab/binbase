/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.xdoclet.task.swt;

import xdoclet.TemplateSubTask;

/**
 * @author wohlgemuth
 * @ant.element   display-name="Change Listener" name="changelistener" parent="edu.ucdavis.genomics.metabolomics.xdoclet.task.swt.SWTTask"
 */
public class ChangeListenerSubTask extends TemplateSubTask {

	private static String DEFAULT_TEMPLATE_FILE = "resources/swt-change-listener.xdt";

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * 
	 */
	public ChangeListenerSubTask() {
		setDestinationFile("{0}ChangeListener.java");
		setTemplateURL(getClass().getResource(DEFAULT_TEMPLATE_FILE));
		setHavingClassTag("swt");
		setAcceptAbstractClasses(true);
		setAcceptInterfaces(true);
	}

}
