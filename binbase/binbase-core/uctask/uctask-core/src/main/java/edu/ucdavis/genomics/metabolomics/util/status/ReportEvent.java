/*
 * Created on Apr 22, 2006
 */
package edu.ucdavis.genomics.metabolomics.util.status;

import java.io.Serializable;

import edu.ucdavis.genomics.metabolomics.util.status.priority.Priority;

/**
 * is used to publish report events
 * @author wohlgemuth
 * @version Apr 22, 2006
 *
 */
public class ReportEvent implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * name of the event
	 */
	private String name;
	
	private Priority priority = Priority.TRACE;
	
	public Priority getPriority() {
		return priority;
	}

	public void setPriority(Priority priority) {
		this.priority = priority;
	}

	/**
	 * description of the event
	 */
	private String description;

	public ReportEvent(String name, String description,Priority priority) {
		super();
		this.name = name;
		this.description = description;
		this.priority = priority;
	}

	public ReportEvent(String name, String description) {
		super();
		this.name = name;
		this.description = description;
		this.priority = Priority.TRACE;
	}

	public ReportEvent() {
		super();
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String toString() {
		return name + "(" + description + ")";
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof ReportEvent){
			return ((ReportEvent)obj).getName().equals(this.getName());
		}
		return false;
	}
}
