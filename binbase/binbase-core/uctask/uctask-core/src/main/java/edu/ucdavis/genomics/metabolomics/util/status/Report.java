/*
 * Created on Apr 22, 2006
 */
package edu.ucdavis.genomics.metabolomics.util.status;

import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

/**
 * is used to report changes in the binbase
 * @author wohlgemuth
 * @version Apr 22, 2006
 *
 */
public abstract class Report implements Serializable{
	
	/**
	 * 
	 * @author wohlgemuth
	 * @version Apr 22, 2006
	 * @param id the id
	 * @param event the reported event
	 * @param type the reported type
	 */
	public final void report(Serializable id, ReportEvent event, ReportType type){
		this.report(id,event,type,getHostName());
	}
	
	/**
	 * 
	 * @author wohlgemuth
	 * @version Apr 22, 2006
	 * @param id the id
	 * @param event the reported event
	 * @param type the type of the report
	 * @param ipaddress the address from the host
	 */
	public final void report(Serializable id, ReportEvent event, ReportType type, String ipaddress){
		this.report(id,event,type,getHostName(),new Date());
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Apr 22, 2006
	 * @param id the id
	 * @param event the reported event
	 * @param type the reported type
	 */
	public final void report(Serializable id, ReportEvent event, ReportType type,Exception e){
		this.report(id,event,type,getHostName(),e);
	}
	
	/**
	 * 
	 * @author wohlgemuth
	 * @version Apr 22, 2006
	 * @param id the id
	 * @param event the reported event
	 * @param type the type of the report
	 * @param ipaddress the address from the host
	 */
	public final void report(Serializable id, ReportEvent event, ReportType type, String ipaddress,Exception e){
		this.report(this.getOwner(),id,event,type,getHostName(),new Date(),e);
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Apr 24, 2006
	 * @param id
	 * @param event
	 * @param type
	 * @param ipaddress
	 * @param time
	 */
	public final void report(Serializable id, ReportEvent event, ReportType type, String ipaddress, Date time){
		this.report(this.getOwner(),id,event,type,ipaddress,time,null);
	}
	
	/**
	 * 
	 * @author wohlgemuth
	 * @version Apr 28, 2006
	 * @param id
	 * @param event
	 * @param type
	 * @param ipaddress
	 * @param time
	 * @param e
	 */
	public abstract void report(String owner,Serializable id, ReportEvent event, ReportType type, String ipaddress, Date time, Exception e);
	
	/**
	 * returns the hostname
	 * @author wohlgemuth
	 * @version Apr 24, 2006
	 * @return
	 */
	public String getHostName(){
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			return e.getMessage();
		}
	}
	/**
	 * owner of this report
	 * @author wohlgemuth
	 * @version Jul 19, 2006
	 * @return
	 */
	public abstract String getOwner();
}
