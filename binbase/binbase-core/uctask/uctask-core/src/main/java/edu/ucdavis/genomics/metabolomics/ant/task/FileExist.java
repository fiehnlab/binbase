/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.ant.task;

import java.io.File;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;

/**
 * @author wohlgemuth
 * sets a variable to true if a file exist or to false
 */
public class FileExist extends Task{
	private String variableToSet;
	
	private String file;
	/**
	 * 
	 */
	public FileExist() {
		super();
	}
	public String getFile() {
		return this.file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public String getVariableToSet() {
		return this.variableToSet;
	}
	public void setVariableToSet(String variableToSet) {
		this.variableToSet = variableToSet;
	}
	public void execute() throws BuildException {
		super.execute();
		if(file == null){
			throw new BuildException("file must be set to a value");
		}
		
		if(variableToSet == null){
			throw new BuildException("file must be set to a value");
		}
		
		
		File f = new File(file);
		
		if(f.exists()){
			this.getProject().setUserProperty(this.getVariableToSet(),"true");
			variableToSet = "true";
		}
		else{
			this.getProject().setUserProperty(this.getVariableToSet(),"false");
		}
	}

}
