package edu.ucdavis.genomics.metabolomics.util.status.notify;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.exception.NotificationException;
import edu.ucdavis.genomics.metabolomics.util.status.priority.Priority;

/**
 * to simplify the implementation
 * 
 * @author wohlgemuth
 */
public abstract class AbstractBaseNotifier implements PriotizedNotifier {

	private Logger logger = Logger.getLogger(getClass());

	private boolean async = false;

	protected boolean isAsync() {
		return async;
	}

	protected void setAsync(boolean async) {
		this.async = async;
	}

	/**
	 * calls the other notify method with the default priority
	 */
	public final void notify(final String message) throws NotificationException {
		notify(message, getDefaultPriority());
	}

	public final void notify(final String message, final Priority priority) throws NotificationException {
		if (message == null) {
			logger.warn("received message was NULL");
		}
		else if (priority == null) {
			logger.warn("priority was NULL");
		}
		else if (priority.compareTo(this.getDefaultPriority()) >= 0) {

			if (this.isAsync() == false) {
				doNotify(message, priority);
			}
			else {
				new Thread(new Runnable() {

					@Override
					public void run() {
						try {
							doNotify(message, priority);
							Thread.sleep(1000);

						}
						catch (NotificationException e) {
							logger.error(e.getMessage(), e);
						}
						catch (InterruptedException e) {
							logger.debug(e.getMessage(), e);

						}
					}
				}, "notifier thread").start();
			}
		}
		else {
			logger.info("priority was to small for this message for this notifier");
		}
	}

	protected abstract void doNotify(String message, Priority priority) throws NotificationException;
}
