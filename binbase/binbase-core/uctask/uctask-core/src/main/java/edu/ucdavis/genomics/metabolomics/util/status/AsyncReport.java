package edu.ucdavis.genomics.metabolomics.util.status;

import java.io.Serializable;
import java.util.Date;

import org.apache.log4j.Logger;

public class AsyncReport extends Report {

	private Logger logger = Logger.getLogger(getClass());
	private Report report;

	public String getHostName() {
		return report.getHostName();
	}

	public String getOwner() {
		return report.getOwner();
	}

	public void report(final String owner, final Serializable id,
			final ReportEvent event, final ReportType type,
			final String ipaddress, final Date time, final Exception e) {

		new Thread(new Runnable() {

			@Override
			public void run() {
				long begin = System.currentTimeMillis();
				report.report(owner, id, event, type, ipaddress, time, e);
				long end = System.currentTimeMillis();
				logger.debug("finsihed async report in time (ms): " + (end - begin));
			}
		}).start();
	}

	public AsyncReport(Report report) {
		super();
		this.report = report;
	}
}
