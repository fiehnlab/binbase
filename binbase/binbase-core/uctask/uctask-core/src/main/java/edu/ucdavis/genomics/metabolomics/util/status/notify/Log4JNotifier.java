package edu.ucdavis.genomics.metabolomics.util.status.notify;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.exception.NotificationException;
import edu.ucdavis.genomics.metabolomics.util.status.priority.Priority;

/**
 * forwards notification events to log4j
 * 
 * @author wohlgemuth
 */
public class Log4JNotifier extends AbstractBaseNotifier {

	/**
	 * our logger
	 */
	private final Logger logger = Logger.getLogger(getClass());

	/**
	 * our default priority is trace, this is the lowest priority we respond too
	 */
	public Priority getDefaultPriority() {
		return Priority.TRACE;
	}

	/**
	 * logging with a given priority forwarded to log4j
	 */
	protected void doNotify(final String message, final Priority priority) throws NotificationException {
		switch (priority) {
			case DEBUG:
				logger.debug(message);
				break;
			case INFO:
				logger.info(message);
				break;
			case WARNING:
				logger.warn(message);
				break;
			case ERROR:
				logger.error(message);
				break;
			case FATAL:
				logger.fatal(message);
				break;
			case TRACE:
				logger.trace(message);
				break;
				
			default:
				logger.info(message);
		}
	}
}
