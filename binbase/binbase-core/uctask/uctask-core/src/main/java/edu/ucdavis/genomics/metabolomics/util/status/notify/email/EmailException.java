package edu.ucdavis.genomics.metabolomics.util.status.notify.email;

import edu.ucdavis.genomics.metabolomics.exception.NotificationException;

public class EmailException extends NotificationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EmailException() {
		super();
	}

	public EmailException(final String arg0, final Throwable arg1) {
		super(arg0, arg1);
	}

	public EmailException(final String arg0) {
		super(arg0);
	}

	public EmailException(final Throwable arg0) {
		super(arg0);
	}

}
