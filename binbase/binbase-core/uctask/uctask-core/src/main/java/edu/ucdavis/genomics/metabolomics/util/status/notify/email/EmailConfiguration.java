package edu.ucdavis.genomics.metabolomics.util.status.notify.email;

import java.util.List;

/**
 * our email configuration
 * 
 * @author wohlgemuth
 */
public interface EmailConfiguration {

	/**
	 * returns the server name
	 * 
	 * @return
	 */
	public String getServer();

	/**
	 * returns the username
	 * 
	 * @return
	 */
	public String getUsername();

	/**
	 * returns the password
	 * 
	 * @return
	 */
	public String getPassword();

	/**
	 * returns a list of all people who are registered to receive emails
	 * 
	 * @return
	 */
	public List<String> getEmailAddress();

	/**
	 * who is sending this email
	 */
	public String getSenderAddress();

	/**
	 * returns the server port
	 * 
	 * @return
	 */
	public Integer getPort();

	/**
	 * does the server need ssl
	 * 
	 * @return
	 */
	public Boolean isSSLRequired();
}
