package edu.ucdavis.genomics.metabolomics.util.status.notify.twitter;

import edu.ucdavis.genomics.metabolomics.exception.NotificationException;

public class TwitterException extends NotificationException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TwitterException() {
		super();
	}

	public TwitterException(final String arg0, final Throwable arg1) {
		super(arg0, arg1);
	}

	public TwitterException(final String arg0) {
		super(arg0);
	}

	public TwitterException(final Throwable arg0) {
		super(arg0);
	}

}
