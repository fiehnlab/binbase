/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.ant.task;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.util.FileUtils;

/**
 * Gets a particular file from a URL source. Options include verbose reporting,
 * timestamp based fetches and controlling actions on failures. NB: access
 * through a firewall only works if the whole Java runtime is correctly
 * configured.
 * 
 * @since Ant 1.1
 * 
 * wohlgemuth: used from ant as basis for an enhanced variant with caching
 * 
 * @ant.task category="network"
 */
public class Fetch extends Task {

    private static final FileUtils FILE_UTILS = FileUtils.getFileUtils();

    private URL source; // required

    private File dest; // required

    private boolean verbose = false;

    private boolean useTimestamp = false; // off by default

    private boolean ignoreErrors = false;

    private String uname = null;

    private String pword = null;

    private File cache;

    private boolean caching = true;

    /**
     * Does the work.
     * 
     * @exception BuildException
     *                Thrown in unrecoverable error.
     */
    public void execute() throws BuildException {

        // set up logging
        int logLevel = Project.MSG_INFO;
        DownloadProgress progress = null;
        if (verbose) {
            progress = new VerboseProgress(System.out);
        }

        // execute the get
        try {
            doGet(logLevel, progress);
        } catch (IOException ioe) {
            log("Error getting " + source + " to " + dest);
            if (!ignoreErrors) {
                throw new BuildException(ioe, getLocation());
            }
        }
    }

    /**
     * make a get request, with the supplied progress and logging info. All the
     * other config parameters are set at the task level, source, dest,
     * ignoreErrors, etc.
     * 
     * @param logLevel
     *            level to log at, see {@link Project#log(String, int)}
     * @param progress
     *            progress callback; null for no-callbacks
     * @return true for a successful download, false otherwise. The return value
     *         is only relevant when {@link #ignoreErrors} is true, as when
     *         false all failures raise BuildExceptions.
     * @throws IOException
     *             for network trouble
     * @throws BuildException
     *             for argument errors, or other trouble when ignoreErrors is
     *             false.
     */
    public boolean doGet(int logLevel, DownloadProgress progress) throws IOException {
        if (source == null) {
            throw new BuildException("src attribute is required", getLocation());
        }

        if (dest == null) {
            throw new BuildException("dest attribute is required", getLocation());
        }

        if (dest.exists() && dest.isDirectory()) {
            throw new BuildException("The specified destination is a directory", getLocation());
        }

        if (dest.exists() && !dest.canWrite()) {
            throw new BuildException("Can't write to " + dest.getAbsolutePath(), getLocation());
        }

        cache = new File(this.getProject().getProperty("user.home") + "/.fetch-cache");

        // dont do any progress, unless asked
        if (progress == null) {
            progress = new NullProgress();
        }

        log("Getting: " + source, logLevel);
        log("To: " + dest.getAbsolutePath(), logLevel);

        // set the timestamp to the file date.
        long timestamp = 0;

        
        if (useTimestamp && dest.exists()) {
            timestamp = dest.lastModified();
            if (verbose) {
                Date t = new Date(timestamp);
                log("local file date : " + t.toString(), logLevel);
            }
        }


        // set up the URL connection
        URLConnection connection = source.openConnection();

        if(checkTimeStamp(timestamp,connection)) {
            return false;
        }
        
        
        InputStream is = null;

        boolean found = false;

        File cached = null;
        if (caching) {
            if (cache.exists() == false) {
                cache.mkdirs();
            }
            cached = new File(cache.getAbsolutePath() + "/" + dest.getName());

            // REVISIT: at this point even non HTTP connections may
            // support the if-modified-since behaviour -we just check
            // the date of the content and skip the write if it is not
            // newer. Some protocols (FTP) don't include dates, of
            // course.

            log("Check cache for: " + cached, logLevel);

            if (cached.exists()) {
                log("found in cache", logLevel);
                
                //check cached timestamp
                
                long cacheTimestamp = cached.lastModified();
                
                boolean ok = checkTimeStamp(cacheTimestamp,connection);
                if(ok) {
                    log("timestamp is ok -> use cache", logLevel);
                    
                    is = new BufferedInputStream(new FileInputStream(cached));
                    found = true;                    
                }
                else {
                    log("timestamp is not ok -> don't use cache", logLevel);
                    found = false;
                }

            }
            else {
                log("cache is empty", logLevel);
            }
        }
        if (found == false) {

            for (int i = 0; i < 3; i++) {
                // this three attempt trick is to get round quirks in different
                // Java implementations. Some of them take a few goes to bind
                // property; we ignore the first couple of such failures.
                try {
                    is = connection.getInputStream();
                    break;
                } catch (IOException ex) {
                    log("Error opening connection " + ex, logLevel);
                }
            }
            if (is == null) {
                log("Can't get " + source + " to " + dest, logLevel);
                if (ignoreErrors) {
                    return false;
                }
                throw new BuildException("Can't get " + source + " to " + dest, getLocation());
            }
        }
        FileOutputStream fos = new FileOutputStream(dest);
        FileOutputStream cacheStream = null;

        if (found == false) {
            cacheStream = new FileOutputStream(cached);
        }
        progress.beginDownload();
        boolean finished = false;
        try {
            byte[] buffer = new byte[100 * 1024];
            int length;
            while ((length = is.read(buffer)) >= 0) {
                fos.write(buffer, 0, length);

                if (found == false) {
                    cacheStream.write(buffer, 0, length);
                }
                progress.onTick();
            }
            finished = true;
        } finally {
            FileUtils.close(fos);
            FileUtils.close(is);
            if (found == false) {
                FileUtils.close(cacheStream);
            }
            // we have started to (over)write dest, but failed.
            // Try to delete the garbage we'd otherwise leave
            // behind.
            if (!finished) {
                dest.delete();
                if (found == false) {
                    cache.delete();
                }
            }
        }

        progress.endDownload();

        // if (and only if) the use file time option is set, then
        // the saved file now has its timestamp set to that of the
        // downloaded file
        if (useTimestamp) {
            long remoteTimestamp = connection.getLastModified();
            if (verbose) {
                Date t = new Date(remoteTimestamp);
                log("last modified = " + t.toString() + ((remoteTimestamp == 0) ? " - using current time instead" : ""), logLevel);
            }
            if (remoteTimestamp != 0) {
                FILE_UTILS.setFileLastModified(dest, remoteTimestamp);
            }
        }

        // successful download
        return true;
    }

    /**
     * validates the given timestamp vs the source timestamp
     * @param timestap
     * @param source
     * @return
     * @throws IOException 
     */
    private boolean checkTimeStamp(long timestamp, URLConnection connection) throws IOException {

        // prepare Java 1.1 style credentials
        if (uname != null || pword != null) {
            String up = uname + ":" + pword;
            String encoding;
            // we do not use the sun impl for portability,
            // and always use our own implementation for consistent
            // testing
            Base64Converter encoder = new Base64Converter();
            encoding = encoder.encode(up.getBytes());
            connection.setRequestProperty("Authorization", "Basic " + encoding);
        }

        // connect to the remote site (may take some time)
        connection.connect();
        // next test for a 304 result (HTTP only)
        if (connection instanceof HttpURLConnection) {
            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            long lastModified = httpConnection.getLastModified();
            if (httpConnection.getResponseCode() == HttpURLConnection.HTTP_NOT_MODIFIED || (lastModified != 0  && timestamp > lastModified)) {
                // not modified so no file download. just return
                // instead and trace out something so the user
                // doesn't think that the download happened when it
                // didn't
                return true;
            }
        }


        return false;
    }

    /**
     * Set the URL to get.
     * 
     * @param u
     *            URL for the file.
     */
    public void setSrc(URL u) {
        this.source = u;
    }

    /**
     * Where to copy the source file.
     * 
     * @param dest
     *            Path to file.
     */
    public void setDest(File dest) {
        this.dest = dest;
    }

    /**
     * If true, show verbose progress information.
     * 
     * @param v
     *            if "true" then be verbose
     */
    public void setVerbose(boolean v) {
        verbose = v;
    }

    /**
     * If true, log errors but do not treat as fatal.
     * 
     * @param v
     *            if "true" then don't report download errors up to ant
     */
    public void setIgnoreErrors(boolean v) {
        ignoreErrors = v;
    }

    /**
     * If true, conditionally download a file based on the timestamp of the
     * local copy.
     * 
     * <p>
     * In this situation, the if-modified-since header is set so that the file
     * is only fetched if it is newer than the local file (or there is no local
     * file) This flag is only valid on HTTP connections, it is ignored in other
     * cases. When the flag is set, the local copy of the downloaded file will
     * also have its timestamp set to the remote file time.
     * </p>
     * 
     * <p>
     * Note that remote files of date 1/1/1970 (GMT) are treated as 'no
     * timestamp', and web servers often serve files with a timestamp in the
     * future by replacing their timestamp with that of the current time. Also,
     * inter-computer clock differences can cause no end of grief.
     * </p>
     * 
     * @param v
     *            "true" to enable file time fetching
     */
    public void setUseTimestamp(boolean v) {
        useTimestamp = v;
    }

    /**
     * Username for basic auth.
     * 
     * @param u
     *            username for authentication
     */
    public void setUsername(String u) {
        this.uname = u;
    }

    /**
     * password for the basic authentication.
     * 
     * @param p
     *            password for authentication
     */
    public void setPassword(String p) {
        this.pword = p;
    }

    /***************************************************************************
     * BASE 64 encoding of a String or an array of bytes.
     * 
     * Based on RFC 1421.
     * 
     **************************************************************************/

    protected static class Base64Converter {

        public final char[] alphabet = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', // 0 to
                                                                                    // 7
                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', // 8 to 15
                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', // 16 to 23
                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', // 24 to 31
                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', // 32 to 39
                'o', 'p', 'q', 'r', 's', 't', 'u', 'v', // 40 to 47
                'w', 'x', 'y', 'z', '0', '1', '2', '3', // 48 to 55
                '4', '5', '6', '7', '8', '9', '+', '/' }; // 56 to 63

        public String encode(String s) {
            return encode(s.getBytes());
        }

        public String encode(byte[] octetString) {
            int bits24;
            int bits6;

            char[] out = new char[((octetString.length - 1) / 3 + 1) * 4];
            int outIndex = 0;
            int i = 0;

            while ((i + 3) <= octetString.length) {
                // store the octets
                bits24 = (octetString[i++] & 0xFF) << 16;
                bits24 |= (octetString[i++] & 0xFF) << 8;
                bits24 |= octetString[i++];

                bits6 = (bits24 & 0x00FC0000) >> 18;
                out[outIndex++] = alphabet[bits6];
                bits6 = (bits24 & 0x0003F000) >> 12;
                out[outIndex++] = alphabet[bits6];
                bits6 = (bits24 & 0x00000FC0) >> 6;
                out[outIndex++] = alphabet[bits6];
                bits6 = (bits24 & 0x0000003F);
                out[outIndex++] = alphabet[bits6];
            }
            if (octetString.length - i == 2) {
                // store the octets
                bits24 = (octetString[i] & 0xFF) << 16;
                bits24 |= (octetString[i + 1] & 0xFF) << 8;
                bits6 = (bits24 & 0x00FC0000) >> 18;
                out[outIndex++] = alphabet[bits6];
                bits6 = (bits24 & 0x0003F000) >> 12;
                out[outIndex++] = alphabet[bits6];
                bits6 = (bits24 & 0x00000FC0) >> 6;
                out[outIndex++] = alphabet[bits6];

                // padding
                out[outIndex++] = '=';
            } else if (octetString.length - i == 1) {
                // store the octets
                bits24 = (octetString[i] & 0xFF) << 16;
                bits6 = (bits24 & 0x00FC0000) >> 18;
                out[outIndex++] = alphabet[bits6];
                bits6 = (bits24 & 0x0003F000) >> 12;
                out[outIndex++] = alphabet[bits6];

                // padding
                out[outIndex++] = '=';
                out[outIndex++] = '=';
            }
            return new String(out);
        }
    }

    public interface DownloadProgress {
        /**
         * begin a download
         */
        public void beginDownload();

        /**
         * tick handler
         * 
         */
        public void onTick();

        /**
         * end a download
         */
        public void endDownload();
    }

    /**
     * do nothing with progress info
     */
    public static class NullProgress implements DownloadProgress {

        /**
         * begin a download
         */
        public void beginDownload() {

        }

        /**
         * tick handler
         * 
         */
        public void onTick() {
        }

        /**
         * end a download
         */
        public void endDownload() {

        }
    }

    /**
     * verbose progress system prints to some output stream
     */
    public static class VerboseProgress implements DownloadProgress {
        private int dots = 0;

        PrintStream out;

        public VerboseProgress(PrintStream out) {
            this.out = out;
        }

        /**
         * begin a download
         */
        public void beginDownload() {
            dots = 0;
        }

        /**
         * tick handler
         * 
         */
        public void onTick() {
            out.print(".");
            if (dots++ > 50) {
                out.flush();
                dots = 0;
            }
        }

        /**
         * end a download
         */
        public void endDownload() {
            out.println();
            out.flush();
        }
    }

    /**
     * is caching enabled
     * 
     * @param caching
     */
    public void setCaching(boolean caching) {
        this.caching = caching;
    }

}
