package edu.ucdavis.genomics.metabolomics.binbase.filter;

import java.io.Serializable;
import java.util.Date;

/**
 * sends out that there was a request from an ip
 * <p/>
 * User: wohlgemuth
 * Date: Jun 28, 2009
 * Time: 3:59:35 PM
 */
public class IPRequest implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String host;

    private String uri;

    private Date date;
    
    public IPRequest() {
    }

    public IPRequest(String host, String uri, String address, int port) {
        this.host = host;
        this.uri = uri;
        this.address = address;
        this.port = port;
        this.date = new Date();
    }
    public IPRequest(String host, String uri, String address, int port,Date date) {
        this.host = host;
        this.uri = uri;
        this.address = address;
        this.port = port;
        this.date = date;
    }

    private String address;

    @Override
    public String toString() {
        return "IPRequest{" +
                "host='" + host + '\'' +
                ", uri='" + uri + '\'' +
                ", address='" + address + '\'' +
                ", port=" + port +
                ", date=" + date +
                
                '}';
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    private int port;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}


}
