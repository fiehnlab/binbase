package edu.ucdavis.genomics.metabolomics.binbase.filter;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockFilterConfig;

import javax.servlet.ServletException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Properties;
import java.util.Date;

import edu.ucdavis.genomics.metabolomics.util.status.*;

public class IPLoggerTest {

    private IPLogger filter;
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockFilterChain chain;

    @Before
    public void setUp() throws Exception {
        filter = new IPLogger();
        MockFilterConfig config = new MockFilterConfig();
        config.addInitParameter("factory", TestFactory.class.getName());
        filter.init(config);
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        chain = new MockFilterChain();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testDoFilter() throws IOException, ServletException {
        request.setRequestURI("http://127.0.0.1/test.html");
        filter.doFilter(request, response, chain);


    }
}