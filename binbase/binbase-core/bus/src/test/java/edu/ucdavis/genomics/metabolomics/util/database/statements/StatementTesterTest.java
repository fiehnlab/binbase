package edu.ucdavis.genomics.metabolomics.util.database.statements;

import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.junit.Assert;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.util.database.test.BinBaseDatabaseTest;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;

/**
 * tests the statements against the database
 * 
 * @author wohlgemuth
 * 
 */
public class StatementTesterTest extends BinBaseDatabaseTest {

	protected IDataSet getDataSet() throws Exception {
		try {
			ResourceSource source = new ResourceSource("/minimum-rtx5.xml");
			assertTrue(source.exist());
			return new XmlDataSet(source.getStream());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void setUp() throws Exception {
		super.setUp();
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test(timeout = 60000)
	public void testStatements() throws FileNotFoundException, IOException,
			SQLException {
		Connection connection = this.getFactory().getConnection();

		try {
			
			StatementTester.testStatements(
					"INSERT INTO SPECTRA(uniquemass,signal_noise,purity,retention_index,sample_id,spectra_id,retention_time,apex,spectra,apex_sn,leco) VALUES(?,?,?,?,?,nextval('spectra_id'),?,?,?,?,?)",
					connection);
			StatementTester.testStatements(
					"SELECT * from bin where bin_id = ? and name = ?",
					connection);
			StatementTester.testStatements("delete from  bin", connection);
			StatementTester.testStatements(
					"insert into bin (bin_id,name) values (?,?)", connection);
			StatementTester.testStatements("select * from bin", connection);

			StatementTester
			.testStatements(
					"update bin set bin_id = ?, name = ? where spectra_id = ?",
					connection);
			try {
				StatementTester
						.testStatements(
								"update bin set bin_id = ? and name = ? where export = ?",
								connection);
				Assert.fail("was not supposed to compile!");
			} catch (SQLException e) {
			}
		} finally {
			this.getFactory().close(connection);
		}
	}
}
