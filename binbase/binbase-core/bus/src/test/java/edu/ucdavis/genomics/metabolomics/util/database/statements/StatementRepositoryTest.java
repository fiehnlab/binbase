package edu.ucdavis.genomics.metabolomics.util.database.statements;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.jdom.Element;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling;
import edu.ucdavis.genomics.metabolomics.util.io.source.ByteArraySource;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;

public class StatementRepositoryTest {

	StatementRepository loader = null;

	@Before
	public void setUp() throws Exception {
		loader = new StatementRepository();
	}

	@After
	public void tearDown() throws Exception {
		loader = null;
	}

	@Test
	public void testAddPropertySource() throws IOException {
		Properties p = new Properties();
		p.put("a", "1");
		p.put("a.b", "2");
		p.put("a.b.c.d", "3");
		p.put("a.b.c.e", "4");

		// store the date
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		p.store(out, "");
		out.flush();
		out.close();
		Source source = new ByteArraySource(out.toByteArray());

		// add source
		loader.addPropertySource(source);

		// check if the statements are right for the given keys
		assertTrue(loader.getStatement("a").equals("1"));
		assertTrue(loader.getStatement("a.b").equals("2"));
		assertTrue(loader.getStatement("a.b.c.d").equals("3"));
		assertTrue(loader.getStatement("a.b.c.e").equals("4"));

	}

	@Test
	public void testAddSource() throws Exception {
		Element a = new Element("a");
		a.setText("1");
		Element b = new Element("b");
		b.setText("2");
		Element c = new Element("c");
		Element d = new Element("d");
		d.setText("3");

		Element e = new Element("e");
		e.setText("4");

		c.addContent(e);
		c.addContent(d);
		b.addContent(c);
		a.addContent(b);

		// store content as a source
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		// write out
		XmlHandling.writeXml(out, a);
		out.flush();
		out.close();
		Source source = new ByteArraySource(out.toByteArray());

		// add source
		loader.addSource(source);

		// check if the statements are right for the given keys
		assertTrue(loader.getStatement("a").equals("1"));
		assertTrue(loader.getStatement("a.b").equals("2"));
		assertTrue(loader.getStatement("a.b.c.d").equals("3"));
		assertTrue(loader.getStatement("a.b.c.e").equals("4"));

	}

	@Test
	public void testCalculatePathToRoot() {
		Element a = new Element("a");
		a.setText("1");
		Element b = new Element("b");
		b.setText("2");
		Element c = new Element("c");
		Element d = new Element("d");
		b.setText("3");

		Element e = new Element("e");
		b.setText("4");

		c.addContent(e);
		c.addContent(d);
		b.addContent(c);
		a.addContent(b);

		// path should be a.b.c.e
		String path = loader.calculatePathToRoot(e);
		assertTrue(path.equals("a.b.c.e"));
		// path should be a.b.c.d
		path = loader.calculatePathToRoot(d);
		assertTrue(path.equals("a.b.c.d"));
		// path should be a.b.c
		path = loader.calculatePathToRoot(c);
		assertTrue(path.equals("a.b.c"));
		// path should be a.b
		path = loader.calculatePathToRoot(b);
		assertTrue(path.equals("a.b"));
		// path should be a
		path = loader.calculatePathToRoot(a);
		assertTrue(path.equals("a"));

	}

}
