package edu.ucdavis.genomics.metabolomics.util.math;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class QuickSimilarityTest {

	QuickSimilarity sim;
	
	@Before
	public void setUp() throws Exception {
		sim = new QuickSimilarity(2);
        sim.setLibrarySpectra("11:11 12:12 13:13 19:44 133:434");
        sim.setUnknownSpectra("11:11 12:12 13:13 19:44 133:434");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testReduce() {
		assertTrue(sim.getMaxIons() == 2);
		assertTrue(sim.getLibrarySpectra().length == 2);
		assertTrue(sim.getUnknownSpectra().length == 2);
		assertTrue(sim.getUnknownSpectra()[0][QuickSimilarity.FRAGMENT_REL_POSITION] == 100);		
		assertTrue(sim.getLibrarySpectra()[0][QuickSimilarity.FRAGMENT_REL_POSITION] == 100);		

	}

	@Test
	public void testCalculateSimimlarity() {
        sim.setLibrarySpectra("11:11 12:12 13:13 19:44 133:434");
        sim.setUnknownSpectra("11:11 12:12 13:13 19:44 133:434");
        assertTrue((sim.calculateSimimlarity() - 1000) < 0.0001);
	}

}
