package edu.ucdavis.genomics.metabolomics.util.collection;

import java.util.Map;

import junit.framework.TestCase;

public class CompressedMapTest extends TestCase {
	CompressedMap<String,String> map = null;
	
	protected void setUp() throws Exception {
		super.setUp();
		map = new CompressedMap<String,String>();
		
		map.put("a", "test 0");
		map.put("b", "test 1");
		map.put("c", null);
		map.put("d", "test 2");
		
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		map = null;
	}

	public void testClear() {
		map.clear();
		assertTrue(map.size() == 0);
	}

	public void testContainsKey() {
		assertTrue(map.containsKey("a"));
		assertTrue(map.containsKey("b"));
		assertTrue(map.containsKey("c"));
	}

	public void testContainsValue() {
		assertTrue(map.containsValue("test 0"));
		assertTrue(map.containsValue("test 1"));
		assertTrue(map.containsValue("test 2"));
		assertTrue(map.containsValue(null));
		
	}

	public void testGet() {
		assertTrue(map.get("a").equals("test 0"));
		assertTrue(map.get("b").equals("test 1"));
		assertTrue(map.get("c") == (null));
		assertTrue(map.get("d").equals("test 2"));
		
	}

	public void testPut() {
		assertTrue(map.put("a","test 0").equals("test 0"));
		assertTrue(map.put("b","test 1").equals("test 1"));
		assertTrue(map.put("c",null) == null);
		assertTrue(map.put("d","test 2").equals("test 2"));
	}

	public void testValues() {
		assertTrue(map.values().contains("test 0"));
		assertTrue(map.values().contains("test 1"));
		assertTrue(map.values().contains("test 2"));
		assertTrue(map.values().contains(null));
		
	}

}
