/*
 * Created on 25.05.2004
 */
package edu.ucdavis.genomics.metabolomics.util.graph.label;

import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.chart.labels.XYItemLabelGenerator;
import org.jfree.data.xy.XYDataset;

import edu.ucdavis.genomics.metabolomics.util.graph.dataset.BinBaseXYDataSet;


/**
 * @author wohlgemuth
 */
public class PeakLabelGenerator extends StandardXYToolTipGenerator implements XYItemLabelGenerator {
    /**
     * Comment for <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 2L;

    /**
     * Creates a new PeakLabelGenerator object.
     */
    public PeakLabelGenerator() {
    }

    /**
     * @see org.jfree.chart.labels.XYItemLabelGenerator#generateItemLabel(org.jfree.data.XYDataset, int, int)
     */
    public String generateItemLabel(XYDataset arg0, int arg1, int arg2) {
        double x = arg0.getX(arg1, arg2).doubleValue();
        double y = arg0.getY(arg1, arg2).doubleValue();

        if ((Math.abs(y) / (((BinBaseXYDataSet) arg0).getMaxY(arg1))) > 0.10) {
            return String.valueOf((int)x);
        } else {
            return "";
        }
    }

    /**
     * @see org.jfree.chart.labels.XYItemLabelGenerator#generateToolTip(org.jfree.data.XYDataset, int, int)
     */
    public String generateToolTip(XYDataset arg0, int arg1, int arg2) {
        return arg0.getX(arg1, arg2).toString();
    }

	public String generateLabel(XYDataset arg0, int arg1, int arg2) {
        double x = arg0.getX(arg1, arg2).doubleValue();
        double y = arg0.getY(arg1, arg2).doubleValue();

        if ((Math.abs(y) / (((BinBaseXYDataSet) arg0).getMaxY(arg1))) > 0.10) {
            return String.valueOf((int)x);
        } else {
            return "";
        }
	}
}
