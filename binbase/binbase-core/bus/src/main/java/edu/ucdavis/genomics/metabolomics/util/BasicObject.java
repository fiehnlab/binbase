package edu.ucdavis.genomics.metabolomics.util;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.util.config.Configable;
import edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfigable;


/**
 * <h3>Title: BinObject</h3>
 *
 * <p>
 * Author: Gert Wohlgemuth <br>
 * Leader: Dr. Oliver Fiehn <br>
 * Company: Max Plank Institute for molecular plant physologie <br>
 * Contact: wohlgemuth@mpimp-golm.mpg.de <br>
 * Version: <br>
 * Description: Standard Class for all BinLib objecs
 * </p>
 */
public class BasicObject implements Configable {
    /**
     * enth?lt den classname
     */
    public String CLASS = this.getClass().getName();

	
	public static final String BINBASE_MULTITHREAD = "binbase_multithread";

    /**
     * dient zum loggen von events
     */
    protected Logger logger = null;

    /**
     * standard constructor and init the logging
     */
    public BasicObject() {
        initLogging();
    }

    /**
     * @see edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.Configable#getConfig()
     */
    public final synchronized XMLConfigable getConfig() {
        return CONFIG;
    }

    /**
     * generate an exception report
     *
     * @param e
     *                    exception
     */
    public final void sendException(Exception e) {
        logger.error(e.getMessage(), e);
    }

    /**
     * initialisiert das logging
     */
    public void initLogging() {
        logger = Logger.getLogger(this.getClass().getName());
    }

    /**
     * DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public Logger getLogger() {
        return logger;
    }
    

	/**
	 * used to discover if we can run in multithread mode
	 * 
	 * @return
	 */
	public boolean isMultithreaded() {
		try {
			if (Boolean.parseBoolean(System
					.getProperty(BINBASE_MULTITHREAD))) {
				return true;
			}
		} catch (NullPointerException e) {
		} catch (Exception e) {
			logger.warn(e.getMessage(), e);
		}
		try {
			if (Boolean.parseBoolean(getConfig().getElement("export.threads")
					.toString())) {
				return true;
			}
		}

		catch (NullPointerException e) {
		} catch (Exception e) {
			logger.warn(e.getMessage(), e);
		}

		return false;
	}
}
