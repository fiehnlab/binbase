/*
 * Created on 06.04.2004
 */
package edu.ucdavis.genomics.metabolomics.util.graph.dataset;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.jfree.data.xy.AbstractXYDataset;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYDataset;


/**
 * @author wohlgemuth definiert ein einfaches datenset f?r diagramme welche auf 2 achsen aufsetzen
 */
public class BinBaseXYDataSet  extends AbstractXYDataset implements XYDataset,
    IntervalXYDataset, edu.ucdavis.genomics.metabolomics.util.graph.dataset.AbstractDataset {
    /**
     * Comment for <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 2L;

    /**
     *
     * @uml.property name="dataset"
     * @uml.associationEnd elementType="java.util.Map" qualifier="constant:java.lang.String java.lang.Double" multiplicity="(0 -1)"
     */
    List <Map>dataset = new Vector<Map>();

    /**
     * @see org.jfree.data.IntervalXYDataset#getEndXValue(int, int)
     */
    public Number getEndX(int arg0, int arg1) {
        return this.getXValue(arg0, arg1);
    }

    /**
     * @see org.jfree.data.IntervalXYDataset#getEndYValue(int, int)
     */
    public Number getEndY(int arg0, int arg1) {
        return this.getYValue(arg0, arg1);
    }

    /**
     * gibt den namen f?r dieses wert zur?ck oder schmeisst eine nullpointerexception
     *
     * @param name
     * @return
     */
    public int getIndex(String name) {
        for (int i = 0; i < this.dataset.size(); i++) {
            if (getName(i).equals(name)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * @see org.jfree.data.XYDataset#getItemCount(int)
     */
    public int getItemCount(int arg0) {
        double[] d = (double[]) this.getDataSet(arg0).get("y");

        return d.length;
    }

    /**
     * DOCUMENT ME!
     *
     * @param i DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public double getMaxY(int i) {
        try {
            return ((Double) this.dataset.get(i).get("yMax")).doubleValue();
        } catch (Exception e) {
            e.printStackTrace();

            return 0;
        }
    }

    /**
     * gibt den namen f?r dieses wert zur?ck oder schmeisst eine nullpointerexception
     *
     * @param i
     * @return
     */
    public String getName(int i) {
        return (String) this.dataset.get(i).get("name");
    }

    /**
     * @see org.jfree.data.AbstractSeriesDataset#getSeriesCount()
     */
    public int getSeriesCount() {
        return this.dataset.size();
    }

    /**
     * @see org.jfree.data.AbstractSeriesDataset#getSeriesName(int)
     */
    public String getSeriesName(int arg0) {
        return (String) this.getDataSet(arg0).get("name");
    }

    /**
     * @see org.jfree.data.IntervalXYDataset#getStartXValue(int, int)
     */
    public Number getStartX(int arg0, int arg1) {
        return this.getXValue(arg0, arg1);
    }

    /**
     * @see org.jfree.data.IntervalXYDataset#getStartYValue(int, int)
     */
    public Number getStartY(int arg0, int arg1) {
        return this.getYValue(arg0, arg1);
    }

    /**
     * DOCUMENT ME!
     *
     * @param i DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public double[] getX(int i) {
        return (double[]) this.dataset.get(i).get("x");
    }

    /**
     * @see org.jfree.data.XYDataset#getXValue(int, int)
     */
    public Number getX(int arg0, int arg1) {
        double[] d = (double[]) this.getDataSet(arg0).get("x");

        return new Double((d[arg1]));
    }

    /**
     * DOCUMENT ME!
     *
     * @param i DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    public double[] getY(int i) {
        return (double[]) this.dataset.get(i).get("y");
    }

    /**
     * @see org.jfree.data.XYDataset#getYValue(int, int)
     */
    public Number getY(int arg0, int arg1) {
        double[] d = (double[]) this.getDataSet(arg0).get("y");

        return new Double((d[arg1]));
    }

    /**
     * f?hrt einen neuen eintrag zum datenset hinzu oder ersetzt ein datenset falls dieser name schon existiert
     *
     * @param x
     * @param y
     * @param name
     */
    public void addDataSet(double[] x, double[] y, String name) {
        this.addDataSet(x, y, name, true);
    }

    /**
     * DOCUMENT ME!
     *
     * @param x DOCUMENT ME!
     * @param y DOCUMENT ME!
     * @param name DOCUMENT ME!
     * @param fireEvent DOCUMENT ME!
     */
    public void addDataSet(double[] x, double[] y, String name,
        boolean fireEvent) {
        if (x.length == y.length) {
            Map <String,Object>map = new HashMap<String,Object>();
            map.put("x", x);
            map.put("y", y);
            map.put("name", name);

            double xMax = 0;
            double yMax = 0;
            double xMin = 0;
            double yMin = 0;

            for (int i = 0; i < x.length; i++) {
                if (i == 0) {
                    xMax = x[i];
                    yMax = y[i];
                    xMin = x[i];
                    yMin = y[i];
                } else {
                    if (x[i] < xMin) {
                        xMin = x[i];
                    }

                    if (y[i] < yMin) {
                        yMin = y[i];
                    }

                    if (x[i] > xMax) {
                        xMax = x[i];
                    }

                    if (y[i] > yMax) {
                        yMax = y[i];
                    }
                }
            }

            map.put("xMin", new Double(xMin));
            map.put("yMin", new Double(yMin));
            map.put("xMax", new Double(xMax));
            map.put("yMax", new Double(yMax));

            int i = 0;

            if ((i = this.dataset.indexOf(map)) > -1) {
                this.dataset.set(i, map);
            } else {
                this.dataset.add(map);
            }
        } else {
            throw new RuntimeException("array must have the same size");
        }

        if (fireEvent) {
            this.fireDatasetChanged();
        }
    }

    /**
     * l?scht das gesammte datenset
     *
     */
    public void clear() {
        this.dataset.clear();
        this.fireDatasetChanged();
    }

    /**
     * l?scht die daten an diesem index
     *
     */
    public void clear(int index) {
        try {
            this.dataset.remove(index);
            this.fireDatasetChanged();
        } catch (Exception e) {
        }
    }

    /**
     * DOCUMENT ME!
     */
    public void refresh() {
        List list = new Vector();

        for (int i = 0; i < dataset.size(); i++) {
            list.add(dataset.get(i));
        }

        this.clear();

        for (int i = 0; i < list.size(); i++) {
            Map map = (Map) list.get(i);
            String name = (String) map.get("name");
            double[] x = (double[]) map.get("x");
            double[] y = (double[]) map.get("y");
            this.addDataSet(x, y, name, false);
        }

        this.fireDatasetChanged();
    }

    /**
     * DOCUMENT ME!
     *
     * @param x DOCUMENT ME!
     * @param y DOCUMENT ME!
     * @param name DOCUMENT ME!
     */
    public void replaceDataSet(double[] x, double[] y, String name) {
        this.replaceDataSet(x, y, name, true);
    }

    /**
     * DOCUMENT ME!
     *
     * @param x DOCUMENT ME!
     * @param y DOCUMENT ME!
     * @param name DOCUMENT ME!
     * @param fireEvent DOCUMENT ME!
     */
    public void replaceDataSet(double[] x, double[] y, String name,
        boolean fireEvent) {
        if (x.length == y.length) {
            Map map = this.dataset.get(this.getIndex(name));
            map.put("x", x);
            map.put("y", y);
            map.put("name", name);

            double xMax = 0;
            double yMax = 0;
            double xMin = 0;
            double yMin = 0;

            for (int i = 0; i < x.length; i++) {
                if (i == 0) {
                    xMax = x[i];
                    yMax = y[i];
                    xMin = x[i];
                    yMin = y[i];
                } else {
                    if (x[i] < xMin) {
                        xMin = x[i];
                    }

                    if (y[i] < yMin) {
                        yMin = y[i];
                    }

                    if (x[i] > xMax) {
                        xMax = x[i];
                    }

                    if (y[i] > yMin) {
                        yMin = y[i];
                    }
                }
            }

            map.put("xMin", new Double(xMin));
            map.put("yMin", new Double(yMin));
            map.put("xMax", new Double(xMax));
            map.put("yMax", new Double(yMax));

            this.dataset.set(this.getIndex(name), map);
        } else {
            throw new RuntimeException("array must have the same size");
        }

        if (fireEvent) {
            this.fireDatasetChanged();
        }
    }

    /*
     * @see edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.util.graph.dataset.AbstractDataset#update()
     */
    public void update() {
        this.fireDatasetChanged();
    }

    /**
     * DOCUMENT ME!
     *
     * @param i DOCUMENT ME!
     *
     * @return DOCUMENT ME!
     */
    private Map getDataSet(int i) {
        try {
            Map map = this.dataset.get(i);

            if (map == null) {
                return new HashMap();
            }

            return map;
        } catch (Exception e) {
            return new HashMap();
        }
    }

	public double getXValue(int arg0, int arg1) {
		return getX(arg0,arg1).doubleValue();
	}

	public double getYValue(int arg0, int arg1) {
		return getY(arg0,arg1).doubleValue();
	}

	public double getStartXValue(int arg0, int arg1) {
		return getStartX(arg0,arg1).doubleValue();
	}

	public double getEndXValue(int arg0, int arg1) {
		return getEndX(arg0,arg1).doubleValue();
	}

	public double getStartYValue(int arg0, int arg1) {
		return getStartY(arg0,arg1).doubleValue();
	}

	public double getEndYValue(int arg0, int arg1) {
		return getEndY(arg0,arg1).doubleValue();
	}

	@Override
	public Comparable getSeriesKey(int arg0) {
		return arg0;
	}
}
