/*
 * Created on 25.04.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.util.thread;


/**
 * @author wohlgemuth
 * dient zum behandeln von ereignissen welche durch threads, welche das interface threadable ausgel?st werden k?nnen
 * sollte eigentlich keinen interessieren, ausser er will eine eigene threadrunner klasse schreiben
 *
 */
public interface ThreadListener {
    /**
     * das ergebniss der threads
     * @param o
     */
    void setResult(Object o);

    /**
     * wird aufgerufen wenn der thread gestarteted wurde
     *
     */
    void startThread();

    /**
     * wird ausgel?st wenn ein thread beendet wurde
     */
    void threadFinshed();
}
