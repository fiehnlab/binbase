package edu.ucdavis.genomics.metabolomics.util.collection;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * a hasmap which has some keys backed up to the harddrive to save memory
 * 
 * @author wohlgemuth
 * 
 * @param <K>
 * @param <V>
 */
public class HDStoredMap<K, V> extends HashMap<K, V> {

	private Collection<K> hdstoredKeys;

	private Map<K, File> localReferences = new ConcurrentHashMap<K, File>();

	//only one executor implementation for all stored map references. We don't want millions over millions of threads, if we have a lot of map references
	private static ExecutorService service = Executors.newCachedThreadPool();

	public HDStoredMap(Collection<K> hdstoredKeys) {
		this.hdstoredKeys = hdstoredKeys;
	}

	@Override
	public V get(Object arg0) {
		if (hdstoredKeys.contains(arg0)) {

			return readContent(arg0);
		}
		return super.get(arg0);
	}

	@Override
	public V put(K arg0, V arg1) {
		if (hdstoredKeys.contains(arg0)) {

			return writeContent(arg0, arg1);
		}
		return super.put(arg0, arg1);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * writes the content to a temp directory
	 * 
	 * @param arg0
	 * @param arg1
	 */
	protected V writeContent(final K arg0, final V arg1) {

		/**
		 * we want it to write in the background
		 */
		Runnable run = new Runnable() {

			@Override
			public void run() {

				// should be send to a thread to safe time
				try {
					File file = File.createTempFile(arg0.toString(), ".entry");
					file.deleteOnExit();

					FileOutputStream fout = new FileOutputStream(file);
					ObjectOutputStream oout = new ObjectOutputStream(fout);
					oout.writeObject(arg1);
					oout.flush();
					fout.flush();
					oout.close();
					fout.close();
					localReferences.put(arg0, file);

				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		};

		service.submit(run);
		return arg1;
	}

	/**
	 * reads the content again
	 * 
	 * @param arg0
	 * @return
	 */
	protected V readContent(Object arg0) {
		try {
			FileInputStream fin = new FileInputStream(localReferences.get(arg0));
			ObjectInputStream oin = new ObjectInputStream(fin);

			@SuppressWarnings("unchecked")
			V result = (V) oin.readObject();

			oin.close();
			fin.close();
			return result;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
