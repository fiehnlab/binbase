/*
 * Created on Feb 13, 2007
 */
package edu.ucdavis.genomics.metabolomics.util.statistics.postprocessing;

import java.util.List;

import org.jdom.Element;

import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.FormatObject;
/**
 * @deprecated
 * @author wohlgemuth
 *
 */
public interface PostProcessing {
	
	/**
	 * reads teh configuration out of the given element
	 * @author wohlgemuth
	 * @version Feb 12, 2007
	 * @param element
	 */
	public boolean configure(Element element);
	
	/**
	 * takes a given list of elements and postprocesses them
	 * 
	 * @author wohlgemuth
	 * @version Feb 12, 2007
	 * @param data
	 * @return
	 */
	public List postProcess(List<FormatObject<Double>> data);
}
