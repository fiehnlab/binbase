/*
 * Created on 15.04.2004
 */
package edu.ucdavis.genomics.metabolomics.binbase.gui.swing.graph.plot;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.event.ChartChangeEvent;
import org.jfree.chart.event.ChartChangeListener;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;

import edu.ucdavis.genomics.metabolomics.util.graph.dataset.BinBaseXYDataSet;

/**
 * @author wohlgemuth erstellt einen chart welcher die werte der analyse zur?ck
 *         gibt
 */
public final class ChrommatogrammChart {
	/**
	 * Creates a new ChrommatogrammChart object.
	 */
	private ChrommatogrammChart() {
	}

	/**
	 * erstellt den chart und gibt ihn zur?ck
	 * 
	 * @return
	 */
	public static JFreeChart createChart(BinBaseXYDataSet data) {
		XYBarRenderer rendererShift = new XYBarRenderer(0.5);

		NumberAxis x = new NumberAxis("retention index");
		ValueAxis y = new NumberAxis("intensity");

		XYPlot plot = new XYPlot(data, x, y, rendererShift);
		JFreeChart chart = new JFreeChart("sample compare chart",plot);

		return chart;
	}

	/**
	 * erstellt den chart und gibt ihn zur?ck
	 * 
	 * @return
	 */
	public static JFreeChart createChart(BinBaseXYDataSet data, String key) {
		XYBarRenderer rendererShift = new XYBarRenderer(0.5);

		NumberAxis x = new NumberAxis("retention index");
		x.setAutoRangeIncludesZero(false);

		ValueAxis y = new NumberAxis(key);
		XYPlot plot = new XYPlot(data, x, y, rendererShift);
		JFreeChart chart = new JFreeChart("sample compare chart",
				plot);

		return chart;
	}
	

	/**
	 * erstellt den chart und gibt ihn zur?ck
	 * 
	 * @return
	 */
	public static JFreeChart createLineChart(BinBaseXYDataSet data) {
		XYLineAndShapeRenderer rendererShift = new XYLineAndShapeRenderer(true,false);

		NumberAxis x = new NumberAxis("retention time");
		x.setAutoRangeIncludesZero(false);

		ValueAxis y = new NumberAxis("intensity");
		XYPlot plot = new XYPlot(data, x, y, rendererShift);
		
		JFreeChart chart = new JFreeChart("sample compare chart",
				plot);

		chart.setAntiAlias(false);
		
		return chart;
	}
}
