/*
 * Created on Sep 30, 2003
 */
package edu.ucdavis.genomics.metabolomics.util.database;

import java.io.IOException;
import java.io.OutputStream;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;


/**
 * @author wohlgemuth
 * @version Sep 18, 2003
 * <br>
 * BinBaseDatabase
 * @description
 */
public class StatementAnalyse {
    /**
     * DOCUMENT ME!
     *
     * @param result DOCUMENT ME!
     */
    public static void analyseResult(ResultSet result) {
        analyseResult(result, System.out);
    }

    /**
     * DOCUMENT ME!
     *
     * @param result DOCUMENT ME!
     * @param stream DOCUMENT ME!
     */
    public static void analyseResult(ResultSet result, OutputStream stream) {
        try {
            ResultSetMetaData meta = result.getMetaData();

            int columns = meta.getColumnCount();

            try {
                stream.write(("column names and table\n").getBytes());

                for (int i = 1; i <= columns; i++) {
                    stream.write(("column\n").getBytes());

                    stream.write(("\tname: ").getBytes());
                    stream.write((meta.getColumnName(i)).getBytes());
                    stream.write(("\n").getBytes());

                    stream.write(("\tlabel: ").getBytes());
                    stream.write((meta.getColumnLabel(i)).getBytes());
                    stream.write(("\n").getBytes());

                    stream.write(("\ttype: ").getBytes());
                    stream.write((meta.getColumnTypeName(i)).getBytes());
                    stream.write(("\n").getBytes());

                    stream.write(("\ttable: ").getBytes());
                    stream.write((meta.getTableName(i)).getBytes());
                    stream.write(("\n").getBytes());
                    stream.write(("\n").getBytes());
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
