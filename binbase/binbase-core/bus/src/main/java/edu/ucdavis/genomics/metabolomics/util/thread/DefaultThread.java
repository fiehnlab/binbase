/*
 * Created on 28.04.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.util.thread;


/**
 * @author wohlgemuth
 */
public abstract class DefaultThread implements Threadable {
    /**
     *
     * @uml.property name="listener"
     * @uml.associationEnd multiplicity="(0 1)"
     */
    ThreadListener listener;

    /**
     *
     * @uml.property name="listener"
     */

    /*
     * @see edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.thread.Threadable#setListener(edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.thread.ThreadListener)
     */
    public void setListener(ThreadListener listener) {
        this.listener = listener;
    }

    /*
     * @see java.lang.Runnable#run()
     */
    public final void run() {
        if (listener == null) {
            throw new RuntimeException("please set a listener!");
        }

        listener.startThread();
        listener.setResult(this.runThread());
        listener.threadFinshed();
    }

    /**
     * f?hrt die berechnung als solches aus
     *
     */
    public abstract Object runThread();
}
