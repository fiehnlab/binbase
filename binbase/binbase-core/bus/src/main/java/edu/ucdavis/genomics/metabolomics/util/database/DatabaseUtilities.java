package edu.ucdavis.genomics.metabolomics.util.database;

import edu.ucdavis.genomics.metabolomics.exception.NotSupportedException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * <p>
 * Title:
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Copyright: Copyright (c) 2002
 * </p>
 * <p>
 * Company: mpimp-golm
 * </p>
 * 
 * @author unascribed
 * @version 1.0
 */
public class DatabaseUtilities {

	private static Logger logger = Logger.getLogger(DatabaseUtilities.class);

	/**
	 * returns a resultset
	 * 
	 * @param connection
	 *            Connection to database
	 * @param query
	 *            Query
	 * @return ResultSet
	 * @throws Exception
	 *             if something does wrong
	 */
	public static ResultSet getQueryResult(Connection connection, String query)
			throws Exception {
		return connection.createStatement().executeQuery(query);
	}

	/**
	 * connect to the datbase
	 * 
	 * @param driver
	 *            Database Driver
	 * @param url
	 *            Datbase url
	 * @param username
	 *            Username
	 * @param password
	 *            Password
	 * @return Connection object
	 * @throws Exception
	 *             if something goes wrong
	 */
	public static Connection createConnection(String driver, String url,
			String username, String password) throws Exception {
		/*
		 * System.out.println("driver: " + driver); System.out.println("url: " +
		 * url); System.out.println("username: " + username);
		 * System.out.println("password: " + password);
		 */
		logger.debug("loading driver: " + driver);
		Class.forName(driver);
		logger.debug("using url: " + url);
		logger.debug("using user: " + username);
		logger.debug("using password: " + password);

		Connection c = DriverManager.getConnection(url, username, password);
		return c;
	}

	/**
	 * erstellt eine neue verbindung unter angabe der herstelle id
	 * 
	 * @param vendor
	 * @param username
	 * @param password
	 * @param host
	 * @param database
	 * @return
	 * @throws Exception
	 */
	public static Connection createConnection(int vendor, String username,
			String password, String host, String database) throws Exception {
		if (vendor == DriverUtilities.ORACLE) {
			return createConnection(DriverUtilities.ORACLE_DRIVER,
					DriverUtilities.generateUrl(host, database, vendor),
					username, password);
		} else if (vendor == DriverUtilities.POSTGRES) {
			return createConnection(DriverUtilities.POSTGRES_DRIVER,
					DriverUtilities.generateUrl(host, database, vendor),
					username, password);
		} else {
			throw new NotSupportedException("unknown vendor, so not supported "
					+ vendor);
		}
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param connection
	 *            DOCUMENT ME!
	 * @param query
	 *            DOCUMENT ME!
	 * 
	 * @return DOCUMENT ME!
	 * 
	 * @throws Exception
	 *             DOCUMENT ME!
	 */
	public static boolean execCommand(Connection connection, String query)
			throws Exception {
		logger.debug("executing...");
		logger.debug(query);
		try {

            if(connection.getAutoCommit()){
                logger.warn("connection is in auto commit mode => disable it");
                connection.setAutoCommit(false);
            }

            logger.debug("starting transaction...");
			connection.commit();
			logger.debug("execute statement..");
			PreparedStatement state = connection.prepareStatement(query);

			boolean result = state.execute();
			logger.debug("closing statement..");

			state.close();
			logger.debug("committing transaction..");

			connection.commit();
			return result;
		} catch (Exception e) {
			logger.warn(e.getMessage());
			connection.rollback();
			throw e;
		}
	}
}
