/*
 * Created on 06.04.2004
 */
package edu.ucdavis.genomics.metabolomics.util.graph.dataset;

import com.keypoint.PngEncoder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;

import java.awt.BorderLayout;
import java.awt.image.BufferedImage;

import java.io.FileOutputStream;
import java.io.OutputStream;

import javax.swing.JFrame;


/**
 * @author wohlgemuth
 */
public class SimpleGraphRenderer {
    /**
     * DOCUMENT ME!
     *
     * @param args DOCUMENT ME!
     *
     * @throws Exception DOCUMENT ME!
     */
    public static void main(String[] args) throws Exception {
        showXY();
    }

    /**
     * DOCUMENT ME!
     *
     * @param chart DOCUMENT ME!
     *
     * @throws Exception DOCUMENT ME!
     */
    public static void show(JFreeChart chart) throws Exception {
        JFrame frame = new JFrame("test");
        ChartPanel panel = new ChartPanel(chart);
        //panel.setHorizontalZoom(true);
        //panel.setVerticalZoom(true);

        frame.getContentPane().add(panel, BorderLayout.CENTER);
        frame.setSize(800, 600);
        frame.setVisible(true);

        BufferedImage image = chart.createBufferedImage(800, 600);

        OutputStream out = new FileOutputStream(chart.getClass().getName() +
                ".jpg");
        PngEncoder encoder = new PngEncoder(image);
        out.write(encoder.pngEncode());

        out.flush();
        out.close();
    }

    /**
     * DOCUMENT ME!
     *
     * @throws Exception DOCUMENT ME!
     */
    public static void showXY() throws Exception {
        BinBaseXYDataSet dataset = new BinBaseXYDataSet();
        double[] x = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
        double[] y = { 9, 8, 7, 6, 5, 6, 7, 8, 9 };

        dataset.addDataSet(x, x, "test");
        dataset.addDataSet(x, y, "test2");

        JFreeChart chart = ChartFactory.createXYLineChart("test", "x", "y",
                dataset, PlotOrientation.HORIZONTAL, true, true, false);
        show(chart);
    }
}
