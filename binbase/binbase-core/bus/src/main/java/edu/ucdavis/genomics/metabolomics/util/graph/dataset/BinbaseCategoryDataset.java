/*
 * Created on 27.04.2004
 *
 */
package edu.ucdavis.genomics.metabolomics.util.graph.dataset;

import java.util.List;

import org.jfree.data.DefaultKeyedValues2D;
import org.jfree.data.category.DefaultCategoryDataset;


/**
 * @author wohlgemuth
 *
 */
public class BinbaseCategoryDataset extends DefaultCategoryDataset
    implements AbstractDataset {
    /**
     * Comment for <code>serialVersionUID</code>
     */
    private static final long serialVersionUID = 2L;

    /**
     * DOCUMENT ME!
     *
     * @uml.property name="data"
     * @uml.associationEnd multiplicity="(1 1)"
     */
    private DefaultKeyedValues2D data;

    /**
     *
     */
    public BinbaseCategoryDataset() {
        super();
        this.data = new DefaultKeyedValues2D();
    }

    /**
     * @return
     */
    public int getColumnCount() {
        return data.getColumnCount();
    }

    /**
     * @param arg0
     * @return
     */
    public int getColumnIndex(Comparable arg0) {
        return data.getColumnIndex(arg0);
    }

    /**
     * @param arg0
     * @return
     */
    public Comparable getColumnKey(int arg0) {
        return data.getColumnKey(arg0);
    }

    /**
     * @return
     */
    public List getColumnKeys() {
        return data.getColumnKeys();
    }

    /**
     * @return
     */
    public int getRowCount() {
        return data.getRowCount();
    }

    /**
     * @param arg0
     * @return
     */
    public int getRowIndex(Comparable arg0) {
        return data.getRowIndex(arg0);
    }

    /**
     * @param arg0
     * @return
     */
    public Comparable getRowKey(int arg0) {
        return data.getRowKey(arg0);
    }

    /**
     * @return
     */
    public List getRowKeys() {
        return data.getRowKeys();
    }

    /**
     * @param arg0
     * @param arg1
     * @param arg2
     */
    public void setValue(Number arg0, Comparable arg1, Comparable arg2) {
        data.setValue(arg0, arg1, arg2);
    }

    /**
     * @param d
     * @param string
     * @param string2
     */
    public void setValue(double d, String string, String string2) {
        this.setValue(new Double(d), string, string2);
    }

    /**
     * @param arg0
     * @param arg1
     * @return
     */
    public Number getValue(int arg0, int arg1) {
        return data.getValue(arg0, arg1);
    }

    /**
     * @param arg0
     * @param arg1
     * @return
     */
    public Number getValue(Comparable arg0, Comparable arg1) {
        return data.getValue(arg0, arg1);
    }

    /**
     * @param arg0
     * @param arg1
     * @param arg2
     */
    public void addValue(Number arg0, Comparable arg1, Comparable arg2) {
        data.addValue(arg0, arg1, arg2);
        this.update();
    }

    /**
     * @param d
     * @param string
     * @param string2
     */
    public void addValue(double d, String string, String string2) {
        this.addValue(new Double(d), string, string2);
    }

    /*
     * @see edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.util.graph.dataset.AbstractDataset#clear()
     */
    public void clear() {
        this.data = new DefaultKeyedValues2D();
    }

    /*
     * @see java.lang.Object#equals(java.lang.Object)
     */
    public boolean equals(Object arg0) {
        return data.equals(arg0);
    }

    /*
     * @see java.lang.Object#hashCode()
     */
    public int hashCode() {
        return data.hashCode();
    }

    /*
     * @see edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.util.graph.dataset.AbstractDataset#refresh()
     */
    public void refresh() {
    }

    /**
     * @param arg0
     */
    public void removeColumn(int arg0) {
        data.removeColumn(arg0);
    }

    /**
     * @param arg0
     */
    public void removeColumn(Comparable arg0) {
        data.removeColumn(arg0);
    }

    /**
     * @param arg0
     */
    public void removeRow(int arg0) {
        data.removeRow(arg0);
    }

    /**
     * @param arg0
     */
    public void removeRow(Comparable arg0) {
        data.removeRow(arg0);
    }

    /**
     * @param arg0
     * @param arg1
     */
    public void removeValue(Comparable arg0, Comparable arg1) {
        data.removeValue(arg0, arg1);
    }

    /*
     * @see java.lang.Object#toString()
     */
    public String toString() {
        return data.toString();
    }

    /*
     * @see edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.util.graph.dataset.AbstractDataset#update()
     */
    public void update() {
        //TODO this.fireDatasetChanged();
    }
}
