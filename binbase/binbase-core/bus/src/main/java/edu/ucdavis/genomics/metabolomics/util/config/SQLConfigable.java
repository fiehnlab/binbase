/**
 * Created on Jul 31, 2003
 */
package edu.ucdavis.genomics.metabolomics.util.config;

import edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfigable;


/**
 * <p>
 * @author wohlgemuth
 * @version Jul 31, 2003
 * </p>
 *
 * <h4>File: SQLConfigable.java </h4>
 * <h4>Project: BinBaseDatabase </h4>
 * <h4>Package: edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.util.transform.abstracthandler.algorythm.binlib </h4>
 * <h4>Type: SQLConfigable </h4>
 */
public interface SQLConfigable {
    /**
     * DOCUMENT ME!
     *
     * @uml.property name="sQL_CONFIG"
     * @uml.associationEnd javaType="XMLConfigable" multiplicity="(0 1)"
     */
    XMLConfigable SQL_CONFIG = XMLConfigurator.getInstance().getXMLConfigable(
            "binbase.sql");
}
