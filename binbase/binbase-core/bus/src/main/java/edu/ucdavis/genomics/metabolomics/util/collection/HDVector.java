package edu.ucdavis.genomics.metabolomics.util.collection;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.AbstractList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.Vector;

import org.apache.log4j.Logger;

/**
 * an implementation of an abstract list to store the content of a vector on the
 * hardrive we work on a basis of hashcodes
 * 
 * @author wohlgemuth
 * 
 */
@SuppressWarnings("unchecked")
public class HDVector extends AbstractList {
	int counter = 0;

	@Override
	protected void finalize() throws Throwable {
		this.clear();
		cleanup = null;
		super.finalize();
	}

	private Cleanup cleanup;

	@Override
	public Object remove(int index) {
		Integer key = keys.get(index);
		Object content = loadObject(key);
		refrences.remove(key);

		keys.remove(index);

		return content;
	}

	@Override
	public Object set(int index, Object element) {
		return this.storeObject(element, index);
	}

	/**
	 * if more than CACHE_HIT memory is taken we don't use the cache anymore in %
	 * of max cache like: 20
	 */
	public static double CACHE_HIT = 40;

	/**
	 * if max % cache is used we start to clean the used cache
	 */
	public static double MAX_CACHE = 25;

	/**
	 * we check all X milliseconds if we need to clean the cache
	 */
	public static long CLEANUP_TIME = 1500;

	/**
	 * contains all our keys
	 */
	private List<Integer> keys = new Vector<Integer>();

	/**
	 * internal ids linked to keys of the files on the hd
	 */
	private Map<Integer, String> refrences = new HashMap<Integer, String>();

	/**
	 * internal cache of our objects
	 */
	private Map<Integer, Object> cache = new HashMap<Integer, Object>();

	/**
	 * used to monitor the age of object and delete the oldest
	 */
	private Stack<Integer> states = new Stack<Integer>();

	private Logger logger = Logger.getLogger(getClass());

	private Runtime runtime = Runtime.getRuntime();

	public HDVector() {
		this.cleanup = new Cleanup();
	}

	private double calculateMemory() {
		long maxMemory = runtime.maxMemory();
		long allocatedMemory = runtime.totalMemory();
		long freeMemory = runtime.freeMemory();

		return ((double) (freeMemory + (maxMemory - allocatedMemory)))
				/ (double) maxMemory * 100.0;
	}

	/**
	 * used to clean the cache and removes vaules if its get to full
	 * 
	 * @author wohlgemuth
	 * 
	 */
	private class Cleanup implements Runnable {
		private Logger logger = Logger.getLogger(getClass());

		private Thread thread;

		private boolean running = true;

		public Cleanup() {
		}

		public void run() {
			while (running) {

				double free = calculateMemory();
				logger.debug("check for free cache " + free + " - cache size: "
						+ cache.size() + " collection size: "
						+ refrences.size());
				if (free < MAX_CACHE) {
					// logger.info("need clean up");
					if (cache.isEmpty() == false) {
						// logger.info("free up cache");

						while (calculateMemory() < CACHE_HIT) {
							if (states.isEmpty() == true) {
								break;
							}
							cache.remove(states.pop());
							System.gc();
						}

					}

				}

				try {
					Thread.sleep(CLEANUP_TIME);
				} catch (InterruptedException e) {
					logger.warn(e.getMessage(), e);
				}
			}
		}

		public boolean isRunning() {
			return running;
		}

		public void setRunning(boolean running) {
			if (!this.running) {
				thread = new Thread(this);
				thread.start();
			}
			this.running = running;
		}

	}

	@Override
	public Object get(int index) {
		// logger.info("using index: " + index + " relating to key: "+
		// keys.get(index));
		return loadObject(keys.get(index));
	}

	@Override
	public int size() {
		return refrences.size();
	}

	@Override
	public synchronized boolean add(Object o) {
		cleanup.setRunning(true);
		// logger.info("add object");
		File result = storeObject(o);

		if (result == null) {
			// logger.info("failed");
			return false;
		} else {
			return true;
		}
	}

	/**
	 * loads an object fromt he harddrive
	 * 
	 * @param key
	 * @return
	 */
	private Object loadObject(Integer key) {
		ObjectInputStream input;
		try {
			if (refrences.isEmpty()) {
				return null;
			}

			if (cache.containsKey(key)) {
				// logger.info("access cache");
				return cache.get(key);
			} else {
				// logger.info("access harddrive");
			}
			input = new ObjectInputStream(new BufferedInputStream(
					new FileInputStream(refrences.get(key))));
			Object o = input.readObject();
			input.close();
			input = null;

			cacheObject(o, key);
			return o;
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}

		return null;

	}

	/**
	 * stores the given object at the end of the internal representation
	 * 
	 * @param o
	 * @return
	 */
	private File storeObject(Object o) {

		try {
			File file = writeOut(o);

			keys.add(counter);
			refrences.put(counter, file.getAbsolutePath());
			cacheObject(o, counter);
			counter++;
			return file;
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	/**
	 * writes the object to the harddrive
	 * 
	 * @param o
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private File writeOut(Object o) throws IOException, FileNotFoundException {
		File file = File.createTempFile("binbase", ".vector");
		file.deleteOnExit();
		ObjectOutputStream out = new ObjectOutputStream(
				new BufferedOutputStream(new FileOutputStream(file)));
		out.writeObject(o);
		out.flush();
		out.close();
		out = null;
		return file;
	}

	/**
	 * stores the object at a given position
	 * 
	 * @param o
	 * @param index
	 * @return
	 */
	private File storeObject(Object o, int index) {
		try {
			File file = writeOut(o);
			refrences.put(keys.get(index), file.getAbsolutePath());
			cacheObject(o, keys.get(index));

			return file;
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	/**
	 * caches an object for faster access
	 * 
	 * @param o
	 * @param file
	 */
	private void cacheObject(Object o, int key) {
		double free = calculateMemory();
		if (free > CACHE_HIT) {
			// logger.info("use cache");
			states.push(key);
			cache.put(key, o);
		} else {
			// logger.info("use no cache");
		}
	}

	@Override
	public void clear() {
		counter = 0;
		this.cache.clear();
		this.refrences.clear();
		this.states.clear();
		cleanup.setRunning(false);
		super.clear();
	}
}
