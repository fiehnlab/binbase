package edu.ucdavis.genomics.metabolomics.util.database;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.ForwardOnlyResultSetTableFactory;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.ITableIterator;
import org.dbunit.dataset.ITableMetaData;
import org.dbunit.dataset.stream.IDataSetProducer;
import org.dbunit.dataset.stream.StreamingDataSet;
import org.dbunit.dataset.xml.XmlProducer;
import org.dbunit.operation.DatabaseOperation;
import org.xml.sax.InputSource;

import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;

/**
 * imports our data into a database
 * 
 * @author wohlgemuth
 */
public class ImportToDatabase {
	private Logger logger = Logger.getLogger(getClass());

	/**
	 * deletes old data from tables without throwing exceptions if a table does
	 * not exist
	 * 
	 * @param in
	 * @throws SQLException
	 * @throws DataSetException
	 */
	public void forceDeleteOldData(InputStream in) throws SQLException, DataSetException {
		logger.info("importing data...");
		ConnectionFactory factory = ConnectionFactory.getFactory();
		factory.setProperties(System.getProperties());

		logger.info("create connection...");
		Connection connection = factory.getConnection();
		try {
			IDataSetProducer producer = new XmlProducer(new InputSource(in));
			IDataSet dataSet = new StreamingDataSet(producer);
			IDatabaseConnection dbunit = new DatabaseConnection(connection, System.getProperty("Binbase.user").toUpperCase());

			dbunit.getConfig().setProperty(DatabaseConfig.PROPERTY_RESULTSET_TABLE_FACTORY, new ForwardOnlyResultSetTableFactory());
			dbunit.getConfig().setProperty(DatabaseConfig.PROPERTY_ESCAPE_PATTERN, "\"?\"");

			logger.info("delete old data");
			ITableIterator it = dataSet.iterator();
			while (it.next()) {
				ITable table = it.getTable();
				ITableMetaData meta = table.getTableMetaData();
				String name = meta.getTableName();
				try {
					logger.info("deleting from: " + name);
					connection.createStatement().execute("delete from " + name);
				}
				catch (Exception e) {
					logger.warn(e.getMessage());
				}
			}
		}
		finally {
			factory.close(connection);
		}
	}

	/**
	 * inserts data in existing tables
	 * 
	 * @param in
	 * @throws SQLException
	 * @throws DatabaseUnitException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void importData(InputStream in) throws SQLException, DatabaseUnitException, ClassNotFoundException, IOException {
		logger.info("importing data...");
		ConnectionFactory factory = ConnectionFactory.getFactory();
		factory.setProperties(System.getProperties());

		logger.info("create connection...");
		Connection connection = factory.getConnection();
		try {
			importData(in, connection);
		}
		finally {
			factory.close(connection);
		}
	}

	public void importData(InputStream in, Connection connection) throws SQLException, DatabaseUnitException, ClassNotFoundException, IOException {
		logger.info("importing data...");

		logger.info("create connection...");
		IDataSetProducer producer = new XmlProducer(new InputSource(in));
		IDataSet dataSet = new StreamingDataSet(producer);
		IDatabaseConnection dbunit = new DatabaseConnection(connection, System.getProperty("Binbase.user").toUpperCase());

		dbunit.getConfig().setProperty(DatabaseConfig.PROPERTY_RESULTSET_TABLE_FACTORY, new ForwardOnlyResultSetTableFactory());
		dbunit.getConfig().setProperty(DatabaseConfig.PROPERTY_ESCAPE_PATTERN, "\"?\"");

		logger.info("insert data...");
		DatabaseOperation.INSERT.execute(dbunit, dataSet);
	}

	/**
	 * throws an exception in case something goes wrong
	 * 
	 * @param in
	 * @throws SQLException
	 * @throws DatabaseUnitException
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void deleteOldData(InputStream in) throws SQLException, DatabaseUnitException, ClassNotFoundException, IOException {
		logger.info("importing data...");
		ConnectionFactory factory = ConnectionFactory.getFactory();
		factory.setProperties(System.getProperties());

		logger.info("create connection...");
		Connection connection = factory.getConnection();
		try {
			deleteOldData(in, connection);
		}
		finally {
			factory.close(connection);
		}
	}
	
	public void deleteOldData(InputStream in,Connection connection) throws SQLException, DatabaseUnitException, ClassNotFoundException, IOException {
		logger.info("importing data...");
			IDataSetProducer producer = new XmlProducer(new InputSource(in));
			IDataSet dataSet = new StreamingDataSet(producer);
			IDatabaseConnection dbunit = new DatabaseConnection(connection, System.getProperty("Binbase.user").toUpperCase());
			// dbunit.getConfig().setFeature(DatabaseConfig.FEATURE_BATCHED_STATEMENTS,
			// true);
			dbunit.getConfig().setProperty(DatabaseConfig.PROPERTY_RESULTSET_TABLE_FACTORY, new ForwardOnlyResultSetTableFactory());
			dbunit.getConfig().setProperty(DatabaseConfig.PROPERTY_ESCAPE_PATTERN, "\"?\"");

			logger.info("insert data...");
			DatabaseOperation.DELETE_ALL.execute(dbunit, dataSet);
	}
}
