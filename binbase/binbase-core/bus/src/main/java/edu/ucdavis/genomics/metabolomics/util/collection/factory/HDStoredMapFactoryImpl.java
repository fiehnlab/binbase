package edu.ucdavis.genomics.metabolomics.util.collection.factory;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.util.collection.HDStoredMap;

public class HDStoredMapFactoryImpl<K,V> extends MapFactory<K, V>{

	@Override
	public Map<K, V> createMap() {
		return new HDStoredMap<K, V>(this.getConfiguration());
	}

}
