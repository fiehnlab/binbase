package edu.ucdavis.genomics.metabolomics.util.database.statements;

import java.io.ByteArrayInputStream;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Random;

import org.apache.log4j.Logger;

/**
 * used to test statements
 * 
 * @author wohlgemuth
 * 
 */
public class StatementTester {

	/**
	 * tests the given sql statement if it can be compiled and executed
	 * 
	 * @param statement
	 * @param connection
	 * @throws SQLException
	 */
	public static void testStatements(String statement, Connection connection)
			throws SQLException {
		Logger logger = Logger.getLogger(StatementTester.class);
		logger.info("preparing statement with value");
		logger.info("");
		logger.info(statement);
		logger.info("");

		PreparedStatement p = connection.prepareStatement((statement));
		logger.info("compile was successfull!");
		ParameterMetaData meta = p.getParameterMetaData();

		logger.info("count of parameters: " + meta.getParameterCount());
		for (int i = 0; i < meta.getParameterCount(); i++) {
			logger.info("setting parameter: " + (i + 1)
					+ " - type of parameter: "
					+ meta.getParameterTypeName(i + 1));

			// check the parameter
			check(p, meta, i);
		}

		try {
			// execute
			if (p.execute()) {
				logger.info("was of operation select");

				ResultSet set = p.getResultSet();

				int count = 0;
				while (set.next()) {
					count++;
				}
				logger.info("found rows: " + count);
				logger.info("closing result set");
				set.close();
			} else {
				logger.info("was of operation insert,delete,update");
			}
		} finally {

			logger.info("closing statement");
			p.close();
		}
	}

	private static void check(PreparedStatement p, ParameterMetaData meta, int i)
			throws SQLException {
		Logger logger = Logger.getLogger(StatementTester.class);
		try {
			Object value = null;

			switch (meta.getParameterType(i + 1)) {
			case Types.BIGINT:
				value = new Random().nextInt(1000) + 5;
				logger.info("setting value: " + value);

				p.setInt(i + 1, (Integer) value);
				break;

			case Types.DATE:
				value = new Date(System.currentTimeMillis());
				logger.info("setting value: " + value);

				p.setDate(i + 1, (Date) value);
				break;

			case Types.DOUBLE:
				value = new Random().nextDouble();
				logger.info("setting value: " + value);

				p.setDouble(i + 1, (Double) value);
				break;

			case Types.DECIMAL:
				value = new Random().nextDouble();
				logger.info("setting value: " + value);

				p.setDouble(i + 1, (Double) value);
				break;

			case Types.INTEGER:
				value = new Random().nextInt(1000) + 5;
				logger.info("setting value: " + value);

				p.setInt(i + 1, (Integer) value);
				break;
			case Types.JAVA_OBJECT:
				value = new Random().nextInt(1000) + 5;
				logger.info("setting value: " + value);

				p.setObject(i + 1, (Integer) value);
				break;

			case Types.NULL:
				p.setNull(i + 1, meta.getParameterType(i + 1));
				break;
			case Types.NUMERIC:
				value = new Random().nextInt(1000) + 5;
				logger.info("setting value: " + value);

				p.setInt(i + 1, (Integer) value);
				break;

			case Types.REAL:
				value = new Random().nextInt(1000) + 5;
				logger.info("setting value: " + value);

				p.setInt(i + 1, (Integer) value);

				break;

			case Types.SMALLINT:
				value = new Random().nextInt(100) + 5;
				logger.info("setting value: " + value);

				p.setShort(i + 1, ((Integer) value).shortValue());
				break;

			case Types.TINYINT:
				value = new Random().nextInt(100) + 5;
				logger.info("setting value: " + value);

				p.setShort(i + 1, ((Integer) value).shortValue());
				break;

			case Types.TIME:
				value = new Time(System.currentTimeMillis());
				logger.info("setting value: " + value);

				p.setTime(i + 1, (Time) value);
				break;

			case Types.TIMESTAMP:
				value = new Timestamp(System.currentTimeMillis());
				logger.info("setting value: " + value);

				p.setTimestamp(i + 1, (Timestamp) value);
				break;

			case Types.BOOLEAN:
				value = new Random().nextBoolean();
				logger.info("setting value: " + value);

				p.setBoolean(i + 1, (Boolean) value);
				break;
			case Types.FLOAT:
				value = new Random().nextFloat();
				logger.info("setting value: " + value);

				p.setFloat(i + 1, (Float) value);
				break;

			// everything now falls throw to default

			case Types.BINARY:
				byte[] content = "tadasdasdasdas".getBytes();
				ByteArrayInputStream byts = new ByteArrayInputStream(content);
				p.setBinaryStream(i+1, byts, content.length);
				logger.info("setting binaery value");
				break;
			case Types.BLOB:
			case Types.CHAR:
			case Types.CLOB:
			case Types.LONGVARBINARY:
			case Types.LONGVARCHAR:
			case Types.OTHER:
			case Types.VARBINARY:
			case Types.VARCHAR:
			
			default:
				value = "a";
				logger.info("setting value: " + value);
				p.setString(i + 1, value.toString());
			}
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
			throw e;
		}
	}
}
