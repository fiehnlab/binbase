package edu.ucdavis.genomics.metabolomics.util.database.test;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.dbunit.dataset.IDataSet;
import org.junit.After;
import org.junit.Before;

import edu.ucdavis.genomics.metabolomics.util.PropertySetter;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.ExtractDataFromDatabase;
import edu.ucdavis.genomics.metabolomics.util.io.dest.Destination;
import edu.ucdavis.genomics.metabolomics.util.io.dest.FileDestination;

/**
 * provides us with access to the database without the use of jmx beans
 * 
 * @author wohlgemuth
 * 
 */
public abstract class BinBaseDatabaseTest {
	private Logger logger = Logger.getLogger(getClass());

	protected static Properties UNMODIFIED_SYSTEM_PROPERTIES = System
			.getProperties();

	@Before
	public void setUp() throws Exception {
		XMLConfigurator.getInstance();
		@SuppressWarnings("unused")
		Properties p = PropertySetter.setPropertiesToSystem("src/test/resources/test.properties");

		
		ConnectionFactory factory = getFactory();
		logger.info("create connection...");
		Connection connection = factory.getConnection();
		try {
			logger.info("create dataset...");
			IDataSet populate = getDataSet();
			logger.info("calling initialisation tool");
			InitializeDBUtil.initialize(connection, populate);
		} catch (Exception e) {
			throw e;
		} finally {
			factory.close(connection);
		}
	}

	/**
	 * provides a factory
	 * 
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	protected ConnectionFactory getFactory() throws FileNotFoundException,
			IOException {
		PropertySetter
				.setPropertiesToSystem("src/test/resources/test.properties");

		logger.info("create factory");

		ConnectionFactory factory = ConnectionFactory.getFactory();
		factory.setProperties(System.getProperties());

		return factory;
	}

	protected abstract IDataSet getDataSet() throws Exception;

	@After
	public void tearDown() throws Exception {
		logger.info("create xml datafile dump...");

		ConnectionFactory factory = getFactory();
		Connection connection = factory.getConnection();
		if(connection.getAutoCommit() == false){
			logger.info("forcing a commint just in case!");			
			connection.commit();
		}
		
		
		try {

			Destination out = new FileDestination("target/content/result");
			out.setIdentifier(this.getClass().getSimpleName() + "-data.xml");

			OutputStream outstream = out.getOutputStream();
			logger.info("writing the content...");
			new ExtractDataFromDatabase().extractData(outstream, connection);
			logger.info("closing the streams");
			outstream.flush();
			outstream.close();
			logger.info("closing the connection");
		} finally {
			factory.close(connection);
		}
	}
}
