package edu.ucdavis.genomics.metabolomics.util.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom.Element;

import edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfigable;

/**
 * is used to validate sql statements against a connection
 * 
 * @author wohlgemuth
 * 
 */
public final class SqlStatementValidator {

	private static Logger logger = Logger.getLogger(SqlStatementValidator.class);
	/**
	 * extremy simple, just takes the statement and tests its against the
	 * connection
	 * 
	 * @param sql
	 * @param c
	 * @throws SQLException
	 */
	public static void validate(String sql, Connection c) throws SQLException {
		logger.debug("valdiating sql: " + sql);
		PreparedStatement state = c.prepareStatement(sql);
		state.close();
	}

	/**
	 * loads all the sql statements from the given file and tests them
	 * 
	 * @param config
	 * @param c
	 * @throws SQLException
	 */
	public static void validate(XMLConfigable config, Connection c)
			throws SQLException {
		Element element = config.getRoot();
		validate(element, c);
	}

	/**
	 * valdiates the statements in the element text
	 * 
	 * @param element
	 * @param c
	 * @throws SQLException
	 */
	@SuppressWarnings("unchecked")
	private static void validate(Element element, Connection c)
			throws SQLException {
		logger.debug("valdiating element: " + element.getName());
		if (element.getText().length() > 0) {
			validate(element.getText(), c);
		}

		List<Element> elements = element.getChildren();

		for (Element e : elements) {
			validate(e, c);
		}
	}
}
