/*
 * Created on Sep 2, 2003
 *
 */
package edu.ucdavis.genomics.metabolomics.util.transform.crosstable.format.txt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFileFactory;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.CrosstableTransformator;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.Transformator;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.format.ascii.XmlToAsciiTransformHandler;


/**
 * @author wohlgemuth
 * @version Sep 2, 2003
 * <br>
 * BinBaseDatabase
 * @description
 */
public class XmlToTxtDatafile {
    /**
     * konvertiert einen xml file in einen datafile...
     * @version Sep 2, 2003
     * @author wohlgemuth
     * <br>
     * @param xmlFile
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static final DataFile convert(String xmlFile)
        throws FileNotFoundException, IOException {
        FileInputStream input = new FileInputStream(xmlFile);
        String file = String.valueOf(File.createTempFile("temp", "tmp")
                                         .getParent() + "/" +
                new Date().getTime() + ".txt");

        FileOutputStream output = new FileOutputStream(file);
        Transformator transform = new CrosstableTransformator();

        transform.setIn(input);
        transform.setOut(output);
        transform.setTransformer(new XmlToAsciiTransformHandler());
        transform.run();

        DataFile data = DataFileFactory.newInstance().createDataFile();
        data.read(new FileInputStream(new File(file)));

        return data;
    }
}
