/*
 * Created on 28.07.2004
 */
package edu.ucdavis.genomics.metabolomics.util.statistics.deskriptiv;

import edu.ucdavis.genomics.metabolomics.util.statistics.AbstractStatistic;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;


/**
 * @author wohlgemuth
 *
 */
public class DescriptiveStatistic extends AbstractStatistic {
    /**
     * DOCUMENT ME!
     */
    private static final double SAMPLE_CLEANING = 0.3;

    /**
     * DOCUMENT ME!
     */
    private static final double CLEAN_FACTOR = 0.5;

    /**
     * DOCUMENT ME!
     *
     * @uml.property name="methods"
     * @uml.associationEnd elementType="edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.util.statistics.deskriptiv.DeskriptiveMethod" multiplicity="(0 -1)"
     */
    private Collection methods = new Vector();

    /**
     * DOCUMENT ME!
     */
    private List classes;

    /**
     * DOCUMENT ME!
     *
     * @uml.property name="data"
     * @uml.associationEnd multiplicity="(0 1)"
     */
    private List data;

    /**
     * DOCUMENT ME!
     */
    private List types;

    /**
     * @see edu.ucdavis.genomics.metabolomics.binbase.utils.transform.Statistics#setMethod(java.lang.Object)
     */
    public void addMethod(Object o) {
        this.methods.add(o);
    }

    /**
     * @see edu.ucdavis.genomics.metabolomics.binbase.utils.transform.Statistics#doStatistics(java.util.Collection,
     *      java.util.Collection)
     */
    public Collection doStatistics(Collection data, Collection classes) {
        if (data.isEmpty()) {
            return new Vector();
        }

        this.classes = (List) classes;

        Collections.sort(this.classes);

        //ermitteln der metaboliten
        List temp = (List) ((List) data).get(0);
        temp.remove(0);
        temp.remove(1);

        Collections.sort(temp);
        this.types = temp;

        List result = new Vector();
        List header = new Vector();
        header.add("class");
        header.add("method");

        for (int i = 0; i < types.size(); i++) {
            header.add(types.get(i));
        }

        Reliableness reliable = new Reliableness();

        // entfernen schlechter samples
        Iterator dataIterator = data.iterator();
        dataIterator.next();
        this.data = new Vector();
        this.data.add(header);

        while (dataIterator.hasNext()) {
            List content = (List) dataIterator.next();

            List test = new Vector();

            for (int i = 2; i < content.size(); i++) {
                test.add(content.get(i));
            }

            double value = reliable.calculate(test);

            if (reliable.calculate(test) > SAMPLE_CLEANING) {
                this.data.add(content);
            } else {
                logger.warn(
                    "ignore current sample, is propertly a calculation error, value = " +
                    value);
            }
        }

        result.add(header);

        Iterator it = methods.iterator();

        while (it.hasNext()) {
            Object o = it.next();

            if (o instanceof DeskriptiveMethod) {
                DeskriptiveMethod method = (DeskriptiveMethod) o;

                for (int i = 0; i < classes.size(); i++) {
                    List list = new Vector();
                    list.add(this.classes.get(i));
                    list.add(method.getName());

                    List sum = new Vector();

                    for (int x = 0; x < types.size(); x++) {
                        try {
                            double value = reliable.calculate(getMetaboliteWithZeros(
                                        this.data, x,
                                        this.classes.get(i).toString()));

                            if (value > CLEAN_FACTOR) {
                                sum.add("1");
                            } else {
                                sum.add("0");
                            }

                            List values = null;

                            //ermittelt ob nullstellen erlaubt sind
                            if (method.acceptZeros()) {
                                values = getMetaboliteWithZeros(this.data, x,
                                        this.classes.get(i).toString());
                            } else {
                                values = getMetabolite(this.data, x,
                                        this.classes.get(i).toString());
                            }

                            if (values.isEmpty()) {
                                list.add(
                                    "ERROR dataset contains only zeros so ignored");
                            } else {
                                list.add(String.valueOf(method.calculate(values)));
                            }
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }
                    }

                    //reinigt das datenset von unsauberen klassen
                    if (reliable.calculate(sum) > CLEAN_FACTOR) {
                        result.add(list);
                    } else {
                        logger.warn("discard class " + this.classes.get(i) +
                            ", to dirty!");
                    }
                }

                result.add(new Vector());
            } else {
                logger.warn("not supported method " + o.getClass());
            }
        }

        return result;
    }
}
