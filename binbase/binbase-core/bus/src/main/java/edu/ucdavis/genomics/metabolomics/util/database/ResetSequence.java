/*
 * Created on Mar 14, 2005
 *
 */
package edu.ucdavis.genomics.metabolomics.util.database;

import edu.ucdavis.genomics.metabolomics.util.SQLObject;

import java.sql.PreparedStatement;
import java.sql.SQLException;


/**
 * @author wohlgemuth
 */
public class ResetSequence extends SQLObject {
    /**
     * DOCUMENT ME!
     */
    private PreparedStatement statement = null;

    /**
     * DOCUMENT ME!
     *
     * @param name DOCUMENT ME!
     * @param value DOCUMENT ME!
     *
     * @throws SQLException DOCUMENT ME!
     */
    public synchronized void setValue(String name, int value)
        throws SQLException {
        value = value - 1;
        this.statement.setString(1, name);
        this.statement.setInt(2, value);
        this.statement.execute();
    }

    /**
     * DOCUMENT ME!
     *
     * @param args DOCUMENT ME!
     */
    public static void main(String[] args) {
        ResetSequence f = new ResetSequence();
        f.setConnection(ConnectionFactory.getFactory().getConnection());

        try {
            f.setValue(args[0], Integer.parseInt(args[1]));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /* (non-Javadoc)
     * @see edu.ucdavis.genomics.metabolomics.binbase.binlib.algorythm.SQLObject#prepareStatements()
     */
    protected void prepareStatements() throws Exception {
        // TODO Auto-generated method stub
        super.prepareStatements();
        this.statement = this.getConnection().prepareCall("begin SETVALUE(?,?); end;");
    }
}
