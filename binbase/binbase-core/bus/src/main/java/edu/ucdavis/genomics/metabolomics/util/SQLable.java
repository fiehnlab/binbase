/*
 * Created on 05.06.2003
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package edu.ucdavis.genomics.metabolomics.util;

import edu.ucdavis.genomics.metabolomics.util.config.Configable;
import edu.ucdavis.genomics.metabolomics.util.config.SQLConfigable;

import java.sql.Connection;
import java.sql.SQLException;


/**
 * @author wohlgemuth
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public interface SQLable extends Configable, SQLConfigable {
    /**
     * sets the database connection
     * @param connection
     *
     * @uml.property name="connection"
     */
    void setConnection(Connection connection);

    /**
     * @return connection object
     *
     * @uml.property name="connection"
     */
    Connection getConnection();
}
