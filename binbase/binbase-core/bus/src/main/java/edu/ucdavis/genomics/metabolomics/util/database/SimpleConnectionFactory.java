/*
 * Created on May 13, 2005
 */
package edu.ucdavis.genomics.metabolomics.util.database;

import java.sql.Connection;
import java.sql.SQLException;

import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;

/**
 * @author wohlgemuth
 */
public class SimpleConnectionFactory extends ConnectionFactory {
	private WrappedConnection connection = null;

	@Override
	protected void finalize() throws Throwable {
		this.close(connection);
		super.finalize();
	}

	/**
	 * @see edu.ucdavis.genomics.metabolomics.binbase.utils.database.ConnectionFactory#getConnection(boolean)
	 */
	public synchronized Connection getConnection(boolean pooled) {
		try {
			logger.info("getting connection...");

			if (connection != null) {
				logger.info("this factory already created a connection!");

				if (connection.isClosed()) {
					logger.info("connection is closed!");
					connection = null;
				}
				else {
					logger.info("returning existing connection!");
					return connection;
				}
			}
			if (this.getProperties() == null) {
				logger.info("loading properies from configurator...");
				this.setProperties(XMLConfigurator.getInstance().getProperties());
			}

			String username = this.getProperties().getProperty(KEY_USERNAME_PROPERTIE);
			String password = this.getProperties().getProperty(KEY_PASSWORD_PROPERTIE);
			String database = this.getProperties().getProperty(KEY_DATABASE_PROPERTIE);
			String host = this.getProperties().getProperty(KEY_HOST_PROPERTIE);
			String type = this.getProperties().getProperty(KEY_TYPE_PROPERTIE);

			logger.info("creating a new connection");
			connection = new WrappedConnection(DatabaseUtilities.createConnection(Integer.parseInt(type), username, password, host, database), this);
			connection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

			return connection;
		}
		catch (ConnectionConfigurationException e) {
			throw e;
		}
		catch (Exception e) {
			throw new ConnectionConfigurationException(e.getMessage(), e);
		}
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @throws Exception
	 *             DOCUMENT ME!
	 */
	public synchronized void close() throws SQLException {
		if (connection != null) {
			connection.getConnection().close();
		}
		connection = null;
	}

	/**
	 * DOCUMENT ME!
	 * 
	 * @param connection
	 *            DOCUMENT ME!
	 * @throws SQLException
	 *             DOCUMENT ME!
	 */
	public synchronized void close(Connection connection) throws SQLException {
		if (connection instanceof WrappedConnection) {
			WrappedConnection c = (WrappedConnection) connection;
			if (c.getFactory().equals(this.connection.getFactory())) {
				close();
				return;
			}
		}
		throw new SQLException("can't close a connection which I don't own!");
	}

	@Override
	public boolean hasConnection() {
		return connection != null;
	}

}
