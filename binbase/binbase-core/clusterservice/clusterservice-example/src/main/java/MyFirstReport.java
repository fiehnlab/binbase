import java.io.Serializable;
import java.util.Date;

import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;

/*
 * Created on Jul 29, 2006
 */

public class MyFirstReport extends Report{

	@Override
	public String getOwner() {
		return "this is me";
	}

	@Override
	public void report(String owner, Serializable id, ReportEvent event, ReportType type, String ipaddress, Date time, Exception e) {
		if(owner.equals(getOwner())){
			System.out.println("time: " + time);
			System.out.println("my object: " + id);
			System.out.println("my event: " + event);
			System.out.println("my type: " + type);
			
			if(type.getAttachement() != null){
				System.out.println("my result: " + type.getAttachement());				
			}
			
			if(e != null){
				System.out.println("my exception: " + e.getMessage());
			}
			System.out.println();
		}
	}
}
