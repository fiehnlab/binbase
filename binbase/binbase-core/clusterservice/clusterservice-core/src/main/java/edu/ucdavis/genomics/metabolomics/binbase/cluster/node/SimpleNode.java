package edu.ucdavis.genomics.metabolomics.binbase.cluster.node;



public class SimpleNode extends CalculationNode{

	public SimpleNode() throws Exception {
		super();
	}

	/**
	 * starts a single node and keeps it running in the background till it finds some work todo
	 * @param args
	 * @throws Exception
	 */
	public static void main(final String[] args) throws Exception {
		try {

			if(args.length == 1){
				//let's specify the application server
			}
			
			do {
				new SimpleNode().start();
			} while (true);

		} catch (final Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}