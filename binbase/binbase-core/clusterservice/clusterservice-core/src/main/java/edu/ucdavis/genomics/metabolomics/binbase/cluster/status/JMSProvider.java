package edu.ucdavis.genomics.metabolomics.binbase.cluster.status;

import java.util.HashMap;

import javax.jms.JMSException;

/**
 * easy access to the jms system
 * @author wohlgemuth
 *
 */
public interface JMSProvider {

	/**
	 * sends a message
	 * @param message
	 * @throws JMSException
	 */
	public void sendMessage(HashMap<?,?> content) throws JMSException;

}
