/*
 * Created on Jul 19, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.util;

import java.io.Serializable;

/**
 * class representing informations about jobs
 * 
 * @author wohlgemuth
 * @version Jul 19, 2006
 * 
 */
public class JobInformation implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private String name;

	private String owner;

	private int slots;

	private int id;

	private String state;

	private String queue;

	private String host;
	
	private String submission;
	
	private double priority;

	/**
	 * id of the job
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * name of the job
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * the owner
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	/**
	 * priority of the job
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public double getPriority() {
		return priority;
	}

	public void setPriority(double priority) {
		this.priority = priority;
	}

	/**
	 * in which queue is the job
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public String getQueue() {
		return queue;
	}

	public void setQueue(String queue) {
		this.queue = queue;
	}

	/**
	 * how many slots uses the job
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public int getSlots() {
		return slots;
	}

	public void setSlots(int slots) {
		this.slots = slots;
	}

	/**
	 * current state of the job
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	/**
	 * when did we submit this job
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public String getSubmission() {
		return submission;
	}

	public void setSubmission(String submission) {
		this.submission = submission;
	}

	/**
	 * on which host is this job running
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}
	
	/**
	 * 
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @see java.lang.Object#toString()
	 */
	public String toString(){
		return this.name;
	}
}
