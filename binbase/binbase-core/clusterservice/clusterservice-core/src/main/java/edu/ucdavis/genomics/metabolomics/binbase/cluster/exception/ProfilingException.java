package edu.ucdavis.genomics.metabolomics.binbase.cluster.exception;

public class ProfilingException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * 
	 */
	public ProfilingException() {
		super();
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public ProfilingException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param arg0
	 */
	public ProfilingException(String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 */
	public ProfilingException(Throwable arg0) {
		super(arg0);
	}

}
