package edu.ucdavis.genomics.metabolomics.binbase.cluster.util;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

public class SchedulingException extends BinBaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SchedulingException() {
	}

	public SchedulingException(String message) {
		super(message);
	}

	public SchedulingException(Throwable cause) {
		super(cause);
	}

	public SchedulingException(String message, Throwable cause) {
		super(message, cause);
	}

}
