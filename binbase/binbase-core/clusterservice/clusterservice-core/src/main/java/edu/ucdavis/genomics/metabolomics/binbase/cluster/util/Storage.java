package edu.ucdavis.genomics.metabolomics.binbase.cluster.util;

import java.io.Serializable;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageObject;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageService;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.StoringException;

/**
 * easy way to store data and retrieve data on the cluster
 * @author wohlgemuth
 * 
 */
public final class Storage {

	private Logger logger = Logger.getLogger(getClass());
	
	private Storage(){
		
	}
	
	private static Storage storage;

	/**
	 * add or update this object
	 * @param key
	 * @param object
	 * @throws StoringException
	 */
	public void put(String key, Serializable object) throws StoringException {
		try {
			StorageService service = Configurator.getStorageService();
			if(service.hasObject(key)){
				logger.debug("key already exist: " + key);
				service.removeObject(key);
			}
			logger.debug("put: " + key);
			service.store(key, new StorageObject(object));
		}catch (NamingException e) {
			throw new StoringException(e);
		}
	}

	/**
	 * retrieve this object
	 * @param key
	 * @return
	 * @throws StoringException
	 */
	public Serializable get(String key) throws StoringException {
		try {
			return Configurator.getStorageService().getObject(key).getObject();
		}catch (NamingException e) {
			throw new StoringException(e);
		}
	}

	/**
	 * remove this object
	 * @param key
	 * @throws StoringException
	 */
	public void remove(String key) throws StoringException {
		try {
			Configurator.getStorageService().removeObject(key);
		} catch (NamingException e) {
			throw new StoringException(e);
		}
	}

	/**
	 * has this object
	 * @param key
	 * @return
	 * @throws StoringException
	 */
	public boolean has(String key) throws StoringException {
		try {
			return Configurator.getStorageService().hasObject(key);
		} catch (NamingException e) {
			throw new StoringException(e);
		}
	}
	
	/**
	 * deletes all objects
	 * @throws StoringException
	 */
	public void clear() throws StoringException{
		try {
			Configurator.getStorageService().clear();
		} catch (NamingException e) {
			throw new StoringException(e);
		}		
	}
	
	/**
	 * returns all keys
	 * @return
	 * @throws StoringException
	 */
	public String[] getKeys() throws StoringException{
		try {
			return Configurator.getStorageService(). getKeys();
		} catch (NamingException e) {
			throw new StoringException(e);
		}		
	}
	
	
	/**
	 * returns the instance
	 * @return
	 */
	public static Storage getInstance(){
		if(storage == null){
			storage = new Storage();
		}
		return storage;
	}
}
