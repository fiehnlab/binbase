/*
 * Created on Apr 18, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.entity.LockingEntityBean;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.exception.LockingFailedException;
import edu.ucdavis.genomics.metabolomics.exception.LockingException;
import org.apache.log4j.Logger;

import javax.ejb.*;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

@Stateless
@ApplicationException(rollback = false)
public class LockingServiceBean implements LockingService {

	private Logger logger = Logger.getLogger(LockingServiceBean.class);

	private static final long serialVersionUID = 2L;


	public LockingServiceBean() {
		super();
	}

	@PersistenceContext
	EntityManager manager;

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void deleteObject(LockingEntityBean bean) {
		manager.remove(bean);
		manager.flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.LockingServic#
	 * releaseRessource(java.lang.String)
	 */
	public void releaseRessource(Serializable o) throws LockingException {
		LockingEntityBean bean = findBean(o);
		deleteObject(bean);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.LockingServic#lock
	 * (long, java.lang.String, java.lang.String)
	 */
	public boolean lock(long id, Serializable o, String ipAddress)
			throws LockingFailedException {
		// lock ressource
		try {
			
			LockingEntityBean bean = new LockingEntityBean(o, ipAddress);
			logger.debug("trying to lock object: " + o
					+ " attempt comes from: " + ipAddress);
			try {
				LockingEntityBean temp = findBean(o);
				if (temp == null) {
					manager.persist(bean);
					manager.flush();
					logger.debug("successfully locked!");
					return true;
				} else {
					logger.debug("opject is already locked!, " + o);
					Date date = temp.getExpires();
					Date current = new Date();

					if (current.getTime() >= date.getTime()) {
						logger.debug("bean is expired, forcefull delete it and attempt to lock this ressource");
						deleteObject(temp);
						logger.debug("we aborted to lock this object, since the lock was expired and we need to recheck! Object was"
								+ o);
						return false;
					} else {
						logger.debug("we were not able to lock this object, since the lock was not yet expired: "
								+ o);
						return false;
					}
				}
			} catch (Exception e) {
				logger.debug("locking failed! " + e.getMessage(), e);
				throw new LockingFailedException(e.getMessage(), e);
			}
		} catch (Exception e) {
			logger.debug("locking failed! " + e.getMessage(), e);
			throw new LockingFailedException(e.getMessage(), e);
		}

	}

	private LockingEntityBean findBean(Serializable o) {
		LockingEntityBean temp = manager.find(LockingEntityBean.class,
				o.hashCode());
		return temp;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.LockingServic#
	 * getLockedRessourcesInformation()
	 */
	@SuppressWarnings("unchecked")
	public Collection<Serializable> getLockedRessourcesInformation()
			throws FinderException, NamingException {
		logger.debug("getting locked ressources informations");
		Collection<Serializable> object = new Vector<Serializable>();
		Collection<LockingEntityBean> temp = manager.createQuery(
				"from " + LockingEntityBean.class.getName()).getResultList();

		Iterator<LockingEntityBean> it = temp.iterator();

		while (it.hasNext()) {
			LockingEntityBean b = it.next();
			String ressource = b.getInetAddress() + " - " + b.getRessource();
			object.add(ressource);
		}
		return object;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.LockingServic#
	 * getLockedRessources()
	 */
	@SuppressWarnings("unchecked")
	public Collection<Serializable> getLockedRessources() {
		logger.debug("getting locked ressources");
		Collection<Serializable> object = new Vector<Serializable>();
		Collection<LockingEntityBean> temp = manager.createQuery(
				"from " + LockingEntityBean.class.getName()).getResultList();

		Iterator<LockingEntityBean> it = temp.iterator();

		while (it.hasNext()) {
			LockingEntityBean b = it.next();
			Serializable ressource = b.getRessource();
			object.add(ressource);
		}
		return object;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.LockingServic#
	 * releaseAllRessources()
	 */
	public void releaseAllRessources() throws LockingException {
		logger.debug("release all ressources");
		Iterator<Serializable> it = getLockedRessources().iterator();

		Collection<Serializable> content = new Vector<Serializable>();
		while (it.hasNext()) {
			content.add(it.next());
		}
		it = content.iterator();

		while (it.hasNext()) {
			releaseRessource(it.next());
		}

		logger.debug("done");
	}
}
