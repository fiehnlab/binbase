/*
 * Created on Apr 22, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.status;

import java.util.Properties;

import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;

/**
 * create an ejb based report
 * 
 * @author wohlgemuth
 * @version Apr 22, 2006
 * 
 */
public class EJBReportFactory extends ReportFactory {

	static JMSProvider provider = JMSQueueProvider.getInstance();

	public EJBReportFactory() {
		super();
	}

	@Override
	public Report create(Properties p, String owner) {
		return new EJBReport(owner, provider);
	}
}
