/*
 * Created on Aug 10, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.ant;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.taskdefs.Java;
import org.apache.tools.ant.types.Path;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;

/**
 * used to execute task which need a classpath ant can't provide in the current
 * version. Basicly allows you to avoid the bug that you need to copy all
 * libraries into the ant home directoy if you want to load classes over
 * Class.forName().
 * 
 * @author wohlgemuth
 * @version Aug 10, 2006
 * 
 */
public abstract class JavaDelegateTask extends ClusterInitializeTask {

	/**
	 * collection of paths containing libraries
	 */
	private Collection<Path> path = new Vector<Path>();

	/**
	 * add a path to the java runtime
	 * 
	 * @author wohlgemuth
	 * @version Aug 9, 2006
	 * @param path
	 */
	public void addPath(Path path) {
		this.path.add(path);
	}

	/**
	 * generates the needed arguments
	 * 
	 * @author wohlgemuth
	 * @version Aug 10, 2006
	 * @param java
	 */
	protected abstract void generateArguments(Java java);

	/**
	 * run the task it self
	 * 
	 * @author wohlgemuth
	 * @version Aug 10, 2006
	 * @param p
	 * @throws Exception
	 */
	protected abstract void run(Properties p) throws Exception;

	/**
	 * create the java runtime with the neccessaery path and executes the main
	 * method
	 * 
	 * @author wohlgemuth
	 * @version Aug 9, 2006
	 * @see edu.ucdavis.genomics.metabolomics.binbase.cluster.ant.ClusterInitializeTask#execute()
	 */
	public final void doExecution(ClusterUtil util) throws BuildException {
		this.validate();
		try {
			Java java = new Java();
			java.setProject(getProject());
			java.createArg().setProject(getProject());

			java.setFork(true);

			if (path.isEmpty()) {
				Enumeration enume = this.getProject().getReferences().elements();

				while (enume.hasMoreElements()) {
					Object o = enume.nextElement();

					if (o instanceof Path) {
						java.setClasspath((Path) o);
					}
				}
			} else {
				Iterator<Path> it = path.iterator();

				while (it.hasNext()) {
					java.setClasspath(it.next());
				}
			}

			java.setClassname(getClass().getName());

			StringWriter writer = new StringWriter();
			PrintWriter print = new PrintWriter(writer);
			this.getProperties().list(print);
			java.createArg().setValue(writer.toString());

			this.generateArguments(java);

			log("execute class: " + getClass().getName());
			java.execute();
		} catch (Exception e) {
			throw new BuildException(e);
		}
	}

	/**
	 * return our path
	 * 
	 * @author wohlgemuth
	 * @version Aug 10, 2006
	 * @return
	 */
	public Collection<Path> getPath() {
		return path;
	}

	/**
	 * needed for validations of parameters
	 * 
	 * @author wohlgemuth
	 * @version Aug 10, 2006
	 */
	protected void validate() throws BuildException {
	}
}
