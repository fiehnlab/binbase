/*
 * Created on Apr 18, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.ClusterException;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx.ReportJMXMBean;
import org.jboss.mx.util.MBeanServerLocator;

import javax.ejb.ApplicationException;
import javax.ejb.Stateless;
import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import java.util.Collection;

@Stateless
@ApplicationException(rollback = false)
public class ClusterReportConfigServiceBean implements ClusterReportConfigService {
	private ReportJMXMBean bean;

	private static final long serialVersionUID = 2L;

	public ClusterReportConfigServiceBean() {
		super();
	}

	protected void sync() throws ClusterException {
		MBeanServer server = MBeanServerLocator.locate();

		try {
			bean = (ReportJMXMBean) MBeanServerInvocationHandler.newProxyInstance(server, new ObjectName("binbase:service=RuntimeReport"),
					ReportJMXMBean.class, false);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ClusterException(e);
		}
	}

	public void addReport(String providerClass) throws ClusterException {
		sync();
		bean.addReport(providerClass);
	}

	public void removeReport(String providerClass) throws ClusterException {
		sync();
		bean.removeReport(providerClass);
	}

	@SuppressWarnings("unchecked")
	public Collection<String> getReports() throws ClusterException {
		sync();
		return bean.getReports();
	}

	public boolean isReportRegistered(String providerClass) throws ClusterException {
		sync();
		return bean.isReportRegistered(providerClass);
	}

}
