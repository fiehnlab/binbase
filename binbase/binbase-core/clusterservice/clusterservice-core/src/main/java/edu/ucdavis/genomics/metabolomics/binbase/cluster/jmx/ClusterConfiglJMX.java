/*
 * Created on Apr 18, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;

import org.apache.log4j.Logger;
import org.jboss.mq.server.jmx.QueueMBean;
import org.jboss.mx.util.MBeanServerLocator;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtilFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.RocksClusterFactoryImpl;

/**
 * 
 * @author wohlgemuth
 * @version Apr 18, 2006
 * @jmx.mbean description = "configure all nodes" extends =
 *            "javax.management.MBeanRegistration"
 */
public class ClusterConfiglJMX implements ClusterConfiglJMXMBean, Serializable {
	/**
	 * registered handler for the cluster
	 */
	private Map handler = new HashMap();

	/**
	 * queue where our messages are
	 */
	private String queue = "queue/queue";

	/**
	 * after which time are our nodes going to shutdown if they have nothing
	 * todo
	 */
	private long iddleTime = 1000 * 60 * 5;

	private Logger logger = Logger.getLogger(getClass());

	/**
	 * are we automatical starting the calculation
	 */
	private boolean autoStart;

	/**
	 * name of the head node
	 */
	private String cluster;

	/**
	 * username
	 */
	private String username;

	/**
	 * password
	 */
	private String password;

	private String clusterFactory;

	private int maxNodes = 1;

	/**
	 * timeout for ejb locks
	 */
	private long timeout = 3600000;

	/**
	 * interval to check for locks
	 */
	private long lockingInterval = 10000;

	/**
	 * do we adjust the timeout automatically based on failed locks
	 */
	private boolean timeoutAdjustement = false;
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	public ClusterConfiglJMX() {
		super();
	}

	/**
	 * @jmx.managed-operation description = "clears all the registered handlers"
	 */
	public void clearHandlers() {
		handler.clear();
	}

	/**
	 * @throws Exception
	 * @jmx.managed-operation description = "clears the cluster queue" clears
	 *                        teh cluster queue
	 * 
	 */
	public void clearQueue() throws Exception {
		MBeanServer server = MBeanServerLocator.locate();

		ClusterConfiglJMXMBean bean = (ClusterConfiglJMXMBean) MBeanServerInvocationHandler
				.newProxyInstance(server, new ObjectName(
						"binbase:service=ClusterConfig"),
						ClusterConfiglJMXMBean.class, false);

		String[] myQueue = bean.getQueue().split("/");
		ObjectName name = new ObjectName(
				"jboss.mq.destination:service=Queue,name=" + myQueue[0]);
		QueueMBean queue = (QueueMBean) MBeanServerInvocationHandler
				.newProxyInstance(server, name, QueueMBean.class, false);

		// our import
		queue.removeAllMessages();
	}

	/**
	 * @jmx.managed-operation description = "adds a new handler"
	 * @author wohlgemuth
	 * @version Jul 18, 2006
	 * @param handlerClass
	 * @param objectOfIntresstClass
	 */
	public void addHandler(String objectOfIntresstClass, String handlerClass) {
		this.getHandler().put(objectOfIntresstClass, handlerClass);
		this.store();
	}

	/**
	 * @jmx.managed-operation description = "removes a handler"
	 * @author wohlgemuth
	 * @version Jul 18, 2006
	 * @param handlerClass
	 */
	public void removeHandler(String objectOfIntresstClass) {
		this.getHandler().remove(objectOfIntresstClass);
		this.store();
	}

	/**
	 * @jmx.managed-operation description = "return all registered handlers!"
	 * @author wohlgemuth
	 * @version Jul 18, 2006
	 * @return
	 */
	public Map getHandler() {
		return handler;
	}

	/**
	 * @jmx.managed-operation description = "return all registered handlers!"
	 * @author wohlgemuth
	 * @version Jul 18, 2006
	 * @return
	 */

	public void setHandler(Map handler) {
		this.handler = handler;
		store();
	}

	/**
	 * @jmx.managed-operation description = "after which time should the nodes
	 *                        shutdown if they have nothing todo!"
	 * @author wohlgemuth
	 * @version Jul 18, 2006
	 * @return
	 */
	public long getIddleTime() {
		return iddleTime;
	}

	/**
	 * @jmx.managed-operation description = "after which time should the nodes
	 *                        shutdown if they have nothing todo!"
	 * @author wohlgemuth
	 * @version Jul 18, 2006
	 * @return
	 */

	public void setIddleTime(long iddleTime) {
		this.iddleTime = iddleTime;
		store();
	}

	public void postDeregister() {

	}

	public void postRegister(Boolean registrationDone) {
		try {
			File file = new File(getClass().getName() + ".properties");
			if (!file.exists()) {
				return;
			}

			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					file));
			try {
				this.handler = (Map) in.readObject();
				this.iddleTime = in.readLong();
				this.queue = in.readUTF();
				this.timeout = in.readLong();
				this.password = (String) in.readObject();
				this.cluster = (String) in.readObject();
				this.username = (String) in.readObject();
				this.autoStart = (Boolean) in.readObject();
				this.maxNodes = (Integer) in.readObject();
				this.clusterFactory = (String) in.readObject();
				this.keepRunning = in.readBoolean();
				this.timeoutAdjustement = in.readBoolean();
				this.lockingInterval = in.readLong();
			}
			finally {
				in.close();
			}
		} catch (Exception e) {
			logger.error("postRegister(Boolean)", e); //$NON-NLS-1$

		}

		if (this.getClusterFactory() == null) {
			logger.debug("there was no cluster factory defined, we use the rocks cluster by default");
			this.setClusterFactory(RocksClusterFactoryImpl.class.getName());
		}
	}

	private void store() {
		try {
			File file = new File(getClass().getName() + ".properties");

			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(file));
			out.writeObject(this.handler);
			out.writeLong(this.iddleTime);
			out.writeUTF(this.queue);
			out.writeLong(this.timeout);
			out.writeObject(getPassword());
			out.writeObject(getCluster());
			out.writeObject(getUsername());
			out.writeObject(isAutoStart());
			out.writeObject(maxNodes);
			out.writeObject(getClusterFactory());
			out.writeBoolean(this.isKeepRunning());
			out.writeBoolean(isTimeoutAdjustement());
			out.writeLong(this.getLockingInterval());
			out.flush();
			out.close();

		} catch (Exception e) {
			logger.error(e.getMessage(), e); //$NON-NLS-1$
		}
	}

	public void preDeregister() throws Exception {
		store();
	}

	public ObjectName preRegister(MBeanServer server, ObjectName name)
			throws Exception {
		return null;
	}

	/**
	 * @jmx.managed-operation description = "queue"
	 * @author wohlgemuth
	 * @version Jul 18, 2006
	 * @return
	 */
	public String getQueue() {
		return queue;
	}

	/**
	 * @jmx.managed-operation description = "queue"
	 * @author wohlgemuth
	 * @version Jul 18, 2006
	 * @return
	 */
	public void setQueue(String queue) {
		this.queue = queue;
		store();
	}

	/**
	 * @jmx.managed-operation description = "timeout for locking"
	 * @return
	 */
	public long getTimeout() {
		return timeout;
	}

	/**
	 * @jmx.managed-operation description = "timeout for locking"
	 * @param timeout
	 */
	public void setTimeout(long timeout) {
		this.timeout = timeout;
		this.store();
	}

	/**
	 * @jmx.managed-operation description="should the nodes start when we find
	 *                        an import"
	 * @author wohlgemuth
	 * @version Aug 14, 2006
	 * @return
	 */
	public boolean isAutoStart() {
		return autoStart;
	}

	/**
	 * @jmx.managed-operation description="should the nodes start when we find
	 *                        an import"
	 * @author wohlgemuth
	 * @version Aug 14, 2006
	 * @param autoStart
	 */
	public void setAutoStart(boolean autoStart) {
		this.autoStart = autoStart;
		this.store();
	}

	/**
	 * @jmx.managed-operation description="ip of the cluster"
	 * @author wohlgemuth
	 * @version Aug 14, 2006
	 * @return
	 */
	public String getCluster() {
		return cluster;
	}

	/**
	 * @jmx.managed-operation description="ip of the cluster"
	 * @author wohlgemuth
	 * @version Aug 14, 2006
	 * @param cluster
	 */
	public void setCluster(String cluster) {
		this.cluster = cluster;
		this.util = null;
		this.store();
	}

	/**
	 * @jmx.managed-operation description="password of the cluster"
	 * @author wohlgemuth
	 * @version Aug 14, 2006
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @jmx.managed-operation description="password of the cluster"
	 * @author wohlgemuth
	 * @version Aug 14, 2006
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
		this.util = null;
		this.store();
	}

	/**
	 * @jmx.managed-operation description="username of the cluster"
	 * @author wohlgemuth
	 * @version Aug 14, 2006
	 * @return
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @jmx.managed-operation description="username of the cluster"
	 * @author wohlgemuth
	 * @version Aug 14, 2006
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
		this.util = null;
		this.store();
	}

	/**
	 * @jmx.managed-operation description="gives you access to the cluster"
	 * @author wohlgemuth
	 * @version Aug 14, 2006
	 * @return
	 */
	public ClusterUtil accessUtil() {

		if (util == null) {
			Map p = new HashMap();

			p.put("username", getUsername());
			p.put("password", getPassword());
			p.put("server", getCluster());
			p.put("java.naming.provider.url", "127.0.0.1:1099");

			util = ClusterUtilFactory.newInstance(this.getClusterFactory())
					.createUtil(p);

		}
		return util;
	}

	private ClusterUtil util;

	private boolean keepRunning;

	/**
	 * @jmx.managed-operation description="max amount of nodes running at the
	 *                        same time"
	 * @return
	 */
	public int getMaxNodes() {
		return maxNodes;
	}

	/**
	 * @jmx.managed-operation description="max amount of nodes running at the
	 *                        same time"
	 * @param maxNodes
	 */
	public void setMaxNodes(int maxNodes) {
		this.maxNodes = maxNodes;
		this.store();
	}

	public String getClusterFactory() {
		return clusterFactory;
	}

	public void setClusterFactory(String clusterFactory) {
		this.clusterFactory = clusterFactory;
		this.store();
	}

	/**
	 * @jmx.managed-operation description = "clears all the registered handlers"
	 */
	public boolean isKeepRunning() {
		return this.keepRunning;
	}

	/**
	 * @jmx.managed-operation description = "clears all the registered handlers"
	 */
	public void setKeepRunning(boolean value) {
		this.keepRunning = value;
		this.store();
	}

	/**
	 * @jmx.managed-operation description="starts a node"
	 */
	public String startNode() {
		logger.info("attempting to start a node...");
		try {
			return accessUtil().startNode();
		} catch (Exception e) {
			logger.info(e.getMessage(), e);
			return e.getMessage();
		}
	}

	/**
	 * @jmx.managed-operation description = "clears all the registered handlers"
	 */
	public long getLockingInterval() {
		return lockingInterval;
	}

	/**
	 * @jmx.managed-operation description = "clears all the registered handlers"
	 */
	public void setLockingInterval(long lockingInterval) {
		this.lockingInterval = lockingInterval;
		this.store();

	}

	/**
	 * @jmx.managed-operation description = "clears all the registered handlers"
	 */
	public boolean isTimeoutAdjustement() {
		return timeoutAdjustement;
	}

	/**
	 * @jmx.managed-operation description = "clears all the registered handlers"
	 */
	public void setTimeoutAdjustement(boolean timeoutAdjustement) {
		this.timeoutAdjustement = timeoutAdjustement;
		this.store();

	}

}
