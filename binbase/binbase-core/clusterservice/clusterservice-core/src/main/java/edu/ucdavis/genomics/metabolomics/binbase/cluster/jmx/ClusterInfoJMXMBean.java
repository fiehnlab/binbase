/*
 * Generated file - Do not edit!
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx;

import java.io.Serializable;

/**
 * MBean interface.
 * @author wohlgemuth
 * @version Jul 21, 2006
 */
public interface ClusterInfoJMXMBean extends javax.management.MBeanRegistration {

  java.util.Collection<Serializable> getLockedRessources() throws edu.ucdavis.genomics.metabolomics.exception.LockingException;

  java.util.Collection<Serializable> getLockedRessourcesInformation() throws edu.ucdavis.genomics.metabolomics.exception.LockingException;

}
