/*
 * Created on Jul 28, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.ant;

import java.io.File;
import java.util.Iterator;
import java.util.Vector;

import org.apache.tools.ant.DirectoryScanner;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.FileSet;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;

/**
 * this task uploads a library to the cluster
 * 
 * @author wohlgemuth
 * @version Jul 28, 2006
 */
public class ClusterUploadLibraryTask extends ClusterInitializeTask {
	private Vector<FileSet> fileSets = new Vector<FileSet>();

	private boolean merge;

	@Override
	public void doExecution(ClusterUtil util) throws Exception {

		Iterator<FileSet> it = this.fileSets.iterator();

		String[] libs = null;
		if (isMerge()) {
			libs = util.getRegisteredLibraries();
		}
		while (it.hasNext()) {
			FileSet file = it.next();
			DirectoryScanner scanner = file.getDirectoryScanner(getProject());

			String files[] = scanner.getIncludedFiles();

			for (int i = 0; i < files.length; i++) {
				if (libs == null) {
					this.upload(util, new File(scanner.getBasedir().toString() + File.separator + files[i]));
				}
				else {
					boolean ignore = false;
					for (int x = 0; x < libs.length; x++) {
						if (libs[x].equals(new File(files[i]).getName())) {
							ignore = true;
						}
					}
					if (ignore) {
						log("ignore file already exist on the server: " + files[i], Project.MSG_VERBOSE);
					}
					else {
						this.upload(util, new File(scanner.getBasedir().toString() + File.separator + files[i]));
					}

				}
			}
		}

	}

	public void addFileSet(FileSet set) {
		this.fileSets.add(set);
	}

	public boolean isMerge() {
		return merge;
	}

	public void setMerge(boolean merge) {
		this.merge = merge;
	}

	/**
	 * stores the file on the server
	 * 
	 * @author wohlgemuth
	 * @version Jul 28, 2006
	 * @param util
	 * @param file
	 * @throws Exception
	 */
	protected void upload(ClusterUtil util, File file) throws Exception {
		if (file.isFile()) {
			if (file.exists()) {
				log("upload file: " + file.toString(), Project.MSG_VERBOSE);

				FileSource source = new FileSource(file);
				util.uploadLibrary(source.getSourceName(), source);
			}
		}
	}
}
