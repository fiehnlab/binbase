/*
 * Created on Apr 18, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.ClusterException;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx.ClusterConfiglJMXMBean;
import org.jboss.mq.server.jmx.QueueMBean;
import org.jboss.mx.util.MBeanServerLocator;

import javax.ejb.ApplicationException;
import javax.ejb.Stateless;
import javax.jms.Message;
import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Map;

@Stateless
@ApplicationException(rollback = false)
public class ClusterConfigServiceBean implements ClusterConfigService {

	private ClusterConfiglJMXMBean bean;

	private static final long serialVersionUID = 2L;

	public ClusterConfigServiceBean() {
		super();
	}

	/**
	 * returns all messages in the current queue
	 * 
	 * @return
	 * @throws ClusterException
	 */
	@SuppressWarnings("unchecked")
	public List<Message> getMessages() throws ClusterException {
		try {
			MBeanServer server = MBeanServerLocator.locate();

			ClusterConfiglJMXMBean bean = (ClusterConfiglJMXMBean) MBeanServerInvocationHandler
					.newProxyInstance(server, new ObjectName(
							"binbase:service=ClusterConfig"),
							ClusterConfiglJMXMBean.class, false);

			String[] myQueue = bean.getQueue().split("/");
			ObjectName name = new ObjectName(
					"jboss.mq.destination:service=Queue,name=" + myQueue[0]);
			QueueMBean queue = (QueueMBean) MBeanServerInvocationHandler
					.newProxyInstance(server, name, QueueMBean.class, false);

			// our import
			return queue.listMessages();
		} catch (Exception e) {
			throw new ClusterException(e);
		}
	}

	/**
	 * synchronize with the beans
	 * 
	 * @throws ClusterException
	 */
	protected void sync() throws ClusterException {
		MBeanServer server = MBeanServerLocator.locate();

		try {
			bean = (ClusterConfiglJMXMBean) MBeanServerInvocationHandler
					.newProxyInstance(server, new ObjectName(
							"binbase:service=ClusterConfig"),
							ClusterConfiglJMXMBean.class, false);
		} catch (MalformedObjectNameException e) {
			e.printStackTrace();
			throw new ClusterException(e);
		} catch (NullPointerException e) {
			e.printStackTrace();
			throw new ClusterException(e);
		}
	}

	/**
	 * adds a handler
	 * 
	 * @param objectOfIntresstClass
	 * @param handlerClass
	 * @throws ClusterException
	 */
	public void addHandler(String objectOfIntresstClass, String handlerClass)
			throws ClusterException {
		sync();
		bean.addHandler(objectOfIntresstClass, handlerClass);
	}

	/**
	 * returns all handlers
	 * 
	 * @return
	 * @throws ClusterException
	 */

	@SuppressWarnings("unchecked")
	public Map<String, String> getHandler() throws ClusterException {
		sync();
		return bean.getHandler();
	}

	/**
	 * clears the queue
	 * 
	 * @throws ClusterException
	 */
	public void clearQueue() throws ClusterException {
		sync();
		try {
			bean.clearQueue();
		} catch (Exception e) {
			throw new ClusterException(e);
		}
	}

	/**
	 * clears all handler
	 * 
	 * @throws ClusterException
	 */
	public void clearHandler() throws ClusterException {
		sync();
		try {
			bean.clearHandlers();
		} catch (Exception e) {
			throw new ClusterException(e);
		}
	}

	/**
	 * the iddle time for the cluster
	 * 
	 * @return
	 * @throws ClusterException
	 */
	public long getIddleTime() throws ClusterException {
		sync();
		return bean.getIddleTime();
	}

	/**
	 * the name of the queue
	 * 
	 * @return
	 * @throws ClusterException
	 */
	public String getQueue() throws ClusterException {
		sync();
		return bean.getQueue();
	}

	/**
	 * deeltes a handler
	 * 
	 * @param objectOfIntresstClass
	 * @throws ClusterException
	 */
	public void removeHandler(String objectOfIntresstClass)
			throws ClusterException {
		sync();
		bean.removeHandler(objectOfIntresstClass);
	}

	/**
	 * adds a handler
	 * 
	 * @param handler
	 * @throws ClusterException
	 */
	public void setHandler(Map<String, String> handler) throws ClusterException {
		sync();
		bean.setHandler(handler);
	}

	/**
	 * sets the iddle time
	 * 
	 * @param iddleTime
	 * @throws ClusterException
	 */
	public void setIddleTime(long iddleTime) throws ClusterException {
		sync();
		bean.setIddleTime(iddleTime);
	}

	/**
	 * sets the queue
	 * 
	 * @param queue
	 * @throws ClusterException
	 */
	public void setQueue(String queue) throws ClusterException {
		sync();
		bean.setQueue(queue);
	}

	/**
	 * returns the timeout
	 * 
	 * @return
	 * @throws ClusterException
	 */
	public long getTimeout() throws ClusterException {
		sync();
		return bean.getTimeout();
	}

	/**
	 * sets the timeout
	 * 
	 * @param timeout
	 * @throws ClusterException
	 */
	public void setTimeout(long timeout) throws ClusterException {
		sync();
		bean.setTimeout(timeout);
	}

	public String getCluster() throws ClusterException {
		sync();
		return bean.getCluster();
	}

	public int getMaxNodes() throws ClusterException {
		sync();
		return bean.getMaxNodes();
	}

	public String getPassword() throws ClusterException {
		sync();
		return bean.getPassword();
	}

	public String getUsername() throws ClusterException {
		sync();
		return bean.getUsername();
	}

	public boolean isAutoStart() throws ClusterException {
		sync();
		return bean.isAutoStart();
	}

	public void setAutoStart(boolean autoStart) throws ClusterException {
		sync();
		bean.setAutoStart(autoStart);
	}

	public void setCluster(String cluster) throws ClusterException {
		sync();
		bean.setCluster(cluster);
	}

	public void setMaxNodes(int maxNodes) throws ClusterException {
		sync();
		bean.setMaxNodes(maxNodes);
	}

	public void setPassword(String password) throws ClusterException {
		sync();
		bean.setPassword(password);
	}

	public void setUsername(String username) throws ClusterException {
		sync();
		bean.setUsername(username);
	}

	public String getClusterFactory() throws ClusterException {
		sync();
		return bean.getClusterFactory();
	}

	public void setClusterFactory(String clusterFactory)
			throws ClusterException {
		sync();
		bean.setClusterFactory(clusterFactory);
	}

	public boolean isKeepRunning() throws ClusterException, RemoteException {
		sync();
		return bean.isKeepRunning();
	}

	public void setKeepRunning(boolean value) throws ClusterException,
			RemoteException {
		sync();
		bean.setKeepRunning(value);

	}

	public long getLockingInterval() throws ClusterException, RemoteException {
		sync();
		return bean.getLockingInterval();
	}

	public void setLockingInterval(long lockingInterval)
			throws ClusterException, RemoteException {
		sync();
		bean.setLockingInterval(lockingInterval);
	}

	public boolean isTimeoutAdjustement() throws ClusterException,
			RemoteException {
		sync();
		return bean.isTimeoutAdjustement();
	}

	public void setTimeoutAdjustement(boolean timeoutAdjustement)
			throws ClusterException, RemoteException {
		sync();
		bean.setTimeoutAdjustement(timeoutAdjustement);

	}

}
