/*
 * Created on Jul 28, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.ant;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtilFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;

import javax.naming.Context;
import java.util.*;

/**
 * a simpe task which initialize the cluster
 * 
 * @author wohlgemuth
 * @version Jul 28, 2006
 */
public class ClusterInitializeTask extends Task {
	/**
	 * neccessaery properties
	 */
	private Collection<ClusterParameter> properties = new Vector<ClusterParameter>();

	/**
	 * the factory to use to create the cluster util
	 */
	private String factory;

	private String ip;
	
	private Configuration config;

	/**
	 * validates our parameters
	 * 
	 * @author wohlgemuth
	 * @version Jul 28, 2006
	 * @param props
	 * @throws Exception
	 */
	protected void validateParameter(Properties p, String[] props) throws Exception {

		Map map = p;
		for (int i = 0; i < props.length; i++) {
			if (map.get(props[i]) == null) {
				log("you need to set the propertie: " + props[i]);
				throw new BuildException("sorry I need more parameters");
			}
		}
	}

	/**
	 * creates the needed factory
	 * 
	 * @author wohlgemuth
	 * @version Jul 28, 2006
	 * @return
	 * @throws Exception
	 */
	protected ClusterUtilFactory createFactory(Properties p) throws Exception {
		log("create factory", Project.MSG_INFO);
		ClusterUtilFactory fact = ClusterUtilFactory.newInstance(getFactory());
		validateParameter(p, fact.getNeededProperties());

		return fact;
	}

	protected ClusterUtilFactory createFactory() throws Exception {
		return createFactory(getProperties());
	}

	/**
	 * automatic configuration based on the application server
	 * 
	 * @throws Exception
	 */
	protected void autoconf() throws Exception {
		XMLConfigurator.getInstance().destroy();
		
		log("using autoconf", Project.MSG_INFO);
		this.addParameter(new ClusterParameter(this.getProject(), "jboss", getIp()));
		this.addParameter(new ClusterParameter(this.getProject(), "java.naming.provider.url", getIp()+":1099"));

		System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		System.setProperty(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");

		this.setFactory(Configurator.getConfigService().getClusterFactory());

		this.addParameter(new ClusterParameter(this.getProject(), "username", Configurator.getConfigService().getUsername()));
		this.addParameter(new ClusterParameter(this.getProject(), "password", Configurator.getConfigService().getPassword()));
		this.addParameter(new ClusterParameter(this.getProject(), "server", Configurator.getConfigService().getCluster()));

		this.addParameter(new ClusterParameter(this.getProject(), Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory"));
		this.addParameter(new ClusterParameter(this.getProject(), Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces"));

		log("defined parameters", Project.MSG_INFO);

	}

	/**
	 * connects to the util and initialize the cluster
	 * 
	 * @author wohlgemuth
	 * @version Jul 28, 2006
	 * @see org.apache.tools.ant.Task#execute()
	 */
	public final void execute() throws BuildException {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();

		try {
			try {
				Thread.currentThread().setContextClassLoader(getClass().getClassLoader());

				if (this.factory == null) {
					autoconf();
				}
				else if (this.factory.length() == 0) {
					autoconf();
				}

				// setting basedir for simple access
				System.setProperty("ant.basedir", this.getProject().getBaseDir().getAbsolutePath());

				log("create factory", Project.MSG_INFO);
				ClusterUtil util = createFactory(this.getProperties()).createUtil(getProperties());

				try {
					doExecution(util);
				}
				finally {
					util.destroy();
				}

			}
			catch (Exception e) {
				e.printStackTrace();
				throw new BuildException(e);
			}
		}
		finally {
			Thread.currentThread().setContextClassLoader(loader);
		}
	}

	/**
	 * does the actual execution of the ant task
	 * 
	 * @param util
	 * @throws Exception
	 */
	public void doExecution(ClusterUtil util) throws Exception {
		log("initialize cluster", Project.MSG_INFO);

		String outPut = util.initializeCluster(getProperties().getProperty("jboss"));
		log(outPut, Project.MSG_VERBOSE);

		log("done", Project.MSG_INFO);
	}

	public String getFactory() {
		log("accessing factory", Project.MSG_VERBOSE);

		if (factory == null) {
			try {
				autoconf();
			}
			catch (Exception e) {
				throw new RuntimeException(e.getMessage(), e);
			}
		}
		return factory;
	}

	public void setFactory(String factory) {
		this.factory = factory;
	}

	public void addParameter(ClusterParameter parameter) {
		log(this.getClass().getSimpleName() + " - adding parameter: " + parameter.getName() + "/" + parameter.getValue(), Project.MSG_INFO);

		if (parameter.getName() != null && parameter.getValue() != null) {
			this.properties.add(parameter);
		}
		else {
			log("sorry ignored the parameter some of the data were null", Project.MSG_WARN);
		}
	}

	/**
	 * calculates the needed properties
	 * 
	 * @author wohlgemuth
	 * @version Aug 8, 2006
	 * @return
	 */
	protected final Properties getProperties() {
		log("generating properties", Project.MSG_VERBOSE);
		if (this.config != null) {
			this.properties = config.getContent();
		}
		Iterator<ClusterParameter> it = properties.iterator();

		Properties result = new Properties();
		result.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		result.setProperty(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");

		while (it.hasNext()) {
			ClusterParameter x = it.next();
			result.put(x.getName(), x.getValue());
			log("property: " + x.getName() + "/" + x.getValue(), Project.MSG_VERBOSE);
		}

		return result;
	}

	/**
	 * configurations of the tasks
	 * 
	 * @author wohlgemuth
	 * @version Aug 8, 2006
	 * @param config
	 */
	public void addConfig(Configuration config) {
		if (properties.isEmpty() == false) {
			throw new BuildException("I'm already configured using another configuration or parameters");
		}

		if (this.config != null) {
			throw new BuildException("config already set");
		}
		this.config = config;
	}

	public String getIp() {
		if(ip == null){
			String ip = getProperties().getProperty("jboss");
			
			if(ip == null){
				throw new RuntimeException("you either need to specify an ip or add a parameter with the name jboss!");
			}
		}
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
}
