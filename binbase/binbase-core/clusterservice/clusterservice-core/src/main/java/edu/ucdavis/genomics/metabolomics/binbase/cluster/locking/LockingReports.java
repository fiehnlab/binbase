/*
 * Created on Apr 24, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.locking;

import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;

/**
 * locking report events
 * 
 * @author wohlgemuth
 * @version Apr 24, 2006
 * 
 */
public interface LockingReports {
	ReportEvent LOCK = new ReportEvent("lock", "something is locked");

	ReportEvent UNLOCK = new ReportEvent("lock", "something is locked");

	ReportType RESSOURCE = new ReportType("ressource", "a ressource");

}
