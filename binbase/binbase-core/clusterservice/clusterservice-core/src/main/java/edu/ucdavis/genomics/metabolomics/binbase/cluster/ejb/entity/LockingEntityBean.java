/*
 * Created on Apr 23, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "locking")
public class LockingEntityBean implements Serializable {

	private int id;

	@Column(nullable = false)
	private Serializable ressource;

	// expiration time for locks equals 24hours
	private static final long EXPIRATION = 86400000;

	@Column(nullable = false)
	private String inetAddress;
	@Column(nullable = false)
	private Date date;
	@Column(nullable = false)
	private Date expires;
	

	private static final long serialVersionUID = 2L;

	public LockingEntityBean() {

		this.setExpires(new Date(System.currentTimeMillis() + EXPIRATION));
		this.setDate(new Date());
	}

	public LockingEntityBean(Serializable ressource, String inetAddress,
			long expirationTime) {
		this.setId(ressource.hashCode());
		this.setRessource(ressource);
		this.setInetAddress(inetAddress);
		this.setExpires(new Date(System.currentTimeMillis() + expirationTime));
		this.setDate(new Date());
	}

	public LockingEntityBean(Serializable ressource, String inetAddress) {
		this.setId(ressource.hashCode());
		this.setRessource(ressource);
		this.setInetAddress(inetAddress);
		this.setExpires(new Date(System.currentTimeMillis() + EXPIRATION));
		this.setDate(new Date());
	}

	public Serializable getRessource() {
		return ressource;
	}

	@Id
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setRessource(Serializable ressource) {
		this.ressource = ressource;
	}

	public String getInetAddress() {
		return inetAddress;
	}

	public void setInetAddress(String inetAddress) {
		this.inetAddress = inetAddress;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Date getExpires() {
		return expires;
	}

	public void setExpires(Date expires) {
		this.expires = expires;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof LockingEntityBean) {
			LockingEntityBean l = (LockingEntityBean) obj;
			if (l.getInetAddress().equals(this.getInetAddress())) {
				if (l.getRessource().equals(this.getRessource())) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getInetAddress().hashCode()
				+ this.getRessource().hashCode();
	}

	@Override
	public String toString() {
		return this.inetAddress + " - " + this.getRessource() + " - "
				+ this.getDate() + " - " + this.getExpires();
	}

}
