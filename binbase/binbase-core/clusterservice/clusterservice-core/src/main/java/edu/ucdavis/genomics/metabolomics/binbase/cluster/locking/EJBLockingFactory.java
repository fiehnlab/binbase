/*
 * Created on Apr 21, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.locking;

import java.util.Properties;
import java.util.Random;

import edu.ucdavis.genomics.metabolomics.util.thread.locking.Lockable;
import edu.ucdavis.genomics.metabolomics.util.thread.locking.LockableFactory;

/**
 * provieds us with an ejb based locking system
 * @author wohlgemuth
 * @version Apr 21, 2006
 *
 */
public class EJBLockingFactory extends LockableFactory{

	private Random random = new Random();
	@Override
	public Lockable create(String owner,Properties p) {
		return new EJBLocking(owner,random.nextLong()+System.currentTimeMillis());
	}

}
