/*
 * Created on Jul 14, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster;

import java.io.Serializable;
import java.util.Properties;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.node.CalculationNode;
import edu.ucdavis.genomics.metabolomics.util.status.Report;

/**
 * provides us with an interface to create jobs for the cluster
 * 
 * @author wohlgemuth
 * @version Jul 14, 2006
 * 
 */
public interface ClusterHandler {
	/**
	 * object for calculation
	 * 
	 * @author wohlgemuth
	 * @version Jul 14, 2006
	 * @param o
	 */
	public void setObject(Serializable o);

	/**
	 * properties needed
	 * 
	 * @author wohlgemuth
	 * @version Jul 14, 2006
	 * @param p
	 */
	public void setProperties(Properties p);

	/**
	 * return the associated properties
	 * 
	 * @author wohlgemuth
	 * @version Jul 14, 2006
	 * @return
	 */
	public Properties getProperties();

	/**
	 * starts the job
	 * 
	 * @author wohlgemuth
	 * @version Jul 14, 2006
	 */
	public boolean start() throws Exception;

	/**
	 * reporting for this handler
	 * 
	 * @author wohlgemuth
	 * @version Jul 14, 2006
	 * @param report
	 */
	public void setReport(Report report);

	/**
	 * returns the assiciated report
	 * 
	 * @author wohlgemuth
	 * @version Jul 31, 2006
	 * @return
	 */
	public Report getReport();

	/**
	 * node the handler run on
	 * @param node
	 */
	public void setNode(CalculationNode node);
	
	/**
	 * node the handler runs on
	 * @return
	 */
	public CalculationNode getNode();
}
