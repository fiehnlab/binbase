package edu.ucdavis.genomics.metabolomics.binbase.cluster.status;

import java.util.HashMap;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.InitialContext;

import org.apache.log4j.Logger;

/**
 * queue provider for our reports
 * 
 * @author wohlgemuth
 * 
 */
public class JMSQueueProvider implements JMSProvider {

	private Logger logger = Logger.getLogger(getClass());

	private static JMSProvider instance;

	public static JMSProvider getInstance() {
		if (instance == null) {
			instance = new JMSQueueProvider();
		}

		return instance;
	}

	private JMSQueueProvider() {

	}

	public void sendMessage(HashMap<?, ?> msg) throws JMSException {
		try {
			logger.debug("open session...");
			InitialContext ctx = new InitialContext();

			QueueConnection queueConnection = null;
			QueueSession session = null;
			MessageProducer producer = null;

			logger.debug("creating factory...");
			QueueConnectionFactory qcf = (QueueConnectionFactory) ctx
					.lookup("QueueConnectionFactory");
			queueConnection = qcf.createQueueConnection();

			logger.debug("creating queue...");

			Queue queue = (Queue) ctx.lookup("queue/Reports");

			logger.debug("creating session...");

			session = queueConnection.createQueueSession(false,
					Session.AUTO_ACKNOWLEDGE);

			logger.debug("creating producer...");

			producer = session.createProducer(queue);

			try {
				logger.debug("creating message...");

				ObjectMessage message = session.createObjectMessage();
				message.setObject(msg);

				logger.debug("send message: " + msg);

				producer.send(message, DeliveryMode.NON_PERSISTENT,
						Message.DEFAULT_PRIORITY, 10000);
				logger.debug("message sent...");

			} finally {
				logger.debug("closing queue...");

				producer.close();
				session.close();
				queueConnection.close();
				logger.debug("queue closed...");

			}
		} catch (Exception e) {
			
			if (e instanceof JMSException) {
				throw (JMSException) e;
			}
			throw new JMSException(e.getMessage());
		}
	}
}