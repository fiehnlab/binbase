package edu.ucdavis.genomics.metabolomics.binbase.cluster.util;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.jdom.Element;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.node.LocalCalculationNode;
import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling;
import edu.ucdavis.genomics.metabolomics.util.io.Copy;
import edu.ucdavis.genomics.metabolomics.util.io.dest.Destination;
import edu.ucdavis.genomics.metabolomics.util.io.dest.FileDestination;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;

/**
 * provides access to a local calculation node which runs inside the application
 * server
 * 
 * @author wohlgemuth
 */
public class LocalCalculationUtil extends ClusterUtil {

	public static final String LOG_FILE = System.getProperty("java.io.tmpdir")
			+ File.separatorChar + "binbase-node.log";

	public static final String LOCK_FILE = System.getProperty("java.io.tmpdir")
			+ File.separatorChar + "binbase-node.lock";

	private final Logger logger = Logger.getLogger(getClass());

	LocalCalculationNode node = null;

	public LocalCalculationNode getNode() {
		return node;
	}

	public void setNode(LocalCalculationNode node) {
		this.node = node;
	}

	public LocalCalculationUtil() {
	}

	public void prepare() throws Exception {
		this.setNode(LocalCalculationNode.getInstance());
	}

	@Override
	public String[] getAvailableLogIds() throws Exception {
		return new String[] { "binbase-node.log" };
	}

	@Override
	protected String getClusterConfigDir() {
		return System.getProperty("user.home") + File.separatorChar + ".config"
				+ File.separator;
	}

	@Override
	protected String getClusterLibDir() {

		return System.getenv().get("JBOSS_HOME") + File.separator + "lib"
				+ File.separator;
	}

	protected String getClusterRuntimeDir() {
		return System.getenv().get("JBOSS_HOME") + File.separator + "server"
				+ File.separator + "all" + File.separator + "lib"
				+ File.separator;
	}

	@Override
	public List<ClusterInformation> getClusterInformation() throws Exception {

		final ClusterInformation info = new ClusterInformation();
		info.setNumberOfCpu("" + Runtime.getRuntime().availableProcessors());
		info.setArch("");
		info.setLoad("");
		info.setMemTotal("" + Runtime.getRuntime().maxMemory());
		info.setMemUse(Runtime.getRuntime().maxMemory()
				- Runtime.getRuntime().freeMemory() + "");
		info.setName("binbase local node");
		final Vector<ClusterInformation> result = new Vector<ClusterInformation>();
		result.add(info);
		return result;
	}

	@Override
	public List<ClusterInformation> getClusterInformationByUser(
			final String username) throws Exception {
		return getClusterInformation();
	}

	@Override
	public int getDeadNodeCount() throws Exception {
		return 0;
	}

	@Override
	public String getErrorLogForJob(final String jobID) throws Exception {
		File file = new File(LOG_FILE);

		if (file.exists()) {

			final Scanner reader = new Scanner(file);

			final StringBuffer result = new StringBuffer();
			while (reader.hasNext()) {
				result.append(reader.nextLine());
			}
			reader.close();
			
			return result.toString();

		} else {
			System.err.println("couldn't find the log file!");
			return "";
		}
	}

	@Override
	public String getErrorLogForJob(final String jobID, final int lastLines)
			throws Exception {
		return getErrorLogForJob(jobID);
	}

	@Override
	public int getFreeNodeCount() throws Exception {
		if (node.lockFileExist()) {
			return 0;
		} else {
			return 1;
		}
	}

	@Override
	public int getFreeSlotCount() throws Exception {
		return getFreeNodeCount();
	}

	@Override
	public String getInfoLogForJob(final String jobID) throws Exception {
		return getErrorLogForJob(jobID);
	}

	@Override
	public String getInfoLogForJob(final String jobID, final int lastLines)
			throws Exception {
		return getErrorLogForJob(jobID);
	}

	@Override
	public int getNodeCount() throws Exception {
		return 1;
	}

	@Override
	public List<JobInformation> getPendingQueue() throws Exception {
		/*
		 * if(LocalCalculationNode.getInstance().lockFileExist()){
		 * if(LocalCalculationNode.getInstance().isRunning() == false){
		 * JobInformation info = new JobInformation();
		 * info.setHost("127.0.0.1"); info.setId(1);
		 * info.setName("binbase local node");
		 * info.setOwner(System.getProperty("user.name")); info.setPriority(1);
		 * info.setSlots(1); info.setState("pending");
		 * info.setQueue("local queue"); Vector<JobInformation> result = new
		 * Vector<JobInformation>(); result.add(info); return result; } }
		 */
		return new Vector<JobInformation>();
	}

	@Override
	public List<JobInformation> getPendingQueueByUser(final String user)
			throws Exception {
		return new Vector<JobInformation>();
	}

	@Override
	public List<JobInformation> getQueue() throws Exception {
		if (node.lockFileExist() == true) {
			if (node.isRunning()) {
				if (getFreeNodeCount() == 0) {
					final JobInformation info = new JobInformation();
					info.setHost("127.0.0.1");
					info.setId(1);
					info.setName("binbase local node");
					info.setOwner(System.getProperty("user.name"));
					info.setPriority(1);
					info.setSlots(1);
					info.setState("running");
					info.setQueue("local queue");
					final Vector<JobInformation> result = new Vector<JobInformation>();
					result.add(info);
					return result;
				}
			}
		}
		logger.debug("queue is empty!");
		return new Vector<JobInformation>();
	}

	@Override
	public List<JobInformation> getQueueByUser(final String user)
			throws Exception {
		return getQueue();
	}

	@Override
	public int getSlotCount() throws Exception {
		return 1;
	}

	@Override
	public int getUsedNodeCount() throws Exception {
		return getNodeCount() - getFreeNodeCount();
	}

	@Override
	public int getUsedSlotCount() throws Exception {
		return getSlotCount() - getFreeSlotCount();
	}

	@Override
	public String killJob(final int id) throws Exception {

		if (node.isRunning()) {

			node.shutdown();

			while (node.lockFileExist() == true) {
				logger.debug("waiting for shutdown...");
				Thread.sleep(1000);
			}

			return "local node is shutdown!";

		} else {
			logger.debug("node was not running so shutdown not needed...");
			return "local node was not running!";
		}
	}

	@Override
	public String killJobs(final String user) throws Exception {
		return killJob(0);
	}

	@Override
	public String startNode() throws Exception {
		if (node == null) {
			this.prepare();
		}
		if (node.lockFileExist()) {
			return "Your job 1 (\"BinBaseNode\") is already running";
		}
		if (node.isRunning()) {
			return "Your job 1 (\"BinBaseNode\") is already running";
		}
		node.start();

		logger.debug("checking if startup procedure is complete!");
		while (node.isRunning() == false) {
			logger.debug("starting...");
			Thread.sleep(1000);
		}
		logger.debug("node started!");
		return "Your job 1 (\"BinBaseNode\") has started";
	}

	@Override
	public String startNodes(final int count) throws Exception {
		return startNode();
	}

	private void addParameterToConfig(final String name, final String value,
			final StringBuffer result) {
		result.append("<param name=\"" + name + "\" value=\"" + value
				+ "\"  public=\"true\"/>");
	}

	@Override
	public Source downloadDataFromCluster(final String fileOnCluster)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String executeCommand(final String command) throws Exception {
		final StringBuffer result = new StringBuffer();
		final BufferedOutputStream out = new BufferedOutputStream(Runtime
				.getRuntime().exec(command).getOutputStream());

		return result.toString();
	}

	@Override
	public Collection<ClusterInformation> getClusterInformationCurrentUser()
			throws Exception {
		return getClusterInformation();
	}

	@Override
	public List<JobInformation> getPendingQueueByCurrentUser() throws Exception {
		return getPendingQueue();
	}

	@Override
	public List<JobInformation> getQueueCurrentUser() throws Exception {
		return getQueue();
	}

	@Override
	public String[] getRegisteredLibraries() throws Exception {
		final Vector<String> result = new Vector<String>();
		for (final String a : executeCommand("ls " + getClusterLibDir()).split(
				"\n")) {
			result.add(a);
		}
		for (final String a : executeCommand("ls " + getClusterRuntimeDir())
				.split("\n")) {
			result.add(a);
		}

		final String[] res = new String[result.size()];

		for (int i = 0; i < result.size(); i++) {
			res[i] = result.get(i);
		}
		return res;
	}

	@Override
	public String initializeCluster(final String applicationServer)
			throws Exception {
		final File f = new File(getClusterConfigDir());

		if (f.exists() == false) {
			f.mkdir();
		}

		final StringBuffer result = new StringBuffer();
		result.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
		result.append("<config>");
		result.append("<parameter>");

		addParameterToConfig("java.naming.provider.url", applicationServer,
				result);
		addParameterToConfig("java.naming.factory.initial",
				"org.jnp.interfaces.NamingContextFactory", result);
		addParameterToConfig("java.naming.factory.url.pkgs",
				"org.jboss.naming:org.jnp.interfaces", result);
		addParameterToConfig(
				"edu.ucdavis.genomics.metabolomics.util.status.ReportFactory",
				"edu.ucdavis.genomics.metabolomics.binbase.cluster.status.EJBReportFactory",
				result);
		addParameterToConfig(
				"edu.ucdavis.genomics.metabolomics.util.thread.locking.LockableFactory",
				"edu.ucdavis.genomics.metabolomics.binbase.cluster.locking.EJBLockingFactory",
				result);

		result.append("</parameter>");
		result.append("</config>");

		final FileOutputStream out = new FileOutputStream(getClusterConfigDir()
				+ "applicationServer.xml");
		final ByteArrayInputStream in = new ByteArrayInputStream(result
				.toString().getBytes());
		Copy.copy(in, out);
		out.flush();
		out.close();
		in.close();
		return "system is initialized to run under the user of the application server";
	}

	@Override
	public String killJobOfCurrentUser() throws Exception {
		return killJob(0);
	}

	@Override
	public void storeDataOnCluster(final String filePathOnCluster,
			final Source source) throws Exception {
		final Destination d = new FileDestination();
		d.setIdentifier(new File(filePathOnCluster));
		Copy.copy(source.getStream(), d.getOutputStream(), false);
	}

	@Override
	public void unregisterConfiguration(final String name) throws Exception {
		Runtime.getRuntime().exec("rm -f " + name);
	}

	@Override
	public void destroy() throws Exception {
	}

	@Override
	public void setClusterProperties(final Properties p) throws Exception {
		final Enumeration keys = p.keys();

		final File file = new File(getClusterConfigDir()
				+ "applicationServer.xml");
		final Element root = XmlHandling.readXml(file);
		final Element parameter = root.getChild("parameter");
		while (keys.hasMoreElements()) {
			final List<Element> children = parameter.getChildren("param");
			final String key = (String) keys.nextElement();

			boolean found = false;
			for (final Element e : children) {
				if (e.getAttribute("name").getValue().equals(key)) {
					e.setAttribute("value", p.getProperty(key));
					found = true;
				}
			}
			if (found == false) {
				final Element chield = new Element("param");
				chield.setAttribute("name", key);
				chield.setAttribute("value", p.getProperty(key));
				parameter.addContent(chield);
			}

		}

		XmlHandling.writeXml(file, root);
	}

	public static void main(final String[] args) throws Exception {
		System.out.println(new LocalCalculationUtil().startNode());

		/*
		 * Thread.sleep(1000);
		 * 
		 * System.out.println(new
		 * LocalCalculationUtil().killJobOfCurrentUser()); Thread.sleep(1000);
		 * System.out.println(new LocalCalculationUtil().startNode());
		 * Thread.sleep(1000);
		 * 
		 * System.out.println(new
		 * LocalCalculationUtil().killJobOfCurrentUser()); Thread.sleep(1000);
		 * System.out.println(new LocalCalculationUtil().startNode());
		 * Thread.sleep(1000);
		 * 
		 * System.out.println(new
		 * LocalCalculationUtil().killJobOfCurrentUser()); Thread.sleep(1000);
		 * System.out.println(new LocalCalculationUtil().startNode());
		 * Thread.sleep(1000);
		 * 
		 * System.out.println(new
		 * LocalCalculationUtil().killJobOfCurrentUser()); Thread.sleep(1000);
		 */
	}
}
