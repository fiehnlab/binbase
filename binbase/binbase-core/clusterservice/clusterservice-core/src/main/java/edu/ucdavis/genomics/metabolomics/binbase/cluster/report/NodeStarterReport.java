package edu.ucdavis.genomics.metabolomics.binbase.cluster.report;

import java.io.Serializable;
import java.util.Date;

import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;

import org.apache.log4j.Logger;
import org.jboss.mq.server.jmx.QueueMBean;
import org.jboss.mx.util.MBeanServerLocator;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.NodeReports;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx.ClusterConfiglJMXMBean;
import edu.ucdavis.genomics.metabolomics.exception.LockingException;
import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;
import edu.ucdavis.genomics.metabolomics.util.thread.locking.Lockable;
import edu.ucdavis.genomics.metabolomics.util.thread.locking.LockableFactory;
import edu.ucdavis.genomics.metabolomics.util.thread.locking.SimpleLockingFactory;

/**
 * listens to events and starts the cluser nodes
 * 
 * @author Administrator
 */
public class NodeStarterReport extends Report {

	private Logger logger = Logger.getLogger(getClass());

	public NodeStarterReport() {
	}

	@Override
	public String getOwner() {
		return "application server";
	}

	/**
	 * checks if the event is of the kind schedule and if this is the case it
	 * starts a new node, if its possible
	 */
	@Override
	public synchronized void report(String owner, Serializable id,
			ReportEvent event, ReportType type, String ipaddress, Date time,
			Exception e) {

		try {
			MBeanServer server = MBeanServerLocator.locate();

			if (event.equals(Reports.SCHEDULE)
					|| getQueue(server).listMessages().isEmpty() == false) {

				if (type.equals(NodeReports.NODE) == false) {

					logger.debug("it's not a node startup so we can proceed");

					ClusterConfiglJMXMBean bean = (ClusterConfiglJMXMBean) MBeanServerInvocationHandler
							.newProxyInstance(server, new ObjectName(
									"binbase:service=ClusterConfig"),
									ClusterConfiglJMXMBean.class, false);

					// lets start the calculation, if we have something already
					// in
					// the
					// queue. ot means we need more nodes
					if (bean.isAutoStart()) {

						Lockable lock = LockableFactory.newInstance(
								SimpleLockingFactory.class.getName()).create(
								this.getOwner());
						// start a node
						ClusterUtil util = bean.accessUtil();

						try {
							lock.aquireRessource("NodeStarterReport", 20000);
							try {

								// check that the queue actually contains any
								// messages
								QueueMBean queue = getQueue(server);

								if (queue.listMessages().isEmpty() == false) {
									// only start a new node if we don't exceed
									// our
									// limit

									int queuesize = util.getQueueCurrentUser()
											.size();
									int pending = util
											.getPendingQueueByCurrentUser()
											.size();

									int total = queuesize + pending;

									int jobs = queue.listMessages().size();

									logger
											.info("currently are "
													+ total
													+ " nodes running and we can run maximal "
													+ bean.getMaxNodes()
													+ " nodes and there are "
													+ jobs + " in the queue");

									logger.debug("nodes, running: " + queuesize
											+ " pending: " + pending
											+ " total: " + total);
									if (total < bean.getMaxNodes()) {
										Report report = ReportFactory
												.newInstance().create(
														getOwner());

										int freeNodes = bean.getMaxNodes()
												- total;

										if (jobs <= freeNodes) {
											for (int i = 0; i < jobs; i++) {
												startNode(util, report);
											}
										} else if (jobs > freeNodes) {
											for (int i = 0; i < freeNodes; i++) {
												startNode(util, report);
											}
										}
									} else {
										logger
												.debug("don't start anymore nodes");
									}
								}

								else {
									logger
											.debug("no nodes are started since queue is empty!");
								}
							} finally {
								lock.releaseRessource("NodeStarterReport");
							}
						} catch (LockingException ex) {
							logger.info("Exception: " + e);
							logger.debug(e.getMessage(), e);
						}
					} else {
						logger.debug("auto start is disabled!");
					}
				}

			}
		} catch (Exception ex) {
			logger.warn(ex.getMessage(), ex);
		}
	}

	private QueueMBean getQueue(MBeanServer server)
			throws MalformedObjectNameException {
		ObjectName name = new ObjectName(
				"jboss.mq.destination:service=Queue,name=queue");
		QueueMBean queue = (QueueMBean) MBeanServerInvocationHandler
				.newProxyInstance(server, name, QueueMBean.class, false);
		return queue;
	}

	private String startNode(ClusterUtil util, Report report) throws Exception,
			InterruptedException {
		String jobId = util.startNode();

		logger.info("scheduled job with id: " +jobId);
		// just sleep for 1.5 seconds to
		// give
		// the scheduler some time to put
		// the
		// job in the queue
		Thread.sleep(1500);
		report.report(jobId, NodeReports.SCHEDULE, NodeReports.NODE);
		return jobId;
	}
}
