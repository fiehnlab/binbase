/*
 * Generated file - Do not edit!
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;

/**
 * MBean interface.
 * 
 * @author wohlgemuth
 * @version Apr 18, 2006
 */
public interface ClusterConfiglJMXMBean extends
		javax.management.MBeanRegistration {

	void clearHandlers();

	void clearQueue() throws java.lang.Exception;

	void addHandler(java.lang.String objectOfIntresstClass,
			java.lang.String handlerClass);

	void removeHandler(java.lang.String objectOfIntresstClass);

	java.util.Map getHandler();

	void setHandler(java.util.Map handler);

	long getIddleTime();

	void setIddleTime(long iddleTime);

	java.lang.String getQueue();

	void setQueue(java.lang.String queue);

	long getTimeout();

	void setTimeout(long timeout);

	public boolean isAutoStart();

	public void setAutoStart(boolean autoStart);

	public String getCluster();

	public void setCluster(String cluster);

	public String getPassword();

	public void setPassword(String password);

	public String getUsername();

	public void setUsername(String username);

	public ClusterUtil accessUtil();

	public int getMaxNodes();

	public void setMaxNodes(int maxNodes);

	public String getClusterFactory();

	public void setClusterFactory(String clusterFactory);

	public void setKeepRunning(boolean value);

	public boolean isKeepRunning();

	public String startNode();

	public long getLockingInterval();

	public void setLockingInterval(long lockingInterval);

	public boolean isTimeoutAdjustement();

	public void setTimeoutAdjustement(boolean timeoutAdjustement);
}
