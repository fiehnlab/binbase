/*
 * Created on Jul 13, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate.ClusterConfigService;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.ClusterException;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.status.EJBReportFactory;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;
import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;
import org.apache.log4j.Logger;

import javax.ejb.CreateException;
import javax.jms.IllegalStateException;
import javax.jms.*;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.File;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * an abstract node for calculation which listens for messages on a specified
 * queue if it doesnt get messages it dies after a while
 *
 * @author wohlgemuth
 * @version Jul 13, 2006
 */
public abstract class AbstracNode implements Runnable {

    /**
     * did we kill this node already
     */
    private boolean killed = false;

    private boolean shutdownComplete = true;

    private boolean inShutdownProcess = false;

    /**
     * needed to react on a strg-c
     *
     * @author wohlgemuth
     * @version Jul 25, 2006
     */
    private class ShutDownHook extends Thread {
        public ShutDownHook() {
            setName("shutdown hook");
        }

        @Override
        public void run() {
            logger.warn("shutdown request received!");
            if (running == true) {
                kill();

                logger.info("waiting for cleanup...");
                try {
                    Thread.sleep(15000);
                } catch (InterruptedException e) {
                }

            } else {
                logger.info("ignored node was not running!");
                logger.info("waiting for cleanup...");
                try {
                    Thread.sleep(15000);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    /**
     * shuts down the system in case of an emergency
     *
     * @author wohlgemuth
     * @version Aug 3, 2006
     */
    private void kill() {
        if (killed == false) {
            logger.info("emergency shutdown was forced by user!");
            logger.info("killing node");
            report.report(getName(), Reports.STOP, NodeReports.NODE);

            if (currentMessage != null) {
                logger.info("send message back");
                try {
                    sendMessageBack(currentMessage);
                } catch (final Exception e) {
                    logger.error(e.getMessage(), e);
                }

            }
            logger.info("calling post shutdown actions...");
            shutdownCompleteAction();
            logger.info("done");
            killed = true;
        }
    }

    private final Logger logger = Logger.getLogger(getClass());

    private String name = "node: ";

    /**
     * needed to stop the node from running
     */
    private boolean running = true;

    /**
     * our properties
     */
    private Properties properties;

    /**
     * access to the report engine
     */
    private Report report;

    /**
     * the message we are currently working one
     */
    private Message currentMessage;

    private boolean singleRun = false;
    private MessageProducer producer;
    private QueueSession session;
    private Queue queue;
    private QueueConnection queueConnection;
    private QueueReceiver receiver;

    /**
     * constructor
     *
     * @throws Exception
     * @author wohlgemuth
     * @version Jul 31, 2006
     */
    public AbstracNode() throws Exception {
        logger.info("creating abstract node...");
        running = false;

        final XMLConfigurator config = XMLConfigurator.getInstance();

        try {
            logger.info("loading configuration...");

            config.addConfiguration(new FileSource(new File(
                    "config/applicationServer.xml")));

            setProperties(config.getProperties());
        } catch (final Exception e) {
            logger.info("warn: " + e.getMessage());

            try {
                logger.info("loading user home configuration...");
                config
                        .addConfiguration(new FileSource(new File(System
                                .getProperty("user.home")
                                + File.separatorChar
                                + ".config/applicationServer.xml")));
                setProperties(config.getProperties());
            } catch (final Exception ex) {
                logger.info(ex.getMessage());

            }
        }

    }

    /**
     * access to the configuration
     *
     * @return
     * @throws NamingException
     * @author wohlgemuth
     * @version Jul 31, 2006
     */
    protected ClusterConfigService getConfig() throws NamingException,
            RemoteException, CreateException {
        return Configurator.getConfigService();
    }

    /**
     * name of the node
     *
     * @return
     * @author wohlgemuth
     * @version Jul 14, 2006
     */
    public String getName() {
        if (name == null) {
            name = getClass().getName();
        }
        return name;
    }

    /**
     * propertoes of this node
     *
     * @return
     * @author wohlgemuth
     * @version Jul 31, 2006
     */
    public Properties getProperties() {

        logger.debug("using properties:");

        if (properties == null) {
            properties = new Properties();
        }
        for (final Object s : properties.keySet()) {
            logger.debug(s + " - " + properties.getProperty(s.toString()));
        }

        return properties;
    }

    /**
     * needed to initialize our queue
     *
     * @author wohlgemuth
     * @version Jul 13, 2006
     */
    protected abstract Queue initializeQueue(InitialContext context)
            throws Exception;

    /**
     * what are we doing when we get a new message
     *
     * @param message
     * @author wohlgemuth
     * @version Jul 13, 2006
     */
    protected abstract void onMessage(Message message) throws Exception;

    /**
     * here happens the action
     *
     * @author wohlgemuth
     * @version Jul 13, 2006
     * @see java.lang.Runnable#run()
     */
    public final void run() {
        this.setName(this.name);

        logger.info("enter run method");
        shutdownComplete = false;
        inShutdownProcess = false;

        logger.info("connect to reports");

        try {
            report = ReportFactory
                    .newInstance(EJBReportFactory.class.getName()).create(
                            getProperties(), getName());
        } catch (Throwable e) {
            logger.fatal("error with report generation: " + e.getMessage(), e);
            this.shutdown();
            return;
        }

        try {


            logger.info("enter startup phase");
            report.report(getName(), NodeReports.STARTUP, NodeReports.NODE);

            logger.info("enter running phase...");
            setRunning(true);

            while (isRunning()) {

                System.gc();
                logger.info("initialize connection to queue");
                openSession();

                logger.info("waiting for message");
                Message message = null;

                try {
                    message = receiver.receive(getIddleTime());
                } catch (final IllegalStateException e) {
                    logger.info("error, tyring again: " + e.getMessage());
                    closeSession();
                    openSession();

                    logger.info("waiting for message");
                    message = receiver.receive(getIddleTime());
                }

                currentMessage = message;

                if (message != null) {

                    // handle message
                    try {

                        report.report(getName(), NodeReports.RECEIVE_MESSAGE,
                                NodeReports.NODE);
                        logger.info("received message");
                        try {
                            onMessage(message);
                        } catch (final ClassNotFoundException ex) {
                            logger
                                    .warn(
                                            "looks like the handler class was not found, please make sure that the responding jar was deployed on the cluster!",
                                            ex);
                            throw ex;
                        }
                        logger.info("finished message");
                        currentMessage = null;
                        report.report(getName(), NodeReports.FINISHED_MESSAGE,
                                NodeReports.NODE);
                    } catch (final Throwable e) {
                        logger.error(e.getMessage(), e);
                        report.report(getName(), Reports.FAILED,
                                NodeReports.NODE, e.getMessage());

                        if (message instanceof ObjectMessage) {
                            logger.info("it's an object message so we can recreate it and put it to the end of the queue...");
                            ObjectMessage m = (ObjectMessage) message;

                            sendMessageBack(m.getObject(), m.getJMSPriority());

                        } else {
                            logger.info("sending message back...");
                            sendMessageBack(message);
                        }
                    }

                    if (isSingleRun()) {
                        setRunning(false);
                        shutdown();
                    }
                } else {
                    logger.info("no message received, quitting node");
                    setRunning(false);
                }

                if (Configurator.getConfigService().isKeepRunning() == false) {
                    logger
                            .info("nodes are not suppsoed to be kept allive, shutdown initialized...");
                    shutdown();
                }
                logger.info("shutdown requested: " + isInShutdownProcess());
                if (isInShutdownProcess()) {
                    setRunning(false);
                }

                System.gc();
                // let the thread sleep a little
                logger.debug("sleep for: " + getIddleTime());
                Thread.sleep(getIddleTime());
            }

            logger.info("left running loop...");
        } catch (final Exception e) {
            logger.error(e.getMessage(), e);
            report.report(getName(), Reports.FAILED, NodeReports.NODE, e);
            logger.info("fire shutdown!");
            shutdown();

        } catch (final Throwable e) {
            logger.error(e.getMessage(), e);
            report.report(getName(), Reports.FAILED, NodeReports.NODE);
            logger.info("fire shutdown!");
            shutdown();

        }
        logger.info("shutdown complete");
        report.report(getName(), Reports.STOP, NodeReports.NODE);

        shutdownComplete = true;
        inShutdownProcess = false;
        logger.info("shudown complete: " + shutdownComplete);

        shutdownCompleteAction();
    }

    /**
     * return the iddle time of the node in ms
     *
     * @return
     * @throws RemoteException
     * @throws ClusterException
     * @throws NamingException
     * @throws CreateException
     */
    public long getIddleTime() throws RemoteException, ClusterException,
            NamingException, CreateException {
        final long iddle = getConfig().getIddleTime();
        logger.debug("iddle time is: " + iddle);
        return iddle;
    }

    /**
     * executed when the shutdown is complete
     */
    protected  void shutdownCompleteAction() {
        logger.info("shutdown complete action called...");
        try {
            closeSession();
        } catch (Exception e) {
            logger.warn(e.getMessage(), e);
        }
    }

    public boolean isSingleRun() {
        return singleRun;
    }

    public void setSingleRun(final boolean single) {
        singleRun = single;
    }

    /**
     * sends a message back to the queue
     *
     * @param message
     * @author wohlgemuth
     * @version Jul 13, 2006
     */
    protected void sendMessageBack(final Message message) throws Exception {
        logger.info("send message back");

        if (session == null) {
            openSession();
        }

        producer.send(message);
        ;
    }

    private void sendMessageBack(Serializable object, int jmsPriority)
            throws NamingException, Exception {
        logger.info("send message back");

        if (session == null) {
            openSession();
        }

        this.producer.setPriority(jmsPriority);

        ObjectMessage message = session.createObjectMessage();
        message.setObject(object);

        logger.debug("send message: " + message + "with priority " + jmsPriority);
        this.producer.send(message);

    }

    protected void openSession() throws NamingException, JMSException,
            Exception {
        try {
            logger.debug("closing session before we open it, you never know...");

            closeSession();
        } catch (Exception e) {
            logger.debug(e.getMessage(), e);
        }
        final InitialContext ctx = new InitialContext(getProperties());

        final QueueConnectionFactory qcf = (QueueConnectionFactory) ctx
                .lookup("QueueConnectionFactory");
        queueConnection = qcf.createQueueConnection();

        queue = initializeQueue(ctx);
        logger.info("connect to queue: " + queue.getQueueName());

        session = queueConnection.createQueueSession(false,
                Session.AUTO_ACKNOWLEDGE);

        logger.info("create producer");
        producer = session.createProducer(queue);

        logger.info("create receiver");
        receiver = session.createReceiver(queue);

        logger.info("start queue");
        queueConnection.start();

    }

    protected void closeSession() throws JMSException {
        if (receiver != null) {
            receiver.close();
        }

        if (session != null) {
            session.close();
        }

        if (producer != null) {
            producer.close();
        }

        if (queueConnection != null) {
            queueConnection.close();
        }


        session = null;
        producer = null;
        receiver = null;
        queueConnection = null;
    }

    /**
     * sets the idle time for the node, if the node doesnt get data after this
     * time it dies
     *
     * @param time
     * @author wohlgemuth
     * @version Jul 13, 2006
     */
    protected void setIdleTime(final long time) {
        try {
            getConfig().setIddleTime(time);
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * the name of the node
     *
     * @param name
     * @author wohlgemuth
     * @version Jul 31, 2006
     */
    public void setName(final String name) {
        logger.info("node name was set to: " + name);
        this.name = name;
        Thread.currentThread().setName(name);
    }

    /**
     * set the needed properties, is internal done by the xml configurator
     *
     * @param properties
     * @author wohlgemuth
     * @version Jul 31, 2006
     */
    protected void setProperties(final Properties properties) {

        if (this.properties == null) {
            this.properties = new Properties();
        }
        for (final Object s : properties.keySet()) {
            logger.info("setting property: " + s + " - "
                    + properties.getProperty(s.toString()));
            this.properties.put(s, properties.getProperty(s.toString()));
        }
    }

    /**
     * starts the node and if multithreading is available and allowed we start
     * multiple instances
     *
     * @author wohlgemuth
     * @version Jul 14, 2006
     */
    public final void start() {
        if (canStartCheck()) {

            logger.info("registering shutdown hook");
            Runtime.getRuntime().addShutdownHook(new ShutDownHook());

            logger.debug("calculate job id");

            if (getName().equals("node: ")) {
                if (System.getProperty("JOB_ID") == null) {
                    setName(getHostName() + " - " + System.currentTimeMillis());
                } else if (System.getenv().get("JOB_ID") != null) {
                    setName(System.getenv("JOB_ID"));
                } else {
                    setName(System.getProperty("JOB_ID"));
                }
            }

            logger.info("starting actual calcation thread...");

            ExecutorService exec = Executors.newSingleThreadExecutor();
            exec.execute(this);
            exec.shutdown();
            try {
                exec.awaitTermination(5000, TimeUnit.DAYS);
            } catch (InterruptedException e) {
                logger.error(e.getMessage(), e);
            }
        } else {
            throw new RuntimeException("sorry can't start another node!");
        }
    }

    protected boolean canStartCheck() {
        logger.info("checking if I can start...");
        return true;
    }

    /**
     * host name of this node
     *
     * @return
     * @author wohlgemuth
     * @version Jul 31, 2006
     */
    private String getHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (final UnknownHostException e) {
            return e.getMessage();
        }
    }

    /**
     * thread associated to this node
     *
     * @return
     * @author wohlgemuth
     * @version Jul 31, 2006
     */
    protected Thread getThread() {
        return Thread.currentThread();
    }

    /**
     * is this node running
     *
     * @return
     * @author wohlgemuth
     * @version Jul 31, 2006
     */
    public final boolean isRunning() {
        logger.debug("checking if node is running: " + running);
        if (inShutdownProcess) {
            logger.info("shutdown was requested...");
            return false;
        }
        return running;
    }

    public void shutdown() {
        logger.debug("shutdown scheduled...");
        shutdownComplete = false;

        if (isRunning()) {
            setRunning(false);
        } else {
            logger.debug("was not running so call shutdown action directly...");
            shutdownCompleteAction();
        }
    }

    protected final void setRunning(final boolean running) {
        if (running == false) {
            logger.info("initialize shutdown for node: "
                    + AbstracNode.this.getName());
            inShutdownProcess = true;
        }
        this.running = running;
    }

    /**
     * report associated with this node
     *
     * @return
     * @author wohlgemuth
     * @version Jul 31, 2006
     */
    public Report getReport() {
        return report;
    }

    public boolean isShutdownComplete() {
        return shutdownComplete;
    }

    public boolean isInShutdownProcess() {
        return inShutdownProcess;
    }

    @Override
    protected void finalize() throws Throwable {
        logger.debug("finalizing node...");
        super.finalize();
    }
}
