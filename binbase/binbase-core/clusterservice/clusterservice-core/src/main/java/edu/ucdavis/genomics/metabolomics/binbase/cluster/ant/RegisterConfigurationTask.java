/*
 * Created on Aug 2, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.ant;

import java.io.File;

import org.apache.tools.ant.Project;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;

/**
 * is used to upload new configurations to the project
 * @author wohlgemuth
 * @version Aug 2, 2006
 *
 */
public class RegisterConfigurationTask extends ClusterUploadLibraryTask{

	@Override
	protected void upload(ClusterUtil util, File file) throws Exception {
		if(file.isFile()){
			if(file.exists()){
				log("upload file: " + file.toString(),Project.MSG_VERBOSE);
				
				FileSource source = new FileSource(file);
				util.registerConfiguration(source.getSourceName(), source);				
			}
		}	}

}
