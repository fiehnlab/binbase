/*
 * Created on Aug 8, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.ant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.types.DataType;
import org.apache.tools.ant.types.Reference;

/**
 * collection of dataobjects
 * 
 * @author wohlgemuth
 * @version Aug 8, 2006
 * 
 */
public class Configuration extends DataType implements Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * collection of parameters
	 */
	private Vector<ClusterParameter> content = new Vector<ClusterParameter>();

	/**
	 * 
	 * @author wohlgemuth
	 * @version Aug 8, 2006
	 * @param project
	 */
	public Configuration(Project project) {
		this.setProject(project);
	}

	/**
	 * adds a configuration parameter
	 * 
	 * @author wohlgemuth
	 * @version Aug 8, 2006
	 * @param parameter
	 */
	public void addParameter(ClusterParameter parameter) {
		if (isReference()) {
			throw noChildrenAllowed();
		}
		this.content.add(parameter);
	}

	/**
	 * 
	 * @author wohlgemuth
	 * @version Aug 8, 2006
	 * @return
	 */
	public Vector<ClusterParameter> getContent() {
		if (this.isReference()) {
			Object o = this.getCheckedRef(Configuration.class, "config");
			this.content = ((Configuration) o).getContent();
		}

		return content;
	}

	@Override
	public String toString() {
		if (this.isReference()) {
			Object o = this.getCheckedRef(Configuration.class, "config");
			this.content = ((Configuration) o).getContent();
		}

		return content.toString();
	}

	public void setRefid(Reference arg0) {
		if (!this.content.isEmpty()) {
			throw tooManyAttributes();
		}
		super.setRefid(arg0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object clone() throws CloneNotSupportedException {
		try {
			Configuration config = (Configuration) super.clone();
			config.content = (Vector<ClusterParameter>) content.clone();

			return config;
		} catch (CloneNotSupportedException e) {
			throw new BuildException(e);
		}
	}

	/**
	 * load properties from file
	 * 
	 * @author wohlgemuth
	 * @version Aug 9, 2006
	 * @param fromFile
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public void setFromFile(File fromFile) throws FileNotFoundException, IOException {
		log("using file: " + fromFile);
		Properties p = new Properties();
		p.load(new FileInputStream(fromFile));

		Iterator it = p.keySet().iterator();

		while (it.hasNext()) {
			String name = (String) it.next();
			ClusterParameter parameter = new ClusterParameter(getProject());
			parameter.setName(name.trim());
			parameter.setValue(p.getProperty(name).trim());
			this.addParameter(parameter);
		}
	}
}
