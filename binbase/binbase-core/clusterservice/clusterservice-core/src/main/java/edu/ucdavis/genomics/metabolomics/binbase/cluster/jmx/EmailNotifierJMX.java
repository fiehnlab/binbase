package edu.ucdavis.genomics.metabolomics.binbase.cluster.jmx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

/**
 * does the general email notification configuration
 * @author wohlgemuth
 *
 */
public class EmailNotifierJMX implements EmailNotifierJMXMBean {
	private Logger logger = Logger.getLogger(getClass());

	private List<String> emailAdress = new ArrayList<String>();
	private String username;
	private String server;
	private Integer port;
	private String password;

	@Override
	public void addEmailAddress(String address) {
		this.getEmailAddress().add(address);
		store();

	}

	@Override
	public List<String> getEmailAddress() {
		return emailAdress;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public Integer getPort() {
		return port;
	}

	@Override
	public String getServer() {
		return server;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public void setPassword(String password) {
		this.password = password;
		store();

	}

	@Override
	public void setPort(Integer port) {
		this.port = port;
		store();

	}

	@Override
	public void setServer(String server) {
		this.server = server;
		store();

	}

	@Override
	public void setUsername(String username) {
		this.username = username;
		store();

	}

	@Override
	public String getSenderAddress() {
		return this.getUsername();
	}

	@Override
	public Boolean isSSLRequired() {
		return true;
	}

	@Override
	public void postDeregister() {
	}

	@Override
	public void postRegister(Boolean arg0) {
		try {
			File file = new File(getClass().getName() + ".properties");
			if (!file.exists()) {
				return;
			}

			ObjectInputStream in = new ObjectInputStream(new FileInputStream(
					file));
			this.emailAdress = (List<String>) in.readObject();
			this.username = in.readUTF();
			this.password = in.readUTF();
			this.port = in.readInt();
			this.server = in.readUTF();

			in.close();

		} catch (Exception e) {
			logger.debug("postRegister(Boolean)", e); //$NON-NLS-1$
		}
	}

	@Override
	public void preDeregister() throws Exception {
		this.store();
	}

	@Override
	public ObjectName preRegister(MBeanServer arg0, ObjectName arg1) throws Exception {
		return null;
	}
	
	
	private void store() {
		try {
			File file = new File(getClass().getName() + ".properties");

			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(file));
			
			out.writeObject(this.emailAdress);
			out.writeUTF(this.username);
			out.writeUTF(this.password);
			out.writeInt(this.port);
			out.writeUTF(this.server);
			
			out.flush();
			out.close();

		} catch (Exception e) {
			logger.error(e.getMessage(), e); //$NON-NLS-1$
		}
	}

}
