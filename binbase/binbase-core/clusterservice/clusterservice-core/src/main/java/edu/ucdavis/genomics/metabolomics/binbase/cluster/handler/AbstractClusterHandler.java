/*
 * Created on Jul 14, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.handler;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterHandler;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.StoringException;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.node.CalculationNode;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.Scheduler;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.Storage;
import edu.ucdavis.genomics.metabolomics.util.status.Report;
import edu.ucdavis.genomics.metabolomics.util.status.ReportType;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;

/**
 * abstract cluster handler with utils to make live easier
 * 
 * @author wohlgemuth
 * 
 */
public abstract class AbstractClusterHandler implements ClusterHandler {

	private static int counter = 0;

	private CalculationNode node;

	private Logger logger = Logger.getLogger(getClass());

	private Serializable o = "<>";

	private Serializable result = null;

	/**
	 * used report for communications
	 */
	public void setReport(Report report) {
		this.report = report;
	}

	private List<Serializable> subJobQueue = new Vector<Serializable>();

	private int subJobPriority = Scheduler.PRIORITY_ASAP;

	/**
	 * starts the calculation and takes care of standard handling of
	 * communications.
	 */
	public final boolean start() throws Exception {

		SubJob job = new SubJob(this.getObject());
		
		try {
			if (this.getObject() instanceof SubJob) {
				throw new StoringException("you can't use a subjob as an object!");
			}

			if(this.hasObject(job)){
				logger.info("this is a sub job: " + job);
			}
			else{
				logger.info("this is a normal job: " + job);
				job.setObject(this.getObject());
				this.storeObject(job);
			}
			try {
				return startProcessing();
			} catch (Exception e) {
				getReport().report(getObject(), Reports.FAILED, Reports.JOB, e);
				throw e;
			}

		} finally {
			this.deleteObject(job);
			if (this.getResult() == null) {
				getReport().report(getObject(), Reports.DONE, Reports.JOB);
			} else {
				logger.info("a result was set so attach it to the notification");
				ReportType type = new ReportType(Reports.JOB.getName(), Reports.JOB.getDescription());
				type.setAttachement(this.getResult());
				getReport().report(getObject(), Reports.DONE, type);
			}
		}
	}

	/**
	 * starts the proccessing
	 * 
	 * @return
	 */
	protected abstract boolean startProcessing() throws Exception;

	private Properties p;

	private Report report;

	public Properties getProperties() {
		return p;
	}

	public Report getReport() {
		return report;
	}

	public void setObject(Serializable o) {
		this.o = o;
	}

	public Serializable getObject() {
		return o;
	}

	public void setProperties(Properties p) {
		this.p = p;
	}

	/**
	 * store an object in the associated cluster database
	 * 
	 * @param key
	 * @param object
	 * @throws StoringException
	 */
	private void storeObject(SubJob job) throws StoringException {
		logger.debug("storing in  storage: \"" + job);

		Storage.getInstance().put(job.generateKey(), job.getObject());
		if (Storage.getInstance().has(job.generateKey()) == false) {
			throw new StoringException("storage was not successfull!");
		}
	}

	/**
	 * deletes an object from the associated database
	 * 
	 * @param key
	 * @throws StoringException
	 */
	private void deleteObject(SubJob sub) throws StoringException {
		logger.debug("removing from storage: \"" + sub.generateKey() + "\" - " + sub);

		try {
			Storage.getInstance().remove(sub.generateKey());
		} catch (StoringException e) {
			logger.warn(e.getMessage());
		}
		if (Storage.getInstance().has(sub.generateKey())) {
			throw new StoringException("removal was not successfull! - " + sub);
		}
		else{
			logger.debug("removal was successfull: " + sub);
		}
	}

	/**
	 * checks for an object in the associated database
	 * 
	 * @param key
	 * @return
	 * @throws StoringException
	 */
	private boolean hasObject(SubJob sub) throws StoringException {
		logger.debug("checking for sub job: " + sub.generateKey());
		return Storage.getInstance().has(sub.generateKey());
	}

	/**
	 * starts a related subjob
	 * 
	 * @param name
	 * @param object
	 * @throws Exception
	 */
	public void startSubJob(Serializable object) throws Exception {
		if (this.subJobQueue.contains(object)) {
			throw new JobException("sorry a subjob with this property already exist!");
		}
		logger.info("create sub job");
		this.storeObject(new SubJob(object));

		Scheduler.schedule(object, getSubJobPriority());

		this.subJobQueue.add(object);

		this.getReport().report(object, Reports.SCHEDULE, Reports.SUB_JOB);

	}

	/**
	 * starts a related subjob
	 * 
	 * @param name
	 * @param object
	 * @throws Exception
	 */
	public void startSubJob(Serializable object, String handler) throws Exception {

		if (this.subJobQueue.contains(object)) {
			throw new JobException("sorry a subjob with this property already exist!");
		}
		this.storeObject(new SubJob(object));

		Scheduler.schedule(object, getSubJobPriority(), handler);

		this.subJobQueue.add(object);

		this.getReport().report(object, Reports.SCHEDULE, Reports.SUB_JOB);

	}

	/**
	 * checks if the sub job is finished
	 * 
	 * @param name
	 * @return
	 * @throws StoringException
	 */
	public boolean isSubJobFinished(Serializable object) throws StoringException {
		SubJob job = new SubJob(object);
		logger.debug("checking if job is finished: " + job );
		boolean result = this.hasObject(new SubJob(object));
		logger.debug("job still registered: " + result + " - finished: " + !result);
		return !result;
	}

	/**
	 * are any sub jobs running
	 * 
	 * @return
	 * @throws StoringException
	 */
	public synchronized boolean isSubJobRunning() throws StoringException {
		Iterator<Serializable> it = this.subJobQueue.iterator();

		while (it.hasNext()) {
			Serializable i = it.next();
			logger.debug("currently: " + i + " - sub job equals: " + new SubJob(i).generateKey());
			ReportType type = new ReportType(Reports.SUB_JOB.getName(), Reports.SUB_JOB.getDescription(), i);
			if (isSubJobFinished(i)) {
				logger.info("job is finished: " + i);
				this.getReport().report(this.getObject(), Reports.DONE, type);
				it.remove();
			} else {
				logger.info("job is still runing: " + i);
				this.getReport().report(this.getObject(), Reports.RUNNING, type);
			}
		}
		logger.info("sub jobs left: " + this.subJobQueue.size());

		return !this.subJobQueue.isEmpty();
	}

	/**
	 * waits till all sub jobs are done
	 * 
	 * @throws StoringException
	 * @throws InterruptedException
	 */
	public void waitForSubjobs() throws StoringException, InterruptedException {
		while (isSubJobRunning()) {
			this.getReport().report(this.getObject(), Reports.WAITING, Reports.SUB_JOB);
			Thread.sleep(5000);
		}
	}

	public int getSubJobPriority() {
		return subJobPriority;
	}

	public void setSubJobPriority(int subJobPriority) {
		this.subJobPriority = subJobPriority;
	}

	/**
	 * starts an additional node in the current jvm
	 * 
	 * @throws Exception
	 */
	public void startLocalNode() throws Exception {
		this.startLocalNode(false);
	}

	/**
	 * you can specify if the node stops after the first run
	 * 
	 * @param singlerun
	 * @throws Exception
	 */
	public void startLocalNode(boolean singlerun) throws Exception {
		this.startLocalNode(singlerun, null);
	}

	/**
	 * starts a node and you can limit it to a specific handler and a single run
	 * 
	 * @param singlerun
	 * @param handlerLimitation
	 * @throws Exception
	 */
	public void startLocalNode(boolean singlerun, String handlerLimitation) throws Exception {
		CalculationNode node = new CalculationNode();
		node.setSingleRun(singlerun);
		node.setLimitToHandler(handlerLimitation);
		node.setWrongHandlerShutdown(false);
		counter = counter + 1;
		node.setName(this.getNode().getName() + "-local-" + counter);
		node.start();
	}

	public CalculationNode getNode() {
		return node;
	}

	public void setNode(CalculationNode node) {
		this.node = node;
	}

	/**
	 * access to a storage
	 * 
	 * @return
	 */
	public Storage accessStorage() {
		return Storage.getInstance();
	}

	/**
	 * just to make sure that the ob
	 * 
	 * @author wohlgemuth
	 * 
	 */
	private class SubJob {
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof SubJob) {
				SubJob comp = (SubJob) obj;

				logger.debug(this + " vs " + obj);
				if (comp.getClass().equals(this.generateKey())) {
					if (comp.getObject().equals(this.getObject())) {
						return true;
					}
					else{
						logger.debug("objects are different");
					}
				}
				else{
					logger.debug("keys are different!");
				}
			}
			else{
				logger.debug("is not a subjob: " + obj);
			}
			return false;
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = 2L;

		private Serializable object;

		private String clazz;

		/**
		 * @param object
		 * @param clazz
		 */
		public SubJob(Serializable object) {
			super();
			this.object = object;
			this.clazz = object.getClass().getSimpleName();
			logger.info("key for this job is: " + this.generateKey());
		}

		public SubJob() {

		}

		public Serializable getObject() {
			return object;
		}

		public void setObject(Serializable object) {
			this.object = object;
		}

		public String getClazz() {
			return clazz;
		}

		public void setClazz(String clazz) {
			this.clazz = clazz;
		}

		public String generateKey() {
			return this.getClass().getSimpleName() + " - " + generateNumber();
		}

		@Override
		public String toString() {
			return this.getClass().getSimpleName() + " - " + this.getObject().toString();
		}

		/**
		 * generates a specific numbert for our object
		 * 
		 * @return
		 */
		private int generateNumber() {
			try {
				ByteArrayOutputStream ou = new ByteArrayOutputStream();
				ObjectOutputStream out = new ObjectOutputStream(ou);
				out.writeObject(object);
				out.flush();
				int unique = ou.toByteArray().length + object.toString().hashCode();
				out.close();
				return unique;
			} catch (Exception e) {
				logger.warn(e.getMessage());
				return object.hashCode();
			}
		}

		@Override
		public int hashCode() {
			return generateNumber();
		}
	}

	/**
	 * access the result of the handler
	 * 
	 * @return
	 */
	private final Serializable getResult() {
		return result;
	}

	/**
	 * public void the result for the handler can be set here
	 * 
	 * @param result
	 */
	protected final void setResult(Serializable result) {
		this.result = result;
	}
}
