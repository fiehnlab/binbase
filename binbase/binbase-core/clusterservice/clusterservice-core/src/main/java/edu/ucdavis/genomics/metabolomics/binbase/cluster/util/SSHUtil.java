/*
 * Created on Jul 18, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.util;

import com.jcraft.jsch.*;
import edu.ucdavis.genomics.metabolomics.util.io.source.ByteArraySource;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * helper class for ssh connections
 *
 * @author wohlgemuth
 * @version Jul 18, 2006
 */
public class SSHUtil {

    /**
     * executes a given command on a ssh server
     *
     * @param servername =
     *                   servername or ip
     * @param username   =
     *                   username
     * @param password   =
     *                   password
     * @param command    =
     *                   the command to execute
     * @param out        =
     *                   the outputstream for the console output of the command
     * @return the exit code
     * @throws JSchException
     * @throws IOException
     * @author wohlgemuth
     * @version Jul 18, 2006
     */
    public static int executeCommand(String servername, String username, String password, String command, OutputStream out) throws Exception {
        Session session = createSession(servername, username, password);
        try {

            return executeCommand(session, command, out);
        } finally {

            session.disconnect();
        }
    }

    /**
     * creates a session
     *
     * @param servername
     * @param username
     * @param password
     * @return
     * @throws Exception
     * @author wohlgemuth
     * @version Jul 31, 2006
     */
    private static Session createSession(String servername, String username, String password) throws Exception {

        Logger.getLogger(SSHUtil.class).info("connecting to: " + servername + " with user: " + username + " and password: " + password);
        JSch jsch = new JSch();

        Session session = jsch.getSession(username, servername, 22);
        session.setPassword(password);

        java.util.Hashtable<String, String> config = new java.util.Hashtable<String, String>();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);

        session.connect();
        return session;
    }

    /**
     * executes a command after a session was obtained
     *
     * @param session
     * @param command
     * @param out
     * @return
     * @throws Exception
     * @author wohlgemuth
     * @version Jul 27, 2006
     */
    public static int executeCommand(Session session, String command, OutputStream out) throws Exception {
        try {
            Channel channel = session.openChannel("exec");
            ((ChannelExec) channel).setCommand(command);
            channel.setInputStream(null);
            ((ChannelExec) channel).setErrStream(out);

            Logger logger = Logger.getLogger(SSHUtil.class);

            logger.debug("command: " + command);

            InputStream in = channel.getInputStream();

            ByteArrayOutputStream out2 = new ByteArrayOutputStream();

            int result = 0;
            channel.connect();
            byte[] tmp = new byte[1024];
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 1024);
                    if (i < 0)
                        break;
                    out.write(tmp, 0, i);
                    out2.write(tmp, 0, i);

                }
                if (channel.isClosed()) {
                    result = channel.getExitStatus();
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (Exception ee) {
                }
            }
            channel.disconnect();

            logger.debug("result: " + result);


            logger.debug("received output:\n\n " + new String(out2.toByteArray()) + "\n\n");


            out2.flush();
            out2.close();
            in.close();

            return result;
        } finally {
            session.disconnect();
        }
    }

    /**
     * @param servername  =
     *                    servername or ip
     * @param username    =
     *                    username
     * @param password    =
     *                    password
     * @param command     =
     *                    the command to execute
     * @param logger=logs the output
     * @return
     * @throws Exception
     * @author wohlgemuth
     * @version Jul 19, 2006
     */
    public static int executeCommand(String servername, String username, String password, String command, Logger logger) throws Exception {
        int ret = executeCommand(servername, username, password, command, new ByteArrayOutputStream());

        return ret;
    }


    /**
     * uploads a file over scp to a server
     *
     * @param source
     * @param session
     * @param filename
     * @return
     * @throws Exception
     * @author wohlgemuth
     * @version Jul 27, 2006
     */
    public static void scpToFile(Source source, Session session, String filename) throws Exception {
        // exec 'scp -t rfile' remotely
        String command = "scp -p -t " + filename;
        Channel channel = session.openChannel("exec");
        ((ChannelExec) channel).setCommand(command);

        // get I/O streams for remote scp
        OutputStream out = channel.getOutputStream();
        InputStream content = source.getStream();
        InputStream in = channel.getInputStream();

        channel.connect();

        int value = 0;
        if ((value = checkAck(in)) != 0) {
            throw new Exception(" return code was not as exspected: " + value);
        }

        long filesize = source.getStream().available();
        command = "C0644 " + filesize + " ";
        if (source.getSourceName().lastIndexOf('/') > 0) {
            command += source.getSourceName().substring(source.getSourceName().lastIndexOf('/') + 1);
        } else {
            command += source.getSourceName();
        }
        command += "\n";
        out.write(command.getBytes());
        out.flush();

        if ((value = checkAck(in)) != 0) {
            throw new Exception(" return code was not as exspected: " + value);
        }
        byte[] buf = new byte[1024];
        while (true) {
            int len = content.read(buf, 0, buf.length);
            if (len <= 0)
                break;
            out.write(buf, 0, len);
            out.flush();
        }

        // send '\0'
        buf[0] = 0;
        out.write(buf, 0, 1);
        out.flush();


        try{
        if ((value = checkAck(in)) != 0) {
            throw new Exception(" return code was not as exspected: " + value);
        }
        }
        finally{
            content.close();

            out.close();
            channel.disconnect();
            in.close();

            session.disconnect();
        }
    }

    /**
     * the source give us the remote file to read
     *
     * @param source
     * @param session
     * @param name
     * @throws Exception
     * @author wohlgemuth
     * @version Jul 27, 2006
     */
    public static Source scpFrom(Session session, String name) throws Exception {
        // exec 'scp -f rfile' remotely
        String command = "scp -f " + name;
        Channel channel = session.openChannel("exec");
        ((ChannelExec) channel).setCommand(command);

        // get I/O streams for remote scp
        OutputStream out = channel.getOutputStream();
        InputStream in = channel.getInputStream();
        ByteArrayOutputStream resultStream = new ByteArrayOutputStream();

        String file = null;

        channel.connect();

        byte[] buf = new byte[1024];

        // send '\0'
        buf[0] = 0;
        out.write(buf, 0, 1);
        out.flush();

        while (true) {
            int c = checkAck(in);
            if (c != 'C') {
                break;
            }

            // read '0644 '
            in.read(buf, 0, 5);

            long filesize = 0L;
            while (true) {
                if (in.read(buf, 0, 1) < 0) {
                    // error
                    break;
                }
                if (buf[0] == ' ')
                    break;
                filesize = filesize * 10L + (buf[0] - '0');
            }

            for (int i = 0; ; i++) {
                in.read(buf, i, 1);
                if (buf[i] == (byte) 0x0a) {
                    file = new String(buf, 0, i);
                    break;
                }
            }

            System.err.println(file);

            // send '\0'
            buf[0] = 0;
            out.write(buf, 0, 1);
            out.flush();

            // read a content of lfile
            int foo;
            while (true) {
                if (buf.length < filesize)
                    foo = buf.length;
                else
                    foo = (int) filesize;
                foo = in.read(buf, 0, foo);
                if (foo < 0) {
                    // error
                    break;
                }
                resultStream.write(buf, 0, foo);
                filesize -= foo;
                if (filesize == 0L)
                    break;
            }
            resultStream.close();

            if (checkAck(in) != 0) {
            }

            // send '\0'
            buf[0] = 0;
            out.write(buf, 0, 1);
            out.flush();
        }

        Source source = new ByteArraySource(resultStream.toByteArray());


        channel.disconnect();
        out.flush();
        out.close();
        in.close();

        session.disconnect();
        return source;

    }

    /**
     * checking for succes of the stream operation
     *
     * @param in
     * @return
     * @throws IOException
     * @author wohlgemuth
     * @version Jul 31, 2006
     */
    private static int checkAck(InputStream in) throws IOException {
        int b = in.read();
        // b may be 0 for success,
        // 1 for error,
        // 2 for fatal error,
        // -1
        if (b == 0)
            return b;
        if (b == -1)
            return b;

        if (b == 1 || b == 2) {
            StringBuffer sb = new StringBuffer();
            int c;
            do {
                c = in.read();
                sb.append((char) c);
            } while (c != '\n');
            if (b == 1) { // error
                throw new IOException(sb.toString());
            }
            if (b == 2) { // fatal error
                throw new IOException(sb.toString());
            }
        }
        return b;
    }

    public static void scpToFile(Source source, String servername, String username, String password, String filePathOnCluster) throws Exception {
        scpToFile(source, createSession(servername, username, password), filePathOnCluster);
    }

    public static Source scpFrom(String servername, String username, String password, String s) throws Exception {
        return scpFrom(createSession(servername, username, password), s);
    }
}
