package edu.ucdavis.genomics.metabolomics.binbase.cluster.util;

import java.util.Properties;

import org.junit.BeforeClass;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.AbstractClusterUtilTest;
import edu.ucdavis.genomics.metabolomics.util.PropertySetter;

/**
 * testing the general functionality
 * 
 * @author wohlgemuth
 * 
 */
public class RocksClusterImplementationTest extends AbstractClusterUtilTest {

	@BeforeClass
	public static void initializeCluster() throws Exception{
		Properties p = PropertySetter.setPropertiesToSystem("src/test/resources/test.properties");
		UTIL = 		new RocksClusterImplementation(System.getProperty("test.binbase.cluster.server"), System.getProperty("test.binbase.cluster.username"), System
				.getProperty("test.binbase.cluster.password"));
		UTIL.initializeCluster(System.getProperty("test.binbase.cluster.server"));

	}

}
