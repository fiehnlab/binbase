package edu.ucdavis.genomics.metabolomics.binbase.cluster.util;

import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.AbstractClusterUtilTest;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;

public class LocalCalculationUtilTest extends AbstractClusterUtilTest{

	@BeforeClass
	public static void initializeCluster() throws Exception{
		
		if(System.getProperty("test.binbase.cluster.server") == null){
			System.setProperty("test.binbase.cluster.server","127.0.0.1");
		}
		
		UTIL = 		new LocalCalculationUtil();
		
		UTIL.initializeCluster(System.getProperty("test.binbase.cluster.server"));
	}
	
	@Override
	public void setUp() throws Exception {
		UTIL.prepare();

		super.setUp();
		
		getLogger().info("specify custom iddle time for testing...");
		Configurator.getConfigService().setIddleTime(5000);
		
	}

	@Override
	protected int getPendingTime() {
		return 100;
	}

	@Test
	public void testExecuteCommand() throws Exception {
		String result = UTIL.executeCommand("ls $HOME");
		getLogger().info("result was: " + result);
		String[] res = result.split("\n");

		assertTrue(res.length >= 1);

	}

	@Override
	protected int getWaitTime() {
		return 100;
	}

}
