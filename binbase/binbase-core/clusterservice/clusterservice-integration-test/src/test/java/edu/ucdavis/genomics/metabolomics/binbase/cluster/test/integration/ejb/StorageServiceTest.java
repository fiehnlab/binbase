package edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.ejb;

import javax.naming.NamingException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageObject;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.StorageService;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.exception.StoringException;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;

import static org.junit.Assert.*;

/**
 * testing that the storage service works like excted
 * 
 * @author wohlgemuth
 * 
 */
public class StorageServiceTest extends AbstractApplicationServerTest {

	private StorageService service = null;

	public StorageServiceTest() {

	}

	@Before
	public void setUp() throws Exception {
		super.setUp();
		service = Configurator.getStorageService();

		assertTrue(service != null);
		service.clear();
		assertTrue(service.getSize() == 0);
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
		service.clear();
		assertTrue(service.getSize() == 0);

	}

	@Test
	public void testDeleteNonExistingObject() {
		// we exspect to get an exception if we delete a non exsiting object
		boolean works = false;
		try {
			assertTrue(service.getSize() == 0);
			service.removeExistingObject("asjdklasdlasd");
			works = false;
		} catch (Exception e) {
			logger.info("exspected error: " + e.getMessage());
			works = true;
		}
		assertTrue(works);

		// we exspect to get no exception if we delete a non exsiting object
		try {
			assertTrue(service.getSize() == 0);
			service.removeObject("asjdklasdlasd");
			works = true;
		} catch (Exception e) {
			e.printStackTrace();
			works = false;
		}
		assertTrue(works);
	}

	@Test
	public void testIfNonExistingObjectsExist() throws StoringException{
		assertTrue(service.getSize() == 0);
		assertTrue(service.hasObject("sdasfdsgv") == false);
		assertTrue(service.hasObject("dad") == false);
		assertTrue(service.hasObject("sdaasxzcsfdsgv") == false);
		assertTrue(service.hasObject("sdascdaszxcasfdsgv") == false);
		assertTrue(service.hasObject("sdasfasdxewfgdsgv") == false);
		
	}
	@Test
	public void testStorageFuntionality() throws NamingException, StoringException {

		// testing storing objects
		for (int i = 0; i < getObjectCount(); i++) {
			service.store(String.valueOf(i), new StorageObject(i));
			assertTrue(service.getSize() == i + 1);
		}
		assertTrue(service.getSize() == getObjectCount());

		// testing of objects exist
		for (int i = 0; i < getObjectCount(); i++) {
			assertTrue(service.hasObject(String.valueOf(i)));
		}

		// testing deleting objects
		for (int i = 0; i < getObjectCount(); i++) {
			int countBefore = service.getSize();
			service.removeObject((String.valueOf(i)));
			int countAfter = service.getSize();

			assertTrue(countBefore - 1 == countAfter);
		}

	}

	/**
	 * how many objects we want to test
	 * 
	 * @return
	 */
	protected int getObjectCount() {
		return 50;
	}
}
