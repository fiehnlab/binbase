package edu.ucdavis.genomics.metabolomics.util.status.notify.twitter;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.exception.NotificationException;
import edu.ucdavis.genomics.metabolomics.util.status.notify.Notifier;
import edu.ucdavis.genomics.metabolomics.util.status.notify.PriotizedNotifier;
import edu.ucdavis.genomics.metabolomics.util.status.priority.Priority;

public class TwitterImplementationTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testNotifyStringPriority() throws NotificationException, InterruptedException {

		TwitterConfiguration configuration = new TwitterConfiguration() {
			
			@Override
			public String getUserName() {
				return "binbasetest"; 
			}
			
			@Override
			public String getPassword() {
				return "binbase-test";
			}
		};
		
		Notifier notifier = new TwitterImplementation(configuration);
		notifier.notify("binbase api twitter test - " + new Date());
		

		Thread.currentThread().sleep(5000);
	}

	@Test
	public void testNotifyStringPriority2() throws NotificationException, InterruptedException {

		TwitterConfiguration configuration = new TwitterConfiguration() {
			
			@Override
			public String getUserName() {
				return "binbasetest";
			}
			
			@Override
			public String getPassword() {
				return "binbase-test";
			}
		};
		
		PriotizedNotifier notifier = new TwitterImplementation(configuration);
		notifier.notify("binbase api twitter test" + new Date(),Priority.ERROR);
		

		Thread.currentThread().sleep(5000);
	}

}
