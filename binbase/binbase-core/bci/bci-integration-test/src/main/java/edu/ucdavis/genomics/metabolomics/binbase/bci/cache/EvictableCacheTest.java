package edu.ucdavis.genomics.metabolomics.binbase.bci.cache;


import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.CacheException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.policies.SizeEvitionPolicy;

public abstract class EvictableCacheTest extends CacheTest{

	@Before
	public void setUp() throws Exception {
		super.setUp();
		this.getCache().clear();
		this.getCache().getPolicies().clear();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * adds a policy
	 * @param policy
	 * @throws Exception 
	 */
	@Test
	public void testAddEvictionPolicy() throws Exception{
		getCache().addEvictionPolicy(new SizeEvitionPolicy(20));
		assertTrue(this.getCache().getPolicies().size() == 1);
	}
	
	/**
	 * removes a policy
	 * @param policy
	 * @throws Exception 
	 */
	@Test
	public void testRemoveEvictionPolicy() throws Exception{
		EvictionPolicy policy = new SizeEvitionPolicy(20);
		getCache().addEvictionPolicy(policy);
		assertTrue(this.getCache().getPolicies().size() == 1);
		getCache().removeEvictionPolicy(policy);
		assertTrue(this.getCache().getPolicies().size() == 0);
	}
	
	/**
	 * return all policies
	 * @return
	 * @throws Exception 
	 */
	@Test
	public void testGetPolicies() throws Exception{
		EvictionPolicy policy = new SizeEvitionPolicy(20);
		getCache().addEvictionPolicy(policy);
		assertTrue(this.getCache().getPolicies().size() == 1);
		getCache().removeEvictionPolicy(policy);
		assertTrue(this.getCache().getPolicies().size() == 0);		
	}

	/**
	 * evicts the given entry
	 * @param entry
	 * @throws Exception 
	 * @throws CacheException 
	 */
	@Test
	public void testEvict() throws CacheException, Exception{
		EvictionPolicy policy = new SizeEvitionPolicy(20);
		this.getCache().addEvictionPolicy(policy);
		for(int i = 0; i < 5000; i++){
			this.getCache().put("key_"+i, i);
			assertTrue(this.getCache().contains("key_"+i));
		}
		System.out.println(this.getCache().size());
		assertTrue(this.getCache().size() == 20);
	}
	
	protected abstract EvictableCache getCache() throws Exception;
}
