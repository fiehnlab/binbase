package edu.ucdavis.genomics.metabolomics.binbase.bci;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;

import javax.jms.ObjectMessage;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.XmlDataSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.Scheduler;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SchedulerFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.job.SchedulerJob;
import edu.ucdavis.genomics.metabolomics.binbase.bci.job.SchedulerJobHandler;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyFactory;

/**
 * test the complete integration of the scheduler in the cluster the task is to
 * schedule an import and wait till the import/export is done.
 * 
 * @author wohlgemuth
 * 
 */
public abstract class AbstractSchedulerIntegrationTest extends
		BinBaseClusterConfiguredIntegrationTest {

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		File file = new File("src/test/resources/xml/minimum-rtx5.xml");
		assertTrue(file.exists());
		return new XmlDataSet(new FileInputStream(file));
	}

	/**
	 * 60 seconds should be more than enough time to schedule 5 samples
	 * 
	 * @throws Exception
	 */
	@Test(timeout = 1800000)
	public void testImportScheduling() throws Exception {
		Scheduler scheduler = SchedulerFactory.createFactory()
				.createScheduler();

		// create fictional class
		ExperimentClass clazz = new ExperimentClass();
		clazz.setId("test");
		clazz.setColumn("test");

		// create fictional samples
		int count = 5;
		ExperimentSample samples[] = new ExperimentSample[count];
		for (int i = 1; i <= count; i++) {
			samples[i - 1] = new ExperimentSample();
			String name = "";
			if (i < 10) {
				name = "080101abcdsa0" + i;
			} else {
				name = "080101abcdsa" + i;
			}
			name = name + "_1";

			assertTrue(this.createTestFile(name, "txt").exists());
			logger.info("file name is: " + name);
			samples[i - 1].setName(name);
			samples[i - 1].setId("sx_" + i);
		}

		// assign samples to class
		clazz.setSamples(samples);

		// class is scheduled
		scheduler.scheduleImport(clazz, KeyFactory.newInstance().getKey());

		// make sure that the queue contains now 1 class to schedule
		assertTrue(this.getMessageQueue().size() == 1);

		// lets register the handler explicit at the jmx
		Configurator.getConfigService().addHandler(
				SchedulerJob.class.getName(),
				SchedulerJobHandler.class.getName());

		// lets start a node to take care of the queue
		UTIL.startNode();

		logger.info("waiting that the pending queue is empty");
		UTIL.waitTillPendingQueueCurrentUserIsEmpty();
		logger.info("waiting for scheduling, it should be done in 30 seconds more or less");
		Thread.sleep(30000);
		logger.info("killing all jobs!");
		UTIL.killJobOfCurrentUser();

		logger.info("waiting that the running queue is empty");
		UTIL.waitTillQueueCurrentUserIsEmpty();

		//we should have now one new calculation in the queue
		assertTrue(this.getMessageQueue().size() == 1);


		//check if the scheduled content is right

		// make sure message is of right instance
		assertTrue(this.getMessageQueue().get(0) instanceof ObjectMessage);
		ObjectMessage message = (ObjectMessage) this.getMessageQueue().get(0);

		logger.info("received: " + message.getObject());
		assertTrue(message.getObject() instanceof ExperimentClass);

		// validate the actual content
		ExperimentClass clazz2 = (ExperimentClass) message.getObject();

		assertTrue(clazz2.getColumn().equals(clazz.getColumn()));
		assertTrue(clazz2.getId().equals(clazz.getId()));
		assertTrue(clazz2.getSamples().length == count);

		// done
	}

	/**
	 * 60 seconds should be more than enough to schedule an experiment
	 * 
	 * @throws Exception
	 */
	@Test(timeout = 1800000)
	public void testExportScheduling() throws Exception {
		Scheduler scheduler = SchedulerFactory.createFactory()
				.createScheduler();

		Experiment exp = new Experiment();
		exp.setColumn("test");
		exp.setId("test");

		// create fictional class
		ExperimentClass clazz = new ExperimentClass();
		clazz.setId("test");
		clazz.setColumn("test");

		// create fictional samples
		int count = 5;
		ExperimentSample samples[] = new ExperimentSample[count];
		for (int i = 1; i <= count; i++) {
			samples[i - 1] = new ExperimentSample();
			String name = "";
			if (i < 10) {
				name = "080101abcdsa0" + i;
			} else {
				name = "080101abcdsa" + i;
			}
			name = name + "_1";

			assertTrue(this.createTestFile(name, "txt").exists());
			logger.info("file name is: " + name);
			samples[i - 1].setName(name);
			samples[i - 1].setId("sx_" + i);
		}

		// assign samples to class
		clazz.setSamples(samples);

		// assign class to experiment
		exp.setClasses(new ExperimentClass[] { clazz });

		// make sure queue is empty
		assertTrue(this.getMessageQueue().size() == 0);

		// trigger export
		scheduler.scheduleExport(exp, KeyFactory.newInstance().getKey());

		// make sure that the queue contains now 1 exp to schedule
		assertTrue(this.getMessageQueue().size() == 1);

		// lets register the handler explicit at the jmx
		Configurator.getConfigService().addHandler(
				SchedulerJob.class.getName(),
				SchedulerJobHandler.class.getName());

		// lets start a node to take care of the queue
		UTIL.startNode();

		logger.info("waiting that the pending queue is empty");
		UTIL.waitTillPendingQueueCurrentUserIsEmpty();
		logger.info("waiting for scheduling, it should be done in 30 seconds more or less");
		Thread.sleep(30000);
		logger.info("killing all jobs!");
		UTIL.killJobOfCurrentUser();
		logger.info("waiting that the running queue is empty");
		UTIL.waitTillQueueCurrentUserIsEmpty();

		//we should have now one new calculation in the queue
		assertTrue(this.getMessageQueue().size() == 1);

		// make sure message is of right instance
		assertTrue(this.getMessageQueue().get(0) instanceof ObjectMessage);

		// make sure content is of right instance
		ObjectMessage message = (ObjectMessage) this.getMessageQueue().get(0);
		logger.info("received: " + message.getObject());
		assertTrue(message.getObject() instanceof Experiment);

		// validate the actual content
		Experiment clazz2 = (Experiment) message.getObject();

		assertTrue(clazz2.getColumn().equals(exp.getColumn()));
		assertTrue(clazz2.getId().equals(exp.getId()));
		assertTrue(clazz2.getClasses().length == 1);
	}
}
