package edu.ucdavis.genomics.metabolomics.binbase.bci;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ServiceJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.io.Copy;

/**
 * to test against the application server without database connection
 * 
 * @author wohlgemuth
 * 
 */
public abstract class BinBaseConfiguredApplicationServerIntegrationTest extends
		AbstractApplicationServerTest {

	private String dataDir = null;

	private Logger logger = Logger.getLogger(getClass());

	@Override
	public void setUp() throws Exception {
		super.setUp();

		Configurator.getKeyManager().generateInternalKey();

		logger.info("disable automatic update features...");
		Configurator.getStatusService().setAutomaticBinBaseUpdate(false);
		Configurator.getStatusService().setAutomaticMetaDataUpdate(false);

		logger.info("configure cluster service...");
		// set jmx variables where to find the import directries
		ServiceJMXFacade importer = Configurator.getImportService();

		importer.setDisableServices(false);

		// configure cluster properties
		Configurator.getConfigService().setAutoStart(false);
		Configurator.getConfigService().setMaxNodes(2);
		Configurator.getConfigService().setCluster(
				System.getProperty("test.binbase.cluster.server"));
		Configurator.getConfigService().setUsername(
				System.getProperty("test.binbase.cluster.username"));
		Configurator.getConfigService().setPassword(
				System.getProperty("test.binbase.cluster.password"));
		importer.resetDatabases();
		importer.addDatabase(System.getProperty("Binbase.user"));
		importer.clearDirectorys();

		logger.info("clean test directory...");

		// creating accessable directory for tests
		File file = new File("target/test");
		file.mkdirs();

		assertTrue(file.exists());
		assertTrue(file.canRead());
		assertTrue(file.canWrite());

		// removing all data from the test directory
		File files[] = file.listFiles();
		for (File f : files) {
			f.delete();
		}

		importer.addDirectory(file.getAbsolutePath());
		importer.setValidateSources(true);

		logger.info("configure export service...");
		// set jmx variables where to find netcdf files and to setup the ftp
		// server
		ExportJMXFacade exporter = Configurator.getExportService();
		exporter.clearNetCDFDirectorys();

		exporter.addNetCDFDirectory(file.getAbsolutePath());
		exporter.addSopDir(file.getAbsolutePath());

		this.dataDir = file.getAbsolutePath();

		logger.info("set result directory");

		File dir = new File("target/content/result");
		dir.mkdirs();
		Configurator.getExportService().setResultDirectory(
				dir.getAbsolutePath());
		logger.info("result dir is: " + dir.getAbsolutePath());
		logger.info("done...");
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * a directory what the application server can access so put your files in
	 * here like import txt files or export txt files to run tests on them
	 * 
	 * @return
	 */
	public String getDataDir() {
		return dataDir;
	}

	/**
	 * creates a testfile in the datadir to provide us with some data
	 * 
	 * @param name
	 * @param content
	 * @param extension
	 * @return
	 * @throws IOException
	 */
	public File createTestFile(String name, String content, String extension)
			throws IOException {

		File file = new File(this.getDataDir() + File.separator + name + "."
				+ extension);
		FileWriter writer = new FileWriter(file);
		writer.write(content);
		writer.flush();
		writer.close();

		assertTrue(file.exists());
		return file;

	}

	/**
	 * creates a test file
	 * 
	 * @param name
	 * @param extension
	 * @return
	 * @throws IOException
	 */
	public File createTestFile(String name, String extension)
			throws IOException {
		return createTestFile(name, "<no content>", extension);
	}

	/**
	 * adds the given path to the list of allowed directories
	 * 
	 * @param file
	 * @throws NamingException
	 * @throws CreateException
	 * @throws BinBaseException
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	protected void addTestImportDirectory(File file) throws CreateException,
			NamingException, BinBaseException, FileNotFoundException,
			IOException {
		logger.info("adding directory: " + file.getAbsolutePath());
		logger.info("exist: " + file.exists());
		ServiceJMXFacade importer = Configurator.getImportService();

		importer.addDirectory(file.getAbsolutePath());

		// uploads all files in the given directory
		for (File f : file.listFiles()) {
			if (f.isFile()) {
				FileInputStream in = new FileInputStream(f);
				ByteArrayOutputStream out = new ByteArrayOutputStream();

				Copy.copy(in, out);

				importer.uploadImportFile(f.getName(), out.toByteArray());

				in.close();
				out.flush();
				out.close();
			}
		}
	}

	/**
	 * uploads a config file to the applocation server
	 * 
	 * @param file
	 * @throws CreateException
	 * @throws NamingException
	 * @throws BinBaseException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	protected void addConfigFile(File file) throws CreateException,
			NamingException, BinBaseException, FileNotFoundException,
			IOException {
		logger.info("checking config file: " + file.getAbsolutePath());
		assertTrue(file.exists());
		ServiceJMXFacade importer = Configurator.getImportService();

		addConfig(new FileInputStream(file), file.getName());

	}

	protected void addConfig(InputStream stream, String name)
			throws CreateException, NamingException, BinBaseException,
			FileNotFoundException, IOException {
		assertTrue("couldn't find: " + name,stream != null);
		ServiceJMXFacade importer = Configurator.getImportService();

		ByteArrayOutputStream out = new ByteArrayOutputStream();

		Copy.copy(stream, out);

		importer.uploadConfigFile(name, out.toByteArray());

		stream.close();
		out.flush();
		out.close();

	}

	/**
	 * adds the given path to the list of allowed directories
	 * 
	 * @param file
	 * @throws NamingException
	 * @throws CreateException
	 * @throws BinBaseException
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	protected void addTestSopDirectory(File file) throws CreateException,
			NamingException, BinBaseException, FileNotFoundException,
			IOException {
		logger.info("adding directory: " + file.getAbsolutePath());
		logger.info("exist: " + file.exists());
		ExportJMXFacade exporter = Configurator.getExportService();

		exporter.addSopDir(file.getAbsolutePath());

		// uploads all files in the given directory
		for (File f : file.listFiles()) {
			if (f.isFile()) {
				FileInputStream in = new FileInputStream(f);
				ByteArrayOutputStream out = new ByteArrayOutputStream();

				Copy.copy(in, out);

				exporter.uploadSop(f.getName(), out.toByteArray());

				in.close();
				out.flush();
				out.close();
			}
		}
	}

	/**
	 * adds the given path to the list of allowed directories
	 * 
	 * @param file
	 * @throws NamingException
	 * @throws CreateException
	 * @throws BinBaseException
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	protected void addTestExportDirectory(File file) throws CreateException,
			NamingException, BinBaseException, FileNotFoundException,
			IOException {
		logger.info("adding directory: " + file.getAbsolutePath());
		logger.info("exist: " + file.exists());
		ExportJMXFacade exporter = Configurator.getExportService();

		exporter.addNetCDFDirectory(file.getAbsolutePath());

		// uploads all files in the given directory
		for (File f : file.listFiles()) {
			if (f.isFile()) {
				FileInputStream in = new FileInputStream(f);
				ByteArrayOutputStream out = new ByteArrayOutputStream();

				Copy.copy(in, out);

				exporter.uploadNetCdf(f.getName(), out.toByteArray());

				in.close();
				out.flush();
				out.close();
			}
		}
	}
}
