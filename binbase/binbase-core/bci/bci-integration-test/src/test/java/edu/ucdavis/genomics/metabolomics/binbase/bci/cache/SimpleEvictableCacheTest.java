package edu.ucdavis.genomics.metabolomics.binbase.bci.cache;


import org.junit.After;
import org.junit.Before;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.SimpleCache;

/**
 * a simple cache with eviction policies this time
 * @author Administrator
 *
 */
public class SimpleEvictableCacheTest extends EvictableCacheTest{

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Override
	protected EvictableCache getCache() {
		return SimpleCache.getCache();
	}

}
