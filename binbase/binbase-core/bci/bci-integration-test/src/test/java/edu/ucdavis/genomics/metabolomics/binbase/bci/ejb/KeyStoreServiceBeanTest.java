package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.SimpleKeyStoreFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyStoreException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyStoreFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;

public class KeyStoreServiceBeanTest extends AbstractApplicationServerTest{

	KeyStoreService store;
	
	@Before
	public void setUp() throws Exception {
		super.setUp();
		store = (KeyStoreService) KeyStoreFactory.newInstance(SimpleKeyStoreFactory.class.getName()).createKeyStore();
		store.deleteAllKeys();

	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
		store.deleteAllKeys();
		
	}


	@Test
	public void testRemoveKey() throws KeyStoreException {
		for(int i = 0; i < 10; i ++){
			store.storeKey("app:"+i, "key:"+i);
			assertTrue(store.containsKey("key:"+i));
		}
		for(int i = 9; i > 0; i --){
			store.removeApplication("app:"+i);
			assertTrue(store.containsKey("key:"+i) == false);
		}

	}

	@Test
	public void testStoreKey() throws KeyStoreException {
		for(int i = 0; i < 10; i ++){
			store.storeKey("app:"+i, "key:"+i);
			assertTrue(store.containsKey("key:"+i));
		}
	}

	@Test
	public void testExistApplication() throws KeyStoreException {
		for(int i = 0; i < 10; i ++){
			store.storeKey("app:"+i, "key:"+i);
			assertTrue(store.containsKey("key:"+i));
			assertTrue(store.existApplication("app:"+i));
			assertTrue(store.existApplication("apdadasp:"+i) == false);

		}
	}

}
