package edu.ucdavis.genomics.metabolomics.binbase.bci.io;

import javax.naming.Context;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.util.PropertySetter;
import edu.ucdavis.genomics.metabolomics.util.io.dest.AbstractDestinationTest;

public class ConfigDestinationTest extends AbstractDestinationTest {

	private Logger logger = Logger.getLogger(getClass());

	@Override
	protected void setUp() throws Exception {
		PropertySetter.setPropertiesToSystem("src/test/resources/test.properties");

		System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		System.setProperty(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");

		// development machine
		if (System.getProperty("test.binbase.cluster.application-server") != null) {
			System.out.println("using defined application server: " + System.getProperty("test.binbase.cluster.application-server"));
			System.setProperty(Context.PROVIDER_URL, System.getProperty("test.binbase.cluster.application-server"));
		}
		// test machine modus
		else {
			System.out.println("using cluster frontend as application server: " + System.getProperty("test.binbase.cluster.server"));
			System.setProperty(Context.PROVIDER_URL, System.getProperty("test.binbase.cluster.server"));
		}

		logger.info("calling super class to set it up");
		super.setUp();
	}

	@Override
	protected String getDestinationFactoryImpl() {
		return ConfigDestinationFactory.class.getName();
	}

	@Override
	protected String getSourceFactoryImpl() {
		return ConfigSourceFactory.class.getName();
	}

	protected String getProperty() {
		return "/testFile";
	}
}
