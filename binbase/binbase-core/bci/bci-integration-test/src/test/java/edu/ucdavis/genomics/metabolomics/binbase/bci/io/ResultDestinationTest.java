package edu.ucdavis.genomics.metabolomics.binbase.bci.io;

import java.io.File;

import javax.naming.Context;

import org.apache.log4j.Logger;
import org.junit.Assert;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacade;
import edu.ucdavis.genomics.metabolomics.util.PropertySetter;
import edu.ucdavis.genomics.metabolomics.util.io.dest.AbstractDestinationTest;

/**
 * tests if our results can be stored as expsected using sources
 * @author wohlgemuth
 *
 */
public class ResultDestinationTest extends AbstractDestinationTest {

	private Logger logger = Logger.getLogger(getClass());

	@Override
	protected void setUp() throws Exception {
		PropertySetter.setPropertiesToSystem("src/test/resources/test.properties");

		System.setProperty(Context.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		System.setProperty(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");

		// development machine
		if (System.getProperty("test.binbase.cluster.application-server") != null) {
			System.out.println("using defined application server: " + System.getProperty("test.binbase.cluster.application-server"));
			System.setProperty(Context.PROVIDER_URL, System.getProperty("test.binbase.cluster.application-server"));
		}
		// test machine modus
		else {
			System.out.println("using cluster frontend as application server: " + System.getProperty("test.binbase.cluster.server"));
			System.setProperty(Context.PROVIDER_URL, System.getProperty("test.binbase.cluster.server"));
		}

		logger.info("connect to service");

		ExportJMXFacade export = Configurator.getExportService();

		logger.info("create file structure for the test");
		File temp = new File("target/result/test");
		temp.mkdirs();

		try {
			File content[] = temp.listFiles();
			for (File f : content) {
				f.delete();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		logger.info("add directory to the service");
		export.setResultDirectory(temp.getAbsolutePath());

		Assert.assertTrue(export.listResults().size() == 0);

		logger.info("calling super class to set it up");
		super.setUp();
	}

	@Override
	protected String getDestinationFactoryImpl() {
		return ResultDestinationFactory.class.getName();
	}

	@Override
	protected String getSourceFactoryImpl() {
		return ResultSourceFactory.class.getName();
	}

	protected String getProperty() {
		return "/testFile";
	}
}
