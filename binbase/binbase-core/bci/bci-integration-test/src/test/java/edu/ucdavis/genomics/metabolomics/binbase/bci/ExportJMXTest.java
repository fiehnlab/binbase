package edu.ucdavis.genomics.metabolomics.binbase.bci;

import java.io.File;
import java.io.IOException;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import org.junit.Assert;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.exception.FileNotFoundException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

/**
 * simple test if we can up and download data
 * 
 * @author wohlgemuth
 * 
 */
public class ExportJMXTest extends AbstractApplicationServerTest {

	@Test
	public void testSop() throws CreateException, NamingException, FileNotFoundException, java.io.FileNotFoundException, IOException, BinBaseException {

		ExportJMXFacade export = Configurator.getExportService();

		export.clearSopDirs();
		
		File temp = new File("target/sop/test");
		temp.mkdirs();

		try {
			File content[] = temp.listFiles();
			for (File f : content) {
				f.delete();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		export.addSopDir(temp.getAbsolutePath());

		Assert.assertTrue(export.listSops().size() == 0);

		export.uploadSop("test.sop", "test.sop-content".getBytes());

		Assert.assertTrue(export.listSops().size() == 1);

		byte[] b = export.getSop("test.sop");
		Assert.assertTrue(new String(b).equals("test.sop-content"));
	}

	@Test
	public void testResults() throws CreateException, NamingException, FileNotFoundException, java.io.FileNotFoundException, IOException, BinBaseException {

		ExportJMXFacade export = Configurator.getExportService();

		File temp = new File("target/result/test");
		temp.mkdirs();

		try {
			File content[] = temp.listFiles();
			for (File f : content) {
				f.delete();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		export.setResultDirectory(temp.getAbsolutePath());

		Assert.assertTrue(export.listResults().size() == 0);

		Assert.assertTrue(export.listResults().isEmpty());
		export.uploadResult("test.result", "test.result-content".getBytes());
		Assert.assertTrue(export.listResults().size() == 1);

		byte[] b = export.getResult("test.result");
		Assert.assertTrue(new String(b).equals("test.result-content"));

	}

}
