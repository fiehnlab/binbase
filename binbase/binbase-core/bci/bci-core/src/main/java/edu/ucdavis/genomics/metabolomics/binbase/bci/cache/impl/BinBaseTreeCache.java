package edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.jboss.cache.CacheException;
import org.jboss.cache.PropertyConfigurator;
import org.jboss.cache.TreeCache;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.AbstractEvictableCache;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictionPolicy;

/**
 * tree cache wrapper for jboss
 * 
 * @author Administrator
 * 
 */
public class BinBaseTreeCache extends AbstractEvictableCache {

	private Logger logger = Logger.getLogger(getClass());
	private static BinBaseTreeCache instance;

	public final static String KEY_CONFIG = BinBaseTreeCache.class
			.getSimpleName();

	public static BinBaseTreeCache getInstance() throws Exception {
		return getInstance(System.getProperties());
	}

	public static BinBaseTreeCache getInstance(Map<?, ?> map) throws Exception {
		if (instance == null) {
			instance = new BinBaseTreeCache();
			instance.setConfiguration(map);
			instance.getCache().startService();
		}
		return instance;
	}

	private TreeCache cache = null;

	private String folder = getClass().getSimpleName();

	List<EvictionPolicy> policies = new Vector<EvictionPolicy>();

	protected BinBaseTreeCache() throws Exception {
		cache = new TreeCache();
	}

	public void clear() {
		try {
			this.cache.remove(folder);
		} catch (CacheException e) {
			throw new edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.CacheException(
					e);
		}
	}

	public boolean contains(String key) {
		try {
			return cache.get(folder, key) != null;
		} catch (CacheException e) {
			throw new edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.CacheException(
					e);
		}
	}

	public int size() {
		try {
			if (cache.getKeys(folder) != null) {
				return cache.getKeys(folder).size();
			}
			return 0;
		} catch (CacheException e) {
			throw new edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.CacheException(
					e);
		}
	}

	@Override
	protected void addEntry(Entry entry) {
		try {
			cache.put(folder, entry.getKey(), entry);
		} catch (CacheException e) {
			throw new edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.CacheException(
					e);
		}
	}

	@Override
	protected Entry getEntry(String key) {
		try {
			return (Entry) cache.get(folder, key);
		} catch (CacheException e) {
			throw new edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.CacheException(
					e);
		}
	}

	@Override
	protected Entry getFirstEntry() {
		try {
			Set set = cache.getKeys(folder);
			if (set != null) {
				Iterator it = set.iterator();

				if (it.hasNext()) {
					return (Entry) cache.get(folder, it.next());
				}
			}
			return null;

		} catch (CacheException e) {
			throw new edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.CacheException(
					e);
		}
	}

	@Override
	protected void removeEntry(Entry entry) {
		try {
			cache.remove(folder, entry.getKey());
		} catch (CacheException e) {
			throw new edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.CacheException(
					e);
		}
	}

	public void setConfiguration(Map<?, ?> map) throws Exception {

		if (map.containsKey(BinBaseTreeCache.KEY_CONFIG)) {
			logger.info("confguring cache with: "
					+ map.get(BinBaseTreeCache.KEY_CONFIG));

			try {
				PropertyConfigurator config = new PropertyConfigurator();
				config.configure(cache, map.get(BinBaseTreeCache.KEY_CONFIG)
						.toString());
				logger.info("configuration is done: "
						+ cache.getCacheLoaderConfig());

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				instance = null;
				throw new edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.CacheException(
						e);
			}
		} else {
			try {
				// support for automatical configuration
				if (BinBaseTreeCache.class
						.getResourceAsStream("/jboss-cache.xml") != null) {
					logger.info("found default config and using it!");
					PropertyConfigurator config = new PropertyConfigurator();
					config.configure(cache, "jboss-cache.xml");
				}
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
				instance = null;
				throw new edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.CacheException(
						e);
			}
		}
	}

	protected TreeCache getCache() {
		return cache;
	}
}
