package edu.ucdavis.genomics.metabolomics.binbase.util.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class DetermineIpForInterface {

	/**
	 * determine the ip address for this device name
	 * 
	 * @param name
	 * @return
	 * @throws IOException
	 */
	public static String getIP(String name) throws IOException {
		Runtime runtime = Runtime.getRuntime();

		ProcessBuilder builder = null;

		if (isUnix()) {
			builder = new ProcessBuilder(
					"/bin/sh",
					"-c",
					"ifconfig | grep "
							+ name
							+ " -A 1 | grep 'inet ' | awk '{print $2}' | awk -F ':' '{print $2}'");

		} else if (isMac()) {
			builder = new ProcessBuilder("/bin/sh", "-c", "ifconfig | grep "
					+ name + "  -A 2 | grep 'inet ' | awk '{print $2}'");

		} else {
			throw new OSNotSupportException(
					"sorry this class does not support: "
							+ System.getProperty("os.name"));
		}

		Process process = builder.start();

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				process.getInputStream()));

		String result = null;

		while ((result = reader.readLine()) != null) {
			if (result.length() > 0) {
				return result;
			}
		}

		throw new InterfaceNotFoundException(
				"sorry we could not find your specified interface: " + name);
	}

	public static boolean isWindows() {

		String os = System.getProperty("os.name").toLowerCase();
		// windows
		return (os.indexOf("win") >= 0);

	}

	public static boolean isMac() {

		String os = System.getProperty("os.name").toLowerCase();
		// Mac
		return (os.indexOf("mac") >= 0);

	}

	public static boolean isUnix() {

		String os = System.getProperty("os.name").toLowerCase();
		// linux or unix
		return (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0);

	}

	public static boolean isSolaris() {

		String os = System.getProperty("os.name").toLowerCase();
		// Solaris
		return (os.indexOf("sunos") >= 0);

	}
}
