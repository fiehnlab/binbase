package edu.ucdavis.genomics.metabolomics.binbase.bci.cache;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl.SimpleCacheFactory;
import edu.ucdavis.genomics.metabolomics.exception.FactoryException;

/**
 * a cahce factory
 * 
 * @author wohlgemuth
 */
public abstract class CacheFactory {

	public static final String DEFAULT_PROPERTY_NAME = CacheFactory.class.getName();

	/**
	 * creates a new cache
	 * 
	 * @return
	 * @throws Exception 
	 */
	public abstract Cache createCache() throws Exception;

	public static CacheFactory getInstance() {
		return getInstance(System.getProperty(DEFAULT_PROPERTY_NAME));
	}

	/**
	 * if no property is set we returb a hashmap based cache
	 * @param className
	 * @return
	 */
	public static CacheFactory getInstance(String className) {
		if (className == null) {
			return new SimpleCacheFactory();
		}
		else {
			try {
				CacheFactory fact = (CacheFactory) Class.forName(className).newInstance();
				return fact;
			}
			catch (Exception e) {
				throw new FactoryException(e);
			}
		}
	}
}
