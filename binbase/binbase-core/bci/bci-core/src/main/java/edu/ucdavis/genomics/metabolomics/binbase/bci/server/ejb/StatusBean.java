/*
 * Created on Mar 16, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.server.ejb;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.ejb.CreateException;
import javax.ejb.EJBException;
import javax.ejb.SessionBean;
import javax.ejb.SessionContext;
import javax.management.MBeanServer;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;

import org.apache.log4j.Logger;
import org.jboss.mq.SpyObjectMessage;
import org.jboss.mq.server.jmx.QueueMBean;
import org.jboss.mx.util.MBeanServerLocator;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.CacheUrlJob;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.PostmatchingJob;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.UpdateBinBaseJob;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

/**
 * @author wohlgemuth show all data in the queues
 * 
 * @ejb.bean name="BinBaseQueue" display-name="BinBaseQueue" description="Is
 *           used for displaying contents of the scheduled views"
 *           jndi-name="ejb/BinBaseQueue" type="Stateless" view-type="both"
 * @ejb.util generate="physical"
 * 
 */
public class StatusBean implements SessionBean {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	Logger logger = Logger.getLogger(StatusBean.class);

	public StatusBean() {
		super();
	}

	/**
	 * @ejb.interface-method view-type = "both" returns all classes currently
	 *                       waiting for an import
	 * @author wohlgemuth
	 * @version Mar 16, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public ExperimentClass[] getImport() throws BinBaseException {
		try {
			ObjectName name = new ObjectName("jboss.mq.destination:service=Queue,name=queue");
			MBeanServer server = MBeanServerLocator.locate();
			QueueMBean queue = (QueueMBean) MBeanServerInvocationHandler.newProxyInstance(server, name, QueueMBean.class, false);
			List list = queue.listMessages();

			Iterator it = list.iterator();

			List classes = new Vector();

			while (it.hasNext()) {
				SpyObjectMessage o = (SpyObjectMessage) it.next();
				if (o.getObject() instanceof ExperimentClass) {
					classes.add(o.getObject());
				}
			}

			return (ExperimentClass[]) classes.toArray(new ExperimentClass[classes.size()]);
		} catch (Exception e) {
			throw new BinBaseException(e.getMessage());
		}
	}

	/**
	 * @ejb.interface-method view-type = "both" return all Experiments currently
	 *                       waiting for the export
	 * @author wohlgemuth
	 * @version Mar 16, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public Experiment[] getExports() throws BinBaseException {
		try {
			ObjectName name = new ObjectName("jboss.mq.destination:service=Queue,name=queue");
			MBeanServer server = MBeanServerLocator.locate();
			QueueMBean queue = (QueueMBean) MBeanServerInvocationHandler.newProxyInstance(server, name, QueueMBean.class, false);
			List list = queue.listMessages();

			Iterator it = list.iterator();

			List classes = new Vector();

			while (it.hasNext()) {
				SpyObjectMessage o = (SpyObjectMessage) it.next();
				if (o.getObject() instanceof Experiment) {
					classes.add(o.getObject());
				}
			}

			return (Experiment[]) classes.toArray(new Experiment[classes.size()]);
		} catch (Exception e) {
			throw new BinBaseException(e.getMessage());
		}
	}
	

	/**
	 * @ejb.interface-method view-type = "both" return all Experiments currently
	 *                       waiting for the export
	 * @author wohlgemuth
	 * @version Mar 16, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public Serializable[] getQueueContent() throws BinBaseException {
		try {
			ObjectName name = new ObjectName("jboss.mq.destination:service=Queue,name=queue");
			MBeanServer server = MBeanServerLocator.locate();
			QueueMBean queue = (QueueMBean) MBeanServerInvocationHandler.newProxyInstance(server, name, QueueMBean.class, false);
			List list = queue.listMessages();

			Iterator it = list.iterator();

			List classes = new Vector();

			while (it.hasNext()) {
				SpyObjectMessage o = (SpyObjectMessage) it.next();
				if (o.getObject() instanceof Serializable) {
					classes.add(o.getObject());
				}
			}

			return (Serializable[]) classes.toArray(new Serializable[classes.size()]);
		} catch (Exception e) {
			throw new BinBaseException(e.getMessage());
		}
	}


	/**
	 * @ejb.interface-method view-type = "both" return all Experiments currently
	 *                       waiting for the export
	 * @author wohlgemuth
	 * @version Mar 16, 2006
	 * @return
	 * @throws BinBaseException
	 */
	public CacheUrlJob[] getCacheBinMetainformationsJob() throws BinBaseException {
		try {
			ObjectName name = new ObjectName("jboss.mq.destination:service=Queue,name=queue");
			MBeanServer server = MBeanServerLocator.locate();
			QueueMBean queue = (QueueMBean) MBeanServerInvocationHandler.newProxyInstance(server, name, QueueMBean.class, false);
			List list = queue.listMessages();

			Iterator it = list.iterator();

			List classes = new Vector();

			while (it.hasNext()) {
				SpyObjectMessage o = (SpyObjectMessage) it.next();
				if (o.getObject() instanceof CacheUrlJob) {
					classes.add(o.getObject());
				}
			}

			return (CacheUrlJob[]) classes.toArray(new CacheUrlJob[classes.size()]);
		} catch (Exception e) {
			throw new BinBaseException(e.getMessage());
		}
	}
	

	/**
	 * 
	 * @ejb.interface-method view-type = "both" clears the queue
	 * clears the queue
	 * @author wohlgemuth
	 * @version Jul 19, 2006
	 * @throws BinBaseException 
	 */
	public UpdateBinBaseJob[] getUpdateBinBaseJobs() throws BinBaseException {
		try {
			ObjectName name = new ObjectName("jboss.mq.destination:service=Queue,name=queue");
			MBeanServer server = MBeanServerLocator.locate();
			QueueMBean queue = (QueueMBean) MBeanServerInvocationHandler.newProxyInstance(server, name, QueueMBean.class, false);
			List list = queue.listMessages();

			Iterator it = list.iterator();

			List classes = new Vector();

			while (it.hasNext()) {
				SpyObjectMessage o = (SpyObjectMessage) it.next();
				if (o.getObject() instanceof UpdateBinBaseJob) {
					classes.add(o.getObject());
				}
			}

			return (UpdateBinBaseJob[]) classes.toArray(new UpdateBinBaseJob[classes.size()]);
		} catch (Exception e) {
			throw new BinBaseException(e.getMessage());
		}
	}

	/**
	 * 
	 * @ejb.interface-method view-type = "both" clears the queue
	 * clears the queue
	 * @author wohlgemuth
	 * @version Jul 19, 2006
	 * @throws BinBaseException 
	 */
	public DSL[] getDSLJobs() throws BinBaseException{
		try {
			ObjectName name = new ObjectName("jboss.mq.destination:service=Queue,name=queue");
			MBeanServer server = MBeanServerLocator.locate();
			QueueMBean queue = (QueueMBean) MBeanServerInvocationHandler.newProxyInstance(server, name, QueueMBean.class, false);
			List list = queue.listMessages();

			Iterator it = list.iterator();

			List classes = new Vector();

			while (it.hasNext()) {
				SpyObjectMessage o = (SpyObjectMessage) it.next();
				if (o.getObject() instanceof DSL) {
					classes.add(o.getObject());
				}
			}

			return (DSL[]) classes.toArray(new DSL[classes.size()]);
		} catch (Exception e) {
			throw new BinBaseException(e.getMessage());
		}		
	}
	/**
	 * 
	 * @ejb.interface-method view-type = "both" clears the queue
	 * clears the queue
	 * @author wohlgemuth
	 * @version Jul 19, 2006
	 * @throws BinBaseException 
	 */
	public PostmatchingJob[] getPostmatchingJobs() throws BinBaseException{
		try {
			ObjectName name = new ObjectName("jboss.mq.destination:service=Queue,name=queue");
			MBeanServer server = MBeanServerLocator.locate();
			QueueMBean queue = (QueueMBean) MBeanServerInvocationHandler.newProxyInstance(server, name, QueueMBean.class, false);
			List list = queue.listMessages();

			Iterator it = list.iterator();

			List classes = new Vector();

			while (it.hasNext()) {
				SpyObjectMessage o = (SpyObjectMessage) it.next();
				if (o.getObject() instanceof PostmatchingJob) {
					classes.add(o.getObject());
				}
			}

			return (PostmatchingJob[]) classes.toArray(new PostmatchingJob[classes.size()]);
		} catch (Exception e) {
			throw new BinBaseException(e.getMessage());
		}		
	}
		
	/**
	 * 
	 * @ejb.interface-method view-type = "both" clears the queue
	 * clears the queue
	 * @author wohlgemuth
	 * @version Jul 19, 2006
	 * @throws BinBaseException 
	 */
	public void clearQueue() throws BinBaseException{
		try{
		ObjectName name = new ObjectName("jboss.mq.destination:service=Queue,name=queue");
		MBeanServer server = MBeanServerLocator.locate();
		QueueMBean queue = (QueueMBean) MBeanServerInvocationHandler.newProxyInstance(server, name, QueueMBean.class, false);
		queue.removeAllMessages();
		}
		catch (Exception e) {
			throw new BinBaseException(e);
		}
	}
	public void setSessionContext(SessionContext arg0) throws EJBException, RemoteException {
	}

	public void ejbRemove() throws EJBException, RemoteException {
	}

	public void ejbActivate() throws EJBException, RemoteException {
	}

	public void ejbPassivate() throws EJBException, RemoteException {
	}

	public void ejbCreate() throws CreateException {

	}
}