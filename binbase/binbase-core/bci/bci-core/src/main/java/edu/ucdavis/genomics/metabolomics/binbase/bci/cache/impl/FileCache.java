package edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.AbstractEvictableCache;

/**
 * stores the data on the harddrive in the temp directory and is a very simple
 * cache and quite slow for a large amount of objects, but should provide an ok
 * performance for small caches
 * 
 * @author wohlgemuth
 */
public class FileCache extends AbstractEvictableCache implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private transient Logger logger = Logger.getLogger(getClass());

	private static FileCache instance = null;

	private String extensions = "bbc";

	private String cacheDir = null;

	private FileCache(String property) {
		File cacheDir = new File(property);

		if (cacheDir.isDirectory()) {
			if (cacheDir.canWrite()) {
				if (cacheDir.canRead()) {
					if (new File(property + File.separator + "binbase-cache").exists() == false) {
						logger.info("creating caching directory...");
						new File(property + File.separator + "binbase-cache").mkdirs();
					}
					this.cacheDir = property + File.separator + "binbase-cache";
				}
				else {
					logger.warn("can't read from the directory! " + property);
				}
			}
			else {
				logger.warn("can't write to the directory! " + property);
			}
		}
		else {
			logger.warn("is not a directory! " + property);

		}

	}

	public static FileCache getInstance() {
		if (instance == null) {
			instance = new FileCache(System.getProperty("java.io.tmpdir"));
		}
		return instance;
	}

	@Override
	protected  void addEntry(Entry entry) {
		try {
			ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(new File(this.cacheDir + File.separator
					+ entry.getKey() + "." + extensions))));
			out.writeObject(entry);
			out.flush();
			out.close();
		}
		catch (Exception e) {
			throw new CacheException(e.getMessage(), e);
		}
	}

	@Override
	protected Entry getEntry(String key) {
		if (this.contains(key) == true) {
			try {
				ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(new File(this.cacheDir + File.separator + key + "."
						+ extensions))));
				Entry entry = (Entry) in.readObject();
				in.close();
				return entry;
			}
			catch (Exception e) {
				throw new CacheException(e.getMessage(), e);
			}

		}
		else {
			throw new CacheException("key not found exception! " + key);
		}
	}

	@Override
	protected Entry getFirstEntry() {

		File file = new File(this.cacheDir);

		File[] files = file.listFiles(new FileFilter() {

			public boolean accept(File pathname) {
				return pathname.getName().endsWith("." + extensions);
			}

		});

		// sorting the files to get the one which was modified the longest ago
		Arrays.sort(files, new Comparator<File>() {

			public int compare(File o1, File o2) {
				return new Long(o1.lastModified()).compareTo(o2.lastModified());
			}
		});
		if (files.length != 0) {
			Entry entry =  getEntry(files[files.length - 1].getName().replace("." + this.extensions, "").trim());

			return entry;
		}
		else {
			return null;
		}
	}

	@Override
	protected  void removeEntry(Entry entry) {
		new File(this.cacheDir + File.separator + entry.getKey() + "." + extensions).delete();
	}

	public void clear() throws CacheException {
		File file = new File(this.cacheDir);

		for (File f : file.listFiles()) {
			if (f.getName().endsWith("." + extensions)) {
				f.delete();
			}
		}

	}

	public  boolean contains(String key) throws CacheException {
		return new File(this.cacheDir + File.separator + key + "." + extensions).exists();
	}

	public void setConfiguration(Map<?, ?> map) throws CacheException, Exception {

	}

	public int size() throws CacheException {
		File file = new File(this.cacheDir);

		return file.listFiles(new FileFilter() {

			public boolean accept(File pathname) {
				return pathname.getName().endsWith("." + extensions);
			}

		}).length;

	}

}
