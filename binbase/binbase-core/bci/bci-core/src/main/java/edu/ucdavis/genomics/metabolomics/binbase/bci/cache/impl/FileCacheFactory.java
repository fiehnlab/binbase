package edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.Cache;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.CacheFactory;

public class FileCacheFactory extends CacheFactory{

	@Override
	public Cache createCache() throws Exception {
		return FileCache.getInstance();
	}

}
