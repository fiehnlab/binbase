package edu.ucdavis.genomics.metabolomics.binbase.bci.validator;


/**
 * used to manage keys
 * 
 * @author wohlgemuth
 */
public interface KeyStore {

	/**
	 * stores a key
	 * @param applicationName
	 * @param key
	 * @throws KeyStoreException 
	 */
	public void storeKey(String applicationName, String key) throws KeyStoreException;

	/**
	 * removes a key
	 * @param applicationName
	 * @throws KeyStoreException 
	 */
	public void removeApplication(String applicationName) throws KeyStoreException;

	/**
	 * does it contain this key
	 * @param key
	 * @return
	 * @throws KeyStoreException
	 */
	public boolean containsKey(String key) throws KeyStoreException;
	
	/**
	 * does it contain the application
	 * @param application
	 * @return
	 * @throws KeyStoreException
	 */
	public boolean containsApplication(String application) throws KeyStoreException;

}
