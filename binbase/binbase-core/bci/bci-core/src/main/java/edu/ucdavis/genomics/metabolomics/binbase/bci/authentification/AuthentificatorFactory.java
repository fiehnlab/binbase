/*
 * Created on Nov 16, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.authentification;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;
import edu.ucdavis.genomics.metabolomics.util.status.Log4JReport;
import edu.ucdavis.genomics.metabolomics.util.status.Report;

/**
 * used to create authentificators
 * 
 * @author wohlgemuth
 * @version Nov 16, 2006
 * 
 */
public abstract class AuthentificatorFactory extends AbstractFactory {
	public static final String DEFAULT_PROPERTY_NAME = AuthentificatorFactory.class.getName();

	/**
	 * creates a new Authentificator
	 * 
	 * @author wohlgemuth
	 * @version Nov 16, 2006
	 * @param properties
	 * @param report
	 * @return
	 * @throws AuthentificationException
	 */
	public abstract Authentificator create(Map<?, ?> properties, Report report) throws AuthentificationException;

	/**
	 * 
	 * @author wohlgemuth
	 * @version Nov 16, 2006
	 * @param report
	 * @return
	 * @throws AuthentificationException
	 */
	public Authentificator create(Report report) throws AuthentificationException {
		return create(System.getenv(), report);
	}

	/**
	 * creates new instance
	 * 
	 * @author wohlgemuth
	 * @version Nov 17, 2006
	 * @return
	 * @throws AuthentificationException
	 */
	public Authentificator create() throws AuthentificationException {
		return create(System.getenv(), new Log4JReport());
	}

	public static AuthentificatorFactory newInstance() {
		return newInstance(System.getProperties());
	}

	public static AuthentificatorFactory newInstance(Map<?, ?> properties) {
		return newInstance((String) properties.get(DEFAULT_PROPERTY_NAME));
	}

	/**
	 * returns an new instance of the specified factory
	 * 
	 * @param factoryClass
	 *            the specified factory to use
	 * @author wohlgemuth
	 * @version Nov 9, 2005
	 * @return
	 */
	public static AuthentificatorFactory newInstance(String factoryClass) {
		Class<?> classObject;
		AuthentificatorFactory factory;

		try {

			if(factoryClass == null){
				return new AuthentificatorFactory(){

					@Override
					public Authentificator create(Map<?, ?> properties, Report report) throws AuthentificationException {
						return new Authentificator(){

							/**
							 * 
							 */
							private static final long serialVersionUID = 2L;

							public User authentificate(String username, String password) throws AuthentificationException {
								throw new AuthentificationException("sorry no factory was specific so no authentification possible");
							}

							public User authentificate(String username, String password, String database) throws AuthentificationException {
								throw new AuthentificationException("sorry no factory was specific so no authentification possible");							
							}

							public void addUser(User user) throws AuthentificationException {
								throw new AuthentificationException("sorry no factory was specific so no authentification possible");							
							}

							public boolean canRegisterUser() throws AuthentificationException {
								throw new AuthentificationException("sorry no factory was specific so no authentification possible");							
							}
					
						};
					};
				};
			}
			classObject = Class.forName(factoryClass);

			factory = (AuthentificatorFactory) classObject.newInstance();
			return factory;

		} catch (Exception e) {
			throw new FactoryException(e);
		}
	}
}
