package edu.ucdavis.genomics.metabolomics.binbase.bci.cache;

import java.util.List;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.AbstractEvictableCache.Entry;

/**
 * a cache which supports that the cache is stored somewhere or evicted objects are stored somewhere
 * @author Administrator
 *
 */
public interface PersistableCache extends Cache{

	/**
	 * is this entry persistant
	 * @param entry
	 * @return
	 */
	public boolean isPersistant(Entry entry);
	
	/**
	 * returns entries which are persistant
	 * @return
	 */
	public List<Entry> getPersistantEntries();
	
	/**
	 * returns entries which are not persistant
	 * @return
	 */
	public List<Entry> getNotPersistantEntries();
	
	/**
	 * make this entry persistant
	 * @param entry
	 */
	public void makePersistant(Entry entry);
}
