package edu.ucdavis.genomics.metabolomics.binbase.util.locking;

import java.io.Serializable;
import java.util.Date;

/**
 * locking resource
 * @author wohlgemuth
 *
 */
public final class Lock implements Serializable {

	private String name;

	private String column;

	private Date date;

	private String identifier;

	public Lock(String name, String column, Date date, String identifier) {
		super();
		this.name = name;
		this.column = column;
		this.date = date;
		this.identifier = identifier;
	}

	@Override
	public int hashCode() {
		return (this.name + "-" + this.column).hashCode();
	}

	@Override
	public boolean equals(Object arg0) {
		if (arg0 instanceof Lock) {
			Lock b = (Lock) arg0;

			if (b.name.equals(this.name)) {
				if (b.column.equals(this.column)) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public String toString() {
		if (System.getenv().get("JOB_ID") != null) {
			return name + "_" + column + "(" + System.getenv("JOB_ID") + "/"
					+ identifier + "/" + date + ")";
		} else {
			return name + "_" + column + "(local" + "/" + identifier + "/"
					+ date + ")";
		}

	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}