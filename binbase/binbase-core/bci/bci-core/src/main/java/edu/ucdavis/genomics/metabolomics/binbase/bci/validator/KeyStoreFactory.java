package edu.ucdavis.genomics.metabolomics.binbase.bci.validator;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.exception.FactoryException;

public abstract class KeyStoreFactory {
	public static final String DEFAULT_PROPERTY_NAME = KeyStoreFactory.class.getName();

	/**
	 * creates a new keystore
	 * 
	 * @return
	 * @throws KeyStoreException 
	 */
	public abstract KeyStore createKeyStore() throws KeyStoreException;

	public static KeyStoreFactory newInstance() {
		return newInstance(System.getProperty(DEFAULT_PROPERTY_NAME));
	}

	public static KeyStoreFactory newInstance(String factoryClass) {
		Class<?> classObject;
		KeyStoreFactory factory;

		try {
			if (factoryClass == null) {
				factoryClass = "edu.ucdavis.genomics.metabolomics.binbase.bci.SimpleKeyStoreFactory";
			}
			Logger.getLogger(KeyStoreFactory.class).info("creating key store factory of type: " + factoryClass);
			classObject = Class.forName(factoryClass);
			factory = (KeyStoreFactory) classObject.newInstance();
			return factory;

		}
		catch (Exception e) {
			throw new FactoryException(e);
		}
	}

}
