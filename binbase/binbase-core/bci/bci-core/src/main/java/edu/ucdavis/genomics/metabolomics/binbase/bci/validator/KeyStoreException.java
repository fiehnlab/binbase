package edu.ucdavis.genomics.metabolomics.binbase.bci.validator;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

public class KeyStoreException extends BinBaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public KeyStoreException() {
		super();
	}

	public KeyStoreException(String message, Throwable cause) {
		super(message, cause);
	}

	public KeyStoreException(String message) {
		super(message);
	}

	public KeyStoreException(Throwable cause) {
		super(cause);
	}
}
