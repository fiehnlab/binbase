package edu.ucdavis.genomics.metabolomics.binbase.bci.cache.policies;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictableCache;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.EvictionPolicy;
import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.AbstractEvictableCache.Entry;

/**
 * removes objects if they are older than a specific lifetime
 *
 */
public class LifeTimeEvictionPolicy implements EvictionPolicy {

	private long lifeTimeInSeconds = 60;

	private Logger logger = Logger.getLogger(getClass());
	
	/**
	 * please specify the needed life time
	 * @param lifeTimeInSeconds
	 */
	public LifeTimeEvictionPolicy(long lifeTimeInSeconds) {
		super();
		this.lifeTimeInSeconds = lifeTimeInSeconds;
	}

	public LifeTimeEvictionPolicy(){
		
	}

	/**
	 * if entries are older than lifetime, they are removed
	 */
	public boolean evict(Entry entry, EvictableCache cache) {
		long time = System.currentTimeMillis() - entry.getCreated().getTime();
		logger.debug("age is: " + (time/1000) + " for " + entry + " and number of entries: " + cache.size() + " and access count is: " + entry.getAccessCount());
		
		if ((time) > (lifeTimeInSeconds * 1000)){
			logger.debug("marking job for eviction: " + entry);
			entry.setNeedsEviction(true);
			return true;
		}
		
		return false;
	}

	public long getLifeTimeInSeconds() {
		return lifeTimeInSeconds;
	}

	public void setLifeTimeInSeconds(long lifeTimeInSeconds) {
		this.lifeTimeInSeconds = lifeTimeInSeconds;
	}
}
