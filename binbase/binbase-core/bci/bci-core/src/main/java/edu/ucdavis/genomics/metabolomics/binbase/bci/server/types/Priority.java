/*
 * Created on Nov 7, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.server.types;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.util.Scheduler;

/**
 * priorities for calculations
 * @author wohlgemuth
 * @version Nov 7, 2006
 *
 */
public class Priority {

	public final static int PRIORITY_WHENEVER = Scheduler.PRIORITY_WHENEVER;

	public final static int PRIORITY_LOWEST = Scheduler.PRIORITY_LOWEST;

	public final static int PRIORITY_VERY_LOW = Scheduler.PRIORITY_VERY_LOW;

	/**
	 * low priority
	 */
	public final static int PRIORITY_LOW = Scheduler.PRIORITY_LOW;

	/**
	 * the normal priority for calculations
	 */
	public final static int PRIORITY_NORMAL = Scheduler.PRIORITY_NORMAL;

	/**
	 * if you are in a hurry
	 */
	public final static int PRIORITY_HIGH = Scheduler.PRIORITY_HIGH;

	public final static int PRIORITY_VERY_HIGH = Scheduler.PRIORITY_VERY_HIGH;

	public final static int PRIORITY_HIGHEST = Scheduler.PRIORITY_HIGHEST;

	/**
	 * should be calculate as soon as ressources are available
	 */
	public final static int PRIORITY_ASAP = Scheduler.PRIORITY_ASAP;

	public final static int EXPERIMENT_CLASS_MAX_PRIORITY = PRIORITY_HIGH;
	
	public final static int EXPERIMENT_MAX_PRIORITY = generateLowerPriority(PRIORITY_LOW);
	
	
	/**
	 * simple method to generate a lower priority than the given one
	 * @param priority
	 * @return
	 */
	public static int generateLowerPriority(int priority){
		return priority - 1;
	}
	

	/**
	 * simple method to generate a higher priority than the given one
	 * @param priority
	 * @return
	 */
	public static int generateHigherPriority(int priority){
		return priority + 1;
	}
	
	
}
