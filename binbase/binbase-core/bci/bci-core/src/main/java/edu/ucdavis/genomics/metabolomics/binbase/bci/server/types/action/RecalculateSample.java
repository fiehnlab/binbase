package edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.action;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;

/**
 * recalculate a sample
 * @author wohlgemuth
 *
 */
public class RecalculateSample extends ExperimentSample{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

}
