package edu.ucdavis.genomics.metabolomics.binbase.bci.cache;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.AbstractEvictableCache.Entry;

/**
 * defines a simple policy to remove objects if they are fit into this policy
 *
 */
public interface EvictionPolicy {

	/**
	 * evicts the given object from the cache
	 * @param key
	 * @param object
	 * @param cache
	 * @return
	 */
	public boolean evict(Entry entry,EvictableCache cache);
}
