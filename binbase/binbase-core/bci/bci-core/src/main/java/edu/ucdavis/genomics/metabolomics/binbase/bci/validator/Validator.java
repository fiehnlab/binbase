package edu.ucdavis.genomics.metabolomics.binbase.bci.validator;

import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;

public interface Validator {
	/**
	 * validates that the given key is valid
	 * @param key
	 * @return
	 * @throws AuthentificationException
	 */
	public abstract boolean isKeyValid(String key) throws AuthentificationException;	
}
