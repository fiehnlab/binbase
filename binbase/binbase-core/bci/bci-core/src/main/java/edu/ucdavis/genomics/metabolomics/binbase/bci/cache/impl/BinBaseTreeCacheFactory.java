package edu.ucdavis.genomics.metabolomics.binbase.bci.cache.impl;

import edu.ucdavis.genomics.metabolomics.binbase.bci.cache.CacheFactory;

public class BinBaseTreeCacheFactory extends CacheFactory{

	@Override
	public BinBaseTreeCache createCache() throws Exception {
		return BinBaseTreeCache.getInstance();
	}

}
