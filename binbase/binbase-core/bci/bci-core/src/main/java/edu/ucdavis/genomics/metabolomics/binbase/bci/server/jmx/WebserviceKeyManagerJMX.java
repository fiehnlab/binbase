package edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyStore;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyStoreException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyStoreFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.Validator;

/**
 * simple jmx for management of keys
 * 
 * @author wohlgemuth
 * @jmx.mbean description = "management of the internal key" extends =
 *            "javax.management.MBeanRegistration"
 *            name="binbase:service=KeyManagement"
 */
public class WebserviceKeyManagerJMX implements WebserviceKeyManagerJMXMBean,
		KeyStore, Validator {

	private String internalKey;

	private boolean disabled;

	/**
	 * @jmx.managed-operation returns the internal key or generates a new one if
	 *                        none exists yet
	 * @return
	 * @throws KeyStoreException
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * @jmx.managed-operation returns the internal key or generates a new one if
	 *                        none exists yet
	 * @return
	 * @throws KeyStoreException
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
		this.store();
	}

	private Logger logger = Logger.getLogger(getClass());

	public void setInternalKey(String key) throws KeyStoreException {
		logger.info("setting new key: " + key);
		this.internalKey = key;

		if (getKeyStore().containsApplication("internal")) {
			removeApplication("internal");
		}
		getKeyStore().storeKey("internal", internalKey);

		logger.info("setting was succhessful: " + containsKey(key));
	}

	private KeyStore getKeyStore() throws KeyStoreException {

		KeyStoreFactory factory = KeyStoreFactory
				.newInstance("edu.ucdavis.genomics.metabolomics.binbase.bci.SimpleKeyStoreFactory");
		KeyStore store = factory.createKeyStore();

		return store;
	}

	/**
	 * @jmx.managed-operation returns the internal key or generates a new one if
	 *                        none exists yet
	 * @return
	 * @throws KeyStoreException
	 */
	public String getInternalKey() throws KeyStoreException {
		return this.internalKey;
	}

	/**
	 * @throws KeyStoreException
	 * @jmx.managed-operation returns the internal key or generates a new one if
	 *                        startup
	 */
	public void generateInternalKey() throws KeyStoreException {
		logger.info("generating internal key");
		setInternalKey(Long.toHexString(System.currentTimeMillis()));
	}

	public void postDeregister() {
	}

	public void postRegister(Boolean arg0) {
		try {
			File file = new File(getClass().getName() + ".properties");
			if (file.exists()) {
				ObjectInputStream in = new ObjectInputStream(
						new FileInputStream(file));
				this.disabled = in.readBoolean();
			}
		} catch (Exception e) {
			logger.debug(e.getMessage(), e); //$NON-NLS-1$
		}

		try {
			generateInternalKey();
		} catch (Exception e) {
			logger.error(e.getMessage(), e); //$NON-NLS-1$

		}
	}

	public void preDeregister() throws Exception {
		store();
	}

	public ObjectName preRegister(MBeanServer arg0, ObjectName arg1)
			throws Exception {
		return null;
	}

	/**
	 * @throws KeyStoreException
	 * @jmx.managed-operation returns the internal key or generates a new one if
	 */
	public void removeApplication(String applicationName)
			throws KeyStoreException {
		logger.info("removing application: " + applicationName);
		getKeyStore().removeApplication(applicationName);
	}

	/**
	 * @throws KeyStoreException
	 * @jmx.managed-operation returns the internal key or generates a new one if
	 */
	public void storeKey(String applicationName, String key)
			throws KeyStoreException {
		getKeyStore().storeKey(applicationName, key);
	}

	protected void store() {
		try {
			File file = new File(getClass().getName() + ".properties");

			ObjectOutputStream out = new ObjectOutputStream(
					new FileOutputStream(file));
			out.writeBoolean(this.disabled);

			out.flush();
			out.close();

		} catch (Exception e) {
			logger.debug(e.getMessage(), e); //$NON-NLS-1$
		}
	}

	/**
	 * @throws KeyStoreException
	 * @jmx.managed-operation returns the internal key or generates a new one if
	 */
	public boolean containsKey(String key) throws KeyStoreException {
		if (disabled) {
			logger.info("service is disabled! so all keys are accepted!");
			return true;
		}
		boolean result = getKeyStore().containsKey(key);

		return result;
	}

	/**
	 * @throws KeyStoreException
	 * @jmx.managed-operation returns the internal key or generates a new one if
	 */
	public boolean containsApplication(String application)
			throws KeyStoreException {
		logger.info("checking for application: " + application);
		return getKeyStore().containsApplication(application);
	}

	/**
	 * @throws KeyStoreException
	 * @jmx.managed-operation returns the internal key or generates a new one if
	 */
	public boolean isKeyValid(String key) throws AuthentificationException {
		if (isDisabled()) {
			logger.info("service is disabled! so all keys are accepted!");
			return true;
		} else {
			try {
				return containsKey(key);
			} catch (KeyStoreException e) {
				throw new AuthentificationException(e.getMessage(), e);
			}
		}
	}
}
