/*
 * Created on Mar 8, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.server.exception;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

public class NoProviderRegisteredException extends BinBaseException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	public NoProviderRegisteredException() {
		super();
		
	}

	public NoProviderRegisteredException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}

	public NoProviderRegisteredException(String arg0) {
		super(arg0);
		
	}

	public NoProviderRegisteredException(Throwable arg0) {
		super(arg0);
		
	}

}
