/*
 * Created on Oct 11, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.bci.server.types;

import java.io.Serializable;

/**
 * provides us with basic infos about a sample
 * @author wohlgemuth
 * @version Oct 11, 2006
 *
 */
public class SampleInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	private int countOfAnnotations;
	
	private int sampleId;
	
	private String sampleName;
	
	private int sampleVersionInBinbase;
	
	private String visible;
	
	private String retentionIndexCorrectionFailed;
	
	private String classId;
	
	private String setupXId;

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public int getCountOfAnnotations() {
		return countOfAnnotations;
	}

	public void setCountOfAnnotations(int countOfAnnotations) {
		this.countOfAnnotations = countOfAnnotations;
	}

	public String getRetentionIndexCorrectionFailed() {
		return retentionIndexCorrectionFailed;
	}

	public void setRetentionIndexCorrectionFailed(String retentionIndexCorrectionFailed) {
		this.retentionIndexCorrectionFailed = retentionIndexCorrectionFailed;
	}

	public int getSampleId() {
		return sampleId;
	}

	public void setSampleId(int sampleId) {
		this.sampleId = sampleId;
	}

	public String getSampleName() {
		return sampleName;
	}

	public void setSampleName(String sampleName) {
		this.sampleName = sampleName;
	}

	public int getSampleVersionInBinbase() {
		return sampleVersionInBinbase;
	}

	public void setSampleVersionInBinbase(int sampleVersionInBinbase) {
		this.sampleVersionInBinbase = sampleVersionInBinbase;
	}

	public String getSetupXId() {
		return setupXId;
	}

	public void setSetupXId(String setupXId) {
		this.setupXId = setupXId;
	}

	public String getVisible() {
		return visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}
}
