package edu.ucdavis.genomics.metabolomics.binbase.ant;

import javax.naming.NamingException;

import org.apache.tools.ant.Task;

import edu.ucdavis.genomics.metabolomics.binbase.BinBaseServices;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.Authentificator;

/**
 * simple ant task to create and delete user
 * 
 * @author wohlgemuth
 */
public class UserTask extends Task {

	/**
	 * returns the actual service
	 * 
	 * @param applicationServer
	 * @return
	 * @throws NamingException
	 * @throws AuthentificationException
	 */
	public Authentificator getService(String applicationServer) throws NamingException, AuthentificationException {

		System.setProperty("java.naming.provider.url", applicationServer + ":1099");
		System.setProperty("java.naming.factory.initial", "org.jnp.interfaces.NamingContextFactory");
		System.setProperty("java.naming.factory.url.pkgs", "org.jboss.naming:org.jnp.interfaces");

		return BinBaseServices.getCentralAuthentification();
	}
}
