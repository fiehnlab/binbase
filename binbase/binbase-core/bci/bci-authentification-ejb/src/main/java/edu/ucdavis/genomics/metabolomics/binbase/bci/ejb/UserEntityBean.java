package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User;

/**
 * a simple user
 * 
 * @author wohlgemuth
 * 
 */
@Entity
@Table
public class UserEntityBean implements Serializable {

	public UserEntityBean() {

	}

	public UserEntityBean(User user) {
		this.setAdmin(user.isMasterUser());
		this.setName(user.getUsername());
		this.setPassword(user.getPassword());
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * username
	 */
	@Id
	private String name;

	@Column(nullable = false)
	private String password;

	/**
	 * is the user an admin
	 */
	@Column(nullable = false)
	private boolean admin;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof UserEntityBean) {
			UserEntityBean user = (UserEntityBean) obj;
			if (user.admin == this.admin) {
				if (user.name == this.name) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getName().hashCode();
	}

	@Override
	public String toString() {
		return this.getName() + " - " + this.isAdmin();
	}
}
