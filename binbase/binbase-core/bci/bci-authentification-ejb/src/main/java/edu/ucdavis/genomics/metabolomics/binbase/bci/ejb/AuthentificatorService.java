package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import java.io.Serializable;

import javax.ejb.Remote;

import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User;

@Remote
public interface AuthentificatorService extends Serializable{

	public abstract User authentificate(String username, String password, String database) throws AuthentificationException;

	public abstract User authentificate(String username, String password) throws AuthentificationException;

	public abstract void addUser(User user) throws AuthentificationException;
}