package edu.ucdavis.genomics.metabolomics.binbase.bci.jmx;

import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXProvider;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

/**
 * jmx access to the delegate
 * 
 * @author wohlgemuth
 */
public interface DelegateSetupXJMXMBean extends javax.management.MBeanRegistration, SetupXProvider {

	/**
	 * @param sampleName
	 * @return the setupX id for this sample or null if nothing found
	 */
	public String getSetupXId(String sampleName) throws BinBaseException;

	/**
	 * return the metadata for the class
	 * 
	 * @param setupXId
	 * @return the metainformations as xml document. or null if nothing is found
	 */
	public String getMetaInformationByClass(String setupXId) throws BinBaseException;

	/**
	 * returns the meta data for the sample
	 * 
	 * @param setupXId
	 * @return
	 * @throws BinBaseException
	 */
	public String getMetaInformationBySample(String setupXId) throws BinBaseException;

	/**
	 * returns the meta data for the experiment
	 * 
	 * @param setupXId
	 * @return
	 * @throws BinBaseException
	 */
	public String getMetaInformationByExperiment(String setupXId) throws BinBaseException;

	/**
	 * send the calculated result to setupX
	 * 
	 * @author wohlgemuth
	 * @version Feb 9, 2006
	 * @param string
	 * @param content
	 * @throws BinBaseException
	 */
	public void upload(String experimentId, String content) throws BinBaseException;

}
