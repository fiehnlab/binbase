package edu.ucdavis.genomics.metabolomics.binbase.bci.jmx;

import java.io.Serializable;

import javax.management.MBeanServer;
import javax.management.ObjectName;

import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.DelegateSetupXFactory;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

/**
 * delegate access to the delegate factory
 * 
 * @author wohlgemuth
 * 
 */
public class DelegateSetupXJMX implements DelegateSetupXJMXMBean, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String getMetaInformationByClass(String setupXId)
			throws BinBaseException {
		return new DelegateSetupXFactory().createProvider()
				.getMetaInformationByClass(setupXId);
	}

	@Override
	public String getMetaInformationByExperiment(String setupXId)
			throws BinBaseException {
		return new DelegateSetupXFactory().createProvider()
				.getMetaInformationByExperiment(setupXId);
	}

	@Override
	public String getMetaInformationBySample(String setupXId)
			throws BinBaseException {
		return new DelegateSetupXFactory().createProvider()
				.getMetaInformationBySample(setupXId);
	}

	@Override
	public String getSetupXId(String sampleName) throws BinBaseException {
		return new DelegateSetupXFactory().createProvider().getSetupXId(
				sampleName);
	}

	@Override
	public void upload(String experimentId, String content)
			throws BinBaseException {
		new DelegateSetupXFactory().createProvider().upload(experimentId,
				content);
	}

	@Override
	public void postDeregister() {
	}

	@Override
	public void postRegister(Boolean arg0) {
	}

	@Override
	public void preDeregister() throws Exception {
	}

	@Override
	public ObjectName preRegister(MBeanServer arg0, ObjectName arg1)
			throws Exception {
		return null;
	}

	@Override
	public boolean canCreateBins(String setupxId) throws BinBaseException {
		return new DelegateSetupXFactory().createProvider().canCreateBins(
				setupxId);
	}

}
