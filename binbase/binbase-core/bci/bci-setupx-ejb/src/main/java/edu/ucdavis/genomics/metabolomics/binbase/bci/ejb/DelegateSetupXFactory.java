package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import java.util.Properties;

import javax.naming.NamingException;

import org.jboss.logging.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXProvider;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.EjbClient;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

public class DelegateSetupXFactory extends SetupXFactory {

	private Logger logger = Logger.getLogger(getClass());

	@Override
	public SetupXProvider createProvider(Properties p) {
		try {

			final DelegateSeupxProvider provider = (DelegateSeupxProvider) EjbClient.getRemoteEjb(
					edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator.BCI_GLOBAL_NAME, DelegateSetupxCommunicationBean.class);

			return new SetupXProvider() {

				public String getMetaInformationByClass(String setupXId) throws BinBaseException {
					logger.info("querry metadata by class for: " + setupXId);
					return provider.getMetaInformationByClass(setupXId);
				}

				public String getMetaInformationByExperiment(String setupXId) throws BinBaseException {
					logger.info("querry metadata by experiment for: " + setupXId);

					return provider.getMetaInformationByExperiment(setupXId);
				}

				public String getMetaInformationBySample(String setupXId) throws BinBaseException {
					logger.info("querry metadata by sample for: " + setupXId);

					return provider.getMetaInformationBySample(setupXId);
				}

				public String getSetupXId(String sampleName) throws BinBaseException {
					logger.info("get setupX id for: " + sampleName);

					String id = provider.getSetupXId(sampleName);

					logger.info("id is: " + id);

					return id;
				}

				public void upload(String experimentId, String content) throws BinBaseException {
					logger.info("update setupx data for " + experimentId);
					provider.upload(experimentId, content);
				}

				@Override
				public boolean canCreateBins(String setupxId)
						throws BinBaseException {
					return provider.canCreateBins(setupxId);
				}
			};
		}

		catch (NamingException e) {
			throw new RuntimeException(e);
		}
	}

}
