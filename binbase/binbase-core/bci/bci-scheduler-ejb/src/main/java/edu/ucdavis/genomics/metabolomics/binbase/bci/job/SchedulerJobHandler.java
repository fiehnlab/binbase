package edu.ucdavis.genomics.metabolomics.binbase.bci.job;

import java.io.Serializable;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseServiceBean;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyFactory;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.EjbClient;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.handler.AbstractClusterHandler;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;

/**
 * used to schedule jobs
 * 
 * @author wohlgemuth
 * 
 */
public class SchedulerJobHandler extends AbstractClusterHandler {

	private Logger logger = Logger.getLogger(getClass());

	@Override
	protected boolean startProcessing() throws Exception {
		if (this.getObject() instanceof SchedulerJob) {
			SchedulerJob job = (SchedulerJob) this.getObject();

			logger.info("received job: " + job);
			Serializable task = job.getTask();

			BinBaseService service = (BinBaseService) EjbClient.getRemoteEjb(
					Configurator.BCI_GLOBAL_NAME, BinBaseServiceBean.class);

			if (task instanceof Experiment) {
				logger.info("it's an experiment: " + task);
				Experiment exp = (Experiment) task;
				try {
					service.triggerExportSecure(exp, KeyFactory.newInstance()
							.getKey());
				} catch (Exception e) {
					getReport().report(exp, Reports.FAILED, Reports.EXPERIMENT,
							e);
					throw e;
				}
			} else if (task instanceof ExperimentClass) {
				logger.info("it's an experiment class: " + task);
				ExperimentClass clazz = (ExperimentClass) task;
				try {
					service.triggerImportClassSecure(clazz, KeyFactory
							.newInstance().getKey());
				} catch (Exception e) {
					getReport().report(clazz, Reports.FAILED, Reports.CLASS, e);
					throw e;
				}
			} else if (task instanceof DSL) {
				logger.info("it's a DSL: " + task);
				DSL clazz = (DSL) task;
				try {
					service.triggerDSLCalculations(clazz,
							KeyFactory.newInstance().getKey());
				} catch (Exception e) {
					getReport().report(clazz, Reports.FAILED, Reports.DSL, e);
					throw e;
				}
			} else {
				logger.info("not supported task!");
				return false;
			}
		}
		return true;
	}

}
