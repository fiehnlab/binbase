package edu.ucdavis.genomics.metabolomics.binbase.dsl.parser;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfigable;

class DSLConfigExtractorTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseDSL() {
		DSLConfigExtractor ex = new DSLConfigExtractor()
		
		XMLConfigable config = ex.parseDSL ("""
		
			test{
			}
			test2{
			}
			config{
				bin{
					allow{
						false
					}
				}
				samples{
					sample{
					}
					sample2{
					}
				}
			}
		
		
		""")
		
		
		assert config.getElement ("bin.allow") != null
		assert config.getElement ("samples.sample") != null
		assert config.getElement ("samples.sample2") != null
		
	}
}
