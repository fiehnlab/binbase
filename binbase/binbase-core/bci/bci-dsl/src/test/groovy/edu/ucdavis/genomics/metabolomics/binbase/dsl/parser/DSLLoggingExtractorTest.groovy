package edu.ucdavis.genomics.metabolomics.binbase.dsl.parser;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfigable;

class DSLLoggingExtractorTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testParseDSL() {
		DSLConfigExtractor ex = new DSLConfigExtractor()
		
		new DSLLoggingExtractor().configureLog4j ("""
		
			test{
			}
			test2{
			}
			config{
				bin{
					allow{
						false
					}
				}
				samples{
					sample{
					}
					sample2{
					}
				}
			}
			logging{
			   'log4j:configuration'('xmlns:log4j':"http://jakarta.apache.org/log4j/"){

			   		appender(name:"console", class:"org.apache.log4j.ConsoleAppender"){
			   			param(name:"Target", value:"System.out")
			   			param(name:"Threshold", value:"debug")
			   				
			   			layout(class:"org.apache.log4j.PatternLayout")
			   		}
			   		
			   		
			   		root{
			   			"appender-ref"(ref:"console")
			   		}
			   		
			   }
			}
		
		
		""")
		
		
		Logger.getLogger(getClass()).info("Tada!!!!")
		
	}
}
