export{
	//name of the generated experiment
	name "test"

	//server of the used binbase instance
	server "lila"

	//database of the used binbase instance
	column "test"

	//definition for class: test with 1 samples
	Class(name: "test") {
		sample "test"
	}
	//end of definition for class: test

	//defintion for the report
	report{

		//specification how far we want to size down the dataset
		sizedown 80

		//do we want to replace zeros
		replace

		//specified output format
		format "xls"
	}
	//end of report definiton
	
	//overwrites the configuration options and elements need to specified as in the binbase.sql target file
	config{
		bin{
			allow(minimumClassSize=1)
		}
	}	
}
//end of DSL definition
