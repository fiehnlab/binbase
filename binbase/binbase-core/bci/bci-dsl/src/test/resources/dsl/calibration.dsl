export{

	server "test"
	
	name "calibration file examle"
	
	newBins false
	
	column "test"
	
	multithread false
	
	caching false
	
	ignoreMissingSamples false
	

	report{

                //specification how far we want to size down the dataset
                sizedown 10

                //specified output format
                format "xls"
                
                //we want to replace values. Mandetory for calibrations
                replace true
                
                //we want the calibration applied in this report
                calibration true
        }

   //this is a calibration class to be used for calibrations. There should only 1 exist for each dsl
	CalibrationClass(name:"test"){
	
		sample "a", 0.01
		sample "b", 0.05
		sample "c", 0.1
		sample "d", 0.25
		sample "e", 0.5
		sample "f", 1
		sample "g", 2.5
		sample "h", 5
		sample "i", 10
		sample "j", 20
		
	}
	
	//we need to specify bins, since we have a calibration
	bin "alanine"
	bin "glucose"
	bin "cytosine"
}