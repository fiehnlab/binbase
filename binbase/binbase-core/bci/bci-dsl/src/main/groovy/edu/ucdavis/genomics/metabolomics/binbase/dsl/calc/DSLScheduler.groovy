package edu.ucdavis.genomics.metabolomics.binbase.dsl.calc

import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseServiceFactory
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample
import edu.ucdavis.genomics.metabolomics.binbase.dsl.io.ConfiguratorSource
import edu.ucdavis.genomics.metabolomics.binbase.dsl.io.GenerateConfigFile
import edu.ucdavis.genomics.metabolomics.binbase.dsl.validator.ValidateDSL
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException
import org.apache.log4j.Logger

import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFileFactory;
import edu.ucdavis.genomics.metabolomics.util.status.ReportFactory
import edu.ucdavis.genomics.metabolomics.util.io.source.Source
import edu.ucdavis.genomics.metabolomics.binbase.dsl.io.SopGenerator
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass
import edu.ucdavis.genomics.metabolomics.util.status.Report

import edu.ucdavis.genomics.metabolomics.util.io.dest.Destination
import edu.ucdavis.genomics.metabolomics.util.io.dest.FileDestination
import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling

/**
 * User: wohlgemuth
 * Date: Sep 11, 2009
 * Time: 1:35:26 PM
 *
 */

public class DSLScheduler{

  Logger logger = Logger.getLogger(getClass())

  File dsl

  public DSLScheduler(File dsl){
    this.dsl = dsl  
  }

  /**
   * calculates all the data on the local pc without any support of clustering
   * or threading
   */
  public calculate(Map definition) {

	long begin = new Date().getTime()
	  
    //make sure the validation works
    assert new ValidateDSL().validate(definition), "sorry the configuration is invalid"

    //generate system settings
    String server = definition.server
    String database = definition.column

    logger.debug "using server: ${server}"
    logger.debug "using database: ${database}"

    //create the configurator
    
    XMLConfigurator.getInstance(new ConfiguratorSource(GenerateConfigFile.generateFile(server)))

	
    //connecto to our service
    BinBaseService service = BinBaseServiceFactory.createFactory().createService()
    String key = Configurator.getKeyManager().getInternalKey()

    //define the experiment
    Experiment experiment = new Experiment()

    experiment.setColumn(database)
    experiment.setId(definition.name)

    ExperimentClass[] classes = new ExperimentClass[definition.classes.size()]

    int counter = 0;


    boolean needCalculation = false
	//go over all classes and calculate the data
    definition.classes.each {
    	ExperimentClass clazz ->

      //assign to the classes arry, since we need it later
      classes[counter] = clazz
      counter++

    
      //make sure the server knows about this samples!
      clazz.getSamples().each {ExperimentSample sample ->
        logger.info("checking for samples: ${sample.getName()}")

        try {

        	
        	
          if (service.containsSample(sample.name, database, key)) {

            logger.info "sample ${sample.name} already exist in the database -> so we us it"
          }
          else {
            service.storeSample(new ExperimentSample(sample.getName(), sample.getId()), database, key)
            logger.info "found and stored!"
            needCalculation = true
          }

        }
        catch (BinBaseException e) {
          logger.error(e.getMessage(), e);
          logger.info "sorry something is wrong with the given file ${sample.getName()}!", e

          //clazz.removeSample sample.getName() 
        }
      }

      if (needCalculation) {
        //now we import the data

        logger.info "importing data"
		service.triggerImportClassSecure(clazz,key)

      }
      else{
    	  logger.info "clazz ${clazz.getId()} was upto data so no calculation neeeded"
      }

    }

    experiment.setClasses(classes)

    //generate sop file
    Source sop = SopGenerator.generateSOP(definition)


    //upload sop to server

    //assign sop o experiment
	
	service.triggerExportSecure(experiment,key)
	

  }

  private Report getReport() {
    return ReportFactory.newInstance().create("local-exporter")
  }

}