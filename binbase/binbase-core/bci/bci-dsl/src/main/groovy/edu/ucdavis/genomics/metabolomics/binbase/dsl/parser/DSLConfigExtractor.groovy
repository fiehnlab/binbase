package edu.ucdavis.genomics.metabolomics.binbase.dsl.parser

import org.apache.log4j.Logger;
import org.apache.xerces.parsers.XML11Configuration;

import edu.ucdavis.genomics.metabolomics.binbase.dsl.io.BinBaseConfigGenerator;
import edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfigable;
import edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfiguration;
import edu.ucdavis.genomics.metabolomics.util.io.source.ByteArraySource;

/**
 * a simple parser wich extracts our configuration part
 * and converts it to a XMLConfiguable
 */
class DSLConfigExtractor {
	Logger logger = Logger.getLogger (getClass())
	
	/**
	 * generate a binbase 
	 * @param content
	 * @return
	 */
	public XMLConfigable parseDSL(String content){
		
		String result = ""
		
		int bracketCounter
		
		boolean started = false
		content.eachLine {String line ->
			
			if(line.trim().startsWith ("config") && line.trim().endsWith("{")){
				bracketCounter = 0
				started = true
			}
			if(started){
				if(line.trim().endsWith("{")){
					bracketCounter++
				}
				else if(line.trim().endsWith("}")){
					bracketCounter--
				}
				result = "${result}\n${line}"
				
				if(bracketCounter == 0){
					started = false
				}
			}
		}
		
		return new BinBaseConfigGenerator().generateBinBaseOptionsAsConfiguable(result)
	}
}
