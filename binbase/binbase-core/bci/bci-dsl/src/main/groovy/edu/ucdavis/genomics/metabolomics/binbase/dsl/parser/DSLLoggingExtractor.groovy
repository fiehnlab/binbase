package edu.ucdavis.genomics.metabolomics.binbase.dsl.parser;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import edu.ucdavis.genomics.metabolomics.binbase.dsl.io.BinBaseConfigGenerator;
import edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfigable;
import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling;

/**
 * used to extract the logging configuration and setting up log4j
 * @author wohlgemuth
 *
 */
public class DSLLoggingExtractor {
	
	
	/**
	 * generate a binbase 
	 * @param content
	 * @return
	 */
	void configureLog4j(String content){
		
		String result = ""
		
		int bracketCounter
		
		boolean started = false
		content.eachLine {String line ->
			
			if(line.trim().startsWith ("logging") && line.trim().endsWith("{")){
				bracketCounter = 0
				started = true
			}
			else{
				if(started){
					if(line.trim().endsWith("{")){
						bracketCounter++
					}
					else if(line.trim().endsWith("}")){
						bracketCounter--
					}
					result = "${result}\n${line}"
					
					if(bracketCounter == 0){
						started = false
					}
				}
			}
		}
		
		
		result = new BinBaseConfigGenerator().generateBinBaseOptions (result)
		
		result = "<!DOCTYPE log4j:configuration SYSTEM \"log4j.dtd\">\n${result}"
		
		File file = File.createTempFile ("binbase", ".log")
		file.deleteOnExit()
		file.withWriter { out -> 
			out.println(result)
		}
		DOMConfigurator.configure(file.absolutePath)
	}
}
