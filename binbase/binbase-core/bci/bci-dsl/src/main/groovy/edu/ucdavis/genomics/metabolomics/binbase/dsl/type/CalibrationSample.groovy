package edu.ucdavis.genomics.metabolomics.binbase.dsl.type

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;

/**
 * a sample used for calibrations 
 * @author wohlgemuth
 *
 */
class CalibrationSample extends ExperimentSample{
	double concentration
	
	public void setConcentration(double conc){
		this.concentration = conc
	}
	
	public double getConcentration(){
		return concentration
	}
}
