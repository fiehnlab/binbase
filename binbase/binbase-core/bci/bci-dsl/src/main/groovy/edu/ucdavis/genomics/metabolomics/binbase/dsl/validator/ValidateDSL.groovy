package edu.ucdavis.genomics.metabolomics.binbase.dsl.validator

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass

/**
 * User: wohlgemuth
 * Date: Sep 15, 2009
 * Time: 1:50:04 PM
 * 
 */

public class ValidateDSL {


	/**
	 * a very very simple validation and does not cover all possibilites	
	 * @param map
	 * @return
	 */
	public boolean validate(Map map){
		assert map != null, "sorry you need to provide a map!"

		assert map.server != null, "you need to provide a server"
		assert map.column != null, "you need to provide a column"
		assert map.name != null, "you need to provide a name"

		if(map.based == null){
			assert map.classes != null, "you need to provide at least one class"


			if (map.classes != null) {
				map.classes.each {ExperimentClass clazz ->
					clazz.setColumn map.column

					assert clazz.getSamples().length > 0, "you need to provide at leat 1 sample for a class"
				}
			}
		}

		if(map.sop){
			assert map.report == null, "sorry you cant specify the use of a default sop and reports"
			assert map.postActions == null, "sorry you cant specify the use of a default sop and post actions"
			assert map.preActions == null, "sorry you cant specify the use of a default sop and pre actions"
			assert map.reference == null, "sorry you cant specify the use of a default sop and a reference file"
		}
		else{

			//if there are no actions, we need a report section
			if(map.postActions == null && map.preActions == null){
				assert map.report != null, "sorry I didn't find a report definition, please provide one or some 'action' elements"
			}

			//if a report is set
			if(map.report != null){
				map = checkReports(map)
			}
	
				}
		if( map.classes != null && map.based != null) throw new AssertionError( "you can not have classes and basedOn speficied at the same time. Please pick one")



		//returning true
		true
	}

	private Map checkReports(Map map) {
		map.report.each { Map current ->

			//validate the calibration settings
			if(current.calibration){
				assert map.compounds != null && map.compounds.size > 0, "you need to provide at least 1 compound for the calibration feature to work"

				assert current.attribute == "height", "sorry calibration mode only works, if your report is using height as attribute"
			}
		}
		return map
	}
}