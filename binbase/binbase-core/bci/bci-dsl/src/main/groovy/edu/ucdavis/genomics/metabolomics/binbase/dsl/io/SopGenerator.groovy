package edu.ucdavis.genomics.metabolomics.binbase.dsl.io

import edu.ucdavis.genomics.metabolomics.util.io.source.ByteArraySource
import edu.ucdavis.genomics.metabolomics.util.io.source.Source
import groovy.xml.StreamingMarkupBuilder
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass
import edu.ucdavis.genomics.metabolomics.binbase.dsl.type.CalibrationClass;
import edu.ucdavis.genomics.metabolomics.binbase.dsl.type.CalibrationSample;

import org.apache.log4j.Logger
import org.jdom.output.XMLOutputter
import org.jdom.output.Format
import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling

/**
 * User: wohlgemuth
 * Date: Sep 17, 2009
 * Time: 12:07:30 PM
 *
 */

public class SopGenerator {
	static Logger logger = Logger.getLogger(SopGenerator.class)
	/**
	 * generates the SOP based on the configuration
	 */
	static Source generateSOP(Map configuration) {


		//use the specified sop from the system
		if(configuration.sop instanceof String){
			return new ByteArraySource(Configurator.getExportService().getSop(configuration.sop))
		}

		//use the default sop from the system
		else if(configuration.sop){
			return new ByteArraySource(Configurator.getExportService().getSop(Configurator.getExportService().getDefaultSop()))
		}
		//let's generate a sop based on the configuration
		else{
			String generated = new StreamingMarkupBuilder().bind({


				//pre actions
				sop(desc: "generated for quantification") {
					if (configuration.preActions != null) {
						configuration.preActions.each { Map settings ->

							"pre-action"(method: settings.method, column: configuration.column) {
								format(type: ((String) settings.format).toUpperCase())

								settings.arguments.keySet().each {String key ->
									if (settings.arguments.get(key) instanceof Collection) {
										argument(name: key) {
											settings.arguments.get(key).each { def val ->
												value(val)
											}
										}
									}
									else {
										argument(name: key, value: settings.arguments.get(key))
									}
								}

								//taking care of any special configurations
								Collection config = settings.config

								if(config != null){
									config.each{ Map map ->
										map.keySet().each { def key ->

											println(map.get(key).getClass())
											"${key}"(map.get(key))
										}
									}
								}
							}
						}
					}

					//post actions
					if (configuration.postActions != null) {
						configuration.postActions.each { Map settings ->

							"post-action"(method: settings.method, column: configuration.column) {
								format(type: ((String) settings.format).toUpperCase())

								settings.arguments.keySet().each {String key ->
									if (settings.arguments.get(key) instanceof Collection) {
										argument(name: key) {
											settings.arguments.get(key).each { def val ->
												value(val)
											}
										}
									}
									else {
										argument(name: key, value: settings.arguments.get(key))
									}
								}

								//taking care of any special configurations
								Collection config = settings.config

								if(config != null){

									config.each{ Map map ->
										map.keySet().each { def key ->
											"${key}"(map.get(key))
										}
									}
								}
							}
						}
					}

					//generate the report section of the SOP file
					configuration.report.each { Map report ->

						//if no sizeDown is specified let's just add at least one element with 0
						if(report.sizeDown == null || report.sizeDown.size() == 0){
							report.sizeDown = []
							report.sizeDown.add(0)
						}

						//iterate over all sizeDown elements and build the XML file
						report.sizeDown.each {int sizeDown ->

							String folder = "${report.attribute}"

							if(report.folder != null && report.folder.toString().length() > 0){
								folder = report.folder
							}

							//generate the transform section
							transform(sizedown: sizeDown, attribute: report.attribute, combine: true, folder:folder) {
								header {
									param(value: "retention_index")
									param(value: "quantmass")
									param(value: "id")
									param(value: "spectra")
								}

								//filtering of bins
								if (configuration.compounds.size() > 0) {

									filter {

										configuration.compounds.each { String name ->

											bin(match: name)
										}
									}
								}

								//we want to use the calibration mode
								if(report.calibration){

									calibration{


										def cals = report.calibration

										if(cals instanceof Map){


											if(cals.combinationMethod){
												combination(method:cals.combinationMethod)
											}
											else{
												combination(method:"edu.ucdavis.genomics.metabolomics.util.statistics.deskriptiv.Mean")
											}


											//pre processing configuration
											cals.preProcess.each { Map settings ->
												"pre-processable"(method: settings.method) {
													format(type: ((String) settings.format).toUpperCase())

													settings.arguments.keySet().each {String key ->
														if (settings.arguments.get(key) instanceof Collection) {
															argument(name: key) {
																settings.arguments.get(key).each { def val ->
																	value(val)
																}
															}
														}
														else {
															argument(name: key, value: settings.arguments.get(key))
														}
													}
												}
											}

											cals.regression.each{  regressions ->
												regression{
													regressions.each{Double value ->
														concentration(value:value)
													}
												}
											}

											//post processing
											cals.postProcess.each { Map settings ->
												"post-processable"(method: settings.method) {
													format(type: ((String) settings.format).toUpperCase())

													settings.arguments.keySet().each {String key ->
														if (settings.arguments.get(key) instanceof Collection) {
															argument(name: key) {
																settings.arguments.get(key).each { def val ->
																	value(val)
																}
															}
														}
														else {
															argument(name: key, value: settings.arguments.get(key))
														}
													}
												}
											}
										}

										configuration.compounds.each { String name ->

											standard(name: name){

												configuration.classes.each{ ExperimentClass clazz ->

													if(clazz instanceof CalibrationClass){

														clazz.getSamples().each{  s ->
															sample(name:s.getName(), concentration:s.getConcentration()){
															}
														}
													}
												}
											}
										}
									}
								}

								//reference files - calibrations are not possible with this file
								if(configuration.reference != null){
									println "writing reference: ${configuration.reference}"
									reference(experiment:configuration.reference)
								}

								//check if we need replacement
								if (report.replace != null) {
									processing {
										report.replace.each { Map replacebleAction ->

											println "replaceable action: ${replacebleAction}"
											//simple default replacement
											if (replacebleAction.replace instanceof Boolean) {
												println "replacement is specified as boolean..."
												if (replacebleAction.replace) {

													"zero-replacement"(method: "edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.ReplaceWithQuantIntesnityBasedOnAverageRTwithRiCurveFallback", range: "sample") {

														if(configuration.report.format instanceof List ){
															configuration.report.format.each{def f ->
																println "${f} - ${f.class}}"
																String value = f.toString()

																format(type: value.toUpperCase())
															}
														}
														else{
															format(type: ((String) configuration.report.format).toUpperCase())
														}
													}
												}
											}
											else if (replacebleAction instanceof Map) {
												println "replacement is specified as map..."

												if (replacebleAction.method == null) {
													replacebleAction.method = "edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.ReplaceWithQuantIntesnityBasedOnAverageRTwithRiCurveFallback"
												}

												"zero-replacement"(method: replacebleAction.method.toString(), range: "sample") {

													//set the format
													if (replacebleAction.format == null) {

														if(configuration.report.format instanceof List ){
															configuration.report.format.each{def f ->
																format(type: f.toString().toUpperCase())
															}
														}
														else{
															format(type: ((String) configuration.report.format).toUpperCase())
														}
													}
													else {
														format(type: ((String) replacebleAction.format).toUpperCase())
													}
													//pre processing configuration
													replacebleAction.preProcess.each { Map settings ->
														"pre-processable"(method: settings.method) {
															format(type: ((String) settings.format).toUpperCase())

															settings.arguments.keySet().each {String key ->
																if (settings.arguments.get(key) instanceof Collection) {
																	argument(name: key) {
																		settings.arguments.get(key).each { def val ->
																			value(val)
																		}
																	}
																}
																else {
																	argument(name: key, value: settings.arguments.get(key))
																}
															}
														}
													}

													//post processing
													replacebleAction.postProcess.each { Map settings ->
														"post-processable"(method: settings.method) {
															format(type: ((String) settings.format).toUpperCase())

															settings.arguments.keySet().each {String key ->
																if (settings.arguments.get(key) instanceof Collection) {
																	argument(name: key) {
																		settings.arguments.get(key).each { def val ->
																			value(val)
																		}
																	}
																}
																else {
																	argument(name: key, value: settings.arguments.get(key))
																}
															}
														}
													}
												}
											}
											//replacements is assumed to be a class name
											else {
												println "replacement is specified as class"

												"zero-replacement"(method: replacebleAction.replace.toString(), range: "sample") {

													if(configuration.report.format instanceof List ){
														configuration.report.format.each{def f ->
															format(type: f.toString().toUpperCase())
														}
													}
													else{
														format(type: ((String) configuration.report.format).toUpperCase())
													}
												}
											}
										}

										//generate the processing instructions
										if (report.processable != null) {
											report.processable.each { Map settings ->

												"processable"(method: settings.method) {
													format(type: ((String) settings.format).toUpperCase())

													settings.arguments.keySet().each {String key ->
														if (settings.arguments.get(key) instanceof Collection) {
															argument(name: key) {
																settings.arguments.get(key).each { def val ->
																	value(val)
																}
															}
														}
														else {
															argument(name: key, value: settings.arguments.get(key))
														}
													}
												}
											}
										}
									}
								}

								rawdata(folder: "rawdata") {
									if(report.format){
										format(type: ((String) report.format).toUpperCase())
									}
									else{
										format(type: "XLS")
									}
								}
							}
						}
					}
				}
			}).toString()

			return new ByteArraySource(generated.getBytes())
		}
	}
}