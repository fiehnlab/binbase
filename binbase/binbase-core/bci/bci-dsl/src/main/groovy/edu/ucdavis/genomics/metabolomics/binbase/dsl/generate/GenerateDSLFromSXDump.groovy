package edu.ucdavis.genomics.metabolomics.binbase.dsl.generate;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.dsl.io.ConfiguratorSource;
import edu.ucdavis.genomics.metabolomics.binbase.dsl.io.GenerateConfigFile;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;

/**
 * generates a dsl from a SX dump file
 * @author wohlgemuth
 *
 */
public class GenerateDSLFromSXDump {

	public String dslGenerateFromXMLDump(File xmlFile, String column, String server = "127.0.0.1", boolean replace = true, int sizeDown = 80, String format = "xls",def closure = null,int classSize = 50,boolean logging = true){

		XMLConfigurator.getInstance(new ConfiguratorSource(GenerateConfigFile.generateFile(server)))
		

		def root = new XmlSlurper().parse(xmlFile)

		//build the studie
		String id = root.@id.text()
		String title = root.@title.text()

		Experiment experiment = new Experiment();
		experiment.setId(id);
		experiment.setColumn(column);


		List<ExperimentClass> classesList = new ArrayList<ExperimentClass>()

		//build the classes
		root.classes."class".each {def clazz ->

			ExperimentClass experiementClass = new ExperimentClass()
			experiementClass.id = clazz.@id.text()

			List<ExperimentSample> samples = new ArrayList<ExperimentSample>()

			//define all the samples
			clazz.samples.sample.each { def sample ->

				String fileName = sample.@fileName.text().split(",")[0]

				//make sure there are no duplicated sampels in this dataset
				ExperimentSample experimentSample = new ExperimentSample(fileName,fileName)
				samples.add(experimentSample)
			}

			experiementClass.setSamples((ExperimentSample[])samples.toArray())
			classesList.add(experiementClass)
		}

		experiment.setClasses((ExperimentClass[])classesList.toArray())

		return new GenerateDSLFromExperiment().dslGenerateForExperiment(experiment,server,replace,sizeDown,format,closure,classSize,true)
	}

}
