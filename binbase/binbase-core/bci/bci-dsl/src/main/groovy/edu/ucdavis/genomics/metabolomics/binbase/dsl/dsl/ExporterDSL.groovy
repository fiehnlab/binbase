package edu.ucdavis.genomics.metabolomics.binbase.dsl.dsl

import java.util.Map.Entry;
import java.util.regex.Matcher
import java.util.regex.Pattern

import org.apache.log4j.Logger

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXFactory
import edu.ucdavis.genomics.metabolomics.binbase.dsl.calc.DSLScheduler
import edu.ucdavis.genomics.metabolomics.binbase.dsl.io.BinBaseConfigGenerator
import edu.ucdavis.genomics.metabolomics.binbase.dsl.parser.*
import edu.ucdavis.genomics.metabolomics.binbase.dsl.type.CalibrationClass;
import edu.ucdavis.genomics.metabolomics.binbase.dsl.type.CalibrationSample
import edu.ucdavis.genomics.metabolomics.binbase.dsl.type.ForcedSample
import edu.ucdavis.genomics.metabolomics.binbase.dsl.validator.ValidateDSL

/**
 * this descripes the complete dialect of the BinBase dsl, which can and should be used for custom
 * calculations
 */
class ExporterDSL {

	private Logger logger = Logger.getLogger(getClass())

	public ExporterDSL() {
		logger.info "init exporter"
	}

	/**
	 * runs the dsl defined in the given file
	 * @param file
	 */
	void run(File file) {
		logger.info "reading file: ${file}"
		assert file.exists(), "please make sure the file exist's"

		logger.info "reading rules"
		Map dsl = readExporterRules(file);

		logger.info "validation rules"
		assert new ValidateDSL().validate(dsl), "sorry your provided dsl is invalid!"

		try {

			DSLScheduler implementation = new DSLScheduler(file)
			logger.info "start caclulation for defined rules"
			implementation.calculate(dsl)
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * this reads the defined DSL into a map and works for files of any length
	 */
	Map readExporterRules(def dsl) {


		String content = ""
		if(dsl instanceof File){
			if (!dsl.exists()) {
				throw new FileNotFoundException("${dsl.getAbsolutePath()} does not exist!")
			}

			//read the file and parse it as script
			content = dsl.text

		}
		else{
			content = dsl.toString()
		}

		logger.info content

		//build the result
		Map result = new HashMap()

		def matcher = (content =~ /.*config *\{.*/)
		//extract possible configuration options
		if(matcher.getCount() > 0){
			logger.info "found matching configuration..."

			DSLConfigExtractor ex = new DSLConfigExtractor()
			result.configuration = ex.parseDSL (content)
		}
		else{
			result.configuration = new BinBaseConfigGenerator().generateBinBaseOptionsAsConfiguable ("config{}")

		}

		matcher = (content =~ /.*logging.*\{.*/)
		//extract possible configuration options
		if(matcher.getCount() > 0){
			logger.info "found logging configuration..."

			DSLLoggingExtractor ex = new DSLLoggingExtractor()
			ex.configureLog4j (content)
		}

		//check if the script is to big.
		if(content.size() > 30000){

			logger.info "it's a very long dsl, so we split it into subfiles"
			//extract the class objects
			Pattern regex = Pattern.compile(/Class.*\{([^}]+)\}/);
			Matcher regexMatcher = regex.matcher(content);

			regexMatcher.each {
				String group = regexMatcher.group().toString()
				GroovyShell shell = new GroovyShell()

				Script dslScript = shell.parse(group)

				try{
					result = parseScript(dslScript,group, result)
				}
				catch (Exception e) {
				}
			}

			content = regex.matcher(content).replaceAll( "")

			GroovyShell shell = new GroovyShell()
			Script dslScript = shell.parse(content)
			result = parseScript(dslScript,content, result)

		}
		//ok its a tiny script so do itt in one session
		else{
			logger.info "it's a short dsl, so we calculate it in one run"

			Script dslScript = new GroovyShell().parse(content)

			result = parseScript(dslScript, content, result)
		}

		//validate our dsl
		new ValidateDSL().validate(result)

		//return our created result
		return result
	}

	/**
	 * actually parses the DSL
	 * @param dslScript
	 * @param content
	 * @param result
	 * @return
	 */
	private Map parseScript(Script dslScript, String content, Map result) {
		dslScript = new GroovyShell().parse(content)

		dslScript.metaClass = createEMC(dslScript.class, { ExpandoMetaClass emc ->

			//work on the top export element
			emc.export = { Closure closure ->

				//assign the delegate
				closure.delegate = new ExportDelegate(result)

				//we want that methods are looked up first in the delegate
				closure.resolveStrategy = Closure.DELEGATE_FIRST

				//execute the closure
				closure()
			}

			//work on the classes which are externalized in some dsl
			emc.Class= { Map configuration, Closure closure ->

				//assign the delegate
				closure.delegate = new ClassDelegate(result,configuration)

				//we want that methods are looked up first in the delegate
				closure.resolveStrategy = Closure.DELEGATE_FIRST

				//execute the closure
				closure()

			}
		})

		//execute the parsed script
		dslScript.run()
		return result
	}

	/**
	 * create the meta class
	 */
	static ExpandoMetaClass createEMC(Class clazz, Closure cl) {
		ExpandoMetaClass emc = new ExpandoMetaClass(clazz, false)

		cl(emc)

		emc.initialize()
		return emc
	}
}

/**
 * defines the new bin closure and allows to modify the bin generation behavior. This does not work for the 
 * DSL scheduler, since there is currently no way to enforce bin generation remotely
 * @author wohlgemuth
 *
 */
public class NewBinDelegate {

	Map result = null

	def NewBinDelegate(Map result){
		this.result = result

		this.result.newBinFilenamePatterns = new HashSet()
		this.result.newBinClassPatterns = new HashSet()

	}

	/**
	 * disables the bin generation for the given pattern
	 * you can add as many as you want
	 * @param pattern
	 * @return
	 */
	def disableForFileName(String pattern){
		this.result.newBinFilenamePatterns.add(pattern)
	}

	/**
	 * disables the generation for the classname or classname pattern
	 * @param pattern
	 * @return
	 */
	def disableForClassName(String pattern){
		this.result.newBinClassPatterns.add(pattern)
	}
}

/**
 * a calibration class. We will automatically
 * @author wohlgemuth
 *
 */
public class CalibrationClassDelegate extends ClassDelegate{

	def CalibrationClassDelegate(Map result, Map configuration) {
		super(result,configuration,new CalibrationClass())
	}

	/**
	 * not supported in this class
	 */
	def sample(String name,String machine = null) {
		throw new RuntimeException("sorry this method is not supported for this kind of class, you need to specify a concentration")
	}


	/**
	 * not supported in this class
	 */
	@Override
	def samples(String[] samples){
		throw new RuntimeException("sorry this method is not supported for this kind of class, you need to specify a concentration")
	}

	/**
	 * register samples as map
	 * @param samples
	 * @return
	 */
	def samples(Map<String, Double> samples){
		samples.each { Entry<String, Double> entry ->
			sample(entry.getKey(),entry.getValue())
		}
	}

	/**
	 * register a sample with the specific concentration
	 * @param name
	 * @param concentration
	 * @return
	 */
	def sample(String name,double concentration) {
		def sample = new CalibrationSample()
		sample.concentration = concentration
		registerSample(sample,name)
	}

	/**
	 * only calibration no import.
	 * @param value
	 * @return
	 */
	def calibrationOnly(boolean value){
		((CalibrationClass)this.clazz).calibrationOnly = value
	}

}
/**
 * defines the class element and it's sub elements in the DSL.
 */
public class ClassDelegate {

	Map result = null;

	Map configuration = null;

	ExperimentClass clazz;

	def ClassDelegate(Map result){
		this(result,[:])
	}

	def ClassDelegate(Map result, Map configuration){
		this(result,configuration, new ExperimentClass())
	}
	def ClassDelegate(Map result, Map configuration, ExperimentClass clazz) {
		this.result = result

		this.clazz = clazz

		if (configuration.name == null) {
			clazz.setId("binbase-${new Random().nextLong()}")
		}
		else {
			clazz.setId(configuration.name)
		}

		this.configuration = configuration

		//check if we already have classes declared, if not add one
		if (result.classes == null) {
			result.classes = []
		}

		//add our class to the result set
		result.classes.add(this.clazz)

	}


	/**
	 * registers a sample for a class with the given name. You can provide as many elements of this as you want
	 */
	def sample(String name,String machine = null) {

		ExperimentSample sample = null

		name = name.trim()

		if(machine != null){

			if(machine.length() != 1){
				throw new Exception("the machine name has to be exactly 1 character long")
			}
			sample = new ForcedSample()
			sample.machine = machine
		}
		else{
			sample = new ExperimentSample()
		}

		registerSample(sample,name)
	}

	/**
	 * add several samples at once
	 * @param samples
	 * @return
	 */
	def samples(String[] samples){
		samples.each{ sample(it) }
	}

	/**
	 * registers the sample
	 * @param name
	 * @return
	 */
	protected def registerSample(ExperimentSample sample,String name){
		sample.setName(name)
		sample.setId(name)

		if (this.clazz.getSamples() == null) {
			ExperimentSample[] samples = new ExperimentSample[1]
			samples[0] = sample
			this.clazz.setSamples(samples)
		}
		else {
			ExperimentSample[] samples = new ExperimentSample[clazz.getSamples().length + 1]
			ExperimentSample[] samples2 = clazz.getSamples()

			for(int i = 0; i < samples2.length; i++){
				samples[i] = samples2[i]
			}
			samples[samples2.length] = sample
			this.clazz.setSamples(samples)
		}
	}
}

/**
 * this is the first and main element of the DSL, definening the actual element and all
 * sub elements
 */
public class ExportDelegate {

	Map result = null;

	/**
	 * basic constructor, should be mostly ignored and is used internally
	 * @param result
	 */
	def ExportDelegate(Map result) {

		this.result = result
		this.result.caching = false
		this.result.newBins = false
		this.result.multithread = false
		this.result.clustered = false
		this.result.compounds = []
	}

	/**
	 * this enables the use of cached data
	 * @return
	 */
	def caching() {
		result.caching = true
	}

	/**
	 * explicetly enables or disables the caching of used data in the system
	 * @param value
	 * @return
	 */
	def caching(boolean value) {
		result.caching = value
	}

	/**
	 * explicetly sets that missing samples are ignored and won't cancel the calculation
	 * @param value
	 * @return
	 */
	def ignoreMissingSamples(boolean value){
		result.ignoreMissingSamples = value
	}

	/**
	 * we ignore missing samples and don't cancel the calculation
	 * @return
	 */
	def ignoreMissingSamples(){
		ignoreMissingSamples(true)
	}

	/**
	 * explecitely sets or disables the new bin generation of this experiment
	 * @param value
	 * @return
	 */
	def newBins(boolean value){
		result.newBins = value
	}

	/**
	 * this experiment has the possibility to generate new bins
	 * @return
	 */
	def newBins(){
		result.newBins = true
	}

	/**
	 * enables the bin generation and allows us to define certain attributes. Please look
	 * at the newBin class for more details
	 * @param closure
	 * @return
	 */
	def newBins(Closure closure){
		newBins()
		closure.delegate = new NewBinDelegate(result)

		//we want that methods are looked up first in the delegate
		closure.resolveStrategy = Closure.DELEGATE_FIRST

		//execute the closure
		closure()

	}

	/**
	 * specifies the classname which should be used to retrieve the metadate. It needs to be an instance of
	 * @class {@link SetupXFactory}
	 * @param className
	 * @return
	 */
	def metaData(String className){
		def data = Class.forName (className).newInstance()
		assert data instanceof SetupXFactory, "sorry you need to provide a class name which is an instanceof the class SetupXFactory"

		result.metaData = className
	}

	/**
	 * on which column do we want to calculate this expiriment
	 * @param column
	 * @return
	 */
	def column(String column) {
		result.column = column
	}

	/**
	 * from which application server do we get additionally needed configuration data for this calculation
	 * @param server
	 * @return
	 */
	def server(String server) {
		result.server = server
	}

	/**
	 * do we want to use the default sop. This means no actios or reports of any kind
	 * @param value
	 * @return
	 */
	def defaultSop(boolean value){
		if(value){
			result.sop = true
		}
	}
	
	/**
	 * use the specified sop on the server
	 * @param name
	 * @return
	 */
	def useSop(String name){
		result.sop = name
	}

	/**
	 * to avoid typing out all the class information
	 * we can just base a dsl on an existing file. You can specify several basedOn fields
	 * @param experimentId
	 * @return
	 */
	def basedOn(String experimentId){
		basedOn ([experimentId])
	}

	/**
	 * if we have an array of string to build the experiment
	 * @param experimentIds
	 * @return
	 */
	def basedOn(String[] experimentIds){
		experimentIds.each { basedOn(it) }
	}

	/**
	 * you can specify several experiments this one is based on.
	 * @param experimentId
	 * @return
	 */
	def basedOn(List experimentIds){
		if(result.classes !=null){
			throw new Exception("sorry you can not specify this feature, if you already specified a class section!")
		}
		if(result.based == null){
			result.based = []
		}
		experimentIds.each {
			if(result.based.contains(it) == false ){
				result.based.add(it)
			}
		}
	}


	/**
	 * are we allowed to use multithreading
	 * @param multithread
	 * @return
	 */
	def multithread(boolean multithread){
		if(result.clustered == true){
			throw new Exception("sorry you cannot specify 'multithread = true' and 'clustered = true' at the same time!")
		}
		result.multithread = multithread
	}

	/**
	 * do we want to use the cluster
	 * @param clustered
	 * @return
	 */
	def clustered(boolean clustered){
		if(result.multithread == true){
			throw new Exception("sorry you cannot specify 'multithread = true' and 'clustered = true' at the same time!")
		}

		result.clustered = clustered
	}

	/**
	 * just a place holder for the configuration dsl.
	 * @param closure
	 * @return
	 */
	def config(Closure closure){

	}

	/**
	 * configures the enviorment and can be invoked several times
	 * @param env
	 * @return
	 */
	def environment(Map environment){
		if(result.environment == null){
			result.environment = [:]
		}

		result.environment.putAll(environment)
	}
	/**
	 * a placeholder for droppin logging configuration
	 * @param closure
	 * @return
	 */
	def logging(Closure closure){

	}

	/**
	 * if you provide the ID of an existing experiment, you can use this as a reference experiment. 
	 * This means you will only see the bins, which are found in the refrence in this experiment. For this function to work
	 * you need to have the refernce calculated prior on the same server/pc as this calcuation, since we access a the existing temporary
	 * data
	 */
	def reference(String reference){
		result.reference = reference
	}

	/**
	 * we define a bin to be listed in this calculation. You can specify as many as you want, but if you specify
	 * one you will need to specify all bins you want in your result
	 */
	def bin(String name) {
		assert name != null, "sorry please provide a name for the bin"

		if (result.compounds == null) {
			result.compounds = new HashSet<String>()
		}
		result.compounds.add(name)
	}

	/**
	 * do we want to use the existing result file? In this case there will be no matching of any form.
	 * @param existing
	 * @return
	 */
	def useExistingResultFile(boolean existing){
		result.useExistingResultFile = existing
	}
	/**
	 * simplistic way to define bins
	 * @param bins
	 * @return
	 */
	def bins(String[] bins){
		bins.each { bin(it) }
	}

	/**
	 * using a collection instead of an array
	 * @param bins
	 * @return
	 */
	def bins(List bins){
		bins.each { bin(it) }
	}



	/**
	 * this defines the name of your experiment
	 */
	def name(String name) {
		result.name = name
	}

	/**
	 * this closures defined the report, please look at @class {@link ReportDelegate} for more informations
	 */
	def report(Closure closure) {
		report(closure,"height")
	}

	/**
	 * this closures defined the report, please look at @class {@link ReportDelegate} for more informations
	 * the attribute specifes one of the alloed attributes for calculationes.
	 * 
	 * currently we support
	 * 
	 * 'spectra_id','apex_sn','unqiuemass','quantmass','purity','match','retentionindex','retentiontime','height','problematic'
	 * 
	 */ 
	def report(String attribute,Closure closure) {
		report(closure,attribute)
	}

	/**
	 * accepted attributes for the DSL report definition
	 */
	def acceptedAttributes = [
		'spectra_id',
		'apex_sn',
		'unqiuemass',
		'quantmass',
		'purity',
		'match',
		'retentionindex',
		'retentiontime',
		'height',
		'problematic'
	]

	/**
	 * this closures defined the report, please look at @class {@link ReportDelegate} for more informations
	 * the attribute specifes one of the alloed attributes for calculationes.
	 * 
	 * currently we support
	 * 
	 * 'spectra_id','apex_sn','unqiuemass','quantmass','purity','match','retentionindex','retentiontime','height','problematic'
	 * 
	 */
	def report(Closure closure,String attribute) {
		if(acceptedAttributes.contains(attribute)){
			//assign the delegate
			closure.delegate = new ReportDelegate(result,attribute)

			//we want that methods are looked up first in the delegate
			closure.resolveStrategy = Closure.DELEGATE_FIRST

			//execute the closure
			closure()
		}
		else{
			throw new RuntimeException("sory this attribute is not accepted ${attribute} , please use on of the accepted attributes (${acceptedAttributes})")
		}
	}

	/**
	 * upload configuration
	 * @param closure
	 * @return
	 */
	def upload(Closure closure){
		closure.delegate = new UploadDelegate(result)

		//we want that methods are looked up first in the delegate
		closure.resolveStrategy = Closure.DELEGATE_FIRST

		//execute the closure
		closure()
	}
	/**
	 * you can define as many Class elements as you want in an DSL and this defines the samples
	 * which will be calculated.
	 * Please look at {@link ClassDelegate} for more informations
	 */
	def Class(Map configuration, Closure closure) {

		if(this.result.based != null){
			throw new RuntimeException("sorry can only specify a class, if you don't use the 'basedOn' feature!")
		}

		//assign the delegate
		closure.delegate = new ClassDelegate(result, configuration)

		//we want that methods are looked up first in the delegate
		closure.resolveStrategy = Closure.DELEGATE_FIRST

		//execute the closure
		closure()
	}

	/**
	 * defines a class to be used for calibrationm which has no name
	 * @param configuration
	 * @param closure
	 * @return
	 */
	def CalibrationClass(Map configuration,Closure closure){
		//assign the delegate
		closure.delegate = new CalibrationClassDelegate(result, configuration)

		//we want that methods are looked up first in the delegate
		closure.resolveStrategy = Closure.DELEGATE_FIRST

		//execute the closure
		closure()

	}

	/**
	 * no name provided. It generates it own name
	 * @param closure
	 * @return
	 */
	def CalibrationClass(Closure closure){
		//assign the delegate
		closure.delegate = new CalibrationClassDelegate(result, [:])

		//we want that methods are looked up first in the delegate
		closure.resolveStrategy = Closure.DELEGATE_FIRST

		//execute the closure
		closure()

	}


	/**
	 * defines a sample to be used. Internally it will be wrapped into a random generated class
	 * @param name
	 * @return
	 */
	def sample(String name){
		new ClassDelegate(result).sample(name)
	}

	/**
	 * short version to define several samples at once
	 */
	def samples(String[] names){
		for(String name : names){
			sample(name)
		}
	}

	/**
	 * a pre Action is executed before any transformations are done on the final dataset, but will not modify the dataset!
	 * you can use it to generate additional reports
	 * 
	 * The map needs to contain the parameter: 'method' with a string value, which specify the class to use. The class needs to be an instance of 
	 * edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.action.Action
	 */
	def preAction(Map params,Closure config = null){
		assert params.method != null, "please provide a method name"

		if(result.preActions == null){
			result.preActions = []
		}
		def form = params.format

		if(params.format == null){
			form = "txt"
		}

		def method = params.method
		params.remove("method")
		params.remove("format")

		def configContent = []

		if(config){
			config.delegate = new ConfigDelegate(configContent)

			//we want that methods are looked up first in the delegate
			config.resolveStrategy = Closure.DELEGATE_FIRST

			//execute the closure
			config()
		}

		result.preActions.add(
				[
					'method':method,
					'arguments':params,
					'format':form,
					'config':configContent
				]
				)
	}


	/**
	 * wrapper around the post action object
	 * @param params
	 * @return
	 */
	def action(Map params, Closure config = null){
		postAction(params, config)
	}

	/**
	 * a post Action is executed after any transformations are done on the final dataset, but will not modify the dataset!
	 * you can use it to generate additional reports, based on the final database. A prime example would be to generate statistics
	 *
	 * The map needs to contain the parameter: 'method' with a string value, which specify the class to use. The class needs to be an instance of
	 * edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.action.Action
	 */
	def postAction(Map params,Closure config = null){
		assert params.method != null, "please provide a method name"

		if(result.postActions == null){
			result.postActions = []
		}
		def form = params.format

		if(params.format == null){
			form = "txt"
		}

		def method = params.method

		params.remove("method")
		params.remove("format")


		def configContent = []

		if(config){
			config.delegate = new ConfigDelegate(configContent)

			//we want that methods are looked up first in the delegate
			config.resolveStrategy = Closure.DELEGATE_FIRST

			//execute the closure
			config()
		}

		result.postActions.add(
				[
					'method':method,
					'arguments':params,
					'format':form,
					'config':configContent
				]
				)
	}


}

/**
 * builds our configuration element
 * @author wohlgemuth
 *
 */
public class ConfigDelegate{
	Collection elements = new Vector()

	public ConfigDelegate(Collection map){

		elements = map
	}

	def propertyMissing(String name) {
	}

	def methodMissing(String name, args) {

		for(Object o : args){
			def value = ["${name}":o]
			elements.add(value)

		}

	}
}

/**
 * this is the delegate for replacement operations and actions.
 * and is defiend in @link {@link ReportDelegate}
 */
public class ReplaceDelegate{

	Map replace = new HashMap()

	def ReplaceDelegate(Map main){
		main.replace.add(replace)

		replace.postProcess = []
		replace.preProcess = []

	}

	/**
	 * what is the output folder for the replaced data
	 */
	def folder(String value){

		replace.folder = value
	}

	/**
	 * what is the desired output format for the replaced data
	 * 
	 */
	def format(String value){
		replace.format = value
	}

	/**
	 * what kind of replacement method would we like. It needs to me an instance of
	 * {@link ZeroReplaceable}
	 */
	def method(String value){
		replace.method = value
	}

	/**
	 * how would we like to pre process the replacement data
	 * the parameter 'method' needs to be a class of type edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.Processable
	 * and it's a chained action. This means the result of this action is applied to final replaced data or any following action
	 */
	def preProcess(Map params){
		assert params.method != null, "please provide a method name"

		def form = params.format

		if(params.format == null){
			form = "txt"
		}

		def method = params.method

		params.remove("method")
		params.remove("format")

		replace.preProcess.add(
				[
					'method':method,
					'arguments':params,
					'format':form
				]
				)
	}

	/**
	 * how would we like to post process the replacement data
	 * the parameter 'method' needs to be a class of type edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.Processable
	 * and it's a chained action.
	 * 
	 * It works on the chained action and works on the already replaced data. This means the actions are executed in order
	 * 
	 */ 
	def postProcess(Map params){
		assert params.method != null, "please provide a method name"

		def form = params.format

		if(params.format == null){
			form = "txt"
		}

		def method = params.method

		params.remove("method")
		params.remove("format")

		replace.postProcess.add(
				[
					'method':method,
					'arguments':params,
					'format':form
				]
				)
	}




}

/**
 * used to uplad data
 * @author wohlgemuth
 *
 */
public class UploadDelegate {


	Map upload = new HashMap()

	def UploadDelegate(Map main){
		main.upload = upload
		upload.cdf = []
		upload.txt = []

	}

	/**
	 * single dir
	 * @param text
	 * @return
	 */
	def txt(String text){
		txt([text])
	}

	/**
	 * collection of dirs
	 * @param txt
	 * @return
	 */
	def txt(Collection txt){
		txt.each{ String value ->
			upload.txt.add(value)
		}
	}

	/**
	 * single dir 
	 * @param cdf
	 * @return
	 */
	def cdf(String cdfFile){
		cdf([cdfFile])
	}

	/**
	 * collections of dirs
	 * @param cdf
	 * @return
	 */
	def cdf(Collection cdf){
		cdf.each{ String value ->
			upload.cdf.add(value)
		}
	}
}

/**
 * this is used to calculate the calibration properties
 * @author wohlgemuth
 *
 */
public class CalibrationDelegate{

	Map map = new HashMap()

	def CalibrationDelegate(Map main){
		main.calibration = map

		map.postProcess = []
		map.preProcess = []

	}

	/**
	 * how to combine multiple samples having the same concentration
	 * @param methodName
	 * @return
	 */
	def combinationMethod(String methodName){
		map.method = methodName
	}

	/**
	 * how would we like to pre process the calibrated data
	 * the parameter 'method' needs to be a class of type edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.Processable
	 * and it's a chained action. This means the result of this action is applied to final replaced data or any following action
	 */
	def preProcess(Map params){
		assert params.method != null, "please provide a method name"

		def form = params.format

		if(params.format == null){
			form = "txt"
		}

		def method = params.method

		params.remove("method")
		params.remove("format")

		map.preProcess.add(
				[
					'method':method,
					'arguments':params,
					'format':form
				]
				)
	}

	/**
	 * how would we like to post process the calibrated data
	 * the parameter 'method' needs to be a class of type edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.Processable
	 * and it's a chained action.
	 *
	 * It works on the chained action and works on the already replaced data. This means the actions are executed in order
	 *
	 */
	def postProcess(Map params){
		assert params.method != null, "please provide a method name"

		def form = params.format

		if(params.format == null){
			form = "txt"
		}

		def method = params.method

		params.remove("method")
		params.remove("format")

		map.postProcess.add(
				[
					'method':method,
					'arguments':params,
					'format':form
				]
				)
	}

	/**
	 * defines a regression group. This feature is useful to generate different regression curves based on concentration. Since detecor are rarley linear
	 * @param concentration
	 * @return
	 */
	def regression(Number[] concentration){
		if(map.regression == null){
			map.regression = new HashSet<Number[]>()
		}

		map.regression.add(concentration)
	}

	/**
	 * if we find a property which is missing we check if it was maybe
	 * a method which has no argements and was so called without parenties
	 */
	def propertyMissing(String name) {

		if (metaClass.respondsTo(this, name)) {
			//calling a method with the given name
			this."$name"()
		}
		name
	}
}
/**
 * this defines the possible reports for the BinBase DSL
 */
public class ReportDelegate {
	Map map = new HashMap()

	def ReportDelegate(Map main,String attribute) {

		if(main.report == null){
			main.report = []
		}

		main.report.add(map)

		map.attribute = attribute
	}

	/**
	 * what is the output formart of this report
	 * @param format
	 * @return
	 */
	def format(String format) {
		map.format = format
	}

	def folder(String folder){
		map.folder = folder
	}

	/**
	 * we want to replace the zero values with the default method
	 * @return
	 */
	def replace() {
		if(map.replace == null){
			map.replace = []
		}

		map.replace.add ([replace:true])
	}

	/**
	 * we want or don't want to replace the zero values with the default method
	 */
	def replace(boolean value) {
		if(map.replace == null){
			map.replace = []
		}

		map.replace.add ([replace:value])
	}

	/**
	 * we want to replce the zero values with a specified class. The value needs to be the name of an instance
	 * of @link {@link ZeroReplaceable}
	 */
	def replace(String value) {
		if(map.replace == null){
			map.replace = []
		}
		map.replace.add ([replace:value])
	}

	/**
	 * we would like to have a calibration
	 * for this report
	 * @param closure
	 * @return
	 */
	def calibration(Closure closure){
		//assign the delegate
		closure.delegate = new CalibrationDelegate(map)

		//we want that methods are looked up first in the delegate
		closure.resolveStrategy = Closure.DELEGATE_FIRST

		//execute the closure
		closure()

	}

	/**
	 * enable calibrations for this report with default settings
	 * @param calibration
	 * @return
	 */
	def calibration(boolean calibration){
		map.calibration = calibration
	}
	/**
	 * we want to specify in detail how the data should be replaced and the exact options can be found at @link {@link ReplaceDelegate}
	 */
	def replace(Closure closure) {
		if(map.replace == null){
			map.replace = []
		}
		//assign the delegate
		closure.delegate = new ReplaceDelegate(map)

		//we want that methods are looked up first in the delegate
		closure.resolveStrategy = Closure.DELEGATE_FIRST

		//execute the closure
		closure()
	}
	/**
	 * how would we like to process the  data
	 * the parameter 'method' needs to be a class of type edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.Processable
	 * and it's a chained action.
	 * 
	 * It works on the chained action and works on the already replaced data. This means the actions are executed in order
	 */
	def processable(Map params){

		if (map.processable == null) {
			map.processable = []
		}


		assert params.method != null, "please provide a method name"

		def form = params.format

		if(params.format == null){
			form = "txt"
		}

		def method = params.method

		params.remove("method")
		params.remove("format")

		map.processable.add(
				[
					'method':method,
					'arguments':params,
					'format':form
				]
				)
	}

	/**
	 * we want to sizedown the reported bins to a list of possible percentages of succesful annotaitons, like
	 * 10, 20, 30, 40, 50
	 * 
	 * each of the values is going to result in a subreport and the related replacements
	 */
	def sizedown(Integer... value){
		def list = []

		value.each{ list.add it }

		sizedown list
	}

	/**
	 * we want to sizedown the reported bins to a certain value of possible percentages of succesful annotaitons
	 */
	def sizedown(def value){
		if(value instanceof Integer){
			assert Integer.parseInt(value.toString()) >= 0,"please provide a value greater or equal to 0"
			assert Integer.parseInt(value.toString()) <= 100, "please provide a value smaller or equal to 100"

			def v = []
			v.add value
			map.sizeDown = v
		}
		else if(value instanceof Collection){
			assert value.size() >= 0,"the list needs to include at least a single value!"

			value.each {
				assert Integer.parseInt(it.toString()) >= 0,"please provide a value greater or equal to 0"
				assert Integer.parseInt(it.toString()) <= 100, "please provide a value smaller or equal to 100"
			}

			map.sizeDown = value
		}
		else if(value == null){
			throw new RuntimeException("sorry you need to assign a valid value and not null!")
		}
		else{
			throw new RuntimeException("please you need to provide either a list of values or an integer and not a class of type: ${value}")
		}
	}


	/**
	 * if we find a property which is missing we check if it was maybe
	 * a method which has no argements and was so called without parenties
	 */
	def propertyMissing(String name) {

		if (metaClass.respondsTo(this, name)) {
			//calling a method with the given name
			this."$name"()
		}
		name
	}
}
