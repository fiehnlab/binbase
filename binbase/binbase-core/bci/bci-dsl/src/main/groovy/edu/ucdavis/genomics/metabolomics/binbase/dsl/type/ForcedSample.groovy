package edu.ucdavis.genomics.metabolomics.binbase.dsl.type

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;

class ForcedSample extends ExperimentSample{

	/**
	 * used machine
	 */
	String machine 
	
	public String getMachine(){
		return machine
	}
	
	public void setMachine(String machine){
		this.machine = machine;
	}
}
