package edu.ucdavis.genomics.metabolomics.binbase.logging.server;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import edu.ucdavis.genomics.metabolomics.binbase.filter.IPRequest;

/**
 * Logging service to access to the entity bean
 * 
 * @author wohlgemuth
 */
@Remote
public interface LoggingService extends Serializable {

	/**
	 * adds a new request to the database
	 * 
	 * @param request
	 * @param date
	 */
	public void registerEvent(IPRequest request);

	/**
	 * deletes all event's by date
	 * 
	 * @param date
	 */
	public void removeEvent(Date begin, Date end);

	/**
	 * return request's by host names
	 * 
	 * @param hostname
	 * @return
	 */
	public List<IPRequest> getRequest(String hostname);

	/**
	 * returns requests by hostname and date
	 * 
	 * @param hostname
	 * @param date
	 * @return
	 */
	public List<IPRequest> getRequest(String hostname, Date date);

	/**
	 * return request by hostname and date range
	 * 
	 * @param hostname
	 * @param begin
	 * @param end
	 * @return
	 */
	public List<IPRequest> getRequest(String hostname, Date begin, Date end);

	/**
	 * returns request by date
	 * 
	 * @param begin
	 * @return
	 */
	public List<IPRequest> getRequest(Date begin);

	/**
	 * returns request by date range
	 * 
	 * @param begin
	 * @param end
	 * @return
	 */
	public List<IPRequest> getRequest(Date begin, Date end);

	/**
	 * returns all host names
	 * 
	 * @return
	 */
	public List<String> getHostnames();

	/**
	 * returns all hostnames by date
	 * 
	 * @param date
	 * @return
	 */
	public List<String> getHostnames(Date date);

	/**
	 * returns all hostnames by date range
	 * 
	 * @param begin
	 * @param end
	 * @return
	 */
	public List<String> getHostnames(Date begin, Date end);

	/**
	 * returns all addresses
	 * 
	 * @return
	 */
	public List<String> getAddresses();

	/**
	 * returns all addresses by date
	 * 
	 * @param date
	 * @return
	 */
	public List<String> getAddresses(Date date);

	/**
	 * returns all addresses by date range
	 * 
	 * @param begin
	 * @param end
	 * @return
	 */
	public List<String> getAddresses(Date begin, Date end);

	/**
	 * returns all hostnames
	 * 
	 * @param hostname
	 * @return
	 */
	public List<Date> getDates(String hostname);

}
