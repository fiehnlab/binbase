package edu.ucdavis.genomics.metabolomics.binbase.bci.ejb;

import javax.naming.NamingException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.client.EjbClient;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;

/**
 * provides us with access to a BinBaseService
 * 
 * @author wohlgemuth
 */
public abstract class BinBaseServiceFactory extends AbstractFactory {

	public final static String DEFAULT_PROPERTY = BinBaseServiceFactory.class.getName();

	/**
	 * creates a new BinBaseService
	 * 
	 * @return
	 * @throws BinBaseException
	 */
	public abstract BinBaseService createService() throws BinBaseException;

	public static BinBaseServiceFactory createFactory() {
		return createFactory(System.getProperty(DEFAULT_PROPERTY));
	}

	/**
	 * if no property is set we create a binbase service based on ejb's
	 * 
	 * @param factoryClass
	 * @return
	 */
	public static BinBaseServiceFactory createFactory(final String factoryClass) {
		Class<?> classObject;
		BinBaseServiceFactory factory;

		try {

			if (factoryClass == null) {
				return new BinBaseServiceFactory() {

					@Override
					public BinBaseService createService() throws BinBaseException {
						try {
							return (BinBaseService) EjbClient.getRemoteEjb(Configurator.BCI_GLOBAL_NAME, BinBaseServiceBean.class);
						}
						catch (final NamingException e) {
							throw new BinBaseException(e);
						}
					}

				};
			}
			classObject = Class.forName(factoryClass);

			factory = (BinBaseServiceFactory) classObject.newInstance();
			return factory;

		}
		catch (final Exception e) {
			throw new FactoryException(e);
		}
	}

}
