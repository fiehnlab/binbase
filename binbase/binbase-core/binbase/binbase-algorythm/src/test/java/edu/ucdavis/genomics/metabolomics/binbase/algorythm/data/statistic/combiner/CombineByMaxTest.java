package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.combiner;


import static org.junit.Assert.*;

import java.util.List;
import java.util.Vector;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.CombinedObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ContentObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.FormatObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.NullObject;

public class CombineByMaxTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDoWork(){
		CombineByMax mx = new CombineByMax();
		List<FormatObject<?>> list = new Vector<FormatObject<?>>();
		
		list.add(null);
		list.add(null);
		list.add(null);
		list.add(null);
		list.add(new ContentObject<String>("5"));
		
		assertTrue(list.size() == 5);
		assertTrue(list.get(4) instanceof ContentObject);
		
		FormatObject<?> object = (FormatObject<?>) mx.doWork(list);
		
		assertTrue(object != null);
		assertTrue(object instanceof CombinedObject);
	}
}
