/**
 * 
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.provider;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author wohlgemuth
 *
 */
public class PegasusASCIIIProviderTest {

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.provider.PegasusASCIIIProvider#read(java.io.InputStream)}.
	 * @throws Exception 
	 */
	@Test
	public void testRead() throws Exception {
		InputStream stream = new FileInputStream("src/test/resources/error.csv");

		PegasusASCIIIProvider provider = new PegasusASCIIIProvider();
		provider.read(stream);

		assertTrue(provider.isContainsErrors());
		assertTrue(provider.getSpectra().length == 955);

		InputStream stream2 =  new FileInputStream("src/test/resources/ok.csv");
		PegasusASCIIIProvider provider2 = new PegasusASCIIIProvider();
		provider2.read(stream2);

		assertFalse(provider2.isContainsErrors());
		assertTrue(provider2.getSpectra().length == 955);
	}

}
