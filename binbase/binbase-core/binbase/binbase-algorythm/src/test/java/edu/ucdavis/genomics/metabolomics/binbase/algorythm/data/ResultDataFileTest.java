package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.XLS;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.RawdataResolver;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.ReplaceWithQuantIntesnityBasedOnAverageRTwithRiCurveFallback;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFileFactory;
import edu.ucdavis.genomics.metabolomics.util.statistics.replacement.ReplaceWithMin;
import edu.ucdavis.genomics.metabolomics.util.statistics.replacement.ZeroReplaceable;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ContentObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.FormatObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.NullObject;

public class ResultDataFileTest {
	ResultDataFile file = null;

	@Before
	public void setUp() throws Exception {
		System.setProperty(DataFileFactory.DEFAULT_PROPERTY_NAME,
				ResultDataFileFactory.class.getName());

		assertTrue(new ResourceSource("/datafile/testFileSop.xml").exist());
		assertTrue(new ResourceSource("/datafile/testFile.xml").exist());

		Document sopDefinition = new SAXBuilder().build(new ResourceSource(
				"/datafile/testFileSop.xml").getStream());
		Element root = sopDefinition.getRootElement();

		List<Element> transform = root.getChildren();
		file = new StaticStatisticActions().parseXMLContent(new ResourceSource(
				"/datafile/testFile.xml"), transform.get(0));

		file.setIndexed(true);
		// we just mock the time and calculate the average over all data
		file.setResolver(new SampleTimeResolver() {

			@Override
			public long resolveTime(String sample) throws BinBaseException {
				return 5000;
			}
		});

	}

	@After
	public void tearDown() throws Exception {
		file = null;
	}

	@Test
	public void testSpectra() {
		{
			FormatObject object = file.getSpectra("f", 21);
			System.out.println(object);
			assertTrue(object == null);
		}
		{
			FormatObject object = file.getSpectra("f", 22);
			assertTrue(object != null);
			assertTrue(object.getAttributes().get("spectra_id").equals("14188126"));
		}
		
	}

	@Test
	public void testInitialize() throws Exception {

		long begin = new Date().getTime();

		file.initialize();

		assertTrue(file.averageRIForSample.size() == 6);
		assertTrue(file.averageRTForSample.size() == 6);

		// both collection should be the same
		for (Long sample : file.averageRIForSample.keySet()) {
			for (Integer id : file.averageRI.keySet()) {
				assertTrue(file.averageRI.get(id).equals(
						file.averageRIForSample.get(sample).get(id)));
			}
		}

		// both collection should be the same
		for (Long sample : file.averageRTForSample.keySet()) {
			for (Integer id : file.averageRT.keySet()) {
				assertTrue(file.averageRT.get(id).equals(
						file.averageRTForSample.get(sample).get(id)));

				assertTrue(file.averageRT.get(id) > 0);
			}
		}

		long end = new Date().getTime();

		System.out.println("required time: " + (end - begin) / 1000);
	}

	@Test
	public void testReplaceZerosWithMin() throws Exception {

		ZeroReplaceable replace = new ReplaceWithMin();

		file.initialize();
		Thread.currentThread();
		Thread.sleep(5000);
		file.replaceZeros(replace, true);
	}

	@Test
	public void testGetMassspecsForSample() throws Exception {
		file.initialize();
		List<ContentObject<Double>> listA = file.getMassspecsForSample("a",
				true);

		int found = 0;
		for (ContentObject<Double> c : listA) {
			if (c.getAttributes().get("retentiontime").toString().equals(
					"448325")) {
				found++;
			}
			if (c.getAttributes().get("retentiontime").toString().equals(
					"451885")) {
				found++;
			}
			if (c.getAttributes().get("retentiontime").toString().equals(
					"775325")) {
				found++;
			}

		}
		assertTrue(found == 3);

	}

	@Test
	public void testReplaceZerosWithNetcdf() throws Exception {
		for (int i = 0; i < 1; i++) {

			ReplaceWithQuantIntesnityBasedOnAverageRTwithRiCurveFallback replace = new ReplaceWithQuantIntesnityBasedOnAverageRTwithRiCurveFallback() {

				private static final long serialVersionUID = 1L;

				@Override
				protected List<RawdataResolver> registerResolvers() {
					List<RawdataResolver> resolver = new ArrayList<RawdataResolver>();

					resolver.add(new RawdataResolver() {

						/**
					 * 
					 */
						private static final long serialVersionUID = 1L;

						@Override
						public File resolveNetcdfFile(String sampleName) {
							return new File("src/test/resources/datafile/"
									+ sampleName + ".cdf");
						}

						@Override
						public Integer getPriority() {
							return 100;
						}

						@Override
						public int compareTo(RawdataResolver o) {
							return o.getPriority().compareTo(o.getPriority());
						}
					});

					return resolver;
				}

			};

			// Thread.currentThread().sleep(5000);

			replace.setFile(file);

			// initializ the file
			file.initialize();
			// Thread.currentThread().sleep(5000);

			// replace the zeros
			file.replaceZeros(replace, true);

			XLS xls = new XLS();
			xls.write(new FileOutputStream(
					"target/testReplaceZerosWithNetcdf.xls"), file);

			// testing bin 228 last file
			{
				NullObject<?> value = (NullObject<?>) file.getColumn(
						file.getBinPosition(228)).get(11);

				System.out.println("value: " + value);
				assertTrue(Double.parseDouble(value.getValue().toString()) == 1621.0);

			}

			// testing bin 205 5th file
			{
				NullObject<?> value = (NullObject<?>) file.getColumn(
						file.getBinPosition(205)).get(10);

				System.out.println("value: " + value);
				assertTrue(Double.parseDouble(value.getValue().toString()) == 27616.0);

			}

			// testing bin 829 3th file
			{
				NullObject<?> value = (NullObject<?>) file.getColumn(
						file.getBinPosition(829)).get(8);

				System.out.println("value: " + value);
				assertTrue(Double.parseDouble(value.getValue().toString()) == 7824.0);

			}

		}
	}
}
