package edu.ucdavis.genomics.metabolomics.binbase.algorythm.matching.similarity;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.jdom.Element;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SpectraLimiterTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCreateLimiterNullCondition() {
		SpectraLimiter limiter = SpectraLimiter.createLimiter(null, "test");

		assertTrue(limiter.getMinFragment() == 85);
		assertTrue(limiter.getMaxFragment() == 500);
		assertFalse(limiter.getMinFragment() == 185);
		assertFalse(limiter.getMaxFragment() == 1500);

	}

	@Test
	public void testCreateLimiterEmptyCondition() {

		Element element = new Element("test");

		SpectraLimiter limiter = SpectraLimiter.createLimiter(element, "test");

		assertTrue(limiter.getMinFragment() == 85);
		assertTrue(limiter.getMaxFragment() == 500);
		assertFalse(limiter.getMinFragment() == 185);
		assertFalse(limiter.getMaxFragment() == 1500);

	}

	@Test
	public void testCreateLimiterMissingAttributesCondition() {

		Element element = new Element("test");
		Element c = new Element("column");
		element.addContent(c);

		SpectraLimiter limiter = SpectraLimiter.createLimiter(element, "test");

		assertTrue(limiter.getMinFragment() == 85);
		assertTrue(limiter.getMaxFragment() == 500);
		assertFalse(limiter.getMinFragment() == 185);
		assertFalse(limiter.getMaxFragment() == 1500);

	}

	@Test
	public void testCreateLimiterMatchNameMissingAttributesCondition() {

		Element element = new Element("test");
		Element c = new Element("column");
		c.setAttribute("name","test");
		element.addContent(c);

		SpectraLimiter limiter = SpectraLimiter.createLimiter(element, "test");

		assertTrue(limiter.getMinFragment() == 85);
		assertTrue(limiter.getMaxFragment() == 500);
		assertFalse(limiter.getMinFragment() == 185);
		assertFalse(limiter.getMaxFragment() == 1500);

	}
	

	@Test
	public void testCreateLimiterNoMatcinghNameMissingAttributesCondition() {

		Element element = new Element("test");
		Element c = new Element("column");
		c.setAttribute("name","test2");
		element.addContent(c);

		SpectraLimiter limiter = SpectraLimiter.createLimiter(element, "test");

		assertTrue(limiter.getMinFragment() == 85);
		assertTrue(limiter.getMaxFragment() == 500);
		assertFalse(limiter.getMinFragment() == 185);
		assertFalse(limiter.getMaxFragment() == 1500);

	}
	

	@Test
	public void testCreateLimiterMatchingNameOneMissingAttributesCondition() throws IOException {

		Element element = new Element("test");
		Element c = new Element("column");
		c.setAttribute("name","test");
		c.setAttribute("beginFragment","50");
		element.addContent(c);

		SpectraLimiter limiter = SpectraLimiter.createLimiter(element, "test");

		assertTrue(limiter.getMinFragment() == 50);
		assertTrue(limiter.getMaxFragment() == 500);
		assertFalse(limiter.getMinFragment() == 85);
		assertFalse(limiter.getMaxFragment() == 1500);

	}

	@Test
	public void testCreateLimiterMatchingNameNoMissingAttributesCondition() throws IOException {

		Element element = new Element("test");
		Element c = new Element("column");
		c.setAttribute("name","test");
		c.setAttribute("beginFragment","50");
		c.setAttribute("endFragment","150");
		element.addContent(c);

		SpectraLimiter limiter = SpectraLimiter.createLimiter(element, "test");

		assertTrue(limiter.getMinFragment() == 50);
		assertTrue(limiter.getMaxFragment() == 150);
		assertFalse(limiter.getMinFragment() == 85);
		assertFalse(limiter.getMaxFragment() == 1500);

		String result  = limiter.limitSpectra("1:1 49:49 55:55 149:149 200:200");
		assertTrue(result.equals("55:55.0 149:149.0"));
	}
	
	

}
