package edu.ucdavis.genomics.metabolomics.binbase.diagnostics;

import java.io.Serializable;

/**
 * a reason why we came to this @see {@link Result}
 * @author wohlgemuth
 *
 */
public class Reason  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String description;

	public Reason(String description) {
		super();
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	protected void setDescription(String description) {
		this.description = description;
	}
	
	public String toString(){
		return description;
	}

}
