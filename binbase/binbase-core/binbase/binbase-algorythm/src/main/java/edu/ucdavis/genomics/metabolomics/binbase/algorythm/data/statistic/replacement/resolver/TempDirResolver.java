package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.resolver;

import java.io.File;

/**
 * checks for the data in the local temp directory
 * 
 * @author wohlgemuth
 */
public class TempDirResolver extends AbstractFileResolver {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public File resolveNetcdfFile(String sampleName) {
		return getFile(System.getProperty("java.io.tmpdir"), sampleName);
	}

	public String toString() {
		return getClass().getName() + " - " + System.getProperty("java.io.tmpdir");

	}
}
