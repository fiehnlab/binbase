package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.netcdf;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.jdom.Element;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool.BasicProccessable;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.SimpleDatafile;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ContentObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.FormatObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.HeaderFormat;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.NullObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.SampleObject;

/**
 * base class for netcdf debugging routines
 * 
 * @author wohlgemuth
 * 
 */
public abstract class NetcdfDebugging extends BasicProccessable {
	Logger logger = Logger.getLogger(getClass());

	@Override
	public String getFolder() {
		return "netcdf";
	}

	@Override
	public final DataFile process(ResultDataFile datafile, Element configuration)
			throws BinBaseException {

		List<SampleObject<String>> samples = datafile.getSamples();

		for (SampleObject<String> sample : samples) {

			logger.info("generating file for sample: " + sample);

			SimpleDatafile file = new SimpleDatafile();
			file.addEmptyColumn("bin");
			file.addEmptyColumn("assumed retention time");
			file.addEmptyColumn("noise");
			file.addEmptyColumn("max intensity");
			file.addEmptyColumn("calculated intensity");
			file.addEmptyColumn("bins average retention time");

			file.addEmptyColumn("bins quant mass");

			List<HeaderFormat<String>> bins = datafile.getBins();

			// generate report
			for (HeaderFormat<String> bin : bins) {
				int id = Integer.parseInt(bin.getAttributes().get("id"));

				FormatObject<?> object = datafile.getSpectra(sample.getValue(),
						id);

				if (object instanceof NullObject<?>) {
					List<Object> list = new Vector<Object>();
					list.add(new HeaderFormat<String>(bin.getAttributes().get(
							"name")));
					list.add(new ContentObject<String>(object.getAttributes()
							.get("assumed_time")));
					list.add(new ContentObject<String>(object.getAttributes()
							.get("noise")));
					list.add(new ContentObject<String>(object.getAttributes()
							.get("max_intensity")));
					list.add(new ContentObject<String>(object.getAttributes()
							.get("calculated_value")));
					list.add(new ContentObject<String>(object.getAttributes()
							.get("average retention time")));
					list.add(new HeaderFormat<String>(bin.getAttributes().get(
							"quantmass")));

					file.addRow(list);

					// generate png chart
					workOnObject((NullObject<?>) object, bin, sample);
				}
			}

			workOnSample(file, configuration, sample);

		}

		return null;
	}

	/**
	 * does some work on the datafile
	 * 
	 * @param file
	 */
	protected void workOnSample(SimpleDatafile file, Element configuration,
			SampleObject<String> sample) throws BinBaseException{
	}

	/**
	 * does some work on the giving object
	 * 
	 * @param object
	 * @param bin
	 * @param sample
	 * @throws BinBaseException
	 */
	protected void workOnObject(ContentObject<?> object,
			HeaderFormat<String> bin, SampleObject<String> sample)
			throws BinBaseException {
	}
}
