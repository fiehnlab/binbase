package edu.ucdavis.genomics.metabolomics.binbase.diagnostics;

import java.io.Serializable;

/**
 * the result of this @see {@link Step}
 * @author wohlgemuth
 *
 */
public class Result implements Serializable {

	public final static Result FAILED = new Result("failed");
	
	public final static Result SUCCESS = new Result("success");
	
	public final static Result WARNING = new Result("warning");
	
	

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String description;

	public Result(String string) {
		this.setDescription(string);
	}

	public String getDescription() {
		return description;
	}

	protected void setDescription(String description) {
		this.description = description;
	}
	

	public String toString(){
		return description;
	}

}
