/*
 * Created on Nov 18, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.export.source;

import java.util.Map;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.io.source.SourceFactory;

public class QuantificationTableSourceFactoryImpl extends SourceFactory{

	public QuantificationTableSourceFactoryImpl() {
		super();
	}

	public Source createSource(Object identifier, Map propertys) throws ConfigurationException {
        Source source =new QuantificationTableSource();
        source.configure(propertys);
        source.setIdentifier(identifier);
        return source;

	}

}
