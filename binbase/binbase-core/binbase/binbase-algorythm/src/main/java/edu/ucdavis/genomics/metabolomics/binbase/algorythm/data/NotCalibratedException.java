package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data;

public class NotCalibratedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotCalibratedException() {
	}

	public NotCalibratedException(String arg0) {
		super(arg0);
	}

	public NotCalibratedException(Throwable arg0) {
		super(arg0);
	}

	public NotCalibratedException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
