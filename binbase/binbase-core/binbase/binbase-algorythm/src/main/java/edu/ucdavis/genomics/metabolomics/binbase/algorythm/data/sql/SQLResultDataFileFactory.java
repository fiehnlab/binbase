package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.sql;

import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFileFactory;

/**
 * creates a sql result datafile
 * 
 * @author wohlgemuth
 */
public class SQLResultDataFileFactory extends DataFileFactory {

	@Override
	public DataFile createDataFile() {
		return new SQLResultDataFile();
	}
}
