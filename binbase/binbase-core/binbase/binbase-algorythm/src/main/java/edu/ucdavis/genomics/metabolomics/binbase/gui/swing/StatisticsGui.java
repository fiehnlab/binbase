/*
 * Created on Mar 2, 2007
 */
package edu.ucdavis.genomics.metabolomics.binbase.gui.swing;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.filechooser.FileFilter;

import org.apache.log4j.Appender;
import org.apache.log4j.Layout;
import org.apache.log4j.Logger;
import org.apache.log4j.spi.ErrorHandler;
import org.apache.log4j.spi.Filter;
import org.apache.log4j.spi.LoggingEvent;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.SimpleStatisticProcessor;
import edu.ucdavis.genomics.metabolomics.util.io.dest.FileDestination;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;

/**
 * a simple gui to convert xml files into other formats
 * 
 * @author wohlgemuth
 * @version Mar 2, 2007
 * 
 */
public class StatisticsGui extends JFrame {

	File sourceXML;

	File sourceSOP;

	File destinationZIP;

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	public StatisticsGui() {
		this.setSize(320, 160);
		this.setTitle("simple xml converter");

		JButton xmlInput = new JButton(new AbstractAction() {

			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent arg0) {
				JFileChooser chooser = new JFileChooser();

				chooser.addChoosableFileFilter(new FileFilter() {

					@Override
					public boolean accept(File arg0) {
						if (arg0.getName().endsWith(".xml")) {
							return true;
						} else if (arg0.isDirectory()) {
							return true;
						}
						return false;
					}

					@Override
					public String getDescription() {
						return null;
					}
				});
				chooser.showOpenDialog(StatisticsGui.this);

				if (chooser.getSelectedFile() != null) {
					sourceXML = chooser.getSelectedFile();
				} else {
					JOptionPane.showMessageDialog(StatisticsGui.this, "you need to select a file!");
				}
			}
		});

		JButton sopInput = new JButton(new AbstractAction() {

			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.addChoosableFileFilter(new FileFilter() {

					@Override
					public boolean accept(File arg0) {
						if (arg0.getName().endsWith(".stat")) {
							return true;
						} else if (arg0.isDirectory()) {
							return true;
						}
						return false;
					}

					@Override
					public String getDescription() {
						return "*.stat";
					}
				});

				chooser.showOpenDialog(StatisticsGui.this);

				if (chooser.getSelectedFile() != null) {
					sourceSOP = chooser.getSelectedFile();
					if (sourceSOP.exists()) {
						if (!sourceSOP.canRead()) {
							JOptionPane.showMessageDialog(StatisticsGui.this, "please select another file you have no read access to this file!");
						}
					} else {
						JOptionPane.showMessageDialog(StatisticsGui.this, "sorry the selected file does not exist!");
					}
				} else {
					JOptionPane.showMessageDialog(StatisticsGui.this, "you need to select a file!");
				}
			}
		});
		JButton outputData = new JButton(new AbstractAction() {

			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();

				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);

				chooser.showSaveDialog(StatisticsGui.this);

				if (chooser.getSelectedFile() != null) {
					destinationZIP = chooser.getSelectedFile();
				} else {
					JOptionPane.showMessageDialog(StatisticsGui.this, "you need to select a directory!");
				}
			}
		});

		JButton calculate = new JButton(new AbstractAction() {

			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				if (sourceXML != null && sourceSOP != null && destinationZIP != null) {
					SimpleStatisticProcessor processor = new SimpleStatisticProcessor();
					try {
						String resultFile = destinationZIP.getAbsolutePath() + File.separator + sourceXML.getName().replaceAll("xml", "zip");
						FileDestination dest = new FileDestination();
						dest.setIdentifier(new File(resultFile));

						processor.process(new FileSource(sourceXML), new FileSource(sourceSOP), dest);

						JOptionPane.showMessageDialog(StatisticsGui.this, "done");
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(StatisticsGui.this, "sorry an error occured: " + e1.getMessage());
						e1.printStackTrace();
					}
				} else {
					JOptionPane.showMessageDialog(StatisticsGui.this, "please select all needed files");
				}
			}
		});

		xmlInput.setText("open result file");
		sopInput.setText("open sop file");
		outputData.setText("set destination directory");
		calculate.setText("Start calculation");

		
		JPanel panel = new JPanel();
		
		panel.setLayout(new FlowLayout());
		panel.add(xmlInput);
		panel.add(sopInput);
		panel.add(outputData);
		getContentPane().add(calculate,BorderLayout.SOUTH);

		this.getContentPane().add(panel,BorderLayout.NORTH);

		this.pack();		

		this.addWindowListener(new WindowListener(){

			public void windowActivated(WindowEvent e) {
			}

			public void windowClosed(WindowEvent e) {
			}

			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}

			public void windowDeactivated(WindowEvent e) {
			}

			public void windowDeiconified(WindowEvent e) {
			}

			public void windowIconified(WindowEvent e) {
			}

			public void windowOpened(WindowEvent e) {
			}
			
		});
		this.setVisible(true);
	}

	public static void main(String[] args) {
		new StatisticsGui();
	}
}
