package edu.ucdavis.genomics.metabolomics.binbase.diagnostics;

/**
 * creates a diagnostics service, which does absolutely nothing.
 * @author wohlgemuth
 *
 */
public class NoDiagnosticsServiceFactory extends DiagnosticsServiceFactory{

	/**
	 * does nothing
	 */
	private static DiagnosticsService service =  new AbstractDiagnosticService() {
		
		@Override
		public void diagnosticAction(int spectraId, int binId, Class<?> caller,
				Step stepOfAlgorithm, Reason reasonForTheArchivedResult,
				Result resultOfAction, Object[] parameters) {
			
		}
		
		@Override
		public void diagnosticAction(int spectraId, Class<?> caller,
				Step stepOfAlgorithm, Reason reasonForTheArchivedResult,
				Result resultOfAction, Object[] parameters) {
			
		}
		
		@Override
		public void diagnosticAction(int sampleId, int spectraId, int binId,
				Class<?> caller, Step stepOfAlgorithm,
				Reason reasonForTheArchivedResult, Result resultOfAction,
				Object[] parameters) {
			
		}
		
		@Override
		public void diagnosticAction(String className, int sampleId, int spectraId,
				int binId, Class<?> caller, Step stepOfAlgorithm,
				Reason reasonForTheArchivedResult, Result resultOfAction,
				Object[] parameters) {
			
		}
	};
	
	@Override
	public DiagnosticsService createService() {
		return service;
	}

}
