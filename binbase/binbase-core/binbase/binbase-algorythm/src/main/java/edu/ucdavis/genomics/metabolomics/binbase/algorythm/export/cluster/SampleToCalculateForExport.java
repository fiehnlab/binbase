package edu.ucdavis.genomics.metabolomics.binbase.algorythm.export.cluster;

import java.io.Serializable;


/**
 * propertie for a sub job
 * 
 * @author wohlgemuth
 * 
 */
public final class SampleToCalculateForExport implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	private Integer id;

	private String database;

	public SampleToCalculateForExport() {

	}

	/**
	 * @param id
	 */
	public SampleToCalculateForExport(Integer id, String database) {
		super();
		this.id = id;
		this.database = database;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " - " + this.getDatabase() + " - " + this.getId();
	}
}
