package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.resolver;

import java.util.ArrayList;
import java.util.List;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.RawdataResolver;

/**
 * very simple implementation, just a prefilled list
 * 
 * @author wohlgemuth
 * 
 */
public class SimpleResolverBuilder implements ResolverBuilder {

	/**
	 * builds the actual resolvers
	 * 
	 * @return
	 */
	public List<RawdataResolver> build() {
		List<RawdataResolver> list = new ArrayList<RawdataResolver>();

		list.add(new TempDirResolver());
		list.add(new EnvDirResolver());
		list.add(new RemoteResolver());

		return list;
	}
}
