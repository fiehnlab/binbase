/*;
 * Created on Jun 10, 2005
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.version;

import java.util.Collection;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.jdom.Element;

import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.config.Configable;
import edu.ucdavis.genomics.metabolomics.util.config.xml.XMLConfigable;

/**
 * @author wohlgemuth build automaticly the needed versions handler from the
 *         configuration
 */
public class PegasusHeaderFactory {
	private Logger logger = Logger.getLogger(getClass());

	/**
	 * DOCUMENT ME!
	 */
	private static PegasusHeaderFactory instance;

	/**
	 * DOCUMENT ME!
	 */
	private XMLConfigable config = Configable.CONFIG;

	/**
	 * @return
	 */
	public static PegasusHeaderFactory create() {
		if (instance == null) {
			instance = new PegasusHeaderFactory();
		}

		return instance;
	}

	/**
	 * creates the needed header and sets the header properties
	 * 
	 * @return
	 * @throws ConfigurationException
	 */
	@SuppressWarnings("unchecked")
	public synchronized PegasusHeader getHeader(final String[] headerLine) throws ConfigurationException {
		try {
			List<Element> elements = (config.getElement("header.pegasus").getChildren("version"));

			PegasusHeader header = null;

			for (Element element : elements) {
				List<Element> headerList = element.getChildren("entry");
				String[] fields = new String[headerList.size()];

				for (int i = 0; i < fields.length; i++) {
					fields[i] = headerList.get(i).getAttributeValue("pegasus");
				}

				Collection found = new Vector();
				for (int i = 0; i < headerLine.length; i++) {
					for (int x = 0; x < fields.length; x++) {
						if (fields[x].equals(headerLine[i])) {
							found.add(fields[x]);
						}
					}
				}

				logger.debug("found: " + found.size());
				logger.debug("header: " + headerList.size());

				if (found.size() == headerList.size()) {
					logger.debug("found implementation");
					header = new PegasusHeader();
					header.setElement(element);
				}
				else {
					logger.debug("found no implementation");
				}
			}
			if (header == null) {
				throw new ConfigurationException("no configuration found for this file!");
			}
			return header;
		}
		catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ConfigurationException(e);
		}
	}
}
