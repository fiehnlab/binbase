package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.netcdf.impl;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import net.sf.mzmine.datastructures.RawDataAtNode;
import net.sf.mzmine.datastructures.Scan;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.netcdf.NetCDFData;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.validate.ValidateSpectra;
import edu.ucdavis.genomics.metabolomics.exception.ConfigurationException;
import edu.ucdavis.genomics.metabolomics.util.io.source.FileSource;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.math.CombinedRegression;
import edu.ucdavis.genomics.metabolomics.util.math.Regression;

/**
 * an implementation of the netcdf file based on the mzmine library
 * 
 * @author wohlgemuth
 * 
 */
public class MZMineImpl extends NetCDFData {
	/**
	 * our internal node
	 */
	private RawDataAtNode node;

	private Logger logger;

	/**
	 * internal cache to speed things up
	 */
	private Map<Double, double[][]> cache;

	/**
	 * contains all calculated noises
	 */
	private Map<Integer, Double> noises;

	private Regression regression;

	public static boolean CACHING_ALLOWED = true;

	public MZMineImpl(Source source) throws IOException {
		super(source);
	}

	@Override
	protected void prepare(Source source) throws IOException {
		logger = Logger.getLogger(getClass());
		cache = new HashMap<Double, double[][]>();
	}

	@Override
	public double[][] getSpectra(double retentionTime, double precision, boolean combine) {

		// combine several scans
		if (combine) {

			List<double[][]> list = this.getAproximateSpectras(retentionTime, precision);

			if (list.size() == 1) {
				return ValidateSpectra.convertToRelativeDouble(list.get(0));
			} else if (list.isEmpty()) {
				return null;
			} else {
				Iterator<double[][]> it = list.iterator();
				double[][] tempSpectra = it.next();
				double[][] spectra = null;
				if (spectra == null) {
					spectra = tempSpectra;
				} else {
					for (int i = 0; i < MAX_ION; i++) {
						double absTemp = tempSpectra[i][FRAGMENT_ABS_POSITION];
						double absAll = spectra[i][FRAGMENT_ABS_POSITION];

						if (absAll <= 0) {
							spectra[i][FRAGMENT_ABS_POSITION] = absTemp;
						} else {
							spectra[i][FRAGMENT_ABS_POSITION] = (absTemp + absAll) / 2;
						}
					}
				}

				return ValidateSpectra.convertToRelativeDouble(spectra);
			}
		}
		// use only one scan
		else {
			double[][] s = getAproximateSpectra(retentionTime, precision);

			if (s != null) {
				return ValidateSpectra.convertToRelativeDouble(s);
			}
		}
		return null;
	}

	/**
	 * @author wohlgemuth
	 * @version Nov 6, 2006
	 * @param spectra
	 * @param s
	 * @return
	 */
	private double[][] createSpectra(Scan s) {
		double[][] spectra = ValidateSpectra.createSpectra();
		double[] mz = s.getMZValues();
		double[] intensity = s.getIntensityValues();

		for (int i = 0; i < mz.length; i++) {
			spectra[(int) mz[i] - 1][ValidateSpectra.FRAGMENT_ION_POSITION] = mz[i];
			spectra[(int) mz[i] - 1][ValidateSpectra.FRAGMENT_ABS_POSITION] = intensity[i];
		}

		return spectra;
	}

	/**
	 * finds the scan in the node
	 * 
	 * @param retentionTime
	 * @param precision
	 * @param combine
	 * @return
	 */
	protected double[][] getAproximateSpectra(double retentionTime, double precision) {
		if (this.regression != null) {
			retentionTime = this.regression.getY(retentionTime);
		}
		if (isCaching()) {
			return getCachedSpectra(retentionTime, precision);
		}
		node.preLoad();
		node.initializeScanBrowser(0, node.getNumberOfScans());

		for (int i = 0; i < node.getNumberOfScans(); i++) {
			Scan s = node.getNextScan();
			double time = node.getScanTime(s.getScanNumber());
			if ((retentionTime >= (time - precision)) && (retentionTime <= (time + precision))) {
				return createSpectra(s);
			}
		}
		return null;
	}

	/**
	 * finds all scans in the given ri windows
	 * 
	 * @param retentionTime
	 * @param range
	 *            defines the window
	 * @return
	 */
	protected List<double[][]> getAproximateSpectras(double retentionTime, double range) {
		if (this.regression != null) {
			retentionTime = this.regression.getY(retentionTime);
		}

		if (isCaching()) {
			return getCachedSpectras(retentionTime, range);
		}

		node.preLoad();
		node.initializeScanBrowser(0, node.getNumberOfScans());

		List<double[][]> result = new Vector<double[][]>();

		for (int i = 0; i < node.getNumberOfScans(); i++) {
			Scan s = node.getNextScan();
			double time = node.getScanTime(s.getScanNumber());
			if ((retentionTime >= (time - range)) && (retentionTime <= (time + range))) {
				result.add(createSpectra(s));
			}
		}

		return result;
	}

	/**
	 * finds all scans in the given ri window
	 * 
	 * @author wohlgemuth
	 * @version Nov 6, 2006
	 * @param retentionTime
	 * @param range
	 * @return
	 */
	private List<double[][]> getCachedSpectras(double retentionTime, double range) {
		Set<Double> cacheSet = cache.keySet();

		Double[] keys = cacheSet.toArray(new Double[cacheSet.size()]);

		// sort the keys to optimize the search!
		Arrays.sort(keys);

		List<double[][]> list = new Vector<double[][]>();
		int high = keys.length;
		int low = -1;
		int probe = 0;

		double targetOne = retentionTime + range;
		double targetTwo = retentionTime - range;

		while (high - low > 1) {
			probe = (low + high) >>> 1;
			if (keys[probe] > targetOne) {
				high = probe;
			} else if (keys[probe] < targetTwo) {
				low = probe;
			} else {
				// value of interesst
				list.add(cache.get(keys[probe]));
				low = probe;
			}
		}

		return list;

	}

	/**
	 * get the closest possible scan
	 * 
	 * @author wohlgemuth
	 * @version Nov 6, 2006
	 * @param retentionTime
	 * @param precision
	 * @return
	 */
	private double[][] getCachedSpectra(final double retentionTime, final double precision) {
		double[][] s = this.cache.get(retentionTime);

		if (s == null) {

			Set<Double> cacheSet = cache.keySet();

			Double[] keys = cacheSet.toArray(new Double[cacheSet.size()]);

			// sort the keys to optimize the search!
			Arrays.sort(keys);

			int high = keys.length;
			int low = -1;
			int probe = 0;

			double targetOne = retentionTime + precision;
			double targetTwo = retentionTime - precision;

			while (high - low > 1) {
				probe = (low + high) >>> 1;

				if (keys[probe] > targetOne) {
					high = probe;
				} else if (keys[probe] < targetTwo) {
					low = probe;
				} else {
					return cache.get(keys[probe]);
				}
			}

		} else {
			return s;
		}

		return null;
	}

	@Override
	protected void load(Source source) throws IOException {
		if (source.exist() == false) {
			throw new IOException("sorry source does not exist!");
		}

		File file = getFile(source);
		node = new RawDataAtNode(0, file);
		node.setWorkingCopy(file);
		cache.clear();

		// functions for loading our values into the cache
		if (isCaching()) {
			node.preLoad();
			node.initializeScanBrowser(0, node.getNumberOfScans());

			noises = new HashMap<Integer, Double>();

			for (int i = 0; i < node.getNumberOfScans(); i++) {
				Scan s = node.getNextScan();
				double time = node.getScanTime(s.getScanNumber());

				double spectra[][] = createSpectra(s);

				cache.put(time, spectra);

				findNoise(noises, spectra);
			}

			if (cache.size() != node.getNumberOfScans()) {
				logger.warn("cache should equal number of scans");
			}
			if (noises.size() != MAX_ION) {
				logger.warn("noises should equal max count of ions, count: " + noises.size());
			}
		}
	}

	/**
	 * can people use the cache
	 * 
	 * @author wohlgemuth
	 * @version Nov 6, 2006
	 * @return
	 */
	public boolean isCaching() {
		return CACHING_ALLOWED;
	}

	/**
	 * can people use the cache
	 * 
	 * @author wohlgemuth
	 * @version Nov 6, 2006
	 * @param caching
	 */
	public void setCaching(boolean caching) {
		CACHING_ALLOWED = caching;
	}

	@Override
	public void close() {
		this.node = null;
		this.cache = null;
		System.gc();
	}

	@Override
	public void setCorrectionCurve(Regression regression) {
		this.regression = regression;
	}

	public static void main(String[] args) throws ConfigurationException, IOException {

		// retnetion time bin
		double x[] = new double[] { 262.320, 323.120, 381.020, 487.220, 582.620, 668.720, 747.420, 819.620, 886.620, 948.820, 1006.900, 1061.700, 1113.100 };

		// retention time spectra
		double y[] = new double[] { 372.144, 414.794, 457.144, 535.344, 605.994, 669.894, 727.944, 781.294, 830.444, 875.944, 918.944, 965.294, 1020.440 };

		Regression reg = new CombinedRegression();
		reg.setData(x, y);

		NetCDFData data = new MZMineImpl(new FileSource(new File(args[0])));
		Logger logger = Logger.getLogger(data.getClass());

		double times[] = new double[] { 692.443 };

		double ris[] = new double[] { 692.43 };

		for (int i = 0; i < ris.length; i++) {
			double[][] beforeRegression = data.getSpectra(times[i]);
			data.setCorrectionCurve(reg);

			for (double z = 0.01; z < 2; z = z + 0.01) {
				double[][] afterRegression = data.getSpectra(ris[i], z, true);
			}

			data.setCorrectionCurve(null);

		}

	}

	@Override
	public Map<Integer, Double> getNoise() {
		if (isCaching()) {
			return noises;
		}

		Map<Integer, Double> noise = new HashMap<Integer, Double>();
		node.preLoad();
		node.initializeScanBrowser(0, node.getNumberOfScans());

		for (int i = 0; i < node.getNumberOfScans(); i++) {
			Scan s = node.getNextScan();
			double time = node.getScanTime(s.getScanNumber());

			double spectra[][] = createSpectra(s);
			findNoise(noise, spectra);
		}

		return noise;
	}

	/**
	 * @author wohlgemuth
	 * @version Nov 14, 2006
	 * @param noise
	 * @param spectra
	 */
	private void findNoise(Map<Integer, Double> noise, double[][] spectra) {
		for (int x = 1; x < ValidateSpectra.MAX_ION; x++) {
			double current = Double.MAX_VALUE;

			if (noise.get(x - 1) != null) {
				current = noise.get(x - 1);
			}

			if (current > (int) spectra[x - 1][ValidateSpectra.FRAGMENT_ABS_POSITION]) {
				noise.put(x - 1, spectra[x - 1][ValidateSpectra.FRAGMENT_ABS_POSITION]);
			}
		}
	}

	@Override
	public Map<Integer, Double> getNoise(double time, int range) {
		Map<Integer, Double> result = new HashMap<Integer, Double>();

		double min = time - range;
		double max = time + range;

		for (double x = min; x < max; x = x + getPrecision()) {
			double[][] spectra = getSpectra(x);

			if (spectra != null) {
				findNoise(result, spectra);
			}
		}

		return result;
	}
}
