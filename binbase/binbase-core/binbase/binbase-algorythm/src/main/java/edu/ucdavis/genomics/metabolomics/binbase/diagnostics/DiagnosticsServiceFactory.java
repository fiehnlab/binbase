package edu.ucdavis.genomics.metabolomics.binbase.diagnostics;

import edu.ucdavis.genomics.metabolomics.exception.FactoryException;
import edu.ucdavis.genomics.metabolomics.util.AbstractFactory;

/**
 * used to generate an concrete implementation of the diagnostics service
 * 
 * @author wohlgemuth
 * 
 */
public abstract class DiagnosticsServiceFactory extends AbstractFactory {

	public static final String DEFAULT_PROPERTY_NAME = DiagnosticsServiceFactory.class
			.getName();

	/**
	 * creates the actual service implementation
	 * 
	 * @return
	 */
	public abstract DiagnosticsService createService();

	/**
	 * creates a new default instance
	 * 
	 * @return
	 */
	public static DiagnosticsServiceFactory newInstance() {
		// Locate Factory
		String foundFactory = findFactory(DEFAULT_PROPERTY_NAME,
				NoDiagnosticsServiceFactory.class.getName());

		return newInstance(foundFactory);
	}

	@SuppressWarnings("unchecked")
	public static DiagnosticsServiceFactory newInstance(String factoryClass) {
		Class<DiagnosticsServiceFactory> classObject;
		DiagnosticsServiceFactory factory;

		try {
			classObject = (Class<DiagnosticsServiceFactory>) Class
					.forName(factoryClass);
			factory = classObject.newInstance();
			return factory;

		} catch (Exception e) {
			throw new FactoryException(e);
		}
	}
}
