package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.resolver;

import java.io.File;
import java.util.Collection;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacade;

/**
 * assumes we are running on the server and so all files should be locally
 * 
 * @author wohlgemuth
 * 
 */
public class ServerDirResolver extends AbstractFileResolver {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public File resolveNetcdfFile(String sampleName) {
		try {
			ExportJMXFacade service = Configurator.getExportService();
			Collection<String> dirs = service.getNetCDFDirectories();

			for (String dir : dirs) {

				File f = getFile(dir, sampleName);

				if (f.exists()) {
					return createProtectedFile(f);
				}
			}
		} catch (Exception e) {
			getLogger().warn(e.getMessage(), e);
		}

		return null;
	}

}
