/*
 * Created on Apr 22, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase;

import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import edu.ucdavis.genomics.metabolomics.util.status.Reports;
import edu.ucdavis.genomics.metabolomics.util.status.priority.Priority;

public interface BinBaseReports extends Reports {
	ReportEvent EXPORT = new ReportEvent("export", "an export",Priority.INFO);

	ReportEvent IMPORT = new ReportEvent("import", "an import",Priority.INFO);

	ReportEvent UPDATE = new ReportEvent("update", "an update",Priority.INFO);

	ReportEvent META = new ReportEvent("storing meta", "storing meta informations");

	ReportEvent META_FAILED = new ReportEvent("storing meta faild", "storing meta informations failed");

	ReportEvent IMPORT_COMPLETE = new ReportEvent("import complete", "the import is complete",Priority.INFO);

	ReportEvent EXPORT_SAMPLE = new ReportEvent("export sample content", "we export now the content of a sample");

	ReportEvent SAVE_RESULT = new ReportEvent("save result", "we save the result at all defined locations");

	ReportEvent RUN_STATISTICS = new ReportEvent("run statistics", "we are running now the predefined statistics");

	ReportEvent MATCHING = new ReportEvent("matching the sample", "we are matching the sample right now against the database");

	ReportEvent POST_MATCHING = new ReportEvent("post matching the sample", "we are post matching the sample right now against the database");
	
	ReportEvent CORRECTION = new ReportEvent("retention index correction", "right now we are correcting retention indexes");

	ReportEvent CORRECTION_FAILED = new ReportEvent("correction failed", "the correction of this sample failed");

	ReportEvent VALIDATION_FAILED = new ReportEvent("validation failed", "the validation of this sample failed");

	ReportEvent VALIDATION = new ReportEvent("validating sample", "this sample will be validated right now");

	ReportEvent CREATE_BIN = new ReportEvent("creating bins", "right now we are creating bins for this sample");

	
}
