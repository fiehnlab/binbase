/*
 * Created on 09.02.2004
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.error;


/**
 * @author wohlgemuth
 * definiert die fehlercodes in der datenbank
 */
public interface ErrorCode {
    /**
     * wenn die datei invalid ist
     *
     * @uml.property name="fILE_IS_INVALID"
     * @uml.associationEnd javaType="Code" multiplicity="(0 1)"
     */
    Code FILE_IS_INVALID = new Code("Sorry, your datafile is not valid", 2, "");

    /**
     * wenn ein standard nicht gefunden wurde
     *
     * @uml.property name="sTANDARD_NOT_FOUND"
     * @uml.associationEnd javaType="Code" multiplicity="(0 1)"
     */
    Code STANDARD_NOT_FOUND = new Code("Sorry, not enough standards found", 1,
            "Matrix is rank deficient");

    /**
     * wenn die datei invalid ist
     *
     * @uml.property name="tRAYNAME_NOT_FOUND"
     * @uml.associationEnd javaType="Code" multiplicity="(0 1)"
     */
    Code TRAYNAME_NOT_FOUND = new Code("Sorry the wanted Trayname element was not found",
            3, "element not found");

    /**
     * definiert einen errorcode
     * @author wohlgemuth
     */
    class Code {
        String description;
        String exceptionMessage;
        int code;

        /**
         *
         * @param description die beschreibung
         * @param code der code
         */
        public Code(String description, int code, String exceptionMessage) {
            this.description = description;
            this.code = code;
            this.exceptionMessage = exceptionMessage;
        }

        /**
         * @return Returns the code.
         *
         * @uml.property name="code"
         */
        public int getCode() {
            return code;
        }

        /**
         * @return Returns the description.
         *
         * @uml.property name="description"
         */
        public String getDescription() {
            return description;
        }

        /**
         * @return Returns the exceptionMessage.
         *
         * @uml.property name="exceptionMessage"
         */
        public String getExceptionMessage() {
            return exceptionMessage;
        }

        /**
         * @see java.lang.Object#toString()
         */
        public String toString() {
            return "Code: " + this.getDescription() + " (" + this.getCode() +
            ")";
        }
    }
}
