/*
 * Created on Feb 12, 2007
 */
package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.postprocess;

import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.jdom.Element;
import org.nfunk.jep.JEP;

import edu.ucdavis.genomics.metabolomics.util.statistics.postprocessing.PostProcessing;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.FormatObject;

/**
 * needs a mathematical formular and works on it on the given list of data
 * 
 * @author wohlgemuth
 * @version Feb 12, 2007
 * 
 */
public class Equation implements PostProcessing {
	String formula = "";

	Logger logger = Logger.getLogger(getClass());

	JEP parser = new JEP();

	public boolean configure(Element element) {
		if (element.getName().equals("postprocess")) {
			if (element.getAttributeValue("formula") != null) {
				this.formula = element.getAttributeValue("formula");

				parser.setAllowUndeclared(true);
				parser.setImplicitMul(true);
				parser.addStandardConstants();
				parser.addStandardFunctions();
				parser.addComplex();
				
				try {
					parser.parseExpression(formula);
					
				} catch (Exception e) {
					logger.error(e.getMessage(),e);
					return false;
				}
			} else {
				logger.warn("attribute formula not defiened!");
				return false;
			}
		} else {
			logger.warn("element has the wrong name!");
			return false;
		}
		return true;
	}

	public List postProcess(List<FormatObject<Double>> data) {
		List<FormatObject<Double>> result = new Vector<FormatObject<Double>>();
		
		for(FormatObject<Double> o : data){
			parser.parseExpression("x="+o.getValue());
			parser.getValueAsObject();
			parser.parseExpression(formula);
			o.setValue((Double) parser.getValueAsObject());
			
			result.add(o);
		}
		return result;
	}
}
