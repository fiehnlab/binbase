package edu.ucdavis.genomics.metabolomics.binbase.diagnostics;

/**
 * returns an implementation using log4j
 * @author wohlgemuth
 *
 */
public class Log4JDiagnosticsServiceFactory extends DiagnosticsServiceFactory {

	private Log4JDiagnosticsService service = new Log4JDiagnosticsService();

	@Override
	public DiagnosticsService createService() {
		return service;
	}

}
