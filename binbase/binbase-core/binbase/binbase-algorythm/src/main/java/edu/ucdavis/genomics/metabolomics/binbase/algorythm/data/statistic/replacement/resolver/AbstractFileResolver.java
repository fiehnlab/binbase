package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.resolver;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URI;
import java.util.zip.GZIPInputStream;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement.RawdataResolver;
import edu.ucdavis.genomics.metabolomics.util.io.Copy;

/**
 * simple helper to get files, it automatically figures out how to unpack them
 * 
 * @author wohlgemuth
 */
public abstract class AbstractFileResolver implements RawdataResolver {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	File getFile(String path, String sampleName) {
		File file = new File(path, sampleName + ".cdf");

		getLogger().info("looking for: " + file);
		if (file.exists() == false) {
			file = new File(path, sampleName + ".cdf.gz");
			getLogger().info("looking for: " + file);

			if (file.exists()) {
				try {
					getLogger()
							.info("file is gziped so we uncompress it in the temp directory");
					File fout = new File(System.getProperty("java.io.tmpdir"),
							sampleName + ".cdf");
					fout.deleteOnExit();
					BufferedOutputStream bout = new BufferedOutputStream(
							new FileOutputStream(fout));
					GZIPInputStream gzip = new GZIPInputStream(
							new BufferedInputStream(new FileInputStream(file)));

					Copy.copy(gzip, bout, true);

					file = fout;
				} catch (IOException e) {
					if (e.getMessage().contains("Not in GZIP format")) {
						if (file.exists()) {
							getLogger().warn("deleting invalid gzip file!");
							file.delete();
							getLogger().debug(e.getMessage(), e);
						}
					}
				} catch (Exception e) {
					getLogger().error(e.getMessage(), e);
				}
			}
		}

		return file;
	}

	/**
	 * looks for the file in the temp directory
	 * 
	 * @param sampleName
	 * @return
	 */
	File getFile(String sampleName) {
		return getFile(System.getProperty("java.io.tmpdir"), sampleName);
	}

	protected Logger getLogger() {
		return Logger.getLogger(getClass());
	}

	@Override
	public Integer getPriority() {
		// TODO Auto-generated method stub
		return 10;
	}

	@Override
	public int compareTo(RawdataResolver o) {
		return o.getPriority().compareTo(getPriority());
	}

	protected File createProtectedFile(File f) {
		return new MyFile(f.getAbsolutePath());
	}

	private class MyFile extends File {

		private Logger logger = Logger.getLogger(getClass());

		public MyFile(String parent, String child) {
			super(parent, child);
		}

		public MyFile(String pathname) {
			super(pathname);
		}

		public MyFile(URI uri) {
			super(uri);
		}

		public MyFile(File parent, String child) {
			super(parent, child);
		}

		@Override
		public boolean canWrite() {
			return false;
		}

		@Override
		public boolean delete() {
			logger.warn("sorry file can't be deleted!");
			return false;
		}

		@Override
		public void deleteOnExit() {
			logger.warn("sorry file can't be deleted!");
		}

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	}
}
