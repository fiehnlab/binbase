package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.tool;

import java.io.IOException;
import java.util.Collection;
import java.util.Vector;

import org.hibernate.Session;
import org.jdom.Element;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.math.BinSimilarityMatrix;
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.Bin;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.NotSupportedException;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.HeaderFormat;

/**
 * calculates a similarity matrix of all the bins in this datafile
 * 
 * @author wohlgemuth
 */
public class CalculateSimilarityMatrix extends BasicProccessable implements
		Processable {

	@Override
	public boolean writeResultToFile() {
		return false;
	}

	public String getFolder() {
		return "report";
	}

	/**
	 * all the work is done in the internal class
	 */
	public DataFile process(ResultDataFile datafile, Element configuration)
			throws BinBaseException {
		DataFile file = new InternalCalculateSimilarityMatrix(datafile)
				.calculateInternalMatrix();

		try {
			writeObject(file, configuration, "similarityMatrix");
		} catch (IOException e) {
			throw new BinBaseException(e);
		}

		return null;
	}

	/**
	 * internally used to work on the file instead of an hibernate session
	 * 
	 * @author wohlgemuth
	 */
	private class InternalCalculateSimilarityMatrix extends BinSimilarityMatrix {

		private ResultDataFile file;

		/**
		 * calculates the internal matrix based on the result file
		 * 
		 * @return
		 */
		public DataFile calculateInternalMatrix() {
			return this.createListVsListMatrix(this.getBins(), this.getBins());
		}

		public InternalCalculateSimilarityMatrix(ResultDataFile file) {
			this.file = file;
		}

		@Override
		public Bin getBin(int id) {
			Bin bin = new Bin();

			HeaderFormat<String> internalBin = file.getBin(id);
			bin.setId(id);
			bin.setMassSpec(internalBin.getAttributes().get("spectra"));
			bin.setName(internalBin.getAttributes().get("name"));
			bin.setRetentionIndex(Integer.parseInt(internalBin.getAttributes()
					.get("retention_index")));

			return bin;
		}

		@Override
		public Collection<Bin> getBins() {
			Vector<Bin> bins = new Vector<Bin>();
			for (HeaderFormat<String> bin : file.getBins()) {
				bins.add(getBin(Integer.parseInt(bin.getAttributes().get("id")
						.toString())));
			}
			return bins;
		}

		@Override
		public Collection<Bin> getKnownBins() {
			// TODO Auto-generated method stub
			return super.getKnownBins();
		}

		@Override
		public Session getSession() {
			throw new NotSupportedException(
					"sorry not possible in this implementation!");
		}

		@Override
		public Collection<Bin> getUnKnownBins() {
			// TODO Auto-generated method stub
			return super.getUnKnownBins();
		}

	}

	public String getDescription() {
		return "calculates a similarity matrix of all the bins against each other";
	}
}
