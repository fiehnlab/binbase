package edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;

/**
 * needed for monitoring
 * @author wohlgemuth
 *
 */
public class BinBaseExperimentClass extends ExperimentClass{

	public BinBaseExperimentClass(ExperimentClass clazz){
		this.setColumn(clazz.getColumn());
		this.setPriority(clazz.getPriority());
		this.setId(clazz.getId());
		
		BinBaseExperimentImportSample[] samples = new BinBaseExperimentImportSample[clazz.getSamples().length];
		
		for(int i = 0; i < samples.length; i++){
			samples[i] = new BinBaseExperimentImportSample(clazz.getSamples()[i],this);
		}
		
		this.setSamples(samples);
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	public boolean isSamplesAreCorrected() {
		for(ExperimentSample sample : this.getSamples()){
			if(((BinBaseExperimentImportSample)sample).isCorrected() == false){
				return false;
			}
		}
		
		return true;
	}

	public boolean isSamplesAreImported() {
		for(ExperimentSample sample : this.getSamples()){
			if(((BinBaseExperimentImportSample)sample).isImported() == false){
				return false;
			}
		}
		
		return true;
	}

	public boolean isSamplesAreMatched() {
		for(ExperimentSample sample : this.getSamples()){
			if(((BinBaseExperimentImportSample)sample).isMatched() == false){
				return false;
			}
		}
		
		return true;
	}

	public boolean isSamplesAreValidated() {
		for(ExperimentSample sample : this.getSamples()){
			if(((BinBaseExperimentImportSample)sample).isValidated() == false){
				return false;
			}
		}
		
		return true;
	}

	public boolean isMetaDateGenerated() {
		for(ExperimentSample sample : this.getSamples()){
			if(((BinBaseExperimentImportSample)sample).isMetaDataStored() == false){
				return false;
			}
		}
		
		return true;
	}
}
