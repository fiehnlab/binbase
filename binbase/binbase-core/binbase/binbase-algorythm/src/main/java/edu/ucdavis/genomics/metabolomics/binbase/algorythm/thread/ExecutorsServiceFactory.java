package edu.ucdavis.genomics.metabolomics.binbase.algorythm.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

public class ExecutorsServiceFactory {

	private static Logger logger = Logger.getLogger(ExecutorsServiceFactory.class);
	
	private static int cpuCount = Runtime.getRuntime().availableProcessors();
	/**
	 * creates an executor service and should be used by all classes
	 * 
	 * @return
	 */
	public static ExecutorService createService() {
		logger.debug("create executor for cpus: " + cpuCount);
		
		return Executors.newFixedThreadPool(cpuCount);
	}
	public static ExecutorService createService(int cpus) {
		logger.debug("create executor for cpus: " + cpus);
		
		return Executors.newFixedThreadPool(cpus);
	}

	public static ExecutorService createServiceWithLoadFactor(int load) {
		logger.debug("create executor for load: " + cpuCount * load);
		
		return Executors.newFixedThreadPool(cpuCount * load);
	}

	public static void shutdownService(ExecutorService service){
		logger.debug("shutting down service: " + service.toString());
		service.shutdown();
		try {
			logger.debug("waiting for calculations to finish: " + service.toString());

			service.awaitTermination(5000, TimeUnit.DAYS);
			logger.debug("shutdown complete");

		}
		catch (InterruptedException e) {
			logger.debug("shutdown error: " + e.getMessage(),e);
		}
	}

}
