package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.ArrayUtils;

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.math.CollectionRegression;
import edu.ucdavis.genomics.metabolomics.util.math.LinearRegression;
import edu.ucdavis.genomics.metabolomics.util.math.Regression;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.ColumnCombiner;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.Position;
import edu.ucdavis.genomics.metabolomics.util.statistics.deskriptiv.DeskriptiveMethod;
import edu.ucdavis.genomics.metabolomics.util.statistics.replacement.ZeroReplaceable;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.BinObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ContentObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.FormatObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.HeaderFormat;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.SampleObject;

/**
 * a special calibration file based on a result datafile and is basically a
 * delegate with some additional methods
 * 
 * @author wohlgemuth
 * 
 */
public class CalibrationFile extends ResultDataFile {

	public Set<String> getAllBinNames() {
		return delegate.getAllBinNames();
	}

	/**
	 * default constructor
	 * 
	 * @param calibrationValues
	 * @param regressionCurves
	 */
	public CalibrationFile(Map<String, Map<Double, Double>> calibrationValues,
			ResultDataFile dataFile, Collection<List<Double>> regressionCurves) {
		this.calibrationValues = calibrationValues;
		this.delegate = dataFile;

		for (String standard : getCalibrationStandards()) {

			Regression regression = null;

			// we want to use regressions grouped by intenisities
			if (regressionCurves != null && regressionCurves.size() > 0) {

				// we are using a collection regression
				regression = new CollectionRegression();

				for (List<Double> e : regressionCurves) {

					Collections.sort(e);

					Regression r = new LinearRegression();

					getLogger().info("current curve: " + e);
					double y[] = ArrayUtils.toPrimitive(e
							.toArray(new Double[0]));

					double x[] = new double[y.length];

					for (int i = 0; i < x.length; i++) {
						x[i] = getCalibrationValue(standard, y[i]);
					}
					r.setData(x, y);

					getLogger().info(
							"set data: X (" + x.length + ") " + " Y ("
									+ y.length + ")");
					((CollectionRegression) regression).addRegression(r);

				}
			}
			// no regression curve limits defined, so we just use a single
			// regression
			else {
				Set<Double> concentrations = getConcentrations();

				regression = new LinearRegression();

				// y == concentration
				// x == intensities
				double y[] = ArrayUtils.toPrimitive(concentrations
						.toArray(new Double[0]));
				double x[] = ArrayUtils.toPrimitive(getCalibrationValues(
						standard).toArray(new Double[0]));

				regression.setData(x, y);

			}

			// register curve for this standard
			addCurve(standard, regression);

			// /apply the calibration curves
			HeaderFormat<String> bin = getBin(standard);

			if (bin != null) {
				for (ContentObject<Double> spectra : getMassspecsForBin(Integer
						.parseInt(bin.getAttributes().get("id")))) {

					spectra.setValue(getCurve(standard)
							.getY(spectra.getValue()));
				}
			}
		}
	}

	/**
	 * removes not calibrated bins from this result file. Since there is no real
	 * point in keeping them
	 */
	public void removeNotCalibratedBins() {
		Set<String> bins = this.getAllBinNames();

		for (String standard : getCalibrationStandards()) {
			bins.remove(standard);
		}

		for (String bin : bins) {
			getLogger().info("removing none calibrated bin: " + bin);
			this.removeBin(this.getBin(bin), true);
		}
	}

	public void ignoreColumn(int position, boolean ignore) {
		delegate.ignoreColumn(position, ignore);
	}

	public void removeBin(HeaderFormat<String> bin, boolean includeFame) {
		delegate.removeBin(bin, includeFame);
	}

	public void removeFameMarkers() {
		delegate.removeFameMarkers();
	}

	private ResultDataFile delegate;

	public Set<String> getBinNamesForGroup(Integer id) {
		return delegate.getBinNamesForGroup(id);
	}

	public void updateGroupCache() {
		delegate.updateGroupCache();
	}

	public HeaderFormat<String> getBin(String name) {
		return delegate.getBin(name);
	}

	/**
	 * stores all the regressions
	 */
	private Map<String, Regression> regressions = new HashMap<String, Regression>();

	public int addColumn(String name, List<?> column) {
		return delegate.addColumn(name, column);
	}

	public String getDatabase() {
		return delegate.getDatabase();
	}

	public void setDatabase(String database) {
		delegate.setDatabase(database);
	}

	public int getResultId() {
		return delegate.getResultId();
	}

	public int addEmptyColumn() {
		return delegate.addEmptyColumn();
	}

	public void setIndexed(boolean indexed) {
		delegate.setIndexed(indexed);
	}

	public SampleTimeResolver getResolver() {
		return delegate.getResolver();
	}

	public void setResolver(SampleTimeResolver resolver) {
		delegate.setResolver(resolver);
	}

	public int addEmptyColumn(Object filler) {
		return delegate.addEmptyColumn(filler);
	}

	public HeaderFormat<String> getBin(int id) {
		return delegate.getBin(id);
	}

	public int addEmptyColumn(String label) {
		return delegate.addEmptyColumn(label);
	}

	public int addEmptyColumn(String label, int position) {
		return delegate.addEmptyColumn(label, position);
	}

	public List<Integer> getBinsForGroup(int id) {
		return delegate.getBinsForGroup(id);
	}

	public void addEmptyRow() {
		delegate.addEmptyRow();
	}

	public void addEmptyRowAtPosition(int position) {
		delegate.addEmptyRowAtPosition(position);
	}

	public BinObject<String> getBinForColumn(int column) {
		return delegate.getBinForColumn(column);
	}

	public void applyFunction(Function function) throws NumberFormatException,
			BinBaseException {
		delegate.applyFunction(function);
	}

	public void addEmptyRow(Object filler) {
		delegate.addEmptyRow(filler);
	}

	public void addRow(List row) {
		delegate.addRow(row);
	}

	public int getBinPosition(int id) {
		return delegate.getBinPosition(id);
	}

	public Object clone() throws CloneNotSupportedException {
		return delegate.clone();
	}

	public int getBinPosition(String name) {
		return delegate.getBinPosition(name);
	}

	public SampleObject<String> getSample(String name) {
		return delegate.getSample(name);
	}

	public void combineColumns(int[] ids, ColumnCombiner combine) {
		delegate.combineColumns(ids, combine);
	}

	public SampleObject<String> getSampleForRow(int row) {
		return delegate.getSampleForRow(row);
	}

	public FormatObject<?> getSpectra(String sampleName, int binId) {
		return delegate.getSpectra(sampleName, binId);
	}

	public FormatObject<?> getSpectra(String sampleName, String binName) {
		return delegate.getSpectra(sampleName, binName);
	}

	public int getSamplePosition(String name) {
		return delegate.getSamplePosition(name);
	}

	public void deleteColumn(int position) {
		delegate.deleteColumn(position);
	}

	public SampleObject<String> getSample(int id) {
		return delegate.getSample(id);
	}

	public List<ContentObject<Double>> getMassspecsForBin(int id,
			boolean ignoreNull) {
		return delegate.getMassspecsForBin(id, ignoreNull);
	}

	public void deleteRow(int position) {
		delegate.deleteRow(position);
	}

	public List findAllObject(Object object) {
		return delegate.findAllObject(object);
	}

	public Position findObject(Object object) {
		return delegate.findObject(object);
	}

	public List<ContentObject<Double>> getMassspecsForBin(int id) {
		return delegate.getMassspecsForBin(id);
	}

	public List<ContentObject<Double>> getMassspecsForBinGroup(int id) {
		return delegate.getMassspecsForBinGroup(id);
	}

	public List findRowsByGroup(Object group, int groupColumn) {
		return delegate.findRowsByGroup(group, groupColumn);
	}

	public List<ContentObject<Double>> getMassspecsForSample(String name) {
		return delegate.getMassspecsForSample(name);
	}

	public List<ContentObject<Double>> getMassspecsForSample(String name,
			boolean includedIgnoredStandards) {
		return delegate.getMassspecsForSample(name, includedIgnoredStandards);
	}

	public Object getCell(int column, int row) {
		return delegate.getCell(column, row);
	}

	public List getColumn(int position) {
		return delegate.getColumn(position);
	}

	public int getColumnCount() {
		return delegate.getColumnCount();
	}

	public int getColumnSize(int position) {
		return delegate.getColumnSize(position);
	}

	public List<List<?>> getData() {
		return delegate.getData();
	}

	public Collection getGroups(int column) {
		return delegate.getGroups(column);
	}

	public List<ContentObject<Double>> getStandardForSample(String sampleName) {
		return delegate.getStandardForSample(sampleName);
	}

	public int[] getIgnoreColumns() {
		return delegate.getIgnoreColumns();
	}

	public int[] getIgnoreRows() {
		return delegate.getIgnoreRows();
	}

	public List getRow(int position) {
		return delegate.getRow(position);
	}

	public List<HeaderFormat<String>> getStandards() {
		return delegate.getStandards();
	}

	public int getRowCount() {
		return delegate.getRowCount();
	}

	public int getTotalColumnCount() {
		return delegate.getTotalColumnCount();
	}

	public int getTotalRowCount() {
		return delegate.getTotalRowCount();
	}

	public List<HeaderFormat<String>> getDefinedStandards() {
		return delegate.getDefinedStandards();
	}

	public List<HeaderFormat<String>> getBins() {
		return delegate.getBins();
	}

	public List<HeaderFormat<String>> getBins(boolean includedIgnoredStandards) {
		return delegate.getBins(includedIgnoredStandards);
	}

	public List<SampleObject<String>> getSamples() {
		return delegate.getSamples();
	}

	public void combineColumns(ColumnCombiner combine) {
		delegate.combineColumns(combine);
	}

	public void filterBins(List<String> toKeep) {
		delegate.filterBins(toKeep);
	}

	public void filterSamples(List<String> toKeep) {
		delegate.filterSamples(toKeep);
	}

	public Double getAverageRetentionTimeForBin(int id) {
		return delegate.getAverageRetentionTimeForBin(id);
	}

	public double getAverageRetentionIndexForBin(int id) {
		return delegate.getAverageRetentionIndexForBin(id);
	}

	public Double getAverageRetentionTimeForBin(int id, String sample)
			throws BinBaseException {
		return delegate.getAverageRetentionTimeForBin(id, sample);
	}

	public Double getAverageRetentionIndexForBin(int id, String sample)
			throws BinBaseException {
		return delegate.getAverageRetentionIndexForBin(id, sample);
	}

	public boolean containsBin(int id) {
		return delegate.containsBin(id);
	}

	public boolean containsGroup(int group) {
		return delegate.containsGroup(group);
	}

	public boolean equals(Object obj) {
		return delegate.equals(obj);
	}

	public List getClazz(String string) {
		return delegate.getClazz(string);
	}

	public int getClazzCount() {
		return delegate.getClazzCount();
	}

	public List<String> getClazzNames() {
		return delegate.getClazzNames();
	}

	public int hashCode() {
		return delegate.hashCode();
	}

	public boolean isIndexed() {
		return delegate.isIndexed();
	}

	public void removeBin(HeaderFormat<String> bin) {
		delegate.removeBin(bin);
	}

	public void removeSample(SampleObject<String> sample) {
		delegate.removeSample(sample);
	}

	public boolean ignoreColumn(int i) {
		return delegate.ignoreColumn(i);
	}

	public boolean ignoreRow(int i) {
		return delegate.ignoreRow(i);
	}

	public boolean isZero(Object o) {
		return delegate.isZero(o);
	}

	public void replaceZeros(ZeroReplaceable replace, boolean sample) {
		delegate.replaceZeros(replace, sample);
	}

	public void print(PrintStream out) {
		delegate.print(out);
	}

	public void replaceZeros(ZeroReplaceable replace, int columnToGroupBy) {
		delegate.replaceZeros(replace, columnToGroupBy);
	}

	public void read(File file) throws IOException {
		delegate.read(file);
	}

	public void read(InputStream read) throws IOException {
		delegate.read(read);
	}

	public void read(Reader read) throws IOException {
		delegate.read(read);
	}

	public void replaceAll(Object old, Object new_) {
		delegate.replaceAll(old, new_);
	}

	public void removeFailedSamples() {
		delegate.removeFailedSamples();
	}

	public void sizeDown(boolean group, int column, double percent) {
		delegate.sizeDown(group, column, percent);
	}

	public void sizeDown(double percent) {
		delegate.sizeDown(percent);
	}

	public double runStatisticalMethod(DeskriptiveMethod statistic) {
		return delegate.runStatisticalMethod(statistic);
	}

	public double runStatisticalMethod(int column, DeskriptiveMethod statistic) {
		return delegate.runStatisticalMethod(column, statistic);
	}

	public double[] runStatisticalMethod(int column, int groupingColumn,
			DeskriptiveMethod statistic) {
		return delegate.runStatisticalMethod(column, groupingColumn, statistic);
	}

	public boolean setCell(int column, int row, Object value) {
		return delegate.setCell(column, row, value);
	}

	public void setColumn(int position, List column) {
		delegate.setColumn(position, column);
	}

	public void setData(List data) {
		delegate.setData(data);
	}

	public void setDimension(int row, int columns) {
		delegate.setDimension(row, columns);
	}

	public void setIgnoreColumn(int position, boolean ignore) {
		delegate.setIgnoreColumn(position, ignore);
	}

	public void setIgnoreColumns(int[] ignoreColumns) {
		delegate.setIgnoreColumns(ignoreColumns);
	}

	public void setIgnoreRow(int position, boolean ignore) {
		delegate.setIgnoreRow(position, ignore);
	}

	public void setIgnoreRows(int[] ignoreRows) {
		delegate.setIgnoreRows(ignoreRows);
	}

	public void setRow(int position, List row) {
		delegate.setRow(position, row);
	}

	public boolean skipColumnIndex(int i) {
		return delegate.skipColumnIndex(i);
	}

	public boolean skipRowIndex(int iX) {
		return delegate.skipRowIndex(iX);
	}

	public InputStream toInputStream() throws IOException {
		return delegate.toInputStream();
	}

	public List toList() {
		return delegate.toList();
	}

	public void initialize() throws Exception {
		delegate.initialize();
	}

	public String toString() {
		return delegate.toString();
	}

	public void write(Writer writer) throws IOException {
		delegate.write(writer);
	}

	public void write(File file) throws IOException {
		delegate.write(file);
	}

	public void write(OutputStream out) throws IOException {
		delegate.write(out);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * contains all the calibration data
	 * 
	 * key: bin second key: concentration value: combined intensity
	 */
	private Map<String, Map<Double, Double>> calibrationValues = new HashMap<String, Map<Double, Double>>();

	/**
	 * all our calibration standards
	 * 
	 * @return
	 */
	public Set<String> getCalibrationStandards() {
		return calibrationValues.keySet();
	}

	/**
	 * give us access to the internally used file.
	 * 
	 * @return
	 */
	public ResultDataFile getInternalFile() {
		return delegate;
	}

	/**
	 * get the calibration value for this intensity
	 * 
	 * @param bin
	 * @param conctration
	 * @return
	 */
	public Double getCalibrationValue(String bin, Double conctration) {
		return calibrationValues.get(bin).get(conctration);
	}

	/**
	 * returns all the concentration calibration values for this bin sorted by
	 * concentration
	 * 
	 * @param bin
	 * @return
	 */
	public List<Double> getCalibrationValues(String bin) {
		List<Double> list = new ArrayList<Double>();

		for (Double conc : getConcentrations()) {
			list.add(getCalibrationValue(bin, conc));
		}

		return list;
	}
	

	/**
	 * returns the regression curve for the given standard
	 * 
	 * @param standard
	 * @return
	 */
	public Regression getCurve(String standard) {
		return regressions.get(standard);
	}

	/**
	 * returns all possible concentrations
	 * 
	 * @return
	 */
	public Set<Double> getConcentrations() {
		Set<Double> set = new TreeSet<Double>();

		for (String s : calibrationValues.keySet()) {
			for (Double c : calibrationValues.get(s).keySet()) {
				set.add(c);
			}
		}

		return set;
	}

	/**
	 * add's a regression curve
	 * 
	 * @param standard
	 * @param regression
	 */
	protected void addCurve(String standard, Regression regression) {
		this.regressions.put(standard, regression);
	}

	/**
	 * returns the concentration for this sample
	 * 
	 * @param sample
	 * @return
	 */
	public Double getConcentration(String sample) {
		try {
			return calibrationValues.get(sample).entrySet().iterator().next()
					.getValue();
		} catch (Exception e) {
			throw new NotCalibratedException(
					"sorry it looks, like this sample was not a calibration sample: " + sample,
					e);
		}
	}
}
