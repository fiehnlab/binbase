/*
 * Created on Jul 14, 2006
 */
package edu.ucdavis.genomics.metabolomics.binbase.cluster.handler;

import edu.ucdavis.genomics.metabolomics.binbase.BinBaseReports;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import.data.BinBaseExportExperiment;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFileFactory;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.export.SQLExportService;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.util.SQLResultCreator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.io.SopSource;
import edu.ucdavis.genomics.metabolomics.binbase.bci.mail.MailService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.mail.MailServiceFactory;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.StatusJMXFacade;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.StatusJMXFacadeUtil;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.setupX.SetupXFactory;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.config.Configable;
import edu.ucdavis.genomics.metabolomics.util.config.XMLConfigurator;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.SimpleConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFileFactory;
import edu.ucdavis.genomics.metabolomics.util.status.ReportEvent;
import org.apache.log4j.Logger;

import javax.ejb.CreateException;
import javax.naming.NamingException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.sql.Connection;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

/**
 * exports experiments from the binbase
 *
 * @author wohlgemuth
 * @version Jul 14, 2006
 */
public class ExperimentHandler extends BinBaseClusterHandler {
    private Logger logger = Logger.getLogger(getClass());

    private Connection connection;

    @Override
    public boolean process(Serializable job) throws Exception {
        // now export them

        BinBaseExportExperiment ex = new BinBaseExportExperiment((Experiment) job);

        getReport().report(ex, BinBaseReports.STARTED, BinBaseReports.EXPERIMENT);
        logger.error("using column: " + ex.getColumn());
        if (ex.getColumn() == null) {
            ex.setColumn(Configable.CONFIG.getValue("server.defaultDatabase"));
            logger.info("set default database: " + ex.getColumn());
        }

        Properties p = this.getProperties();
        p.setProperty(SimpleConnectionFactory.KEY_USERNAME_PROPERTIE, ex.getColumn());

        this.setProperties(p);

        ConnectionFactory fact = ConnectionFactory.getFactory();
        fact.setProperties(this.getProperties());
        connection = fact.getConnection();
        connection.setAutoCommit(true);
        int result = storeExperimentDefinition(ex);

        ex.setDefinitionStored(true);

        getReport().report(ex, new ReportEvent("stored in database", "experiment is stored in the database"), BinBaseReports.EXPERIMENT);

        try {
            String content = createContent(ex, connection);


            sendEmail(ex.getId(),ex.getEmail(), content);

            ex.setEmailSend(true);
            getReport().report(ex, BinBaseReports.DONE, BinBaseReports.EXPERIMENT);

            return true;
        } catch (Exception e) {
            getReport().report(ex, BinBaseReports.FAILED, BinBaseReports.EXPERIMENT, e);
            dropExperiment(result);

            throw e;
        } finally {
            fact.close(connection);
        }

    }

    /**
     * stores the experiment description in the database
     *
     * @param ex
     * @param cl
     * @param t
     * @return
     * @throws Exception
     */
    private int storeExperimentDefinition(Experiment ex) throws Exception {
        return new SQLResultCreator(connection, getReport()).createResultDefinition(ex);
    }

    /**
     * drops the experiment
     *
     * @param id
     * @throws Exception
     */
    private void dropExperiment(int id) throws Exception {
        new SQLResultCreator(connection, getReport()).dropResult(id);
    }

    /**
     * create the actual content
     *
     * @param ex
     * @param connection
     * @param result
     * @return
     * @throws BinBaseException
     * @throws MalformedURLException
     * @throws CreateException
     * @throws RemoteException
     * @throws NamingException
     */
    private String createContent(BinBaseExportExperiment ex, Connection connection) throws BinBaseException, MalformedURLException, CreateException,
            RemoteException, NamingException {
        String content = null;

        if (DataFileFactory.newInstance().createDataFile() instanceof ResultDataFile) {
            logger.info("configured datafile factory is of the right instance");
        } else {
            logger.error("configured datafile factory does not generate an instanceof ResultDataFile, we are now forcing the use of the default ResultDatafile factory");
            System.setProperty(DataFileFactory.DEFAULT_PROPERTY_NAME, ResultDataFileFactory.class.getName());
        }
        if (ex.getSopUrl() != null) {
            logger.info("using sop provided by experiment");
            content = new SQLExportService(connection, this.getReport(), this).export(ex.getId(), ex.getId(), new SopSource(ex.getSopUrl()));
        } else {
            ExportJMXFacade facade = ExportJMXFacadeUtil.getHome(XMLConfigurator.getInstance().getProperties()).create();
            logger.info("using default sop " + facade.getDefaultSop());
            content = new SQLExportService(connection, this.getReport(), this).export(ex.getId(), ex.getId(), new SopSource(facade.getDefaultSop()));
        }
        ex.setStatisticsGenerated(true);


        getReport().report(ex, BinBaseReports.RUN_STATISTICS, BinBaseReports.EXPERIMENT);
        logger.info("send result to setup X: " + ex.getId() + " url is: " + content);

        ex.setSendToSetupX(true);
        try {
            SetupXFactory.newInstance().createProvider(this.getProperties()).upload(ex.getId(), content);

            getReport().report(ex, new ReportEvent("stored in setupx", "experiment is uploaded to setupx"), BinBaseReports.EXPERIMENT);
        } catch (Exception e) {
            logger.warn("can't upload to setupX: " + e.getMessage());
        }

        return content;
    }


    public Connection getConnection() {
        return connection;
    }
}
