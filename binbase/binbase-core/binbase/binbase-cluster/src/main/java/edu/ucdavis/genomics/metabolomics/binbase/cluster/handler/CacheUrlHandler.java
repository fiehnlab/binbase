package edu.ucdavis.genomics.metabolomics.binbase.cluster.handler;

import java.io.BufferedInputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.Scanner;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.BinBaseReports;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.job.CacheUrlJob;

/**
 * stores some meta informations in the cache
 * @author wohlgemuth
 *
 */
public class CacheUrlHandler extends BinBaseClusterHandler{

	private Logger logger = Logger.getLogger(getClass());
	@Override
	protected boolean process(Serializable job) throws Exception {
		CacheUrlJob current = (CacheUrlJob) job;
		getReport().report(job, BinBaseReports.META,
				BinBaseReports.CACHE);

		for(String url : current.getUrls()){
			logger.info("query url:  "+ url);
			URL u = new URL(url);
			Scanner scanner = new Scanner(new BufferedInputStream(u.openConnection().getInputStream()));
			StringBuffer result = new StringBuffer();
			while(scanner.hasNextLine()){
				result.append(scanner.nextLine());
			}
			logger.trace(result.toString());
		}
		
		logger.info("we are done with this job");
		return true;
	}

}
