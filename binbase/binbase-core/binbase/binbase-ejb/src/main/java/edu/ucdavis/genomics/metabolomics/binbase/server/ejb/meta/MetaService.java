package edu.ucdavis.genomics.metabolomics.binbase.server.ejb.meta;

import java.io.Serializable;

import javax.ejb.Remote;

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

@Remote
public interface MetaService extends Serializable {

	/**
	 * returns the experiment class for the given meta id, this can be a setupX
	 * id or any other form of id
	 * 
	 * @param metaId
	 * @param database
	 * @return
	 */
	ExperimentClass getExperimentClassForMetaId(String metaId, String database, String key) throws BinBaseException;

	/**
	 * returns all experiment classes in the databases
	 * 
	 * @param database
	 * @param key
	 * @return
	 */
	ExperimentClass[] getAllExperimentClassesInDatabase(String database, String key) throws BinBaseException;

	/**
	 * returns the experiment id for the class
	 * 
	 * @param clazz
	 * @param database
	 * @param key
	 * @return
	 */
	String getExperimentIdForClass(ExperimentClass clazz, String database, String key) throws BinBaseException;

	/**
	 * returns the experiment id for the class by the id
	 * @param classId
	 * @param database
	 * @param key
	 * @return
	 * @throws BinBaseException
	 */
	String getExperimentIdForClassByClassId(String classId, String database, String key) throws BinBaseException;

	/**
	 * checks if the given database actually has this experiment class
	 * @param metaId
	 * @param database
	 * @param key
	 * @return
	 * @throws BinBaseException
	 */
	boolean hasExperimentClass(String metaId, String database, String key) throws BinBaseException;
}
