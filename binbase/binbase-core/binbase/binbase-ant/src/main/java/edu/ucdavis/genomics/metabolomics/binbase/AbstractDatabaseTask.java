package edu.ucdavis.genomics.metabolomics.binbase;

import org.apache.log4j.Logger;
import org.apache.tools.ant.BuildException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.util.database.ConnectionFactory;
import edu.ucdavis.genomics.metabolomics.util.database.DriverUtilities;

public abstract class AbstractDatabaseTask extends AbstractApplicationServerTask {
	protected Logger logger = Logger.getLogger(getClass());
	
	private String type = String.valueOf(DriverUtilities.POSTGRES);

	private String password;

	private String database;

	private String server;

	private String username;

	private boolean fetchConfiguration = false;

	@Override
	protected void validateProperties() throws BuildException {

		if (this.isFetchConfiguration() == false) {
			check(password, "please provide a password");
			check(database, "please provide a database");
			check(server, "please provide a server address");
		}
		check(username, "please provide a username");
	}
	
	@Override
	protected final void execueTask() throws Exception {
		if(this.isFetchConfiguration()){
			logger.info("configure from defined application server");
			this.setDatabase(Configurator.getDatabaseService().getDatabase());
			this.setServer(Configurator.getDatabaseService().getDatabaseServer());
			this.setPassword(Configurator.getDatabaseService().getDatabaseServerPassword());
			this.setType(Configurator.getDatabaseService().getDatabaseServerType());
		}
		
		System.setProperty(ConnectionFactory.KEY_DATABASE_PROPERTIE, this.getDatabase());
		System.setProperty(ConnectionFactory.KEY_TYPE_PROPERTIE, this.getType());
		System.setProperty(ConnectionFactory.KEY_HOST_PROPERTIE, this.getServer());
		System.setProperty(ConnectionFactory.KEY_PASSWORD_PROPERTIE, this.getPassword());
		System.setProperty(ConnectionFactory.KEY_USERNAME_PROPERTIE, this.getUsername());
		
		executeDatabaseTask();
	}
	
	protected abstract void executeDatabaseTask() throws Exception;


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getServer() {
		return server;
	}

	public void setServer(String server) {
		this.server = server;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isFetchConfiguration() {
		return fetchConfiguration;
	}

	public void setFetchConfiguration(boolean fetchConfiguration) {
		this.fetchConfiguration = fetchConfiguration;
	}

}
