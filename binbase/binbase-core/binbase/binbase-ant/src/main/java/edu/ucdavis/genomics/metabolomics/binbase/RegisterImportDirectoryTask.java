package edu.ucdavis.genomics.metabolomics.binbase;

import org.apache.tools.ant.BuildException;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;

public class RegisterImportDirectoryTask extends AbstractApplicationServerTask {
	String importDirectory;

	@Override
	protected void execueTask() throws Exception {
		logger.info("adding directory: " + importDirectory
				+ " as txt directory");

		Configurator.getImportService().addDirectory(importDirectory);

		logger.info("done...");
	}

	@Override
	protected void validateProperties() throws BuildException {
		check(importDirectory, "please provide a directory for the import");
	}

	public String getImportDirectory() {
		return importDirectory;
	}

	public void setImportDirectory(String importDirectory) {
		this.importDirectory = importDirectory;
	}

}
