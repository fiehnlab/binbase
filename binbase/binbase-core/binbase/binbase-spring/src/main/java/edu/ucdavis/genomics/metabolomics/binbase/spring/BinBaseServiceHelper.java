package edu.ucdavis.genomics.metabolomics.binbase.spring;

import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.DSL;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.exception.CommunicationExcpetion;

/**
 * spring managed helper to access the binbase service
 * 
 * @author Administrator
 * 
 */
public class BinBaseServiceHelper implements BinBaseService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BinBaseService service;

	public void triggerDBUpdate(String database, String key)
			throws BinBaseException {
		service.triggerDBUpdate(database, key);
	}

	public boolean hasTextFile(String sampleName, String key)
			throws BinBaseException {
		return service.hasTextFile(sampleName, key);
	}

	public void triggerDSLCalculations(DSL dsl, String key) throws BinBaseException {
		service.triggerDSLCalculations(dsl, key);
	}

	public boolean hasNetCdfFile(String sampleName, String key) throws BinBaseException {
		return service.hasNetCdfFile(sampleName, key);
	}

	public void storeSample(ExperimentSample sample, String column, String key) throws BinBaseException {
		service.storeSample(sample, column, key);
	}

	public BinBaseService getService() {
		return service;
	}

	public void setService(BinBaseService service) {
		this.service = service;
	}

	public boolean containsSample(String arg0, String arg1, String arg2)
			throws BinBaseException {
		return service.containsSample(arg0, arg1, arg2);
	}

	public boolean containsSampleObject(ExperimentSample arg0, String arg1,
			String arg2) throws BinBaseException {
		return service.containsSampleObject(arg0, arg1, arg2);
	}

	public String createCustomizedExperiment(String arg0, String[] arg1,
			String arg2, String arg3, String arg4) throws BinBaseException {
		return service.createCustomizedExperiment(arg0, arg1, arg2, arg3, arg4);
	}

	public String[] getAllExperimentIds(String arg0, String arg1)
			throws BinBaseException {
		return service.getAllExperimentIds(arg0, arg1);
	}

	public String[] getAvailableSops(String arg0) throws BinBaseException {
		return service.getAvailableSops(arg0);
	}

	public Experiment getExperiment(String arg0, String arg1, String arg2)
			throws BinBaseException {
		return service.getExperiment(arg0, arg1, arg2);
	}

	public byte[] getNetCdfFile(String arg0, String arg1)
			throws BinBaseException {
		return service.getNetCdfFile(arg0, arg1);
	}

	public String[] getOutDatedSampleIds(String arg0, String arg1)
			throws BinBaseException {
		return service.getOutDatedSampleIds(arg0, arg1);
	}

	public ExperimentClass getStoredClass(String arg0, String arg1, String arg2)
			throws BinBaseException {
		return service.getStoredClass(arg0, arg1, arg2);
	}

	public ExperimentClass[] getStoredClasses(String arg0, String arg1)
			throws BinBaseException {
		return service.getStoredClasses(arg0, arg1);
	}

	public ExperimentSample[] getStoredSample(String arg0, String arg1,
			String arg2) throws BinBaseException {
		return service.getStoredSample(arg0, arg1, arg2);
	}

	public void triggerCalculateSample(String arg0, String arg1,
			ExperimentSample arg2) throws BinBaseException {
		service.triggerCalculateSample(arg0, arg1, arg2);
	}

	public void triggerExportById(String arg0, String arg1, String arg2)
			throws BinBaseException {
		service.triggerExportById(arg0, arg1, arg2);
	}

	public void triggerExportClass(ExperimentClass arg0, String arg1)
			throws BinBaseException {
		service.triggerExportClass(arg0, arg1);
	}

	public void triggerExportClassById(String arg0, String arg1, String arg2)
			throws BinBaseException {
		service.triggerExportClassById(arg0, arg1, arg2);
	}

	public void triggerExportClasses(ExperimentClass[] arg0, String arg1)
			throws BinBaseException {
		service.triggerExportClasses(arg0, arg1);
	}

	public void triggerExportClassesById(String arg0, String[] arg1, String arg2)
			throws BinBaseException {
		service.triggerExportClassesById(arg0, arg1, arg2);
	}

	public void triggerExportDatabase(String arg0, String arg1)
			throws BinBaseException {
		service.triggerExportDatabase(arg0, arg1);
	}

	public void triggerExportSecure(Experiment arg0, String arg1)
			throws CommunicationExcpetion {
		service.triggerExportSecure(arg0, arg1);
	}

	public void triggerImportClassSecure(ExperimentClass arg0, String arg1)
			throws BinBaseException {
		service.triggerImportClassSecure(arg0, arg1);
	}

	public void triggerReCalculateSample(String arg0, String arg1,
			ExperimentSample arg2) throws BinBaseException {
		service.triggerReCalculateSample(arg0, arg1, arg2);
	}

	public void triggerReImportClassById(String arg0, String arg1, String arg2)
			throws BinBaseException {
		service.triggerReImportClassById(arg0, arg1, arg2);
	}

	public void triggerReImportClassesById(String arg0, String[] arg1,
			String arg2) throws BinBaseException {
		service.triggerReImportClassesById(arg0, arg1, arg2);
	}

	public void triggerReImportExperimentById(String arg0, String arg1,
			String arg2) throws BinBaseException {
		service.triggerReImportExperimentById(arg0, arg1, arg2);
	}

	public void triggerUpdateDatabase(String arg0, String arg1)
			throws BinBaseException {
		service.triggerUpdateDatabase(arg0, arg1);
	}

	public void updateInternalMetaDataForSample(String arg0, String arg1,
			ExperimentSample arg2) throws BinBaseException {
		service.updateInternalMetaDataForSample(arg0, arg1, arg2);
	}

	public void updateMetaDataForSample(String arg0, String arg1,
			ExperimentSample arg2, String arg3) throws BinBaseException {
		service.updateMetaDataForSample(arg0, arg1, arg2, arg3);
	}

	public void uploadSop(String arg0, String arg1, String arg2)
			throws BinBaseException {
		service.uploadSop(arg0, arg1, arg2);
	}

	public String[] getRegisteredColumns(String key) throws BinBaseException {
		return service.getRegisteredColumns(key);
	}

	public boolean isExperimentInNeedOfRecalculation(Experiment exp, String database, String key) throws BinBaseException {
		return service.isExperimentInNeedOfRecalculation(exp, database, key);
	}

	public boolean isSampleInNeedOfRecalculation(String name, String database, String key) throws BinBaseException {
		return service.isSampleInNeedOfRecalculation(name, database, key);
	}

	public int getSampleIdForLatestVersion(ExperimentSample sample, String database, String key) throws BinBaseException {
		return service.getSampleIdForLatestVersion(sample, database, key);
	}

	public boolean isExperimentAbleToBeRecalculated(Experiment exp, String database, String key) throws BinBaseException {
		return service.isExperimentAbleToBeRecalculated(exp, database, key);
	}

	public long getTimeStampForSample(String sample, String key) throws BinBaseException {
		return service.getTimeStampForSample(sample, key);
	}


}
