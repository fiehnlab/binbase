package edu.ucdavis.genomics.metabolomics.binbase.spring;

import java.util.Collection;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.AuthentificationException;
import edu.ucdavis.genomics.metabolomics.binbase.bci.authentification.User;
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.SimpleAuthentificationService;

public class BinBaseSimpleAuthentificationHelper implements SimpleAuthentificationService{

	private Logger logger = Logger.getLogger(getClass());
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private SimpleAuthentificationService service;

	public void addUser(User user) throws AuthentificationException {
		service.addUser(user);
	}

	public User authentificate(String username, String password, String database) throws AuthentificationException {
		return service.authentificate(username, password, database);
	}

	public User authentificate(String username, String password) throws AuthentificationException {
		return service.authentificate(username, password);
	}

	public boolean canRegisterUser() throws AuthentificationException {
		return service.canRegisterUser();
	}

	public boolean existUser(User user) {
		return service.existUser(user);
	}

	public Collection<User> listUser() {
		return service.listUser();
	}

	public void removeUser(User user) throws AuthentificationException {
		service.removeUser(user);
	}

	public void updateUser(User user) throws AuthentificationException {
		service.updateUser(user);
	}

	public SimpleAuthentificationService getService() {
		return service;
	}

	public void setService(SimpleAuthentificationService service) {
		logger.info("setting service implementation: " + service);
		this.service = service;
	}
}
