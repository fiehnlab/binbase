package edu.ucdavis.genomics.metabolomics.binbase.integration.cluster;

import org.apache.log4j.Logger;

import edu.ucdavis.genomics.metabolomics.binbase.BinBaseServices;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.validator.KeyFactory;

/**
 * works direct on the service and uses no scheduler to handle the load
 * 
 * @author wohlgemuth
 * 
 */
public abstract class BinBaseNoSchedulerClusterCalculationTest extends
		BinBaseClusterCalculationTest {

	private Logger logger = Logger.getLogger(getClass());

	@Override
	protected void scheduleExport(Experiment exp) throws Exception {
		logger.info("scheduling export...");
		// schedule the export
		BinBaseServices.getService().triggerExportSecure(exp, KeyFactory.newInstance().getKey());

		// wait till the scheduling is done
		while (this.getMessageQueue().isEmpty()) {
			logger.info("waiting for the scheduler to schedule the message");
			Thread.sleep(1000);
		}
		logger.info("done");
	}

	@Override
	protected void scheduleImport(ExperimentClass clazz) throws Exception {
		logger.info("scheduling export...");
		// schedule the import 
		BinBaseServices.getService().triggerImportClassSecure(clazz,
				KeyFactory.newInstance().getKey());

		// wait till the scheduling is done
		while (this.getMessageQueue().isEmpty()) {
			logger.info("waiting for the scheduler to schedule the message");
			Thread.sleep(1000);
		}
		logger.info("done");
	}

	// needs to be called...
	@Override
	public void setUp() throws Exception {
		super.setUp();
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}
}
