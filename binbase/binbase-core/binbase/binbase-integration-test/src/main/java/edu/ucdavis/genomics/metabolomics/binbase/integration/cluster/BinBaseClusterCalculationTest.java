package edu.ucdavis.genomics.metabolomics.binbase.integration.cluster;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.handler.ExperimentClassHandler;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.handler.ExperimentHandler;
import edu.ucdavis.genomics.metabolomics.binbase.integration.AbstractBinBaseCalculationTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;

/**
 * runs the test on the cluster using several nodes, the scheduler and so on
 * needs to be subclassed by the actuall cluster implementation
 * 
 * @author wohlgemuth
 * 
 */
public abstract class BinBaseClusterCalculationTest extends
		AbstractBinBaseCalculationTest {
	protected static ClusterUtil UTIL = null;

	private Logger logger = Logger.getLogger(getClass());

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		UTIL.destroy();
	}

	/**
	 * standard setup of the cluster util
	 * 
	 * @throws Exception
	 */
	public static void initializeUtil() throws Exception {

		// development machine
		if (System.getProperty("test.binbase.cluster.application-server") != null) {
			System.out
					.println("using defined application server: "
							+ System
									.getProperty("test.binbase.cluster.application-server"));
			UTIL.initializeCluster(System
					.getProperty("test.binbase.cluster.application-server"));
		}
		// test machine modus
		else {
			System.out.println("using cluster frontend as application server: "
					+ System.getProperty("test.binbase.cluster.server"));
			UTIL.initializeCluster(System
					.getProperty("test.binbase.cluster.server"));
		}
	}

	@Override
	public void setUp() throws Exception {
		super.setUp();

		logger.info("setting cluster service up");
		if (UTIL == null) {
			logger
					.error("you need to setup the util in one of the subclasses. Please define a method called: \"public static void setUpBeforeClass() throws Exception\" and add the annotation \"@BeforeClass\" The name of the variable to setup is UTIL");
			Assert
					.fail("you need to setup the util in one of the subclasses. Please define a method called: \"public static void setUpBeforeClass() throws Exception\" and add the annotation \"@BeforeClass\" The name of the variable to setup is UTIL");
		}

		logger.info("killing queue of the current user");
		UTIL.killJobOfCurrentUser();
		logger.info("waiting till its really empty");
		UTIL.waitTillQueueCurrentUserIsEmpty();

		logger.info("empty calculation queue of the application server");
		Configurator.getConfigService().clearQueue();


		Configurator.getConfigService().setIddleTime(5000);
		Configurator.getConfigService().setTimeout(5000);

		Configurator.getConfigService().addHandler(Experiment.class.getName(),
				ExperimentHandler.class.getName());
		Configurator.getConfigService().addHandler(
				ExperimentClass.class.getName(),
				ExperimentClassHandler.class.getName());

	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
		logger.info("cleaning up after the test");

		if (UTIL == null) {
			logger
					.error("you need to setup the util in one of the subclasses. Please define a method called: \"public static void setUpBeforeClass() throws Exception\" and add the annotation \"@BeforeClass\" The name of the variable to setup is UTIL");
			Assert
					.fail("you need to setup the util in one of the subclasses. Please define a method called: \"public static void setUpBeforeClass() throws Exception\" and add the annotation \"@BeforeClass\" The name of the variable to setup is UTIL");

		} else {
			logger.info("killing queue of the current user");
			UTIL.killJobOfCurrentUser();
			logger.info("waiting till its really empty");
			UTIL.waitTillQueueCurrentUserIsEmpty();
			logger.info("empty calculation queue of the application server");
			Configurator.getConfigService().clearQueue();
		}
	}

	@Override
	protected void calculateScheduledExperiment() throws Exception {
		// start two nodes for the calculation
		UTIL.startNode();

		// wait till the jobs are done
		UTIL.waitTillPendingQueueCurrentUserIsEmpty();
		UTIL.waitTillQueueCurrentUserIsEmpty();
	}

	@Override
	protected void calculateScheduledExperimentClass() throws Exception {
		// start a node
		UTIL.startNode();
		UTIL.startNode();

		// wait till the jobs are done
		UTIL.waitTillPendingQueueCurrentUserIsEmpty();
		UTIL.waitTillQueueCurrentUserIsEmpty();
	}
}
