package edu.ucdavis.genomics.metabolomics.binbase;

import static org.junit.Assert.*;

import org.apache.tools.ant.Project;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;

public class ConfigureImportTaskTest extends AbstractApplicationServerTest{

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testExecute() {
		ConfigureImportTask task = new ConfigureImportTask();
		task.setProject(new Project());
		
		task.setApplicationServerIp(System.getProperty("test.binbase.cluster.server"));
		task.setClusterAddress(System.getProperty("test.binbase.cluster.server"));
		task.setClusterUsername(System.getProperty("test.binbase.cluster.username"));
		task.setClusterPassword(System.getProperty("test.binbase.cluster.password"));
				
	}
}
