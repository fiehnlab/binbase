package edu.ucdavis.genomics.metabolomics.binbase.algorythm.Import;

import static org.junit.Assert.*;

import java.rmi.RemoteException;
import java.sql.SQLException;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass;
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample;
import edu.ucdavis.genomics.metabolomics.binbase.integration.GeneralAbstractCalculationTest;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.status.Log4JReport;

public class ClassImportTest extends GeneralAbstractCalculationTest {

	@Override
	public void setUp() throws Exception {
		super.setUp();
	}

	@Override
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test    
	public void testSingleThreadedImport() throws Exception {

			
			ClassImporter importer = new SingleThreadClassImporter();
			importer.setConnection(getConnection());

			ExperimentClass clazz = createClazz();
			importer.setReport(new Log4JReport());

			importer.setNewBinsAllowed(true);
			importer.importData(clazz);

			validateResult();


	}

	@Test
	public void testMultiThreadedImport() throws Exception {
			ClassImporter importer = new MultithreadClassImporter();
			importer.setConnection(getConnection());

			ExperimentClass clazz = createClazz();
			importer.setReport(new Log4JReport());

			importer.setNewBinsAllowed(true);
			importer.importData(clazz);

			validateResult();

	}

	protected void validateResult() throws SQLException {
		System.out.println("validating result...");
		assertTrue(getSpectraAnotatedCount("4125ea02_2") == 133);
		assertTrue(getSpectraAnotatedCount("4125ea03_2") == 134);
		assertTrue(getSpectraAnotatedCount("4125ea04_2") == 134);
		assertTrue(getSpectraAnotatedCount("4125ea05_2") == 134);
		assertTrue(getSpectraAnotatedCount("4125ea06_2") == 133);
		assertTrue(getSpectraAnotatedCount("4125ea07_2") == 32);

		assertTrue(getSampleCount() == 7);
		assertTrue(getBinCount() == 134);
		assertTrue(getSpectraAnotatedCount() == 713);
		assertTrue(getSpectraNotAnotatedCount() == 1130);
	}

	protected ExperimentClass createClazz() throws BinBaseException, RemoteException, CreateException, NamingException {
		ExperimentClass clazz = new ExperimentClass();
		clazz.setColumn(Configurator.getImportService().getDatabases()[0]);
		ExperimentSample sample1 = new ExperimentSample();
		ExperimentSample sample2 = new ExperimentSample();
		ExperimentSample sample3 = new ExperimentSample();
		ExperimentSample sample4 = new ExperimentSample();
		ExperimentSample sample5 = new ExperimentSample();
		ExperimentSample sample6 = new ExperimentSample();

		sample1.setId("1");
		sample2.setId("2");
		sample3.setId("3");
		sample4.setId("4");
		sample5.setId("5");
		sample6.setId("6");

		sample1.setName("4125ea02_2");
		sample2.setName("4125ea03_2");
		sample3.setName("4125ea04_2");
		sample4.setName("4125ea05_2");
		sample5.setName("4125ea06_2");
		sample6.setName("4125ea07_2");

		clazz.setId(getClass().getName());
		clazz.setSamples(new ExperimentSample[] { sample1, sample2, sample3, sample4, sample5, sample6 });

		assertTrue(clazz.getSamples().length == 6);
		return clazz;
	}
}
