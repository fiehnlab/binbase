package edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.statistic.replacement;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Vector;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.ResultDataFile;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.StaticStatisticActions;
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.data.output.XLS;
import edu.ucdavis.genomics.metabolomics.binbase.bci.BinBaseConfiguredApplicationServerIntegrationTest;
import edu.ucdavis.genomics.metabolomics.binbase.bci.io.ResultDestination;
import edu.ucdavis.genomics.metabolomics.binbase.bci.io.SopSource;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;
import edu.ucdavis.genomics.metabolomics.util.io.source.ResourceSource;
import edu.ucdavis.genomics.metabolomics.util.io.source.Source;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.ContentObject;
import edu.ucdavis.genomics.metabolomics.util.transform.crosstable.object.FormatObject;

/**
 * tests if the replacement of the netcdf works
 * 
 * @author wohlgemuth
 * 
 */
public class ReplaceWithQuantIntensityBasedOnAverageBinRtOverADayTest extends
		BinBaseConfiguredApplicationServerIntegrationTest {

	private Logger logger = Logger.getLogger(getClass());

	private ResultDataFile datafile = null;

	@Before
	public void setUp() throws Exception {
		super.setUp();

		this.addTestExportDirectory(new File(
				"src/test/resources/datafile/netcdfTest"));
		this.addTestSopDirectory(new File("src/test/resources/sop"));
		super.addTestImportDirectory(new File(
				"src/test/resources/sampleExperiment/createBins"));
		super.addTestImportDirectory(new File(
				"src/test/resources/sampleExperiment/noBins"));
		super.addTestImportDirectory(new File(
				"src/test/resources/sampleExperiment/reproduce"));
		super.addTestImportDirectory(new File(
				"src/test/resources/sampleExperiment/complex"));

		Source sop = new SopSource("netcdf.stat");
		Source rawdata = new ResourceSource("/datafile/netcdfTest.xml");

		Document sopDefinition = new SAXBuilder().build(sop.getStream());
		Element root = sopDefinition.getRootElement();

		datafile = new StaticStatisticActions().readFile("no id", rawdata, root
				.getChild("transform"));
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testFunctionality() throws IOException, BinBaseException {
		ReplaceWithQuantIntensityBasedOnAverageBinRtOverADay replace = new ReplaceWithQuantIntensityBasedOnAverageBinRtOverADay();

		replace.setFile(datafile);
		datafile.replaceZeros(replace, true);

		// just that we can have a look at the generated file
		OutputStream out = new ResultDestination(this.getClass().getName()
				+ ".xls").getOutputStream();
		new XLS().write(out, datafile);
		out.flush();
		out.close();

		// all from same day and would be off if we would calculate them over
		// both classes
		assertTrue(datafile.getAverageRetentionTimeForBin(1, "4126ea02_2") == 655966);
		assertTrue(datafile.getAverageRetentionTimeForBin(2, "4126ea02_2") == 679700);
		assertTrue(datafile.getAverageRetentionTimeForBin(3, "4126ea02_2") == 691700);

		// access the row with the failed sample
		int position = datafile.getSamplePosition("4126ea04_2");
		List<FormatObject<?>> data = datafile.getRow(position);
		List<ContentObject<Double>> content = new Vector<ContentObject<Double>>();
		for (int i = 3; i < data.size(); i++) {
			content.add((ContentObject<Double>) data.get(i));
		}

		// check if the retention times match what we exspect
		assertTrue(content.get(0).getValue() == 588487);
		assertTrue(content.get(1).getValue() == 133521);
		assertTrue(content.get(2).getValue() == 84388);

	}

}
