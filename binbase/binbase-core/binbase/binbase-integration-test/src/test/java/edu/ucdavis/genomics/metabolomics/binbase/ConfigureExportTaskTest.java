package edu.ucdavis.genomics.metabolomics.binbase;

import java.rmi.RemoteException;

import javax.ejb.CreateException;
import javax.naming.NamingException;

import org.apache.tools.ant.Project;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import edu.ucdavis.genomics.metabolomics.binbase.bci.Configurator;
import edu.ucdavis.genomics.metabolomics.binbase.cluster.test.integration.AbstractApplicationServerTest;
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException;

public class ConfigureExportTaskTest extends AbstractApplicationServerTest{

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@After
	public void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testExecute() throws RemoteException, BinBaseException, CreateException, NamingException {
		ConfigureExportTask task = new ConfigureExportTask();
		task.setProject(new Project());
		task.setApplicationServerIp(System.getProperty("test.binbase.cluster.server"));
		task.setNetcdfDirectory("sdfdasdasda");
		task.setResultDirectory("dadasdasa");
		task.setSopDirectory("dasdasdasd");
		
		Configurator.getExportService().getNetCDFDirectories().contains(task.getNetcdfDirectory());
		Configurator.getExportService().getSopDirs().contains(task.getSopDirectory());
		Configurator.getExportService().getResultDirectory().contains(task.getResultDirectory());
		
	}

}
