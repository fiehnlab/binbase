delete from standard
;
delete from bin where bin_id not in (select bin_id from standard)
;
delete from samples where sample_id not in (select sample_id from bin)
;
delete from spectra where sample_id not in (select sample_id from bin)
;
delete from result_link
;
delete from result
;
delete from rawdata
;
delete from bin_group where group_id not in (select group_id from bin)
;
delete from reference where bin_id not in (select bin_id from bin)
;
delete from library
;
delete from library_match
;
delete from library_spec
;
delete from correction_data;
;
delete from runtime;
;
delete from configuration;
;
delete from quantification;
;
DROP SEQUENCE binbase.bin_id
;
DROP SEQUENCE binbase.comment_id
;
DROP SEQUENCE binbase.hibernate_sequence
;
DROP SEQUENCE binbase.job_id
;
DROP SEQUENCE binbase.link_id
;
DROP SEQUENCE binbase.quantification_id
;
DROP SEQUENCE binbase.result_id
;
DROP SEQUENCE binbase.sample_id
;
DROP SEQUENCE binbase.spectra_id
;
DROP SEQUENCE binbase.type_id
;
CREATE SEQUENCE binbase.bin_id
;
CREATE SEQUENCE binbase.comment_id
;
CREATE SEQUENCE binbase.hibernate_sequence
;
CREATE SEQUENCE binbase.job_id
;
CREATE SEQUENCE binbase.link_id
;
CREATE SEQUENCE binbase.quantification_id
;
CREATE SEQUENCE binbase.result_id
;
CREATE SEQUENCE binbase.sample_id
;
CREATE SEQUENCE binbase.spectra_id
;
CREATE SEQUENCE binbase.type_id
;

