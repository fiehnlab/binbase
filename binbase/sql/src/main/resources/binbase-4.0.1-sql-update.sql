   
SET search_path = binbase, pg_catalog; 
--
-- We need a column for the bin table to hold the date of generation
-- which should be by default NOW
--

alter table bin add column date_of_generation timestamp; 

alter table bin alter column date_of_generation SET DEFAULT CURRENT_TIMESTAMP;

--
-- InchiKey to link bins to each other and different compounds
--
alter table bin add column inchi_key char(27);
