#patch for the image to fix current issues 

#install missing subversion
yum install subversion.x86_64

#correct permissions in these dirs
chmod -R 777 /mnt/binbase/dsl
chmod -R 777 /mnt/binbase/sop
chmod -R 777 /mnt/binbase/msp

#install binbase sources
svn co https://binbase.svn.sourceforge.net/svnroot/binbase/trunk /state/partition1/home/binbase/sources

#install latest minix version
wget http://minix.googlecode.com/files/minix-jboss-4.2.0-GA.war

mv minix-jboss-4.2.0-GA.war $JBOSS_HOME/server/all/deploy/

#update database schema

cat /state/partition1/home/binbase/sources/binbase/sql/binbase-4.0.1-sql-update.sql > /tmp/update.sql

su postgres -c  'psql binbase < /tmp/update.sql'

rm -f /tmp/update.sql

#install newest BinView version
wget http://binbase.googlecode.com/files/BinView-0.1.zip
mv BinView-0.1.zip /mnt/binbase/tools/

#update the jboss startup file

cat /state/partition1/home/binbase/sources/patches/vmware/startup/jboss > /etc/init.d/jboss
chkconfig --del jboss
chkconfig --add jboss

#update the database configuration
cat /state/partition1/home/binbase/sources/patches/vmware/configuration/minix-ds.xml > $JBOSS_HOME/server/all/deploy/minix-ds.xml

#update the samba configuration
cat /state/partition1/home/binbase/sources/patches/vmware/configuration/smb.conf > /etc/samba/smb.conf


echo "binbase image is patched, please reboot your server!"
