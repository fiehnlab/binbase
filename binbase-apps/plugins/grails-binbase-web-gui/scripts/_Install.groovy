//
// This script is executed by Grails after plugin was installed to project.
// This script is a Gant script so you can use all special variables provided
// by Gant (such as 'baseDir' which points on project base dir). You can
// use 'ant' to access a global instance of AntBuilder
//
// For example you can create directory under project tree:
//
//    ant.mkdir(dir:"${basedir}/grails-app/jobs")
//

def ant = binding.variables['ant'] ?: binding.variables['Ant']

//install configuration
def dest = "${basedir}/grails-app/conf"
def destFile = new File(dest, "BinBaseConfig.groovy")
def exists = false

if (destFile.exists()) {
    exists = true
    ant.input(message: """

        STOP!

        BinBaseConfig configuration already exists:

            ${destFile}

        do you want to overwrite it?

            """,
            validargs: "y,n",
            addproperty: "grails.install.binbase.config.warning")

    def answer = ant.antProject.properties."grails.install.binbase.config.warning"

    if (answer == "y") {

        ant.copy(file: "${binbaseWebGuiPluginDir}/src/conf/BinBaseConfig.groovy", todir: dest)
        ant.echo(message: """
                BinBaseConfig configuration file ${exists ? 're-' : ''}created:

                    ${destFile}

            """)
    }
}

//install default views, templates and css files/images

ant.input(message: """

        would you like to install the default views and templates?

            """,
        validargs: "y,n",
        addproperty: "grails.install.binbase.views.warning")

def answer = ant.antProject.properties."grails.install.binbase.views.warning"

if (answer == "y") {

    ant.copy(todir: "${basedir}/grails-app/views") {
        fileset(dir: "${binbaseWebGuiPluginDir}/grails-app/views") {
            include(name: "**/*.*")
        }
    }

    ant.copy(todir: "${basedir}/grails-app/i18n") {
        fileset(dir: "${binbaseWebGuiPluginDir}/grails-app/i18n") {
            include(name: "webgui.properties")
        }
    }
    ant.copy(todir: "${basedir}/web-app/css") {
        fileset(dir: "${binbaseWebGuiPluginDir}/web-app/css") {
            include(name: "**/*.*")
        }
    }
    ant.copy(todir: "${basedir}/web-app/images") {
        fileset(dir: "${binbaseWebGuiPluginDir}/web-app/images") {
            include(name: "**/*.*")
        }
    }
    ant.copy(todir: "${basedir}/src/templates") {
        fileset(dir: "${binbaseWebGuiPluginDir}/src/templates") {
            include(name: "**/*.*")
        }
    }
    ant.copy(todir: "${basedir}/web-app/jquery-ui") {
        fileset(dir: "${binbaseWebGuiPluginDir}/web-app/jquery-ui") {
            include(name: "**/*.*")
        }
    }


}