<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>

<div class="article">
    <h2 class="star">Class Details: ${BBExperimentClassInstance.name}</h2>

    <g:if test="${BBExperimentClassInstance.experiment.publicExperiment}">

        <g:render template="details" model="[c:BBExperimentClassInstance]"/>


        <g:javascript>
            jQuery(document).ready(function() {
                $('#classArcodion').accordion();
                $('#classArcodion').bind('accordionchange', function(event, ui) {

                    //load the content now to increase the performance
                });

            });

        </g:javascript>

        <div id="classArcodion">

            <h2><a href="#">Samples</a></h2>

            <div class="height320px">

                <div class="help-message ui-state-highlight ui-corner-all"
                     style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                        <g:message code="class.samples"/>
                    </p>
                </div>
                <g:render template="samples" model="[c:BBExperimentClassInstance]"/>
            </div>

            <h2><a href="#">MetaData</a></h2>

            <div class="height320px">
                <div class="help-message ui-state-highlight ui-corner-all"
                     style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                        <g:message code="class.metaData"/>
                    </p>
                </div>
                <g:render template="metadata" model="[c:BBExperimentClassInstance]"/>

            </div>
        </div>

    </g:if>

    <g:else>

        <div class="warning-message ui-state-error ui-corner-all"
             style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                <g:message code="public.noaccess"/>
            </p>
        </div>
    </g:else>
</div>

</body>
</html>
