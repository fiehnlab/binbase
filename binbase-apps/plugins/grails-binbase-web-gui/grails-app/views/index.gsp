<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>
<div class="article">

<h2> ${grailsApplication.config.binbase.welcomeTitle}</h2>

<div class="clr"></div>


<div class="post_content">
   ${grailsApplication.config.binbase.welcomeText}
</div>


<div class="clr"></div>
</div>

</body>
</html>
