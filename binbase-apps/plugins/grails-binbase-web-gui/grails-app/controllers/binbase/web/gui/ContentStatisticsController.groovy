package binbase.web.gui

import binbase.web.core.BBBin
import binbase.web.core.BBExperimentSample
import binbase.web.core.BBSpectra
import binbase.web.core.BBExperimentClass
import binbase.web.core.BBExperiment
import binbase.web.core.BBOrgan
import binbase.web.core.BBSpecies
import binbase.web.core.BBKingdom
import core.SpeciesGraphService
import grails.converters.JSON
import core.StatisticsService
import binbase.web.core.BBStatisticSnapshot

/**
 * provides some simple real time statistics
 */
class ContentStatisticsController {

    StatisticsService statisticsService

    def showStatistics = {


    }

    /**
     * shows statistics over time
     */
    def showTimeBasedStatistics = {
        [object:params.object]
    }

    /**
     * simple function to render the count of objects quickly
     */
    def ajaxRenderCountByType = {
        def value = BBStatisticSnapshot.findByObjectName(params.object,
                [
                        max: 1,
                        offset: 0,
                        sort: "createdAt",
                        order: "desc"
                ]
        )?.objectCount

        render value
    }

}
