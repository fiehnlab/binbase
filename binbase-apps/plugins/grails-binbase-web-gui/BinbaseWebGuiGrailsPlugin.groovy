class BinbaseWebGuiGrailsPlugin {
    // the plugin version
def version = "5.1.0"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "1.3.5 > *"
    // the other plugins this plugin depends on
    def dependsOn = [
            binbaseWebCore: "4.1.0 >*",
            quartz: "0.4.2",
            jqueryDatatables: "1.7.5",
            jqueryUi: "1.8.11  > *",
            flot: "0.2.3"
    ]
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
            "grails-app/views/error.gsp",
            "grails-app/conf/BinBaseConfig.groovy"
    ]

    def author = "Gert Wohlgemuth"
    def authorEmail = "berlinguyinca@gmail.com"
    def title = "Gui Plugin for BinBase integration"
    def description = '''\\

        this plugin is used to provide a generic gui for use with BinBase based databases
'''
    // URL to the plugin's documentation
    def documentation = "http://grails.org/plugin/binbase-web-gui"

    def doWithWebDescriptor = { xml ->
    }

    def doWithSpring = {
    }

    def doWithDynamicMethods = { ctx ->
    }

    def doWithApplicationContext = { applicationContext ->
    }

    def onChange = { event ->
    }

    def onConfigChange = { event ->
    }
}
