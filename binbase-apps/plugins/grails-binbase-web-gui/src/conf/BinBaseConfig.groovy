/**
 * this file contains the binbase configuration for this plugin.
 *
 */
binbase {

    /**
     * key to access the binbase instance
     */

    key = "please enter your private key here"

    /**
     * ip of the jboss server which is running binbase
     */
    server = "binbase application server as ip"

    /**
     * the used binbase voc db for this instance
     */
    database = "binbase database column"

    /**
     * the list of experiments we want to sync or the titles of it. We will try to find the id's of them in the metadata files. You can also specify true to sync all experiments or false to skip the synchornisation all together
     */
    syncExperiments = [

    ]

    /**
     * we only synchronize the metadata and nothing else
     */
    syncMetaDataOnly = false

    /**
     * where can we find our metadata
     */
    metaDataPath = "./metaData"

    /**
     * fetches the metadata from the given url
     */
    metaDataUrl = "http://127.0.0.1:8080/minix/communications/studieDataAsXML"

    /**
     * strict means we need to have the metadata file or the experiment won't be syncd
     */
    strict = true

    /**
     * title of this instance
     */
    title = "Please set your title for the webpage here"

    /**
     * the binbase header, useful to describe the exact database
     */
    logoDescription = "please set your header here, it's under the BinBase symbol"

    /**
     * name of the logo
     */
    logoName = "BinBase"

    /**
     * the welcome title on the index.gsp page
     */
    welcomeTitle = "Please set your welcome title here"
    /**
     * the welcome text on the index.gsp page
     */
    welcomeText = """
                    please set your welcome message here, you can use any valid html
    """

    /**
     * the msp file name
     */
    mspFileName = "The BinBase as MSP file"

    /**
     * are we allowed to attach msp files for downloads?
     */
    mspEnabled = true

    /**
     * pre cache bin similarities
     */
    similarityPreCaching = false

    /**
     * only synchronize bins which are found in the synchronized experiments
     */
    simplifyBins = true

    /**
     * are all data in this database public, if false, than we will use the minix service to find out if the data are public or not
     */
    allDataArePublic = false
}