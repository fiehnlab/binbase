package binbase.core

import grails.util.GrailsUtil
import org.codehaus.groovy.grails.commons.GrailsApplication
import org.codehaus.groovy.grails.commons.ApplicationHolder as AH
import org.codehaus.groovy.grails.commons.ConfigurationHolder
import org.springframework.context.ApplicationContextAware
import org.springframework.context.ApplicationContext
/**
 * historic class and really needs to be rewritten. It basically provides some config variables
 */
class BinBaseConfigReader implements ApplicationContextAware {

    public static JBOSS_ENVIRONMENT = "jboss"
    private static ConfigObject config

    private def context = null

    /**
     * initializes the object
     * @return
     */
    static ConfigObject initialize() {
        ConfigObject config = new ConfigSlurper().parse(new GroovyClassLoader(BinBaseConfigReader.class.getClassLoader()).loadClass('BinBaseConfig'))

        BinBaseConfigReader.config = config
        return config;
    }

    static String getServer() {
        return new BinBaseConfigReader().getInitializedServer()
    }
    /**
     * returns the server
     * @return
     */
    private String getInitializedServer() {
        if (config == null) {
            config = initialize()
        }

        if (GrailsUtil.getEnvironment().toLowerCase().equals(JBOSS_ENVIRONMENT)) {
            if (config?.binbase?.server != null) {
                return config.binbase.server.toString()
            }
            else {
                return "127.0.0.1"
            }
        }
        else {
            return config.binbase.server.toString()
        }
    }

    /**
     * returns the binbase key
     * @return
     */
    static String getKey() {
        return new BinBaseConfigReader().getInitializedKey()
    }

    private String getInitializedKey() {
        if (config == null) {
            config = initialize()
        }

        if (GrailsUtil.getEnvironment().toLowerCase().equals(JBOSS_ENVIRONMENT)) {

            //using provided key
            if (config?.binbase?.key != null) {

                return config.binbase.key.toString()
            }
            else {
                def ctx = AH.application.mainContext
                def keyStoreService = ctx.keyStoreService

                return keyStoreService.listKeys().get("binbase")
            }
        }

        else {
            return config.binbase.key.toString()
        }
    }



    void setApplicationContext(ApplicationContext applicationContext) {
        context = applicationContext
    }


}
