package core

import grails.test.*
import binbase.web.core.BBStatisticSnapshot
import core.SnapShotService.Grouping

class SnapShotServiceTests extends GrailsUnitTestCase {

    def dataSource

    Date begin = null

    Date end = null

    protected void setUp() {
        super.setUp()

        Calendar c = Calendar.getInstance()
        c.setTime(new Date())
        begin = c.getTime()
        for (int i = 0; i < 100; i++) {
            c.add(Calendar.HOUR, 1)
            BBStatisticSnapshot s = new BBStatisticSnapshot()
            s.objectName = "test"
            s.objectCount = i * i

            s.save(flush: true)
            s.createdAt = c.getTime()
            s.save(flush: true)

        }

        end = c.getTime()

        BBStatisticSnapshot.list().each {
            println it
        }
    }

    protected void tearDown() {
        super.tearDown()
        BBStatisticSnapshot.list().each {it.delete()}
    }

    void testQuerySnapShotsForRange() {

        SnapShotService service = new SnapShotService()
        service.dataSource = dataSource

        def result = service.querySnapShotsForRange(begin, end, "test", Grouping.HOURLY)

        println result

    }


    void testQuerySnapShotsForDay() {

        SnapShotService service = new SnapShotService()
        service.dataSource = dataSource

        def result = service.querySnapShotsForDay("test", new Date())

        println result

    }


}
