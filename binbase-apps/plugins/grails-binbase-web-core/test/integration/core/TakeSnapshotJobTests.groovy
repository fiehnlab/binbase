package core

import grails.test.*
import binbase.web.core.TakeSnapshotJob
import org.quartz.Scheduler

class TakeSnapshotJobTests extends GrailsUnitTestCase {

    def grailsApplication

    Scheduler quartzScheduler

    protected void setUp() {

    }

    protected void tearDown() {

        while(quartzScheduler.currentlyExecutingJobs.size() > 0){
            Thread.currentThread().wait(1000)
        }
    }

    void testScheduling() {

        grailsApplication.getDomainClasses().each {
            println "current: ${it.class.getName()}"

            TakeSnapshotJob take = new TakeSnapshotJob()
            take.grailsApplication = grailsApplication
            take.execute([mergedJobDataMap: [objectName: it.name]])
        }
    }

    void testSchedulingQuartz() {

        grailsApplication.getDomainClasses().each {
            println "current: ${it.class.getName()}"
            TakeSnapshotJob.triggerNow([objectName: it.name])
        }
    }

}
