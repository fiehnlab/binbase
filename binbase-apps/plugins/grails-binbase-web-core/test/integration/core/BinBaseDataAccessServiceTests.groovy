package core

import grails.test.*
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment

class BinBaseDataAccessServiceTests extends GrailsUnitTestCase {

  BinBaseService binbaseEjbService

  protected void setUp() {
    super.setUp()
  }

  protected void tearDown() {
    super.tearDown()
  }


  public void testGetExperiment() {

    BinBaseDataAccessService dataAccessService = new BinBaseDataAccessService()
    dataAccessService.binbaseEjbService = binbaseEjbService

    String database = "test"

    boolean found = false
    dataAccessService.getExperimentIds(database).each {
      found = true
      Experiment experiment = dataAccessService.getExperiment(it, database)
      assertTrue(experiment != null)
    }

    assert (found)

  }

  public void testGetExperimentIds() {
    BinBaseDataAccessService dataAccessService = new BinBaseDataAccessService()
    dataAccessService.binbaseEjbService = binbaseEjbService

    String database = "test"

    assert (dataAccessService.getExperimentIds(database).size() > 0)
  }
}
