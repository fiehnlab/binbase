grails.project.class.dir = "target/classes"
rails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
//grails.project.war.file = "target/${appName}-${appVersion}.war"
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {
        mavenLocal()

        grailsPlugins()
        grailsHome()
        grailsCentral()

        // uncomment the below to enable remote dependency resolution
        // from public Maven repositories
        mavenCentral()
        mavenRepo "http://snapshots.repository.codehaus.org"
        mavenRepo "http://repository.codehaus.org"
        mavenRepo "http://download.java.net/maven/2/"
        mavenRepo "http://repository.jboss.com/maven2/"
        mavenRepo "http://binbase-maven.googlecode.com/svn/trunk/repo"
    }


    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.

        //needed to connect to binbase

        runtime("jfree:jfreechart:1.0.12") {
        }

        compile("edu.ucdavis.genomics.metabolomics.binbase.binbase-core.binbase:binbase-spring:latest.integration") {
            excludes 'sl4j-api',
                    'hibernate.core',
                    'hibernate-commons-annotations',
                    'hibernate-entitymanager',
                    'log4j',
                    'logkit',
                    'avalon-framework',
                    'servlet-api',
                    'ant:ant',
                    'antlr:antlr',
                    'junit:junit',
                    'commons-logging',
                    'commons-collections',
                    'commons-net',
                    'jms',
                    'activation',
                    'poi',
                    'Jama',
                    'hibernate',
                    'asm',
                    'cglib',
                    'cglib-nodep',
                    'c3p0',
                    'spring-core',
                    'spring-beans',
                    'spring-context',
                    'spring-webmvc',
                    'spring',
                    'cglib-nodep',
                    'activation',
                    'xmlParserAPIs',
                    'xercesImpl',
                    'xdoclet',
                    'xdoclet-xdoclet-module',
                    'xdoclet-hibernate-module',
                    'uctask-xdoclet',
                    "xercesImpl",
                    "xmlParserAPIs",
                    "xml-apis",
                    'p6spy',
                    'groovy-all',
                    'ant',
                    'antlr',
                    'dbunit',
                    'junit',
                    'jep',
                    'org.hibernate',
                    'ehcache',
                    'commons-beanutils',
                    'ejb3-persistence',
                    'geronimo-spec',
                    'axis',
                    'axis-jaxrpc',
                    'jcommon',
                    'commons-discovery',
                    'groovy',
                    'xbean',
                    'wsdl4j',
                    'saaj-api',
                    'axis2',
                    'axis2-transport-http',
                    'axis2-kernel',
                    'axis2-transport-local',
                    'xmlbeans'
        }
        /*
        runtime('org.hibernate:hibernate-validator:3.1.0.GA') {
          excludes 'sl4j-api', 'hibernate.core', 'hibernate-commons-annotations', 'hibernate-entitymanager'
        }
        */
    }

    plugins {
        runtime(':jquery:1.7.1')
        runtime(':bluff:0.1.1')
        runtime(':executor:0.3')
        runtime('edu.ucdavis.genomics.metabolomics.binbase.minix.plugins:jboss-client:latest.integration'){
		
		excludes                    'commons-collections'
}
        runtime(':jquery-ui:1.8.15')
        runtime(':searchable:0.6.1')
    }
}
