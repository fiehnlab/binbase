package binbase.web.core

import org.codehaus.groovy.grails.commons.GrailsDomainClass
import groovy.sql.Sql

/**
 * is used to take a snapshot of a domain class model
 */
class TakeSnapshotJob {

    def concurrent = false

    def grailsApplication

    def execute(def context) {
        if (context.mergedJobDataMap.objectName != null) {

            grailsApplication.getDomainClasses().each {GrailsDomainClass current ->
                if (current.getName() == context.mergedJobDataMap.objectName) {
                    BBStatisticSnapshot snapshot = new BBStatisticSnapshot()

                    try {
                        if (current.getName() != snapshot.class.getName()) {

                            snapshot.createdAt = new Date()
                            snapshot.objectName = current.getName()

                            snapshot.objectCount = current.newInstance().count()
                            log.debug "new snapshot: ${snapshot}"
                            if (snapshot.validate()) {
                                snapshot.save(flush: true)
                            }
                            else {
                                log.debug(snapshot.errors)
                            }

                        }
                    }
                    catch (Exception e) {
                        log.debug(e.getMessage(), e)
                    }

                }
            }

        }
        else {
            log.debug("sorry job failed no object provided")
        }
    }
}
