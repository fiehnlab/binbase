package binbase.web.core

class BBDownloadController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    static scaffold = true
    def dataSource

    def index = {
        redirect(action: "list", params: params)
    }

    /**
     * downloads a result to the local harddrive
     */
    def download = {
        log.info "attempting to download the file: ${params.id}"
        BBDownload attachment = BBDownload.get(params.id)

        if (attachment == null) {
            render "sorry this file was not found!"
        }
        else {

            byte[] content = attachment.downloadContent()

            if (content == null) {
                render "sorry but there was no actuall content attached!"
            }
            else {
                response.setContentType("application/octet-stream")
                response.setHeader("Content-disposition", "attachment;filename=${attachment.fileName.replaceAll(' ', '_')}")


                response.outputStream << new ByteArrayInputStream(content)
                response.outputStream.flush()

                return null
            }
        }
    }
}
