package binbase.web.core

class BBSpeciesController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index = {
        redirect(action: "list", params: params)
    }
    def scaffold = true

    /**
     * displays the species and if provided limit it by organs
     */
    def show = {
        BBSpecies species = BBSpecies.get(params.id)

        BBOrgan organ = BBOrgan.get(params.organId)

        [BBSpeciesInstance: species, BBOrganInstance: organ]
    }
}
