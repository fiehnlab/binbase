package binbase.web.core

import core.SnapShotService
import core.SnapShotService.Grouping

class BBStatisticSnapshotController {

    static scaffold = true
    static allowedMethods = [save: "POST", update: "POST", delete: "POST", renderGraphForRange: "POST"]

    SnapShotService snapShotService

    def renderGraphForDayByHour = {

        java.util.LinkedHashMap labels
        java.util.ArrayList data

        (data, labels) = buildResult(snapShotService.querySnapShotsForDay(params.object, new Date()))


        render(template: "/graph/lineChart", model: [data: ["count for ${params.object}": data], labels: labels], plugin: "binbase-web-core")
    }

    private List buildResult(Map<Date, Long> result) {
        def labels = [:]
        def data = []

        result.keySet().eachWithIndex {def date, def index ->
            labels.put(index, date)
            data.add(result.get(date))
        }
        return [data, labels]
    }

    def renderGraphForWeekByDay = {

        java.util.LinkedHashMap labels
        java.util.ArrayList data

        (data, labels) = buildResult(snapShotService.querySnapShotsForWeek(params.object, new Date()))

        render(template: "/graph/lineChart", model: [data: ["count for ${params.object}": data], labels: labels], plugin: "binbase-web-core")

    }

    def renderGraphForMonthByWeek = {
        java.util.LinkedHashMap labels
        java.util.ArrayList data

        (data, labels) = buildResult(snapShotService.querySnapShotsForMonth(params.object, new Date(),Grouping.WEEKLY))

        render(template: "/graph/lineChart", model: [data: ["count for ${params.object}": data], labels: labels], plugin: "binbase-web-core")

    }

    def renderGraphForYearByMonth = {
        java.util.LinkedHashMap labels
        java.util.ArrayList data

        (data, labels) = buildResult(snapShotService.querySnapShotsForYear(params.object, new Date()))

        render(template: "/graph/lineChart", model: [data: ["count for ${params.object}": data], labels: labels], plugin: "binbase-web-core")

    }
}
