package binbase.web.core

class BBBinController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def statisticsService

    def mspService

    def index = {
        redirect(action: "list", params: params)
    }
    def scaffold = true

    def listBinsByAnnotationCountByExperiment = {
        def id = params.id
        def hitrate = params.hitrate

        def bins = statisticsService.findBinsForHitRateForExperiment(id, hitrate)

        [bins: bins]
    }

    /**
     * downloads a msp file
     */
    def downloadMspFile = {

        BBBin bin = BBBin.get(params.id)
        def mspFile = mspService.generateMspFile(bin)

        response.setContentType("application/octet-stream")
        response.setHeader("Content-disposition", "attachment;filename=${bin.name}.msp")

        response.outputStream << new ByteArrayInputStream(mspFile.toString().getBytes())
        response.outputStream.flush()

        return null

    }


}
