<div id="bins_by_organ" class="list">
    <table>
        <thead>
        <tr>
            <util:remoteSortableColumn class="ui-state-default" params="[organId:organId]" action="ajaxBinsForOrgan" property="binbaseBinId"
                                       update="bins_by_organ"
                                       title="BinBase Bin Id"/>
            <util:remoteSortableColumn class="ui-state-default" params="[organId:organId]" action="ajaxBinsForOrgan" property="name"
                                       update="bins_by_organ"
                                       title="Name"/>
            <util:remoteSortableColumn class="ui-state-default" params="[organId:organId]" action="ajaxBinsForOrgan" property="retentionIndex"
                                       update="bins_by_organ"
                                       title="Retention Index"/>
        </tr>
        </thead>
        <tbody>
        <g:each in="${bins}" var="bin" status="i">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                <td>
                    <g:link controller="BBBin" action="show"
                            id="${bin.id}">
                        ${bin.binbaseBinId}
                    </g:link>
                </td>
                <td>${bin.name}</td>
                <td>${bin.retentionIndex}</td>
            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="paginateButtons">
        <util:remotePaginate controller="query" action="ajaxBinsForOrgan" params="[organId:organId]" id="${organId}" total="${total}"
                             update="bins_by_organ" max="10"/>
    </div>
</div>