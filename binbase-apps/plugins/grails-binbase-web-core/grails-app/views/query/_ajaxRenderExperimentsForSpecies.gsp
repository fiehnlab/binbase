<div id="species_experiment" class="list">
    <table>
        <thead>
        <tr>

            <util:remoteSortableColumn class="ui-state-default"  params="[id:speciesId]" action="ajaxRenderExperimentsForSpecies" property="name"  update="species_experiment" title="${message(code: 'BBExperiment.name.label', default: 'Name')}"/>

            <util:remoteSortableColumn  class="ui-state-default"   params="[id:speciesId]" action="ajaxRenderExperimentsForSpecies" update="species_experiment"  property="title"
                    title="${message(code: 'BBExperiment.title.label', default: 'Title')}"/>

        </tr>
        </thead>
        <tbody>
        <g:each in="${BBExperimentInstanceList}" status="i" var="BBExperimentInstance">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                <td><g:link controller="BBExperiment" action="show"
                        id="${BBExperimentInstance.id}">${fieldValue(bean: BBExperimentInstance, field: "name")}</g:link></td>

                <td>${fieldValue(bean: BBExperimentInstance, field: "title")}</td>

            </tr>
        </g:each>
        </tbody>
    </table>
    <div class="paginateButtons">

        <util:remotePaginate controller="query" action="ajaxRenderExperimentsForSpecies" id="${speciesId}" total="${total}"
                update="species_experiment" max="10"/>

    </div>
</div>
