<div id="bins_by_species" class="list">
    <table>
        <thead>
        <tr>
            <util:remoteSortableColumn class="ui-state-default"  params="[speciesId:speciesId]" action="ajaxBinsForSpecies" property="binbaseBinId"
                                       update="bins_by_species"
                                       title="BinBase Bin Id"/>
            <util:remoteSortableColumn class="ui-state-default"  params="[speciesId:speciesId]" action="ajaxBinsForSpecies" property="name"
                                       update="bins_by_species"
                                       title="Name"/>
            <util:remoteSortableColumn class="ui-state-default"  params="[speciesId:speciesId]" action="ajaxBinsForSpecies" property="retentionIndex"
                                       update="bins_by_species"
                                       title="Retention Index"/>
        </tr>
        </thead>
        <tbody>
        <g:each in="${bins}" var="bin" status="i">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                <td>
                    <g:link controller="BBBin" action="show"
                            id="${bin.id}">
                        ${bin.binbaseBinId}
                    </g:link>
                </td>
                <td>${bin.name}</td>
                <td>${bin.retentionIndex}</td>
            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="paginateButtons">
        <util:remotePaginate controller="query" action="ajaxBinsForSpecies" params="[speciesId:speciesId]" id="${speciesId}" total="${total}"
                             update="bins_by_species" max="10"/>
    </div>
</div>