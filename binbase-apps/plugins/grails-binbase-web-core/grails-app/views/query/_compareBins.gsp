<%@ page import="edu.ucdavis.genomics.metabolomics.binbase.algorythm.validate.ValidateSpectra; edu.ucdavis.genomics.metabolomics.binbase.bdi.util.type.converter.SpectraConverter" %><g:javascript>
    jQuery(document).ready(function() {
        $('.bin_tab').tabs();
    });

</g:javascript>
<div class="dialog">
<table>
<g:each in="${binSimilarityInstances}" var="binSimilarity" status="i">

<g:if test="${i == 0}">
    <g:set var="BBBinInstance" value="${binSimilarity.from}"/>
    <!-- original bin -->
    <tr class="bin_original">

        <td>
            <h2>All bins are compared to
            <g:formatNumber
                    number="${BBBinInstance.binbaseBinId}"
                    format="########0"/>
            </h2>
            <table>
                <tbody>

                <tr class="prop">
                    <td valign="top" class="name"><g:message code="BBBin.binbaseBinId.label"
                                                             default="Binbase Bin Id"/></td>

                    <td valign="top" class="value"><g:link controller="BBBin" action="show"
                                                           id="${BBBinInstance.id}"><g:formatNumber
                                number="${BBBinInstance.binbaseBinId}"
                                format="########0"/></g:link></td>

                    <td rowspan="5">
                        <binbase:renderBin height="220" widh="400" id="${BBBinInstance.id}"/>
                    </td>
                </tr>

                <tr class="prop">
                    <td valign="top" class="name"><g:message code="BBBin.name.label" default="Name"/></td>

                    <td valign="top" class="value">${fieldValue(bean: BBBinInstance, field: "name")}</td>

                </tr>

                <tr class="prop">
                    <td valign="top" class="name"><g:message code="BBBin.quantMass.label"
                                                             default="Quant Mass"/></td>

                    <td valign="top"
                        class="value">${fieldValue(bean: BBBinInstance, field: "quantMass")}</td>

                </tr>


                <tr class="prop">
                    <td valign="top" class="name"><g:message code="BBBin.uniqueMass.label"
                                                             default="Unique Mass"/></td>

                    <td valign="top"
                        class="value">${fieldValue(bean: BBBinInstance, field: "uniqueMass")}</td>

                </tr>

                <tr class="prop">
                    <td valign="top" class="name"><g:message code="BBBin.retentionIndex.label"
                                                             default="Retention Index"/></td>

                    <td valign="top" class="value"><g:formatNumber number="${BBBinInstance.retentionIndex}"
                                                                   format="########0"/></td>

                </tr>

                </tbody>
            </table>
        </td>
    </tr>
</g:if>

<!-- comparisons to the original bin -->
<g:set var="BBBinInstance" value="${binSimilarity.to}"/>

<tr class="bin_compare">

    <td>
        <h2>
            Bin <g:formatNumber
                    number="${BBBinInstance.binbaseBinId}"
                    format="########0"/>
        </h2>

        <div class="bin_tab">

            <ul>
                <li><a href="#tab-properties-${i}">Bin <g:formatNumber
                        number="${BBBinInstance.binbaseBinId}"
                        format="########0"/> Properties</a></li>
                <li><a href="#tab-apex-${i}">Bin <g:formatNumber
                        number="${BBBinInstance.binbaseBinId}"
                        format="########0"/> Apexing Masses</a></li>
                <li><a href="#tab-ion-${i}">Bin <g:formatNumber
                        number="${BBBinInstance.binbaseBinId}"
                        format="########0"/> Ion table</a></li>

            </ul>

            <div id="tab-properties-${i}">
                <table>
                    <tbody>

                    <tr class="prop">
                        <td valign="top" class="name"><g:message code="BBBin.binbaseBinId.label"
                                                                 default="Binbase Bin Id"/></td>

                        <td valign="top" class="value"><g:link controller="BBBin" action="show"
                                                               id="${BBBinInstance.id}"><g:formatNumber
                                    number="${BBBinInstance.binbaseBinId}"
                                    format="########0"/></g:link></td>

                        <td rowspan="5">
                            <binbase:renderBin height="220" widh="400" id="${BBBinInstance.id}"/>
                        </td>
                    </tr>

                    <tr class="prop">
                        <td valign="top" class="name"><g:message code="BBBin.name.label"
                                                                 default="Name"/></td>

                        <td valign="top"
                            class="value">${fieldValue(bean: BBBinInstance, field: "name")}</td>

                    </tr>

                    <tr class="prop">
                        <td valign="top" class="name"><g:message code="BBBin.quantMass.label"
                                                                 default="Quant Mass"/></td>

                        <td valign="top"
                            class="value">${fieldValue(bean: BBBinInstance, field: "quantMass")}</td>

                    </tr>


                    <tr class="prop">
                        <td valign="top" class="name"><g:message code="BBBin.uniqueMass.label"
                                                                 default="Unique Mass"/></td>

                        <td valign="top"
                            class="value">${fieldValue(bean: BBBinInstance, field: "uniqueMass")}</td>

                    </tr>

                    <tr class="prop">
                        <td valign="top" class="name"><g:message code="BBBin.retentionIndex.label"
                                                                 default="Retention Index"/></td>

                        <td valign="top" class="value"><g:formatNumber
                                number="${BBBinInstance.retentionIndex}"
                                format="########0"/></td>

                    </tr>

                    <tr class="prop">
                        <td valign="top" class="name"><g:message code="BBBin.retentionIndexDistance.label"
                                                                 default="Retention Index Distance from "/>${binSimilarity.from.binbaseBinId}</td>

                        <td valign="top" class="value"><g:formatNumber
                                number="${binSimilarity.from.retentionIndex - BBBinInstance.retentionIndex}"
                                format="########0"/></td>

                    </tr>


                    <tr class="prop">
                        <td valign="top" class="name"><g:message code="BBBin.similarity.label"
                                                                 default="Similarity to "/>${binSimilarity.from.binbaseBinId}</td>

                        <td valign="top" class="value"><g:formatNumber
                                number="${binSimilarity.similarity}"
                                format="########0"/></td>

                    </tr>
                    </tbody>
                </table>
            </div>

            <div id="tab-apex-${i}">
                <h2>Apexing Masses</h2>

                <%
                    def spectra = ValidateSpectra.convert(BBBinInstance.spectraString)
                    def spectraFrom = ValidateSpectra.convert(binSimilarity.from.spectraString)

                    def apex = "${BBBinInstance.apexMasses.toString()}"
                %>
                <table>
                    <thead>
                    <th class="ui-state-default" >Mass</th>
                    <th class="ui-state-default" >Relative Intensity of ${BBBinInstance.binbaseBinId}</th>
                    <th class="ui-state-default" >Relative Intensity of ${binSimilarity.from.binbaseBinId}</th>

                    </thead>
                    <tbody>

                    <g:each in="${apex.split('\\+').collect {Integer.parseInt(it)}.sort()}">
                        <tr>
                            <td>${it}</td>
                            <td><g:formatNumber
                                    number="${spectra[it-1][ValidateSpectra.FRAGMENT_REL_POSITION]}"
                                    format="########0.00"/></td>
                            <td><g:formatNumber
                                    number="${spectraFrom[it-1][ValidateSpectra.FRAGMENT_REL_POSITION]}"
                                    format="########0.00"/></td>

                        </tr>
                    </g:each>
                    </tbody>

                </table>

            </div>

            <div id="tab-ion-${i}">
                <h2>Ion Table</h2>

                <%
                    spectra = ValidateSpectra.convert(BBBinInstance.spectraString)
                    spectraFrom = ValidateSpectra.convert(binSimilarity.from.spectraString)

                %>
                <table>
                    <thead>
                    <th class="ui-state-default" >Mass</th>
                    <th class="ui-state-default" >Relative Intensity of ${BBBinInstance.binbaseBinId}</th>
                    <th class="ui-state-default" >Relative Intensity of ${binSimilarity.from.binbaseBinId}</th>

                    </thead>
                    <tbody>

                    <g:each in="${spectra}" status="currentIon" var="current">
                        <g:if test="${(spectra[currentIon][ValidateSpectra.FRAGMENT_REL_POSITION] < 0.05 && spectraFrom[currentIon][ValidateSpectra.FRAGMENT_REL_POSITION] < 0.05) == false}">
                            <tr>
                                <td>${spectra[currentIon][ValidateSpectra.FRAGMENT_ION_POSITION]}</td>
                                <td><g:formatNumber
                                        number="${spectra[currentIon][ValidateSpectra.FRAGMENT_REL_POSITION]}"
                                        format="########0.00"/></td>

                                <td><g:formatNumber
                                        number="${spectraFrom[currentIon][ValidateSpectra.FRAGMENT_REL_POSITION]}"
                                        format="########0.00"/></td>

                            </tr>
                        </g:if>
                    </g:each>
                    </tbody>

                </table>

            </div>
        </div>
    </td>
</tr>
</g:each>
</table>
</div>
