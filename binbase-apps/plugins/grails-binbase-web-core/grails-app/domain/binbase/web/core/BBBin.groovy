package binbase.web.core

/**
 * a unique spectra
 */
class BBBin {

    static searchable = [only: ['name', 'binbaseBinId']]

    static belongsTo = [BBDatabase]
    static hasMany = [spectra: BBSpectra]

    static constraints = {

    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
        version false
        spectraString type: 'text'
        apexMasses type: 'text'
        spectra cascade: "all-delete-orphan"

    }

    /**
     * related samples
     */
    Set<BBSpectra> spectra

    /**
     * the unique name of the bin
     */
    String name

    /**
     * owning database
     */
    BBDatabase database

    /**
     * the binbase bin id
     */
    int binbaseBinId

    /**
     * retention index
     */
    double retentionIndex

    /**
     * the unique mass
     */
    int uniqueMass

    /**
     * quant mass
     */
    int quantMass

    /**
     * the string of the spectra
     */
    String spectraString

    /**
     * apexmasses
     */
    String apexMasses

    boolean equals(Object o) {
        if (o instanceof BBBin) {

            if (o.id != null && this.id != null) {
                return this.id.equals(o.id)
            }
        }
        return false
    }
}
