package binbase.web.core

/**
 * a definition of a spectra
 */
class BBSpectra implements Comparable {

    static searchable = [only: ['bin', 'sample']]

    static belongsTo = [sample: BBExperimentSample, bin: BBBin]

    static constraints = {
        sample(nullable: true)
        bin(nullable: true)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
        version false
        spectraString type: 'text'
        apexMasses type: 'text'


    }

    /**
     * owning sample
     */
    BBExperimentSample sample

    /**
     * related bin
     */
    BBBin bin

    /**
     * retention index
     */
    double retentionIndex

    /**
     * retention time
     */
    double retentionTime

    /**
     * the unique mass
     */
    int uniqueMass

    /**
     * the base peak
     */
    int basePeak

    /**
     * intensity of this sprectra
     */
    double intensity

    /**
     * the string of the spectra
     */
    String spectraString

    /**
     * the apexing masses of this spectra
     */
    String apexMasses

    /**
     * similarity to the bin
     */
    double similarity

    /**
     * signal noise
     */
    double sn

    /**
     * leco version
     */
    String leco

    int compareTo(Object t) {

        return this.retentionIndex.compareTo(t.retentionIndex)
    }

}
