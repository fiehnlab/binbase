package binbase.web.core

/**
 * a stored download for easy access
 */
class BBDownload implements Comparable {

    static constraints = {
        description(nullable: true)
        uploadDate(nullable: true)
    }

    static searchable = {
        only: ['description', 'fileName']
    }

    static mapping = {
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
        version false
        tablePerHierarchy false
    }

    String fileName

    /**
     * description of the result data
     */
    String description = "no description provided!"

    Date uploadDate = new Date()

    int compareTo(Object t) {

        if (t instanceof BBDownload) {
            BBDownload r = t

            return r.fileName.compareTo(fileName)
        }
        return toString().compareTo(t.toString())
    }


    boolean existingContent() {
        if (this.validate()) {
            this.save(flush: true)
        }
        return BBContent.findByResult(this) != null
    }

    byte[] downloadContent() {
        if (existingContent()) {
            return BBContent.findByResult(this).content
        }
        return null
    }

    void uploadContent(byte[] data) {
        if (existingContent()) {
            BBContent.findByResult(this).delete(flush: true)
        }

        BBContent content = new BBContent()
        content.setResult(this)
        content.setContent(data)
        content.save(flush: true)
    }
}
