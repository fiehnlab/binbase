package binbase.web.core

/**
 * used to calculate the similarity to each other
 */
class BBBinSimilarity implements Comparable {

    static belongsTo = [from:BBBin,to:BBBin]

    static searchable = false

    static mapping = {
        batchSize 1000
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
        version false
    }
    static constraints = {
    }

    /**
     * the used library spectra
     */
    BBBin from

    /**
     * the used unkwnown spectra
     */
    BBBin to

    /**
     * the calculated similarity between these too
     */
    double similarity

    /**
     * just return the similarity
     * @return
     */
    String toString() {
        return similarity
    }

    /**
     * compare method
     * @param t
     * @return
     */
    int compareTo(Object t) {
        return this.toString().compareTo(t.toString())
    }
}
