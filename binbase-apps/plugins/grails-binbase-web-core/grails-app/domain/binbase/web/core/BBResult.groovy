package binbase.web.core

/**
 * attached results
 */
class BBResult extends BBDownload {

    static belongsTo = [experiment: BBExperiment]

    static mapping = {
        version false
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
        tablePerHierarchy false
    }

    /**
     * associated experiment
     */
    BBExperiment experiment
}
