package binbase.web.core

class BBMetadata {

    static belongsTo = [BBExperimentClass]

    static constraints = {
        value(nullable: false)

        name(nullable: false)
    }

    static  mapping = {
        version false
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']

    }

    static searchable = [only: ['name', 'value']]

    /**
     * name of a meta data object
     */
    String name

    /**
     * value of a meta database
     */
    String value
}
