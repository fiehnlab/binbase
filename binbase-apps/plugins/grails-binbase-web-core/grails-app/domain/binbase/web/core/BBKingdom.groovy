package binbase.web.core

class BBKingdom  implements Serializable {

    static auditable = true

    static searchable = [only: ['name']]

    static hasMany = [species: BBSpecies]

    static constraints = {
        name(unique: true, blank: false)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'BINBASE_CORE_ID']
        version false
        tablePerHierarchy false
    }

    /**
     * associated species
     */
    Set<BBSpecies> species

    /**
     * name of the kingdom
     */
    String name

    String toString() {
        return name
    }
}
