package core

import binbase.web.core.BBDatabase
import binbase.web.core.BBExperiment
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ExportJMXFacade
import binbase.web.core.BBResult
import java.util.zip.ZipOutputStream
import binbase.web.core.BBExperimentClass
import binbase.web.core.BBExperimentSample
import java.util.zip.ZipEntry
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.ServiceJMXFacade

/**
 * is used to attach result data to the internal dataset
 */
class BinBaseResultService {

    ExportJMXFacade binbaseExportConfiguration

    ServiceJMXFacade binbaseServiceConfiguration

    def dslGenerationService

    static transactional = true

    /**
     * attaches the dsl file for the given setupx id
     * @param database
     * @param setupXid
     * @return
     */
    boolean synchronizeDSLFiles(String database, String setupXid) {

        try {
            BBDatabase db = BBDatabase.findByName(database)

            if (db == null) {
                log.warn "you need to synchronize the given database ${database} first!"
                return false
            }

            BBExperiment exp = BBExperiment.findByDatabaseAndName(db, setupXid)

            if (exp == null) {
                log.warn "you need to synchronize the given experiment first!"
                return false
            }

            def value = dslGenerationService.generateForExperiment(database, exp.id)

            if (value != false) {
                BBResult re = BBResult.findByExperimentAndFileName(exp, "${setupXid}.dsl")

                if (re == null) {
                    re = new BBResult()
                }
                re.uploadDate = new Date()
                re.fileName = "${setupXid}.dsl"
                re.description = "binbase calculation script for this experiment"

                re.save(flush: true)
                exp.addToResults(re)
                exp.save(flush: true)
                re.uploadContent(value.toString().bytes)

            }

            exp.save(flush: true)

            return true
        }
        catch (Exception e) {
            log.error(e.getMessage(), e)
            return false
        }
    }

    /**
     * attaches a zipfile of all the txt files this experiment is based on
     * @param database
     * @param setupXid
     * @return
     */
    boolean synchronzieTxtFiles(String database, String setupXid) {

        try {
            BBDatabase db = BBDatabase.findByName(database)

            if (db == null) {
                log.warn "you need to synchronize the given database ${database} first!"
                return false
            }

            BBExperiment exp = BBExperiment.findByDatabaseAndName(db, setupXid)

            if (exp == null) {
                log.warn "you need to synchronize the given experiment first!"
                return false
            }

            ByteArrayOutputStream out = new ByteArrayOutputStream()
            ZipOutputStream zipFile = new ZipOutputStream(out)

            zipFile.setComment("all txt files for experiment ${setupXid}")


            exp.classes.each {BBExperimentClass c ->
                c.samples.each {BBExperimentSample s ->

                    if (binbaseServiceConfiguration.sampleExist(s.fileName)) {
                        byte[] content = binbaseServiceConfiguration.downloadFile(s.fileName)

                        zipFile.putNextEntry(new ZipEntry("${s.fileName}.txt"))
                        zipFile.write(content)
                        zipFile.closeEntry()

                    }
                    else {
                        log.info("sorry sample: ${s.fileName} was missing on server => skipped")
                    }
                }

            }

            zipFile.close()

            BBResult re = BBResult.findByExperimentAndFileName(exp, "${setupXid}-data.zip")

            if (re == null) {
                re = new BBResult()
            }
            re.uploadDate = new Date()

            re.fileName = "${setupXid}-data.zip"
            re.description = "data files for this experiment, required for calculation"

            re.save(flush: true)
            exp.addToResults(re)
            exp.save(flush: true)
            re.uploadContent(out.toByteArray())

            return true
        }
        catch (Exception e) {
            log.error(e.getMessage(), e)
            return false
        }
    }

    /**
     * attaches the BinBase result for the given setupxId
     * @param database
     * @param setupXid
     * @return
     */
    boolean synchronizeResult(String database, String setupXid) {

        try {
            BBDatabase db = BBDatabase.findByName(database)

            if (db == null) {
                log.warn "you need to synchronize the given database ${database} first!"
                return false
            }

            BBExperiment exp = BBExperiment.findByDatabaseAndName(db, setupXid)

            if (exp == null) {
                log.warn "you need to synchronize the given experiment first!"
                return false
            }


            byte[] result = binbaseExportConfiguration.getResult("${setupXid}.zip")

            if (result == null) {
                log.info "no result found for ${setupXid}"

                return false
            }

            //attach actual result

            BBResult res = BBResult.findByExperimentAndFileName(exp, "${setupXid}.zip")

            if (res == null) {
                res = new BBResult()
            }

            res.uploadDate = new Date()
            res.fileName = "${setupXid}.zip"
            res.description = "binbase caclulated result as zipfile"

            res.save(flush: true)
            exp.addToResults(res)

            exp.save(flush: true)
            res.uploadContent(result)
            return true
        }
        catch (Exception e) {
            log.error(e.getMessage(), e)
            return false
        }
    }
}
