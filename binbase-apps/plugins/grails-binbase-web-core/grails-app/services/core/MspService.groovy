package core

import binbase.web.core.BBBin
import edu.ucdavis.genomics.metabolomics.binbase.bdi.util.msp.BinToMspConverter
import binbase.web.core.BBDatabase
import edu.ucdavis.genomics.metabolomics.exception.BinBaseException
import binbase.web.core.BBDownload

class MspService {

    static transactional = true

    /**
     * generates an msp file for the given bin
     * @param bin
     * @return
     */
    def generateMspFile(BBBin bin) {
        return BinToMspConverter.convert(bin.name, bin.spectraString)
    }

    /**
     * generates a msp file for all bins in the database
     * @param database
     * @return
     */
    def generateMspFileForDatabase(String database) {
        BBDatabase db = BBDatabase.findByName(database)

        if (db == null) {
            throw new BinBaseException("sorry the database was not found: ${database}")
        }

        StringBuffer buffer = new StringBuffer()

        BBBin.findAllByDatabase(db).each {BBBin bin ->

            buffer.append(generateMspFile(bin))
            buffer.append("\n")

        }

        return buffer.toString()
    }

    /**
     *
     * @param database
     * @return
     */
    def attachMspFileForDatabase(String database, def title = "${database}", def description = "a msp file over all Bin's in this database") {

        BBDownload.withTransaction {
            def file = generateMspFileForDatabase(database)

            log.info "attaching file: ${title}"
            BBDownload download = BBDownload.findByFileName("${title}.msp")

            if (download == null) {
                download = new BBDownload()
            }
            else{
                log.info "file was already attached, so just updating"
            }

            download.uploadDate = new Date()
            download.fileName = "${title}.msp"
            download.description = description

            if(download.validate() == false){
                log.info "${download.errors}"
            }
            download.save(flush: true)

            download.uploadContent(file.getBytes())

            log.info "done..."

            return true
        }
    }

}
