package core

import binbase.web.core.BBExperiment
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass
import binbase.web.core.BBExperimentClass
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample
import binbase.web.core.BBExperimentSample
import edu.ucdavis.genomics.metabolomics.binbase.dsl.generate.GenerateDSLFromExperiment
import binbase.core.BinBaseConfigReader

class DslGenerationService {

    static transactional = true

    /**
     * generates the calculation dsl for the given id and database
     * @param id
     * @return
     */
    def generateForExperiment(String database, long id) {

        BBExperiment exp = BBExperiment.get(id)

        if (exp == null) {
            log.warn "no experiment with id: ${id} found"
            return false
        }

        Experiment exp2 = new Experiment()
        exp2.column = database
        exp2.id = exp.name

        exp2.classes = new ExperimentClass[exp.classes.size()]


        exp.classes.eachWithIndex {BBExperimentClass c, int i ->
            exp2.classes[i] = new ExperimentClass()
            exp2.classes[i].column = database
            exp2.classes[i].id = c.name

            exp2.classes[i].samples = new ExperimentSample[c.samples.size()]

            c.samples.eachWithIndex {BBExperimentSample s, int x ->
                exp2.classes[i].samples[x] = new ExperimentSample()

                exp2.classes[i].samples[x].name = s.fileName
                exp2.classes[i].samples[x].id = s.fileName

            }

        }

        return new GenerateDSLFromExperiment().generateDSL(exp2, BinBaseConfigReader.getServer())

    }
}
