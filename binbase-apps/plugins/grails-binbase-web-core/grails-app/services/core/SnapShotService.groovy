package core

import groovy.sql.Sql

import static java.util.Calendar.*
import java.text.DateFormat
/**
 * used to query snapshot data
 */
class SnapShotService {

    static transactional = true

    def dataSource

    /**
     * returns snapshot's for a given range
     * @param from
     * @param to
     * @param object
     * @return
     */
    Map<Date, Long> querySnapShotsForRange(Date from, Date to, String object, Grouping group) {

        Sql sql = Sql.newInstance(dataSource)

        def result = [:]

        def params = []

        def query = ""

        params[0] = object
        params[1] = new java.sql.Date(from.getTime())
        params[2] = new java.sql.Date(to.getTime())

        //used to build our parameter object depending on the grouping
        switch (group) {
            case Grouping.HOURLY:
                query = "select max(object_count) as object,date_trunc('hour',created_at) as date from bbstatistic_snapshot where object_name = ? and created_at between ? and ? group by date_trunc('hour',created_at) order by date_trunc('hour',created_at)"
                break
            case Grouping.DAILY:
                query = "select max(object_count) as object,date_trunc('day',created_at) as date from bbstatistic_snapshot where object_name = ? and created_at between ? and ? group by date_trunc('day',created_at) order by date_trunc('day',created_at)"
                break
            case Grouping.WEEKLY:
                query = "select max(object_count) as object,date_trunc('week',created_at) as date from bbstatistic_snapshot where object_name = ? and created_at between ? and ? group by date_trunc('week',created_at) order by date_trunc('week',created_at)"
                break
            case Grouping.MONTHLY:
                query = "select max(object_count) as object,date_trunc('month',created_at) as date from bbstatistic_snapshot where object_name = ? and created_at between ? and ? group by date_trunc('month',created_at) order by date_trunc('month',created_at)"
                break
            case Grouping.YEARLY:
                query = "select max(object_count) as object,date_trunc('year',created_at) as date from bbstatistic_snapshot where object_name = ? and created_at between ? and ? group by date_trunc('year',created_at) order by date_trunc('year',created_at)"
                break
            default:
                query = "select max(object_count) as object,created_at as date from bbstatistic_snapshot where object_name = ? and created_at between ? and ? group by created_at"
                break;
        }

        Calendar c = Calendar.getInstance()

        sql.eachRow(query, params) {
            Date date = new Date(((java.sql.Timestamp) it.date).getTime())
            c.setTime(new Date(((java.sql.Timestamp) it.date).getTime()))

            switch (group) {
                case Grouping.HOURLY:
                    result.put(date.format("h a"), it.object)
                    break
                case Grouping.DAILY:
                    result.put(date.format("EEE"), it.object)
                    break
                case Grouping.WEEKLY:
                    result.put(date.format("W"), it.object)
                    break
                case Grouping.MONTHLY:
                    result.put(date.format("MMM"), it.object)
                    break
                case Grouping.YEARLY:
                    result.put(date.format("yyyy"), it.object)
                    break
                default:
                    result.put(date, it.object)
                    break;
            }
        }

        return result
    }

    /**
     * returns the snapshot of this current day
     * @param object
     * @param day
     * @return
     */
    Map<Date, Long> querySnapShotsForDay(String object, Date day) {

        Calendar calendar = Calendar.getInstance()
        calendar.setTime(day)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.HOUR, 0)

        Date from = calendar.getTime()

        calendar.set(Calendar.HOUR_OF_DAY, 23)
        calendar.set(Calendar.MINUTE, 59)
        calendar.set(Calendar.SECOND, 59)
        calendar.set(Calendar.HOUR, 23)
        Date to = calendar.getTime()

        return querySnapShotsForRange(from, to, object, Grouping.HOURLY)

    }

    /**
     * returns the distribution fr the last 24h
     * @param object
     * @param day
     * @return
     */
    Map<Date, Long> querySnapShotsFor24h(String object, Date day) {

        Date to = day
        Date from = day.minus(1)

        return querySnapShotsForRange(from, to, object, Grouping.HOURLY)

    }

    /**
     * snapshot distribution for the last week
     * @param object
     * @param day
     * @return
     */
    Map<Date, Long> querySnapShotsForWeek(String object, Date day) {

        Date to = day
        Date from = day.minus(7)

        return querySnapShotsForRange(from, to, object, Grouping.DAILY)

    }

    /**
     * snapshot distribution for the last month
     * @param object
     * @param day
     * @param grouping
     * @return
     */
    Map<Date, Long> querySnapShotsForMonth(String object, Date day, Grouping grouping = Grouping.DAILY) {

        Date to = day
        Date from = day.minus(31)

        return querySnapShotsForRange(from, to, object, grouping)

    }

    /**
     * snapshot distribution for the year
     * @param object
     * @param day
     * @param grouping
     * @return
     */
    Map<Date, Long> querySnapShotsForYear(String object, Date day, Grouping grouping = Grouping.MONTHLY) {
        Date to = day
        Date from = day.minus(365)

        return querySnapShotsForRange(from, to, object, grouping)
    }

    /**
     * how can the data be grouped
     */
    enum Grouping {
        HOURLY, DAILY, WEEKLY, MONTHLY, YEARLY
    }

}
