package core

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment
import edu.ucdavis.genomics.metabolomics.binbase.bci.ejb.BinBaseService
import binbase.core.BinBaseConfigReader

/**
 * provides you with access to the binbase data structures
 */
class BinBaseDataAccessService {

  static transactional = false

  BinBaseService binbaseEjbService

  /**
   * retrieves an experiment from the database
   * @param id
   * @param database
   * @return
   */
  Experiment getExperiment(String id, String database) {
    return binbaseEjbService.getExperiment(database, id, BinBaseConfigReader.key)
  }

  /**
   * retrieve all stored experiment ids
   * @param database
   * @return
   */
  Collection<String> getExperimentIds(String database) {
    def result = new HashSet()
    binbaseEjbService.getAllExperimentIds(database, BinBaseConfigReader.key).each {String id -> result.add(id)}

    return result
  }

}
