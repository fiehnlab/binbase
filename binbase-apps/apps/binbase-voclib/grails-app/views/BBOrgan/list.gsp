<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

    <g:setProvider library="jquery"/>

    <bluff:resources/>

</head>

<body>
<div class="article">
    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>

    <div class="left">

        <h2 class="star">Organ Browser</h2>

        <!-- description of this element -->
        <div class="help-message ui-state-highlight ui-corner-all" style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                <g:message code="organ.list"/>
            </p>
        </div>

        <div class="list">
            <table class="hover_table" id="organs">
                <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <div class="right">

        <h2 class="star">Sample Distribution</h2>

        <div class="help-message ui-state-highlight ui-corner-all" style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                <g:message code="organ.graph.sampleDistribution"/>
            </p>
        </div>

        <binbase:pieSampleByOrganDistribution/>

    </div>
</div>


<g:javascript>
             $(function() {


        $(document).ready(function() {

            $('#organs').dataTable({
            bProcessing: true,
          bServerSide: true,
          sAjaxSource: '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxListOrgansAsJSON", plugin: "binbase-web-gui")}' ,
          bJQueryUI: true,
"bAutoWidth" : false,
"bPaginate": true,
		"bInfo": true,
		"bLengthChange": false,
	 "aoColumns" : [
	 	{ sWidth : "25%" },
	 	{ sWidth : "75%" }
 			 ],


          "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		"bFilter": false
    ,

                //add export button
                "fnDrawCallback": function()
                {
                    $("#organs_info").prepend("<input type='button' id='export_data_organs' value='' class='xls_export'/>");
                }
            });

        $('#export_data_organs').live('click', function() {
                var search = $('#organs_filter input').val();
                window.location = '${createLink(controller: "BBOrganAndSpeciesQuery", action: "ajaxListOrgansAsJSON", plugin: "binbase-web-gui")}'+'?format=Excel&sSearch=' + search
          } );
        });
    });
</g:javascript>

<!-- used to load the related bin on the click on a row -->
<g:javascript>
    $(function() {

        $(document).ready(function() {

            $('#organs tbody tr').live('click', function() {
                var nTds = $('td', this);
                var textId = $(nTds[0]).text();

                window.location = '${createLink(controller: "BBOrgan", action: "show")}'+'/'+textId;
            });


        });
    });
</g:javascript>
</body>
</html>
