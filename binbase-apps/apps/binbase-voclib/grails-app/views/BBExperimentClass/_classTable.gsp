<g:setProvider library="jquery"/>

<div class="article">

    <h2 class="star">Class Details: ${c.name}</h2>
    <g:render template="/BBExperimentClass/details" model="[c:c]"/>

    <g:render template="/BBExperimentClass/samples" model="[c:c]"/>
</div>