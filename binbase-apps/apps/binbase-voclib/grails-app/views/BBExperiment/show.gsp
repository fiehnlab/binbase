<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>

<div class="article">

    <div style="width:100%">
        <h2 class="star">Experiment Details</h2>

        <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
        </g:if>

        <div class="help-message ui-state-highlight ui-corner-all"
             style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
            <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                <g:message code="experiment.details"/>
            </p>
        </div>

        <div class="dialog" style="height:190px float:left">

            <table>
                <tbody>

                <tr class="prop">
                    <td valign="top" class="name" style="width:120px">
                        <g:message code="BBExperiment.name.label" default="Name"/>
                    </td>

                    <td valign="top" class="value" style="width:120px">
                        ${fieldValue(bean: BBExperimentInstance, field: "name")}
                    </td>
                    <td rowspan="2" style="width:120px">
                        <div style="margin-right:130px">
                            <img style="height:100%" src="${resource(dir: 'images', file: 'Conicalflasks.jpg')}"
                                 alt=""/>
                        </div>
                    </td>
                </tr>

                <tr class="prop">
                    <td valign="top" class="name">
                        <g:message code="BBExperiment.title.label" default="Title"/>
                    </td>

                    <td valign="top" class="value">${fieldValue(bean: BBExperimentInstance, field: "title")}</td>
                </tr>

                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="article">

    <div class="height450px">
        <g:javascript>
            jQuery(document).ready(function() {
                $('#experimentArcodion').accordion({collapsible:true,active:false});

            });

        </g:javascript>

        <div id="experimentArcodion" class="height450px">

            <h2><a href="#">Annotation Statistics</a></h2>

            <div class="height320px">

                <div class="help-message ui-state-highlight ui-corner-all"
                     style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                        <g:message code="experiment.annotation.statistic"/>
                    </p>
                </div>

                <div style="float:left;width: 100%">
                    <h2>Annotation Count by percentage</h2>

                    <div class="help-message ui-state-highlight ui-corner-all"
                         style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
                        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                            <g:message code="experiment.annotation.graph"/>
                        </p>
                    </div>

                    <div class="horizontal-spacer"></div>

                    <div style="float:left;">
                        <div style="width: 80%;margin-left: 10%;margin-right: 10%;padding: 0 0 0 0">
                            <binbase:binHitRateDistributionByExperiment experimentId="${BBExperimentInstance.id}"/>
                        </div>
                    </div>

                    <div style="float:left;width:auto">
                        <p><strong><p>Explanation:</p></strong>

                            This graph shows the annotation counts for different percentage windows. For example a
                            number N in a window of 90-100% means that there where N annotations, which
                            where found in at least 90-100% of the samples in this experiment.
                    </div>
                </div>

                <div style="float:left;width: 100%">
                    <h2>Accumulative Annotation Count</h2>

                    <div class="help-message ui-state-highlight ui-corner-all"
                         style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
                        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                            <g:message code="experiment.annotation.graph"/>
                        </p>
                    </div>

                    <div class="horizontal-spacer"></div>

                    <div style="float:left">
                        <div style="width: 80%;margin-left: 10%;margin-right: 10%;padding: 0 0 0 0">
                            <binbase:accumulativeBinAnnotationCountByExperiment
                                    experimentId="${BBExperimentInstance.id}"/>
                        </div>
                    </div>

                    <div style="float:left;width:auto">
                        <p><strong><p>Explanation:</p></strong>

                            This graph shows the accumulative annotation count for all bins in this experiment. It
                            basically shows how many bins are found how often
                    </div>
                </div>

            </div>

            <h2><a href="#">Classes</a></h2>

            <div class="height320px">

                <g:if test="${BBExperimentInstance.publicExperiment}">

                    <div class="help-message ui-state-highlight ui-corner-all"
                         style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
                        <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                            <g:message code="experiment.classes"/>
                        </p>
                    </div>

                    <g:each in="${BBExperimentInstance.classes}" var="c">

                        <g:render template="../BBExperimentClass/classTable" model="[c:c]"/>
                    </g:each>

                </g:if>

                <g:else>

                    <div class="warning-message ui-state-error ui-corner-all"
                         style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
                        <p><span class="ui-icon ui-icon-alert" style="float: left; margin-right: .3em;"></span>
                            <g:message code="public.noaccess"/>
                        </p>
                    </div>
                </g:else>

            </div>


            <h2><a href="#">Downloads</a></h2>

            <div class="height320px">

                <div class="help-message ui-state-highlight ui-corner-all"
                     style="margin-top: 20px; margin-bottom: 20px;padding: 0 .7em;">
                    <p><span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
                        <g:message code="experiment.downloads"/>
                    </p>
                </div>


                <g:render template="downloads" model="[exp:BBExperimentInstance]"/>
            </div>
        </div>
    </div>
</div>
</body>
</html>
