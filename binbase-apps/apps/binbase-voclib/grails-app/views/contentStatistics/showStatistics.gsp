<%@ page import="grails.converters.JSON" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

</head>

<body>

<div class="article">

    <div class="left">
        <h2 class="star">Content Statistics</h2>

        <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
        </g:if>

        <div class="dialog">
            <table>
                <tbody>

                <tr class="prop">
                    <td valign="top" class="name">Bins</td>

                    <td valign="top" class="value"><g:formatNumber number="${result.bin}"
                                                                   format="########0"/></td>
                </tr>

                <tr class="prop">
                    <td valign="top" class="name">Annotated Spectra</td>

                    <td valign="top" class="value"><g:formatNumber number="${result.spectra}"
                                                                   format="########0"/></td>
                </tr>

                <tr class="prop">
                    <td valign="top" class="name">Samples</td>

                    <td valign="top" class="value"><g:formatNumber number="${result.sample}"
                                                                   format="########0"/></td>
                </tr>

                <tr class="prop">
                    <td valign="top" class="name">Classes</td>

                    <td valign="top" class="value"><g:formatNumber number="${result.clazz}"
                                                                   format="########0"/></td>
                </tr>

                <tr class="prop">
                    <td valign="top" class="name">Experiments</td>

                    <td valign="top" class="value"><g:formatNumber number="${result.exp}"
                                                                   format="########0"/></td>
                </tr>

                <tr class="prop">
                    <td valign="top" class="name">Species</td>

                    <td valign="top" class="value"><g:formatNumber number="${result.species}"
                                                                   format="########0"/></td>
                </tr>

                <tr class="prop">
                    <td valign="top" class="name">Organs</td>

                    <td valign="top" class="value"><g:formatNumber number="${result.organ}"
                                                                   format="########0"/></td>
                </tr>

                </tbody>
            </table>
        </div>

    </div>

    <div class="right">

        <h2 class="star">Sample Distribution by Species</h2>

        <binbase:pieSampleBySpeciesDistribution/>

    </div>


    <div class="right">
        <h2 class="star">Experiment Distribution by Species</h2>

        <binbase:pieExperimentByOrganDistribution/>
    </div>
</div>

<div class="clr"></div>

</body>
</html>
