package minix


class DesignStudieComplicatedWebTests extends grails.util.WebTest {

  // Unlike unit tests, functional tests are sometimes sequence dependent.
  // Methods starting with 'test' will be run automatically in alphabetical order.
  // If you require a specific sequence, prefix the method name (following 'test') with a sequence
  // e.g. test001XclassNameXListNewDelete
  void test2Species6Organs3Treatments() {

    //needed to make ajax work
    config easyajax: 'true', easyajaxdelay: '10000';

    //clear database
    resetDatabase()

    //dumpDatabase(fileName: "testCreateSimpleStudy_before.xml")

    //our url
    invoke "http://127.0.0.1:8080/minix/"

    setInputField(name: "username", value: "admin")
    setInputField(description: "Set password field password: admin", name: "password", value: "admin")
    clickButton "Sign in"

    clickLink "user home"
      clickLink "design a new studie"

    setInputField(name: "title", value: "a complicated studie")
    setInputField(name: "species_0", value: "a")
    clickButton "add additional species"

    setInputField(name: "species_0", value: "b")

    clickButton "define organs"
    setInputField(name: "organ_1000_0", value: "o1")
    clickButton "define another organ"

    setInputField(name: "organ_1000_0", value: "o2")
    clickButton "define another organ"

    setInputField(name: "organ_1001_0", value: "o3")
    clickButton "define another organ"

    setInputField(name: "organ_1001_0", value: "o4")
    clickButton "define another organ"

    setInputField(name: "organ_1001_0", value: "o5")
    clickButton "define another organ"

    setInputField(name: "organ_1001_0", value: "o6")

    clickButton "define treatments"
    setInputField(name: "treatment_name_0", value: "t1")
    setInputField(name: "treatment_value_0", value: "1")
    clickButton(name: "addTreatment")

    setInputField(name: "treatment_name_0", value: "t1")
    setInputField(name: "treatment_value_0", value: "2")
    clickButton(name: "addTreatment")

    setInputField(name: "treatment_name_0", value: "t1")
    setInputField(name: "treatment_value_0", value: "3")
    clickButton(name: "addTreatment")

    clickButton(name: "submitToSamples")
    setInputField(name: "numberOfSamples", value: "2")

    clickButton "display classes"
    clickButton "select all classes"
    clickButton "generate selected classes"


    verifyOrgan(name: "o1")
    verifyOrgan(name: "o2")
    verifyOrgan(name: "o3")
    verifyOrgan(name: "o4")
    verifyOrgan(name: "o5")
    verifyOrgan(name: "o6")

    verifyOrgan(expsectedCount: 7)

    verifySpecies(name: "a")
    verifySpecies(name: "b")

    verifySpecies(expsectedCount: 4)

    //next page
    verifyStudyExist(studieName: "a complicated studie")

    verifyStudyExist(studieCount: 1)

  }
  void test3Species6Organs3Treatments() {

    //needed to make ajax work
    config easyajax: 'true', easyajaxdelay: '10000';

    //clear database
    resetDatabase()

    //dumpDatabase(fileName: "testCreateSimpleStudy_before.xml")

    //our url
    invoke "http://127.0.0.1:8080/minix/"

    setInputField(name: "username", value: "admin")
    setInputField(description: "Set password field password: admin", name: "password", value: "admin")
    clickButton "Sign in"


    clickLink "user home"
      clickLink "design a new studie"

    setInputField(name: "title", value: "a complicated studie")
    setInputField(name: "species_0", value: "a")
    clickButton "define another species"

    setInputField(name: "species_0", value: "b")
    clickButton "define another species"

    setInputField(name: "species_0", value: "c")

    clickButton "define organs"
    setInputField(name: "organ_1000_0", value: "o1")
    clickButton "define another organ"

    setInputField(name: "organ_1000_0", value: "o2")
    clickButton "define another organ"

    setInputField(name: "organ_1001_0", value: "o3")
    clickButton "define another organ"

    setInputField(name: "organ_1001_0", value: "o4")
    clickButton "define another organ"

    setInputField(name: "organ_1002_0", value: "o5")
    clickButton "define another organ"

    setInputField(name: "organ_1002_0", value: "o6")

    clickButton "define treatments"
    setInputField(name: "treatment_name_0", value: "t1")
    setInputField(name: "treatment_value_0", value: "1")
    clickButton(name: "addTreatment")

    setInputField(name: "treatment_name_0", value: "t1")
    setInputField(name: "treatment_value_0", value: "2")
    clickButton(name: "addTreatment")

    setInputField(name: "treatment_name_0", value: "t1")
    setInputField(name: "treatment_value_0", value: "3")
    clickButton(name: "addTreatment")

    clickButton(name: "submitToSamples")
    setInputField(name: "numberOfSamples", value: "2")

    clickButton "display classes"
    clickButton "select all classes"
    clickButton "generate selected classes"


    verifyOrgan(name: "o1")
    verifyOrgan(name: "o2")
    verifyOrgan(name: "o3")
    verifyOrgan(name: "o4")
    verifyOrgan(name: "o5")
    verifyOrgan(name: "o6")

    verifyOrgan(expsectedCount: 7)

    verifySpecies(name: "a")
    verifySpecies(name: "b")
    verifySpecies(name: "c")

    verifySpecies(expsectedCount: 4)

    //next page
    verifyStudyExist(studieName: "a complicated studie")

    verifyStudyExist(studieCount: 1)

  }

  void test1Species6Organs1Treatments() {

    //needed to make ajax work
    config easyajax: 'true', easyajaxdelay: '10000';

    //clear database
    resetDatabase()

    //dumpDatabase(fileName: "testCreateSimpleStudy_before.xml")

    //our url
    invoke "http://127.0.0.1:8080/minix/"

    setInputField(name: "username", value: "admin")
    setInputField(description: "Set password field password: admin", name: "password", value: "admin")
    clickButton "Sign in"


    clickLink "user home"
      clickLink "design a new studie"

    setInputField(name: "title", value: "a complicated studie")
    setInputField(name: "species_0", value: "a")
    clickButton "define another species"

    clickButton "define organs"
    setInputField(name: "organ_1000_0", value: "o1")
    clickButton "define another organ"

    setInputField(name: "organ_1000_0", value: "o2")
    clickButton "define another organ"

    setInputField(name: "organ_1000_0", value: "o3")
    clickButton "define another organ"

    setInputField(name: "organ_1000_0", value: "o4")
    clickButton "define another organ"

    setInputField(name: "organ_1000_0", value: "o5")
    clickButton "define another organ"

    setInputField(name: "organ_1000_0", value: "o6")

    clickButton "define treatments"
    setInputField(name: "treatment_name_0", value: "t1")
    setInputField(name: "treatment_value_0", value: "1")
    clickButton(name: "addTreatment")

    clickButton(name: "submitToSamples")
    setInputField(name: "numberOfSamples", value: "2")

    clickButton "display classes"
    clickButton "select all classes"
    clickButton "generate selected classes"


    verifyOrgan(name: "o1")
    verifyOrgan(name: "o2")
    verifyOrgan(name: "o3")
    verifyOrgan(name: "o4")
    verifyOrgan(name: "o5")
    verifyOrgan(name: "o6")

    verifyOrgan(expsectedCount: 7)

    verifySpecies(name: "a")

    verifySpecies(expsectedCount: 2)

    //next page
    verifyStudyExist(studieName: "a complicated studie")

    verifyStudyExist(studieCount: 1)

  }

}