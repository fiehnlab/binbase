package minix
import org.apache.shiro.SecurityUtils



class LoginFilterWebTests extends grails.util.WebTest {

  // Unlike unit tests, functional tests are sometimes sequence dependent.
  // Methods starting with 'test' will be run automatically in alphabetical order.
  // If you require a specific sequence, prefix the method name (following 'test') with a sequence
  // e.g. test001XclassNameXListNewDelete

  /**
   * the login should be successful
   */
  void testLogin() {

      invoke "http://127.0.0.1:8080/minix/"

      //set username field
      setInputField(name: "username", value: "admin")

      //set password field
      setInputField(description: "Set password field password: admin", name: "password", value: "admin")

      //execute login
      clickButton "Sign in"
  }

  void testLogout() {

    invoke "http://127.0.0.1:8080/minix/"

    //set username field
    setInputField(name: "username", value: "admin")

    //set password field
    setInputField(description: "Set password field password: admin", name: "password", value: "admin")

    //execute login
    clickButton "Sign in"

    clickLink "logout"

    verifyText(text: 'you have been logout!')

  }
  /**
   * the login fails
   */
  void testLoginFailed() {
    invoke "http://127.0.0.1:8080/minix/"

    //set username field
    setInputField(name: "username", value: "adminDoesNotExist")

    //set password field
    setInputField(description: "Set password field password: admin", name: "password", value: "adminHasNoPassword")

    //execute login
    clickButton "Sign in"

    //ok we should now see the message that the login was successful
    verifyText(text: 'Invalid username and/or password')

  }
}