import minix.Species
import minix.Organ
import static org.junit.Assert.assertTrue
import static org.junit.Assert.fail

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 12/15/10
 * Time: 7:16 AM
 * To change this template use File | Settings | File Templates.
 */
class VerifyOrganStep extends com.canoo.webtest.steps.Step {

  String name

  Integer expsectedCount

  @Override
  void doExecute() {
    if (name != null) {
      assertTrue(Organ.findByName(name) != null)

    }
    else if (expsectedCount != null) {
      assertTrue(Organ.list().size() == expsectedCount.intValue())
    }
    else {
      fail("please provide a name or an expsected count")
    }
  }
}
