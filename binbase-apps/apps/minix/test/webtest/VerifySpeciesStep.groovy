import minix.Treatment
import minix.Species
import static org.junit.Assert.assertTrue
import static org.junit.Assert.fail

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 12/15/10
 * Time: 7:17 AM
 * To change this template use File | Settings | File Templates.
 */
class VerifySpeciesStep extends com.canoo.webtest.steps.Step {

  String name

  Integer expsectedCount

  @Override
  void doExecute() {
    if (name != null) {
      assertTrue(Species.findByName(name) != null)

    }
    else if (expsectedCount != null) {
      assertTrue(Species.list().size() == expsectedCount.intValue())
    }
    else {
      fail("please provide a name or an expsected count")
    }
  }
}
