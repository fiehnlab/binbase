import minix.Treatment
import static org.junit.Assert.assertTrue
import static org.junit.Assert.fail
import minix.TreatmentSpecific

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 12/15/10
 * Time: 7:16 AM
 * To change this template use File | Settings | File Templates.
 */
class VerifyTreatmentStep extends com.canoo.webtest.steps.Step {


  String name

  String value

  Integer expsectedCount

  @Override
  void doExecute() {
    if (name != null && value != null) {
      Treatment treatment = Treatment.findByDescription(name)
      assertTrue(treatment != null)

      TreatmentSpecific specific = TreatmentSpecific.findByTreatmentAndValue(treatment, value)

      assertTrue(specific != null)


    }
    else if (expsectedCount != null) {
      assertTrue(Treatment.list().size() == expsectedCount.intValue())
    }
    else {
      fail("please provide a name or an expsected count")
    }
  }
}
