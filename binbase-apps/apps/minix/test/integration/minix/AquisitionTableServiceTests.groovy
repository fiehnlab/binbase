package minix

import edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile
import grails.test.GrailsUnitTestCase

class AquisitionTableServiceTests extends GrailsUnitTestCase {
	protected void setUp() {
		super.setUp()
	}

	protected void tearDown() {
		super.tearDown()
	}

	void testGenerateDataAquisitionTableNoMethods() {

		def study = generateNamedStudie(20)

		GCTof tof = GCTof.list()[0]

		AquisitionTableService service = new AquisitionTableService()
		DataFile file = service.generateDataAquisitionTable(study, tof)

		file.print System.out

		log.info "cols: $file.columnCount"
		assertTrue(file.columnCount == 19)
		log.info "rows: $file.rowCount"
		assertTrue(file.rowCount == 20 + 1)

	}

	/**
	 * called from other test classes as well
	 * @param size
	 * @param rollover
	 * @return
	 */
	static Studie generateNamedStudie(int size, boolean rollover = true) {

		GCTof tof = GCTof.list()[0]

		log.info("do we got a gctof: " + tof)
		tof.rollOver = rollover
		tof.save()

		GenerateFileNameService generateFileNameService = new GenerateFileNameService()
		Studie study = GenerateFileNameServiceTests.generateStudie(size, "200basa")

		study = generateFileNameService.generateFileNames(study, tof, new Date(), ShiroUser.findAll()[0])

		return study
	}

	void testGenerateDataAquisitionTableNoMethods2() {

		Studie study = generateNamedStudie(200)

		GCTof tof = GCTof.list()[0]


		AquisitionTableService service = new AquisitionTableService()
		DataFile file = service.generateDataAquisitionTable(study, tof)

		assertTrue(file.columnCount == 19)
		println(file.rowCount)
		assertTrue(file.rowCount == 200 + 1)


	}

	void testGenerateDataAquisitionTableNoMethods3() {

		Studie study = generateNamedStudie(200, false)

		GCTof tof = GCTof.list()[0]

		AquisitionTableService service = new AquisitionTableService()
		DataFile file = service.generateDataAquisitionTable(study, tof)

		assertTrue(file.columnCount == 19)
		println(file.rowCount)
		assertTrue(file.rowCount == 200 + 1)


	}


	void testGenerateDataAquisitionTableNoMethodsWithCalibrationClass() {

		Studie study = generateNamedStudie(20, false)

		GCTof tof = GCTof.list()[0]

		AquisitionTableService service = new AquisitionTableService()
		GenerateFileNameService generateFileNameService = new GenerateFileNameService()
		generateFileNameService.modifyClassService = new ModifyClassService()

		generateFileNameService.addCalibrationSamples(study, tof, new Date(), ShiroUser.list()[0])
		DataFile file = service.generateDataAquisitionTable(study, tof)

		assertTrue(file.columnCount == 19)
		println(file.rowCount)
		assertTrue(file.rowCount == 20 + 1 + 6)


	}


	void testGenerateDataAquisitionTableNoMethodsWithCalibrationClassAndReactionBlanksAndQualityControl() {

		Studie study = generateNamedStudie(20, false)

		GCTof tof = GCTof.list()[0]

		AquisitionTableService service = new AquisitionTableService()
		GenerateFileNameService generateFileNameService = new GenerateFileNameService()
		generateFileNameService.modifyClassService = new ModifyClassService()

		generateFileNameService.addCalibrationSamples(study, tof, new Date(), ShiroUser.list()[0])
		generateFileNameService.addReactionBlankSamples(study, tof, new Date(), ShiroUser.list()[0])
		generateFileNameService.addQualityControlSamples(study, tof, new Date(), ShiroUser.list()[0])


		DataFile file = service.generateDataAquisitionTable(study, tof)

		assertTrue(file.columnCount == 19)
		println(file.rowCount)
		assertTrue(file.rowCount == 20 + 6 + 4 + 2)


	}


	void testGenerateDataAquisitionTableNoMethodsWithReactionBlanks() {

		Studie studie = generateNamedStudie(20, false)

		GCTof tof = GCTof.list()[0]

		AquisitionTableService service = new AquisitionTableService()
		GenerateFileNameService generateFileNameService = new GenerateFileNameService()
		generateFileNameService.modifyClassService = new ModifyClassService()

		generateFileNameService.addReactionBlankSamples(studie, tof, new Date(), ShiroUser.list()[0])

		DataFile file = service.generateDataAquisitionTable(studie, tof)

		assertTrue(file.columnCount == 19)
		println(file.rowCount)
		assertTrue(file.rowCount == 20 + 4)


	}

	void testGenerateDataAquisitionTableNoMethodsWithCalibrationClass2() {

		Studie studie = generateNamedStudie(200, false)

		GCTof tof = GCTof.list()[0]

		AquisitionTableService service = new AquisitionTableService()
		GenerateFileNameService generateFileNameService = new GenerateFileNameService()
		generateFileNameService.modifyClassService = new ModifyClassService()

		generateFileNameService.addCalibrationSamples(studie, tof, new Date(), ShiroUser.list()[0])
		DataFile file = service.generateDataAquisitionTable(studie, tof)

		assertTrue(file.columnCount == 19)
		println(file.rowCount)
		assertTrue(file.rowCount == 200 + 7)


	}


	void testGenerateDataAquisitionTableNoMethodsWithCalibrationClassAndReactionBlanksAndQualityControl2() {

		Studie studie = generateNamedStudie(200, false)

		GCTof tof = GCTof.list()[0]

		AquisitionTableService service = new AquisitionTableService()
		GenerateFileNameService generateFileNameService = new GenerateFileNameService()
		generateFileNameService.modifyClassService = new ModifyClassService()

		generateFileNameService.addCalibrationSamples(studie, tof, new Date(), ShiroUser.list()[0])
		generateFileNameService.addReactionBlankSamples(studie, tof, new Date(), ShiroUser.list()[0])
		generateFileNameService.addQualityControlSamples(studie, tof, new Date(), ShiroUser.list()[0])


		DataFile file = service.generateDataAquisitionTable(studie, tof)

		assertTrue(file.columnCount == 19)
		println(file.rowCount)
		assertTrue(file.rowCount == 200 + 6 + 20 + 22)


	}


	void testGenerateDataAquisitionTableNoMethodsWithReactionBlanks200() {

		Studie studie = generateNamedStudie(200, false)

		GCTof tof = GCTof.list()[0]

		AquisitionTableService service = new AquisitionTableService()
		GenerateFileNameService generateFileNameService = new GenerateFileNameService()
		generateFileNameService.modifyClassService = new ModifyClassService()

		generateFileNameService.addReactionBlankSamples(studie, tof, new Date(), ShiroUser.list()[0])

		DataFile file = service.generateDataAquisitionTable(studie, tof)

		assertTrue(file.columnCount == 19)
		println(file.rowCount)
		assertTrue(file.rowCount == 200 + 22)


	}



	void testGenerateDataAquisitionTableNoMethodsWithCalibrationClassAndReactionBlanksAndQualityControlSmall() {

		Studie study = generateNamedStudie(5, false)

		GCTof tof = GCTof.list()[0]

		AquisitionTableService service = new AquisitionTableService()
		GenerateFileNameService generateFileNameService = new GenerateFileNameService()
		generateFileNameService.modifyClassService = new ModifyClassService()

		generateFileNameService.addCalibrationSamples(study, tof, new Date(), ShiroUser.list()[0])
		generateFileNameService.addReactionBlankSamples(study, tof, new Date(), ShiroUser.list()[0])
		generateFileNameService.addQualityControlSamples(study, tof, new Date(), ShiroUser.list()[0])


		DataFile file = service.generateDataAquisitionTable(study, tof)

		assertTrue(file.columnCount == 19)
		println(file.rowCount)
		assertTrue(file.rowCount == 5 + 6 + 3 + 1)


	}



	void testGenerateDataAquisitionTableNoMethodsWithCalibrationClassAndReactionBlanksAndQualityControlSmallTwice() {

		Studie study = generateNamedStudie(5, false)

		GCTof tof = GCTof.list()[0]

		AquisitionTableService service = new AquisitionTableService()
		GenerateFileNameService generateFileNameService = new GenerateFileNameService()
		generateFileNameService.modifyClassService = new ModifyClassService()

		generateFileNameService.addCalibrationSamples(study, tof, new Date(), ShiroUser.list()[0])
		generateFileNameService.addReactionBlankSamples(study, tof, new Date(), ShiroUser.list()[0])
		generateFileNameService.addQualityControlSamples(study, tof, new Date(), ShiroUser.list()[0])

		//first run
		DataFile file = service.generateDataAquisitionTable(study, tof)

		assertTrue(file.columnCount == 19)
		println(file.rowCount)

		file.print(System.out)
		assertTrue(file.rowCount == 5 + 6 + 3 + 1)

		//rerun to make sure classes are correctly delete
		file = service.generateDataAquisitionTable(study, tof)

		assertTrue(file.columnCount == 19)
		println(file.rowCount)
		assertTrue(file.rowCount == 5 + 6 + 3 + 1)

	}


	void testDoNothing() {
		assert true
	}
}
