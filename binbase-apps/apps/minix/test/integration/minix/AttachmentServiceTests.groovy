package minix

import grails.test.GrailsUnitTestCase

/**
 * Created by IntelliJ IDEA.
 * User: wohlgemuth
 * Date: 6/14/11
 * Time: 7:35 PM
 * To change this template use File | Settings | File Templates.
 */
class AttachmentServiceTests extends GrailsUnitTestCase {

    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testAttachFileToStudie() {
        AttachmentService service = new AttachmentService()

        Studie studie = GenerateFileNameServiceTests.generateStudie(10, 'test', 1)

        byte[] content = new byte[1024]
        service.attachFileToStudie(content, studie, "test.txt")

        assert FileAttachment.findByFileName("test.txt") != null
    }

    void testDeleteFileFromStudie() {

        AttachmentService service = new AttachmentService()

        Studie studie = GenerateFileNameServiceTests.generateStudie(10, 'test-2', 1)

        byte[] content = new byte[1024]
        service.attachFileToStudie(content, studie, "test=2.txt")

        assert FileAttachment.findByFileName("test=2.txt") != null

        studie.attachments.each {
            service.deleteFileFromStudie(it)
        }

        assert FileAttachment.findAllByStudie(studie).size() == 0

    }
}
