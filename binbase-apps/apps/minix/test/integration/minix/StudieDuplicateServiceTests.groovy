package minix

import grails.test.*
import binbase.web.core.BBExperimentSample

class StudieDuplicateServiceTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testDuplicate() {

        def first = GenerateFileNameServiceTests.generateStudie(10, "first_")

        StudieDuplicateService service = new StudieDuplicateService()

        service.grailsApplication = new org.codehaus.groovy.grails.commons.DefaultGrailsApplication()

        Architecture architecture = new Architecture()
        architecture.description = "test architecture"
        architecture.name = "test"
        architecture.relatedAction = "none"
        architecture.relatedController = "none"
        architecture.save()

        def second = service.duplicate(first, architecture)

        assertTrue second.title == first.title
        assertTrue second.description == first.description
        assertTrue second.classes.size() == first.classes.size()
        assertTrue second.architecture != first.architecture
        assertTrue second.id != first.id

        def copySamples = []
        second.classes.each {BaseClass clazz ->
            clazz.samples.each {BBExperimentSample sample ->
                copySamples.add(sample)
            }
        }

        def originalSamples = []

        first.classes.each {BaseClass clazz ->
            clazz.samples.each {BBExperimentSample sample ->
                originalSamples.add(sample)
            }
        }


        assertTrue(copySamples.size() == originalSamples.size())

        assertTrue(first.users.size() == second.users.size())


        assertTrue(first.users.size() == 1)

    }
}
