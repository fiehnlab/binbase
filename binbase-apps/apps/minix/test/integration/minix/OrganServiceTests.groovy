package minix

import grails.test.*

class OrganServiceTests extends GrailsUnitTestCase {
    protected void setUp() {
        super.setUp()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testCombineOrgans() {


        OrganService service = new OrganService()

        Species monkey = new Species()
        monkey.name = "monkey-1"
        monkey.kingdom = Kingdom.findAll()[0]
        monkey.save()

        Species monkey2 = new Species()
        monkey2.name = "monkey-2"
        monkey2.kingdom = Kingdom.findAll()[0]
        monkey2.save()



        Organ a = new Organ(name: "kidney")

        a.addToSpeciees(monkey)
        a.addToSpeciees(monkey2)
        a.save()

        Organ b = new Organ(name: "brain")
        b.addToSpeciees(monkey)
        b.addToSpeciees(monkey2)

        b.save()

        Organ c = new Organ(name: "knee")
        c.addToSpeciees(monkey)
        c.addToSpeciees(monkey2)
        c.save()


        Organ d = new Organ(name: "knee-1")
        d.addToSpeciees(monkey)
        d.addToSpeciees(monkey2)

        d.save()


        Organ e = new Organ(name: "knee-2")
        e.addToSpeciees(monkey)
        e.save()



        def first = GenerateFileNameServiceTests.generateStudie(10, "first_", 1, c, monkey)
        def second = GenerateFileNameServiceTests.generateStudie(10, "second_", 1, d, monkey)
        def third = GenerateFileNameServiceTests.generateStudie(10, "third_", 1, e, monkey)

        def combined = service.combineOrgan("knee", [c, d, e])

        def checkOrgan = {ExperimentClass ex ->
            println "checking if correct organ is applied..."
            assertTrue(ex.organ.equals(combined))
            assertTrue(ex.species.organs.contains(combined))
            assertTrue(ex.species.organs.contains(combined))
            assertTrue(ex.species.organs.contains(combined))
            assertTrue(ex.organ.speciees.contains(ex.species))


        }

        first.classes.each {checkOrgan(it)}
        second.classes.each {checkOrgan(it)}
        third.classes.each {checkOrgan(it)}

        assertTrue(combined.name == "knee")
        assertTrue(combined.speciees.size() == 2)


        assertNotNull(Organ.findByName("knee"))

        assertTrue(Organ.findByName("knee").speciees.contains(Species.findByName("monkey-1")))


        println "checking if the organs"
        assertNull(Organ.findByName("knee-1"))
        assertNull(Organ.findByName("knee-2"))

    }
}
