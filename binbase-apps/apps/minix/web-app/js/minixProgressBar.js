/**
 * used to display a progress bar in the system
 * @param session unique session id
 * @param url url to invoke for maintaining progress messages
 * @param selector where should this be anchored
 * @param message our message we like to send
 */
function showProgressBar(session, url, selector, message) {
    $(document).ready(function () {
        var jqSelector = "#" + selector;

        var progressSelector = selector + "_progress";
        var progressMessageSelector = selector + "_progress_message";

        $(jqSelector).empty();
        $(jqSelector).append("<div class='element' id='" + progressSelector + "'></div>");

        $("#" + progressSelector).progressbar({value: 0});
        if (message) {
            $(jqSelector).append("<div id='" + progressMessageSelector + "'></div>");
            $("#" + progressMessageSelector).html("<div class='message'>" + message + "</div>");
        }

        //call the function the given interval in ms
        var progressTimer = setInterval((function (session) {
            return function () {
                $.ajax({
                    method: 'get',
                    url: url,
                    dataType: 'text',
                    success: function (text) {
                        var value = parseFloat(text);

                        //we are done
                        if (value >= 100) {
                            $("#" + progressSelector).progressbar("value", 100);
                            clearInterval(progressTimer);
                        }

                        //an un exspected error happened
                        else if (value < 0) {
                            $("#" + progressSelector).progressbar("destroy");
                            clearInterval(progressTimer);

                            if (message) {
                                $("#" + progressMessageSelector).html("<div class='errors'>there was some kind of error...</div>");
                            }
                        }
                        //update the progress bar
                        else {
                            $("#" + progressSelector).progressbar("value", value);
                        }
                    }
                });
            };
        })(session), 250);

    });
}

