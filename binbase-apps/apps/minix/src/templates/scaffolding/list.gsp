<%=packageName%>

<% import org.codehaus.groovy.grails.orm.hibernate.support.ClosureEventTriggeringInterceptor as Events %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <title>${className} List</title>
</head>

<body>
<g:render template="/shared/navigation"/>

<div class="body">

    <div class="center-full-page">
        <div class="header">${className} List</div>
        <g:if test="\${flash.message}">
            <div class="message">\${flash.message}</div>
        </g:if>

        <div class="element">
            <ul class="none-horizontal">
                <!-- here we are registering our gctofs systems -->
                <li><g:link class="add" action="create">register a new ${className}</g:link></li>
            </ul>
        </div>

        <div class="element">

            <table>
                <thead>
                <tr>
                    <%
                        excludedProps = ['version',
                                Events.ONLOAD_EVENT,
                                Events.BEFORE_DELETE_EVENT,
                                Events.BEFORE_INSERT_EVENT,
                                Events.BEFORE_UPDATE_EVENT]

                        props = domainClass.properties.findAll { !excludedProps.contains(it.name) && it.type != Set.class }
                        Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
                        props.eachWithIndex { p, i ->
                            if (i < 6) {
                                if (p.isAssociation()) { %>
                    <th>${p.naturalName}</th>
                    <% } else { %>
                    <g:sortableColumn property="${p.name}" title="${p.naturalName}"/>
                    <% }
                    }
                    } %>
                </tr>
                </thead>
                <tbody>
                <g:each in="\${${propertyName}List}" status="i" var="${propertyName}">
                    <tr class="\${(i % 2) == 0 ? 'odd' : 'even'}">

                        <% props.eachWithIndex { p, i ->
                            if (i == 0) { %>
                        <td><g:link action="show"
                                    id="\${${propertyName}.id}">\${${propertyName}.${p.name}?.encodeAsHTML()}</g:link></td>
                        <% } else if (i < 6) { %>
                        <td>\${${propertyName}.${p.name}?.encodeAsHTML()}</td>
                        <% }
                        } %>
                    </tr>
                </g:each>
                </tbody>
            </table>

            <div class="paginateButtons">
                <g:paginate total="\${${className}.count()}"/>
            </div>
        </div>
    </div>

</div>
</body>
</html>

