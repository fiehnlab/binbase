/**
 * not longer needed
 */
  ALTER TABLE public.bbdownload
      DROP COLUMN version
  GO

/**
 * we could drop this, but than we would loose all the content
 */
ALTER TABLE "public"."bbdownload" ALTER "content" DROP NOT NULL
GO

ALTER TABLE "public"."file_attachment" ALTER "name" DROP NOT NULL
GO

ALTER TABLE "public"."file_attachment" ALTER "content" DROP NOT NULL
GO

ALTER TABLE "public"."file_attachment" ALTER "description" DROP NOT NULL
GO

ALTER TABLE "public"."file_attachment" ALTER "internal" DROP NOT NULL
GO

ALTER TABLE "public"."file_attachment" ALTER "upload_date" DROP NOT NULL
GO


/**
 * we could drop this, but than we would loose all the content
 */
ALTER TABLE "public"."bbresult" ALTER "upload_date" DROP NOT NULL
GO

ALTER TABLE "public"."bbresult" ALTER "content" DROP NOT NULL
GO

ALTER TABLE "public"."bbresult" ALTER "file_name" DROP NOT NULL
GO

ALTER TABLE "public"."bbresult" ALTER "description" DROP NOT NULL
GO



/**
 * move old attachment data, since we don't want to loose these
 */

insert into bbdownload(id,description,file_name,upload_date) select id,description,name,upload_date from file_attachment where description is not null and id not in (select id from bbdownload)

GO

insert into bbdownload(id,description,file_name,upload_date) select id,description,name,upload_date from file_attachment where description is not null and id not in (select id from bbdownload)

GO

insert into bbcontent(id,result_id,content) select id,id,content from file_attachment where description is not null and id not in (select id from bbcontent)

go

/**
 * move old result data
 */


insert into bbdownload(id,description,file_name,upload_date) select id,'no description',file_name,upload_date from bbresult where file_name is not null and id not in (select id from bbdownload)

GO

insert into bbcontent(id,result_id,content) select id,id,content from bbresult where file_name is not null and id not in (select id from bbcontent)


/**
 * drop no longer required content
 */

  ALTER TABLE public.bbdownload
      DROP COLUMN content
  GO


/**
 * drop no longer required content
 */

  ALTER TABLE public.bbresult
      DROP COLUMN content
  GO

