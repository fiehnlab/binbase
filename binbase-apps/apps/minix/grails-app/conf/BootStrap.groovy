import grails.plugin.searchable.SearchableService
import grails.util.GrailsUtil
import minix.*
import org.apache.shiro.SecurityUtils
import org.apache.shiro.subject.Subject
import org.apache.shiro.util.ThreadContext
import org.codehaus.groovy.grails.commons.ConfigurationHolder
import org.codehaus.groovy.grails.web.context.ServletContextHolder
import org.springframework.web.context.WebApplicationContext
import org.springframework.web.context.support.WebApplicationContextUtils

import javax.servlet.ServletContext
import javax.sql.DataSource

class BootStrap {

	DataSource dataSource

	WebApplicationContext context

	SearchableService service

	def init = { servletContext ->

		switch (GrailsUtil.environment) {
			case "test":

				//create test user here called admin with password admin if not exist
				checkAdminUser()

				println "register subject for test mode"
				def subject = [getPrincipal: { "admin" },
						isAuthenticated: { true }
				] as Subject

				ThreadContext.put(ThreadContext.SECURITY_MANAGER_KEY,
						[getSubject: { subject }] as SecurityManager)

				SecurityUtils.metaClass.static.getSubject = { subject }
				break;
			default:
				println "no special setup needed"
		}

		/*
		try {
			log.info "init xml datafile..."
			DbUnitOperator.create()

		}
		catch (Exception e) {
			log.warn("error during xml initialization, please check the log if it's serious. We ignore it for now (${e.getMessage()})")
			log.debug(e.getMessage(), e)
		}

*/
		log.info "init vendor and platform file..."

		if (context == null) {

			ServletContext sctx = ServletContextHolder.getServletContext();
			if (sctx != null)
				context = WebApplicationContextUtils.getWebApplicationContext(sctx)
		}

		String realPath = context.getServletContext().getRealPath('.')
		def path = new File(realPath)
		def rootPath = path.getPath()
		if (rootPath.endsWith('.')) {
			rootPath = rootPath.substring(0, rootPath.length() - 1)
		}


		String dataPath = "${rootPath}/data/platform/architectures.txt"

		File data = new File(dataPath)

		if (data.exists()) {

			log.info "importing platform file: ${data.absolutePath}"
			data.text.eachLine { String line ->
				def pair = line.split("\t")

				if (pair.size() == 2) {
					Vendor vendor = Vendor.findByName(pair[0])

					if (vendor == null) {
						vendor = new Vendor(name: pair[0].trim())

						if (vendor.validate()) {
							vendor.save(flush: true)
						} else {
							log.error vendor.errors
						}
					}

					Platform platform = Platform.findByName(pair[1])

					if (platform == null) {
						platform = new Platform(name: pair[1].trim(), vendor: vendor)

						if (platform.validate()) {
							platform.save(flush: true)
						} else {
							log.error platform.errors
						}
					}


				} else {
					log.info "invalid line!"
				}
			}

		} else {
			log.info "sorry platform file not found..."
		}

		//make sure standard concentrations exist
		def config = ConfigurationHolder.config

		config.minix.standard.concentrations.each { double value ->
			def standard = QualityControlSampleConcentration.findByConcentration(value)

			if (!standard) {
				standard = new QualityControlSampleConcentration()
				standard.concentration = value
				standard.save(flush: true)
			}
		}

		//create test data
		initDB()
	}


	private void initDB() {

		//create default architecture for binbase
		Architecture architecture = Architecture.findByName("Leco GC-Tof")

		if (architecture == null) {
			architecture = new Architecture(description: "Leco GC-Tof with BinBase analysis system",
					relatedController: "binbaseScheduling",
					relatedAction: "schedule",
					name: "Leco GC-Tof",
					id: 101)

			if (architecture.validate()) {
				architecture.save(flush: true)
			} else {
				log.error architecture.errors
			}
		}

		//create kingdom 'None' with 303
		Kingdom kingdom = Kingdom.findByName("None")
		if (kingdom == null) {
			kingdom = new Kingdom(id: 303, name: "None")
			if (kingdom.validate()) {
				kingdom.save(flush: true)
			} else {
				log.error kingdom.errors
			}
		}

		//create Organ 'None' if not exist with id 505 associated to species 404
		Organ organ = Organ.findByName("None")
		if (organ == null) {
			organ = new Organ(id: 505, name: "None")
			if (organ.validate()) {
				organ.save(flush: true)
			} else {
				log.error organ.errors
			}
		}

		//create Species 'None' if not exist with id 404!
		Species species = Species.findByName("None")
		if (species == null) {
			species = new Species(id: 404, kingdom: kingdom, name: "None")
			if (species.validate()) {
				species.save(flush: true)
			} else {
				log.error species.errors
			}
		}

		//create treatment 'None' with id 606
		Treatment treatment = Treatment.findByDescription("None")
		if (treatment == null) {
			treatment = new Treatment(id: 606, description: "None")
			if (treatment.validate()) {
				treatment.save(flush: true)
			} else {
				log.error treatment.errors
			}
		}

		//create the 'tada' vendor for the 'tada' platform
		Vendor vendor = Vendor.findByName("tada")
		if (vendor == null) {
			vendor = new Vendor(id: "0", version: "0", name: "tada")
			if (vendor.validate()) {
				vendor.save(flush: true)
			} else {
				log.error vendor.errors
			}
		}

		//create the 'tada' platform for the instruments
		Platform platform = Platform.findByName("tada")
		if (platform == null) {
			platform = new Platform(id: "0", version: "0", description: "tada", name: "tada", vendor: vendor)
			if (platform.validate()) {
				platform.save(flush: true)
			} else {
				log.error platform.errors
			}
		}

		//create 2 GCTof a and b in test env. but only a in production
		Instrument a = GCTof.findByName("core GC-Tof")
		if (a == null) {
			a = new GCTof(id: 202,
					architecture: architecture,
					dailyCapacity: 50,
					description: "a standard gc tof",
					identifier: "a",
					autoSamplerSize: 85,
					autoSamplerStartPosition: 5,
					name: "core GC-Tof",
					defaultFolder: "Aquired Samples",
					rollOver: true,
					platform: platform)

			if (a.validate()) {
				a.save(flush: true)
			} else {
				log.error a.errors
			}
		}
		log.info "Instruments: ${Instrument.count()}"

		if (GrailsUtil.environment.equals("test")) {
			Instrument b = GCTof.findByName("research GC-Tof")
			if (b == null) {
				b = new GCTof(id: 203,
						architecture: architecture,
						dailyCapacity: 50,
						description: "a standard gc tof",
						identifier: "b",
						autoSamplerSize: 85,
						autoSamplerStartPosition: 5,
						name: "research GC-Tof",
						defaultFolder: "\\Acquired Samples",
						rollOver: true,
						platform: platform)

				if (b.validate()) {
					b.save(flush: true)
				} else {
					log.error b.errors
				}
			}
		}

        log.info "Checking that the guest attributes of registered guests is not null"
        ShiroUser.findAllByGuestIsNull().each {
            it.guest = false
            it.save()
            log.info("updated: ${it}")

        }
	}

	private void checkAdminUser() {
		ShiroUser admin = ShiroUser.findByUsername("admin")

		Set<ShiroRole> admin_role = new HashSet<ShiroRole>()
		admin_role.add(ShiroRole.findByName('admin'))

		if (admin == null) {
			admin = new ShiroUser(id: -999,
					email: "a@b.com",
					firstName: "admin",
					lastName: "admin",
					passwordHash: "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918",
					samplePrefix: "aa",
					username: "admin",
					locked: false,
					roles: admin_role)

			if (admin.validate()) {
				admin.save(flush: true)
			} else {
				log.error admin.errors
			}

			ShiroUserPermission perm = ShiroUserPermission.findByPermissionAndUser("*:*", admin)
			if (perm == null) {
				perm = new ShiroUserPermission(id: -1, permission: "*.*", user: admin)

				if (perm.validate()) {
					perm.save(flush: true)
				} else {
					log.error perm.errors
				}
			}
		}
	}

	def destroy = {

	}
}