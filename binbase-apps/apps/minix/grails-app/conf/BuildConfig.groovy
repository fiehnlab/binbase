import grails.util.Environment

grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"


println(Environment.current.name + " - " + Environment.DEVELOPMENT.name)
/**
 * enviorment specific builds
 */

if (Environment.current.name == "jboss") {
    println "we are in the jboss binbase local mode so no servlet api or binbase api's required!"
    grails.war.resources = { stagingDir, args ->

        delete(file: "${stagingDir}/WEB-INF/lib/servlet-api-2.5-20081211.jar")

        delete {

            /**
             * unrequired dependcies can be removed
             */
            fileset(dir: "${stagingDir}/WEB-INF/lib/") {

                //the libs already exist in jboss, since binbase is deployed
                include(name: "binbase-ejb-*")
                include(name: "bci-scheduler-ejb-*")
                include(name: "bci-authentification-ejb-*")
                include(name: "bci-service-ejb-*")
                include(name: "bci-logging-ejb-*")
                include(name: "bci-setupx-ejb-*")
                include(name: "bci-core-*")

                //jboss provides log4j
                include(name: "log4j*")

                //jboss has a different version of jasper
                include(name: "jasper-*")

            }

            /**
             * log4j should be managed by jboss

             fileset(dir: "${stagingDir}") {}*/
        }
    }

    grails.project.war.file = "target/minix-jboss-4.2.0-GA.war"

}
else if (Environment.current.name == Environment.DEVELOPMENT.name){
    //nothing special needed
}
else {
    grails.project.war.file = "target/minix.war"

    //add the jetty-web.xml file
    println "copy needed jetty-web.xml file for jetty development"
    grails.war.resources = { stagingDir ->
        copy(file:"${basedir}/src/main/jetty-web.xml",toFile:"${stagingDir}/WEB-INF/jetty-web.xml")
        delete  {
            fileset(dir: "${stagingDir}/WEB-INF/lib/") {
                include(name: "slf4j-api-1.7.*.jar")
                include(name: "slf4j-log4j12-1.7.*.jar")

            }
        }
    }
}

/**
 * grails dependencies resolution
 */
grails.project.dependency.resolution = {
    // inherit Grails' default dependencies
    inherits("global") {
        // uncomment to disable ehcache
        // excludes 'ehcache'
        excludes 'serializer', "slf4j-api", "slf4j-log4j12", "jul-to-slf4j", "jcl-over-slf4j"

    }
    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
    repositories {
        mavenLocal()

        grailsPlugins()
        grailsHome()
        grailsCentral()

        mavenCentral()

//        mavenRepo "http://snapshots.repository.codehaus.org"
//        mavenRepo "http://repository.codehaus.org"

//        mavenRepo "http://download.java.net/maven/2/"
//        mavenRepo "http://repository.jboss.com/maven2/"
//        mavenRepo("http://webtest.canoo.com/webtest/m2-repo-snapshots")
//        mavenRepo("http://htmlunit.sourceforge.net/m2-repo-snapshots")
//        mavenRepo "http://ftp.us.xemacs.org/pub/mirrors/maven2/"
//        mavenRepo "http://build.canoo.com/NekoHTML/artifacts/m2-repo"

    }
    dependencies {
        // specify dependencies here under either 'build', 'compile', 'runtime', 'test' or 'provided' scopes eg.

        // runtime 'mysql:mysql-connector-java:5.1.5'

        runtime 'postgresql:postgresql:8.2-504.jdbc3'

        def slf4jVersion = "1.7.3"
        compile "org.slf4j:slf4j-api:$slf4jVersion"
        runtime "org.slf4j:slf4j-log4j12:$slf4jVersion", "org.slf4j:jul-to-slf4j:$slf4jVersion", "org.slf4j:jcl-over-slf4j:$slf4jVersion"

        plugins {
            compile('edu.ucdavis.genomics.metabolomics.binbase.minix.plugins:binbase-web-core:latest.integration')
            runtime('edu.ucdavis.genomics.metabolomics.binbase.minix.plugins:jboss-client:latest.integration')

            runtime(':audit-logging:0.5.4')
            runtime(':bluff:0.1.1')
            runtime(':ckeditor:3.6.0.0')
            runtime(':cxf:0.7.0')
            runtime(':dbunit-operator:1.6.1')
            runtime(':executor:0.3')
            runtime(':export:1.0')
            runtime(':flot:0.2.3')
            runtime(':geoip:0.2')
            runtime(':hibernate:1.3.7')
            runtime(':jetty:1.2-SNAPSHOT')
            runtime(':jprogress:0.2')
            runtime(':jquery:1.7.1')
            runtime(':jquery-datatables:1.7.5')
            runtime(':jquery-ui:1.8.15')
            runtime(':quartz:0.4.2')
            runtime(':remote-pagination:0.3')
            runtime(':searchable:0.6.3')
            runtime(':shiro:1.1.3')
            runtime(':tinyurl:0.1')


        }


/*
        plugin("webtest") {
            test('net.sourceforge.htmlunit:htmlunit:2.9-SNAPSHOT') {
                excludes 'xalan'
                excludes 'xml-apis'
            }
            test('com.canoo.webtest:webtest:3.1-SNAPSHOT') {
                excludes 'xalan'
                excludes 'xml-apis'
            }
            test('xalan:xalan:2.7.0') {
                excludes 'xml-apis'
            }
        }
*/
        runtime('org.hibernate:hibernate-validator:3.1.0.GA') {
            excludes 'sl4j-api', 'hibernate.core', 'hibernate-commons-annotations', 'hibernate-entitymanager'
        }
    }

}

