class UrlMappings {

	static mappings = {

        "/BBExperimentSample"(controller:"Sample")

		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/"(view:"/index")
		"500"(view:'/error')
	}
}
