<%@ page import=" org.apache.shiro.SecurityUtils; minix.ShiroUser" %>


<g:if test="${ShiroUser.hasManagmentRights()}">



    <div style="margin-top:30px;">
        <div class="moduleheader">Quality Control</div>

        <li class="audit_log">
            <g:link controller="qualityControl" action="binbaseQualityControl">BinBase Quality Control Reports</g:link>
        </li>
    </div>

</g:if>
