<!-- every user should have access to these -->
<div id="userActions">

    <ul class="actions">

        <!-- if it's a standard user render the basic actions -->
        <g:render template="actions/basicActions"/>

        <!-- if it's a management user render the management actions -->
        <g:render template="actions/managerActions"/>

        <!-- quality control actions -->
        <g:render template="actions/qualityActions"/>

        <!-- if it's an admin user render the admin actions -->
        <g:render template="actions/adminActions"/>

        <div style="margin-top:30px;">

            <div class="moduleheader">Related Tools</div>

            <li class="download">
                <g:link url="http://code.google.com/p/binbase/downloads/list">Download BinView</g:link>
            </li>

            <li class="download">
                <g:link url="http://code.google.com/p/binbase-tools/downloads/list">Download BinBase Tools</g:link>
            </li>

        </div>

    </ul>
</div>