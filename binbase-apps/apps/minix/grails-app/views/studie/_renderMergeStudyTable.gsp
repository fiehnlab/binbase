<table>
    <thead>
    <tr>
        <th>id</th>
        <th>title</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${studieInstance?.members}" var="study">
        <tr>
            <td>${study.id}</td>
            <td>${study.title}</td>
            <td><g:link controller="studie" action="show" id="${study.id}" target="_blank">show</g:link> </td>

        </tr>
    </g:each>
    </tbody>
</table>
