<%@ page import="minix.MergedStudie; minix.ExperimentClass; minix.ShiroUser" %><g:setProvider library="jquery"/>
<g:setProvider library="jquery"/>

<%
    int classLimit = 6
%>
<div>
    <g:javascript>
        jQuery(document).ready(function() {
            $('#studieArrcodion').accordion({    autoHeight: false});
        });

    </g:javascript>


    <div class="studieArrcodion">
        <div id="studieArrcodion">

            <h3><a href="#">Comments</a></h3>

            <div>
                <g:render template="renderComments" model="[studieInstance:studieInstance]"/>
            </div>

            <h3><a href="#">Results</a></h3>
            <!-- results -->
            <div>

                <g:render template="resultTable" model="[studieInstance:studieInstance]"/>

            </div>

            <h3><a href="#">Attachments</a></h3>
            <!-- attachments -->
            <div class="attachmentData">
                <g:render template="renderAttachment" model="[studieInstance:studieInstance]"/>
            </div>

            <!--  display the classes if we have only a few -->

            <h4><a href="#">Classes</a></h4>

            <div class="classesData">

                <g:if test="${studieInstance.classes.size() <= classLimit}">
                    <g:render template="displayClasses" model="[studieInstance:studieInstance]"/>
                </g:if>

                <g:else>

                    This study has quite a large list of samples for performance reasons you need to click on the link to show these.
                    <g:link controller="studie" action="displayClasses" class="element"
                            id="${studieInstance.id}">display list of classes</g:link>
                </g:else>
            </div>


            <g:if test="${ShiroUser.hasManagmentRights() || ShiroUser.ownsStudy(studieInstance)}">
                <h3><a href="#">Management</a></h3>

                <div class="userData">
                    <div class="header">Associated Users</div>

                    <div id="userContent">
                        <div class="element">
                            <g:render template="renderUserTable" model="[studieInstance:studieInstance]"/>
                        </div>
                    </div>
                </div>

            </g:if>


            <g:if test="${studieInstance instanceof MergedStudie}">
                <h3><a href="#">Merged Study Information</a></h3>

                <div class="mergeStudy">
                    <div class="header">Study is based on the following studies</div>

                    <div id="mergeContent">
                        <div class="element">
                            <g:render template="renderMergeStudyTable" model="[studieInstance:studieInstance]"/>
                        </div>
                    </div>
                </div>

            </g:if>


            <g:if test="${studieInstance.subStudies.size() > 0}">
                <h3><a href="#">Associated Study Information</a></h3>

                <div class="mergeStudy">
                    <div class="header">Associated sub studies</div>

                    <div id="substudieContent">
                        <div class="element">
                            <g:render template="renderAssociatedSubStudies" model="[studieInstance:studieInstance]"/>
                        </div>
                    </div>
                </div>

            </g:if>


        </div>
    </div>
</div>
