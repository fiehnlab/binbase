<g:setProvider library="jquery"/>

<%
  assert studieInstance != null, "you need to provide a studie instance"
%>


<!-- uploads the attachment data to the server -->
<g:javascript>

  jQuery(document).ready(function() {

    var uploader = new qq.FileUploader({
      // pass the dom node (ex. $(selector)[0] for jQuery users)
      element: document.getElementById('file-uploader'),
      // path to server-side upload script
      action: "${g.createLink(controller: 'studie',action: 'uploadAttachment')}",
      onComplete: function(id, fileName, responseJSON){
        jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'studie',action: 'ajaxRenderAttchmentTable',id:studieInstance.id)}",success:function(data,textStatus){jQuery('#attachmentContent').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
      }
    });

            uploader.setParams({
           studie: '${studieInstance.id}'
        });
  });

</g:javascript>

<div id="attachments">
  <div>

    <g:if test="${flash.attachmentMessage}">
      <div class="message">${flash.attachmentMessage}</div>
    </g:if>


    <div id="attachmentContent">

      <g:render template="attachmentTable" model="[studieInstance:studieInstance]"/>
    </div>

    <div class="button-margin-top">
      <div id="file-uploader">
        <noscript>
          <p>Please enable JavaScript to use file uploader.</p>
          <!-- or put a simple form for upload here -->
        </noscript>
      </div>

    </div>
  </div>
  <div>

  </div>
</div>