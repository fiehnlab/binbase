<g:setProvider library="jquery"/>


<g:javascript>
    $(document).ready(function () {
        // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
        $("#dialog:ui-dialog").dialog("destroy");

        $("#dialog-modal").dialog({
            height: 190,
            width: 450,
            modal: true
        });
    });
</g:javascript>

<%
    def pageId = System.currentTimeMillis()
    def sessionId = System.currentTimeMillis().toString()
%>
<div id="dialog-modal" title="Delete study dialog">

    <div id="page_${pageId}">
        <g:form controller="studie" action="delete">

            <div class="element">
                <div class="description">are you sure you want to delete this study?</div>

            </div>
            <div class="element">
                <div id="progress_target"></div>

                <div id="progress_info"></div>
            </div>
            <g:jprogress progressTarget="progress_target" progressId="${sessionId}"
                         message="please wait for the delete to finish" trigger="submit"/>

            <div class="element">
                <div class="myButton">
                    <div class="buttons">
                        <g:submitButton name="submit" value="yes" class="remove" id="submit"/>
                    </div>
                </div>
            </div>

            <g:hiddenField name="sessionId" value="${sessionId}"/>
            <g:hiddenField name="pageId" value="${pageId}"/>
            <g:hiddenField name="id" value="${studie.id}"/>
        </g:form>
    </div>
</div>