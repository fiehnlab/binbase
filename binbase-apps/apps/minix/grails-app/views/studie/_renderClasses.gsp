<%@ page import="minix.ReactionBlankClass; minix.ExperimentClass" %>


<g:each in="${studieInstance.classes}" var="c">
    <!-- render experiment class -->
    <g:if test="${c instanceof ExperimentClass}">
        <g:render template="/experimentClass/displayClass" model="[clazz:c,locked:locked]"/>
    </g:if>
    <g:else>
        <g:render template="/baseClass/displayClass" model="[clazz:c]"/>
    </g:else>

</g:each>