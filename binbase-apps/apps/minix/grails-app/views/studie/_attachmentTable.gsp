<g:setProvider library="jquery"/>

<g:if test="${studieInstance.attachments.size() == 0}">
    <div class="void">
        <div class="message">
            sorry at this time there are no attachments available.
        </div>
    </div>
</g:if>
<g:else>

    <!-- deletes an attachment -->
    <g:javascript>

        function deleteAttachment(reference) {
            $(document).ready(function() {
                $('#' + reference).remove();
            });
        }
    </g:javascript>

    <div id="attachmentDeleteMessage"></div>
    <table id="attachmentTable" class="colored">
        <thead>
        <tr>
            <th>name</th>
            <th>date</th>
            <th>time</th>
            <th>remove</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${studieInstance.attachments}" var="r">

            <tr id="attachment_${r?.id}">
                <td><g:link class="download" controller="BBDownload" action="download" id="${r?.id}"><span
                        class="alt">${r?.fileName}</span></g:link></td>
                <td><g:formatDate date="${r?.uploadDate}" format="yyyy-MM-dd"/></td>
                <td><g:formatDate date="${r?.uploadDate}" format="hh:mm"/></td>
                <td><g:remoteLink after="deleteAttachment('attachment_${r?.id}');" update="attachmentDeleteMessage"
                                  controller="fileAttachment" action="delete_ajax" class="remove" id="${r?.id}"><span
                            class="alt">delete</span></g:remoteLink></td>
            </tr>
        </g:each>
        </tbody>
    </table>

</g:else>
