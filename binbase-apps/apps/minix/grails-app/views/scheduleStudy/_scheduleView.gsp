<div class="textDescription">
    the following studies are being scheduled, with there associated samples. Please wait for the progress to finish.
</div>



<div class="element">

    <table class="contentTable">
        <thead>
        <tr>
            <th>study id</th>
            <th>description</th>
            <th>destination</th>
            <th>reference</th>
            <th>scheduling progress</th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${toSchedule}" var="schedule">
            <tr id="row_loaded_${schedule.study.id}">
                <td>${schedule.study.id}</td>

                <td title="${schedule.study.description}">${schedule.study.description.length() <= 30 ? schedule.study.description : schedule.study.description[0..30]}...</td>
                <td>${schedule.destination}</td>
                <td>${schedule.reference}</td>
                <td><div class="element" id="scheduleProgress_${schedule.study.id}"></div></td>

                <g:set var="sessionId" value="${new Date().time}"/>
                <g:javascript>

                    var selector = "#row_loaded_${schedule.study.id}";
                    var sessionId = '${sessionId}';
                    var url = "${g.createLink(controller: "scheduleStudy", action: "ajaxRenderProgress")}/"+sessionId;
                    var backlink = "${g.createLink(controller: "scheduleStudy", action: "ajaxUpdateColumn")}";

                    $(selector).ready( function(){
                        scheduleStudy(${schedule.study.id},'${schedule.url}','${schedule.destination}','${schedule.reference}','${schedule.classes}',sessionId,url,backlink);
                    });
                </g:javascript>
            </tr>

        </g:each>

        </tbody>
    </table>
</div>