<%@ page import="edu.ucdavis.genomics.metabolomics.util.statistics.data.DataFile" %>
<%
    assert file != null, "please provide us with a valid datafile"
    assert file instanceof DataFile, "please make sure that the instance file if of type DataFile"
%>

<div class="header">

    Generated Aquisition File

</div>

<div class="description">
    this is your generated aquisition file. You can either download it here or find it under the study attachments.</div>
<ul class="none-vertical">
    <li>
        <g:link class="download" controller="BBDownload" action="download" id="${attachment?.id}"><span
                class="alt">download</span></g:link>
        <g:link class="showDetails" controller="studie" action="show" id="${studie?.id}"><span
                class="alt">view studie</span></g:link>

    </li>
</ul>

<div class="element">
    <table>
        <thead>
        <g:each in="${file.getRow(0)}" var="entry">
            <th>${entry}</th>
        </g:each>
        </thead>
        <tbody>
        <%
            for (int i = 1; i < file.getRowCount(); i++) {
        %>
        <tr>
            <g:each in="${file.getRow(i)}" var="entry">
                <td>${entry}</td>
            </g:each>

        </tr>

        <%
            }
        %>
        </tbody>
    </table>
</div>