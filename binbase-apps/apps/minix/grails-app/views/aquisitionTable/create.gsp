<%@ page import="minix.TreatmentSpecific; minix.Studie" %>

<html>
<head>

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'binbaseSchedule.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'aquisitionTable.css')}"/>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

    <g:set var="entityName" value="${message(code: 'studie.label', default: 'study')}"/>
    <title>Generate Aquisition Table</title>

</head>

<body>
<g:render template="/shared/navigation"/>

<g:javascript>
    jQuery(document).ready(function() {


        $("#instrument").bind("change", function() {

            jQuery.ajax({   data: "id=" + this.value,type:'POST', url:"${createLink(controller: 'aquisitionTable',action: 'ajax_renderMethodPage ')}",success:function(data, textStatus) {
                jQuery('#instrumentSelection').html(data);
            },error:function(XMLHttpRequest, textStatus, errorThrown) {
            }});

        });
    });
</g:javascript>
<g:javascript>
    jQuery(document).ready(function() {


        $('#generate_samples').change(function () {
            if ($('#generate_samples').is(":checked")) {
                $('#showGenerateDataPicker').show();
                $('#generate_calibration_class').removeAttr("disabled");
                $('#generate_quality_class').removeAttr("disabled");
                $('#generate_reaction_blank_class').removeAttr("disabled");
                $('#randomize_table').removeAttr("disabled");


            }
            else {
                $('#showGenerateDataPicker').hide();
                $('#generate_calibration_class').attr("disabled", true);
                $('#generate_quality_class').attr("disabled", true);
                $('#generate_reaction_blank_class').attr("disabled", true);
                $('#randomize_table').attr("disabled",true);


            }

        });
    });
</g:javascript>

<div class="body">
<g:form controller="aquisitionTable" name="">

    <g:hiddenField name="studieId" value="${studie.id}"/>

    <div class="design">

        <div class="left">
            <div class="header">

                Information

            </div>

            <div class="left_information_box">
                <p><g:message code="description.aquisition"/></p>
            </div>

        </div>

        <div class="left-center-column">

            <div class="step-dialog">

                <!-- this div contains the generated table -->
                <div id="resultForAquisitionTable">

                    <div class="header">
                        Instrument aquisition datatable generation
                    </div>

                    <g:if test="${flash.message}">
                        <div class="message">${flash.message}</div>
                    </g:if>

                    <div class="description">
                        please enter the required information to generate the aquisition table for your selected instrument
                    </div>


                    <div class="standardWidth">
                        <div class="content-box">
                            <div class="content-header">BinBase specific settings</div>

                            <div class="content-inner-content">

                                <div class="element">

                                    <div class="textDescription">
                                        please select your instrument
                                    </div>

                                    <div class="textField">
                                        <g:select from="${studie.architecture.instruments}" name="instrument"
                                                  optionKey="id"
                                                  noSelection="['':'-please select your instrument-']"/>
                                    </div>

                                    <div class="textDescription">
                                        please select if you want to generate BinBase comform filenames
                                    </div>


                                    <g:if test="${studie.sampleFilesGenerated}">
                                        <div class="textDescription">

                                            <div class="errors">
                                                filenames were already generated for this study so please make sure you really want to regenerate them! Pressing this button will overwrite all filenames for this study design.
                                            </div>
                                        </div>
                                    </g:if>

                                    <div class="textField">

                                        <g:checkBox name="generate_samples" checked="true"/>
                                    </div>


                                    <div class="textDescription">
                                        would you like to add a class containing the standard samples for the calibration?
                                    </div>

                                    <div class="textField">

                                        <g:checkBox name="generate_calibration_class" checked="true"/>
                                    </div>

                                    <div class="textDescription">
                                        would you like to add a class containing the recommended quality control samples?
                                    </div>

                                    <div class="textField">

                                        <g:checkBox name="generate_quality_class" checked="true"/>
                                    </div>

                                    <div class="textDescription">
                                        would you like to add a class containing the recommended reaction blanks?
                                    </div>

                                    <div class="textField">
                                        <g:checkBox name="generate_reaction_blank_class" checked="true"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                <div class="standardWidth">
                    <div class="content-box">
                        <div class="content-header">Randomization settings</div>

                        <div class="content-inner-content">

                            <div class="element">

                                <div class="textDescription">
                                    would you like to randomize your acquisition table
                                </div>

                                <div class="textField">
                                    <g:checkBox name="randomize_table" checked="true"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div id="showGenerateDataPicker">

                        <div class="standardWidth">

                            <div class="content-box ">
                                <div class="content-header">sample name generation settings</div>

                                <div class="content-inner-content">

                                    <div class="element">

                                        <div class="textDescription">
                                            <g:if test="${studie.sampleFilesGenerated}">

                                                <div class="errors">
                                                    the generated filename will overwrite all existing filenames for this study!
                                                </div>

                                            </g:if>
                                            <span class="scaffoldingPropertyLabel">
                                                start date:
                                            </span>
                                            <span class="scaffoldingPropertyValue">
                                                <div class="textField">

                                                    <g:datePicker name="calculationDate" value="${new Date()}"
                                                                  precision="day"
                                                                  noSelection="['':'-Choose-']"/>
                                                </div>
                                            </span>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="instrumentSelection">

                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
</g:form>

</body>
</html>
