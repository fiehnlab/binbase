<%@ page import="minix.GCTof" %>
<%
    assert gctof != null, "please provide a gctof instance"
    assert gctof instanceof GCTof, "please make sure the gctof variable is an actual gctof"
%>

<div class="standardWidth">

    <div class="content-box">
        <div class="content-header">data aquistion settings</div>

        <div class="content-inner-content">

            <div class="element">
                <div class="textDescription">

                    please select your dataprocessing method
                </div>

                <div class="textField">

                    <g:if test="${gctof.dpMethods.size() > 0}">
                        <g:select from="${gctof.dpMethods}" name="dp"
                                  noSelection="['':'-please select your data processing method-']" value="${gctof.dpMethods.iterator().next()}"/>
                    </g:if>
                    <g:else>
                        no methods defined!
                        <g:hiddenField name="dp" value=""/>
                    </g:else>
                </div>

                <div class="textDescription">

                    please select your MS method
                </div>

                <div class="textField">

                    <g:if test="${gctof.msMethods.size() > 0}">
                        <g:select from="${gctof.msMethods}" name="ms"
                                  noSelection="['':'-please select your massspec method-']" value="${gctof.msMethods.iterator().next()}"/>
                    </g:if>
                    <g:else>
                        no methods defined!
                        <g:hiddenField name="ms" value=""/>
                    </g:else>
                </div>

                <div class="textDescription">

                    please select your GC method
                </div>

                <div class="textField">

                    <g:if test="${gctof.gcMethods.size() > 0}">
                        <g:select value="${gctof.gcMethods.iterator().next()}" from="${gctof.gcMethods}" name="gc"
                                  noSelection="['':'-please select your gc method-']"/>
                    </g:if>
                    <g:else>
                        no methods defined!
                        <g:hiddenField name="gc" value=""/>
                    </g:else>
                </div>

                <g:hiddenField name="gctofId" value="${gctof.id}"/>
                <g:hiddenField name="progressId" value="progress_id_${gctof.id}"/>

            </div>
        </div>
    </div>

</div>

<div id="progressField">
    <g:submitToRemote action="generate_gctofs_with_progress" id="progressButton" name="progressButton"
                      value="generate table" update="resultForAquisitionTable"/>
</div>

<div class="button-margin-top">
    <g:jprogressDialog progressId="progress_id_${gctof.id}" trigger="progressButton"/>
</div>
