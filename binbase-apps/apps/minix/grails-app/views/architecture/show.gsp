<%@ page import="minix.Architecture" %>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="layout" content="main"/>
  <g:set var="entityName" value="${message(code: 'architecture.label', default: 'Architecture')}"/>
  <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

  <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>
<body>
<g:render template="/shared/navigation"/>



<div class="body">

  <div class="design">

    <div class="left">
      <div class="header">

        Information

      </div>

      <div class="left_information_box">
        <p><g:message code="description.architecture"/></p>
      </div>

    </div>
    <div class="left-center-column">

      <div class="header"><g:message code="default.show.label" args="[entityName]"/></div>
      <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
      </g:if>

      <div class="scaffoldingProperties">

        <span class="scaffoldingPropertyLabel">
          <g:message code="architecture.id.label" default="Id"/>
        </span>

        <span class="scaffoldingPropertyValue">

          ${fieldValue(bean: architectureInstance, field: "id")}

        </span>




        <span class="scaffoldingPropertyLabel">
          <g:message code="architecture.name.label" default="Name"/>
        </span>

        <span class="scaffoldingPropertyValue">

          ${fieldValue(bean: architectureInstance, field: "name")}

        </span>




        <span class="scaffoldingPropertyLabel">
          <g:message code="architecture.description.label" default="Description"/>
        </span>

        <span class="scaffoldingPropertyValue">

          ${fieldValue(bean: architectureInstance, field: "description")}

        </span>




        <span class="scaffoldingPropertyLabel">
          <g:message code="architecture.relatedController.label" default="Related Controller"/>
        </span>

        <span class="scaffoldingPropertyValue">

          ${fieldValue(bean: architectureInstance, field: "relatedController")}

        </span>




        <span class="scaffoldingPropertyLabel">
          <g:message code="architecture.relatedAction.label" default="Related Action"/>
        </span>

        <span class="scaffoldingPropertyValue">

          ${fieldValue(bean: architectureInstance, field: "relatedAction")}

        </span>




        %{--<span class="scaffoldingPropertyLabel">--}%
          %{--<g:message code="architecture.designs.label" default="Designs"/>--}%
        %{--</span>--}%

        %{--<span class="scaffoldingPropertyValue">--}%

          %{--<ul class="scaffoldingList">--}%
            %{--<g:each in="${architectureInstance.designs}" var="d">--}%
              %{--<li class="scaffoldingItem">--}%
                %{--<g:link class="showDetails" controller="studieDesign" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link>--}%
              %{--</li>--}%
            %{--</g:each>--}%
          %{--</ul>--}%

        %{--</span>--}%




        <span class="scaffoldingPropertyLabel">
          <g:message code="architecture.instruments.label" default="Instruments"/>
        </span>

        <span class="scaffoldingPropertyValue">

          <ul class="scaffoldingList">
            <g:each in="${architectureInstance.instruments}" var="i">
              <li class="scaffoldingItem">
                <g:link class="showDetails" controller="${i.class.simpleName}" action="show" id="${i.id}">${i?.encodeAsHTML()}</g:link>
              </li>
            </g:each>
          </ul>

        </span>




        %{--<span class="scaffoldingPropertyLabel">--}%
          %{--<g:message code="architecture.studies.label" default="Studies"/>--}%
        %{--</span>--}%

        %{--<span class="scaffoldingPropertyValue">--}%

          %{--<ul class="scaffoldingList">--}%
            %{--<g:each in="${architectureInstance.studies}" var="s">--}%
              %{--<li class="scaffoldingItem">--}%
                %{--<g:link class="showDetails" controller="studie" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link>--}%
              %{--</li>--}%
            %{--</g:each>--}%
          %{--</ul>--}%

        %{--</span>--}%
      </div>
    </div>
  </div>
</div>
</body>
</html>
