
<%@ page import="minix.Species" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'species.label', default: 'Species')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${speciesInstance}">
            <div class="errors">
                <g:renderErrors bean="${speciesInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${speciesInstance?.id}" />
                <g:hiddenField name="version" value="${speciesInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="name"><g:message code="species.name.label" default="Name" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: speciesInstance, field: 'name', 'errors')}">
                                    <g:textField name="name" value="${speciesInstance?.name}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="kingdom"><g:message code="species.kingdom.label" default="Kingdom" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: speciesInstance, field: 'kingdom', 'errors')}">
                                    <g:select name="kingdom.id" from="${minix.Kingdom.list()}" optionKey="id" value="${speciesInstance?.kingdom?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="organs"><g:message code="species.organs.label" default="Organs" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: speciesInstance, field: 'organs', 'errors')}">
                                    <g:select name="organs" from="${minix.Organ.list()}" multiple="yes" optionKey="id" size="5" value="${speciesInstance?.organs}" />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
