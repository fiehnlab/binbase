<%@ page import="minix.Species" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'species.label', default: 'Species')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<g:render template="/shared/navigation"/>



<div class="body">

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <div class="design">

        <div class="left-thrirty">

            <div class="header">

                Information

            </div>

            <div class="left_information_box">
                <p><g:message code="description.species"/></p>
            </div>


            <div class="topSpacer"></div>

            <div class="header">
                Species details
            </div>

            <div class="scaffoldingProperties">

                <span class="scaffoldingPropertyLabel">
                    <g:message code="species.name.label" default="Name"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    <span id="updateName"
                          class="button-float-right">${fieldValue(bean: speciesInstance, field: "name")}</span><g:remoteLink
                        class="edit" action="ajaxEditName"
                        update="updateName" id="${speciesInstance.id}"><span
                            class="hideText">edit</span></g:remoteLink>
                </span>




                <span class="scaffoldingPropertyLabel">
                    <g:message code="species.kingdom.label" default="Kingdom"/>
                </span>

                <span class="scaffoldingPropertyValue">
                    <g:if test="${speciesInstance?.kingdom != null}">
                        <g:link class="showDetails" controller="kingdom" action="show"
                                id="${speciesInstance?.kingdom?.id}">${speciesInstance?.kingdom?.encodeAsHTML()}</g:link>
                    </g:if>
                </span>

            </div>

        </div>

        <div class="right-seventy left-side-border">
            <div class="header">
                Associated Studies
            </div>

            <div class="element">
                <table>
                    <thead>
                    <tr>
                        <g:sortableColumn property="id" title="Id"/>
                        <g:sortableColumn property="description" title="Description"/>
                        <th>Architecture</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${studieInstanceList}" status="i" var="studieInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                            <td><g:link controller="studie" action="show"
                                        id="${studieInstance.id}">${studieInstance.id?.encodeAsHTML()}</g:link></td>

                            <td>${studieInstance.description?.encodeAsHTML()}</td>

                            <td>${studieInstance.architecture?.encodeAsHTML()}</td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>

                <div class="paginateButtons">
                    <g:paginate id="${speciesInstance.id}" action="show" controller="species" total="${studieTotal}"
                                offset="${session.studieSpeciesPagination?.offset}" params="${[paginate:'Studie']}"/>
                </div>
            </div>

            <div class="header">
                Associated Organs
            </div>

            <div class="element">
                <table>
                    <thead>
                    <tr>
                        <g:sortableColumn property="id" title="Id"/>
                        <g:sortableColumn property="name" title="Name"/>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${organs}" status="i" var="organInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                            <td><g:link controller="organ" action="show"
                                        id="${organInstance.id}">${organInstance.id?.encodeAsHTML()}</g:link></td>

                            <td>${organInstance.name?.encodeAsHTML()}</td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>

                <div class="paginateButtons">
                    <g:paginate id="${speciesInstance.id}" action="show" controller="species" total="${organTotal}"
                                max="5" offset="${session.organPagination?.offset}" params="${[paginate:'Organ']}"/>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
