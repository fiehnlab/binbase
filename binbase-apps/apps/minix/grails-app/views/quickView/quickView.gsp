<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>

    <meta name="layout" content="main"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'retentionindex.css?id=1')}"/>

    <title>Home</title>
</head>

<body>

<g:javascript library="flot/jquery.flot"/>
<g:javascript library="flot/jquery.flot.selection"/>
<g:javascript library="flot/jquery.flot.time"/>


<div class="body">

    <div class="element">
        <div class="header">Loading existing report</div>
    </div>
    <!-- load this url into the content page -->
    <div id="content">

    </div>
</div>

<g:javascript>
    $(document).ready(function(){
        $("#content").load("${url}");
    });
</g:javascript>

</body>

</html>