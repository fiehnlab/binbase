<%@ page import="minix.ShiroUser" %>
<g:setProvider library="jquery"/>

<!-- defines the progres bar -->
<g:javascript>

    function initiateScheduling(session) {

        startValidation(session);
    }

        /**
        * starts the validation process
        * @param session
        */
    function startValidation(session) {
        $(document).ready(function() {

            $("#sampleValidationProgress").progressbar({value:0});
            $("#sampleValidationProgressMessage").html("<div class='message'>please wait we are checking if all the files exist...</div>");

            $("#error").empty();

            progressTimer = setInterval((function(session) {
                return function() {
                    $.ajax({
                        method: 'get',
                        url : "${g.createLink(controller: 'binbaseScheduling', action: 'ajaxRenderProgress')}/" + session,
                        dataType : 'text',
                        success: function (text) {
                            var value = parseFloat(text);

                            //we are done
                            if (value >= 100) {
                                $("#sampleValidationProgress").progressbar("value", 100);

                                clearInterval(progressTimer);

                                //monitor the schedulung now
                                startScheduling(session);

                            }

                                //an un exspected error happened
                            else if (value < 0) {
                                clearInterval(progressTimer);
                                $("#sampleValidationProgress").progressbar("destroy");
                                $("#sampleValidationProgressMessage").html("<div class='errors'>there was some kind of error...</div>");

                            }
                                //update the progress bar
                            else {
                                $("#sampleValidationProgress").progressbar("value", value);
                            }
                        }
                    });
                };
            })(session), 250);

        });
    }

        /**
        * starts the scheduling process
        * @param session
        */
    function startScheduling(session) {
        $(document).ready(function() {

            $("#sampleSchedulingProgress").progressbar({value:0});
            $("#sampleSchedulingProgressMessage").html("<div class='message'>please wait we are now scheduling your experiment...</div>");
            $("#error").empty();

            progressTimer2 = setInterval((function(session) {
                return function() {
                    $.ajax({
                        method: 'get',
                        url : "${g.createLink(controller: 'binbaseScheduling', action: 'ajaxRenderProgress')}/" + session,
                        dataType : 'text',
                        success: function (text) {
                            var value = parseFloat(text);

                            //we are done
                            if (value >= 100) {
                                $("#sampleSchedulingProgress").progressbar("value", 100);
                                $("#sampleSchedulingProgress").progressbar("destroy");

                                $("#sampleSchedulingProgressMessage").empty();

                                clearInterval(progressTimer2);
                            }
                            else if (value < 0) {
                                clearInterval(progressTimer2);
                                $("#sampleSchedulingProgress").progressbar("destroy");
                                $("#sampleSchedulingProgressMessage").html("<div class='errors'>there was some kind of error...</div>");

                            }
                                //update the progress bar
                            else {
                                $("#sampleSchedulingProgress").progressbar("value", value);
                            }
                        }
                    });
                };
            })(session), 250);
        });
    }

    function selectAllSamples(){
       $('span[id*="txt_sample"]').each(function(){
            var id = $(this).attr('id').replace("txt_sample_","checked_");
            $('form input:checkbox[id='+id+']').attr('checked', true);
        });

    }

    function unselectMissingSamples(){

        $('span[id*="txt_sample"]').each(function(){

                var id = $(this).attr('id').replace("txt_sample_","checked_");

                if($(this).hasClass("file_false")){
                    $('form input:checkbox[id='+id+']').attr('checked', false);
                }
        })
    }

//callbacks of all events
    var callbacks = new Object();

</g:javascript>

<div class="">

<div class="element">
    <div class="description">
        please select the samples you wish to export and make sure that these also exist!
    </div>

</div>

<div id="sampleValidation">
    <div id="sampleValidationProgressMessage">

    </div>

    <div id="sampleValidationProgress">

    </div>


    <div id="sampleSchedulingProgressMessage">

    </div>

    <div id="sampleSchedulingProgress">

    </div>

    <div id="error"></div>

</div>

<g:if test="${success == false}">
    <div class="errors">
        sorry it looks like some of your selected files are not available. Please make sure they exist before you try again!
    </div>
</g:if>
<g:set var="sessionId" value="${new Date().time}"/>

<g:form name="selectionTable">

<div class="element">

    <span class="myButton">
        <div class="buttons">
            <g:submitToRemote class="next" before="initiateScheduling(${sessionId});"
                              update="[success: 'next', failure: 'error']" value="schedule calculation"
                              name="scheduleCalculation" action="ajaxValidateSampleForExport"
                              style="width: 250px; margin-right: 5px;"/>
        </div>
    </span>

    <span class="myButton">
        <div class="buttons">
            <input class="check" onclick="selectAllSamples();" type="button" name="selectAll"
                   value="select all samples" style="width: 250px;margin-left: 5px;"/>
        </div>
    </span>
    <span class="myButton">
        <div class="buttons">
            <input class="check" onclick="unselectMissingSamples();" type="button" name="unselectMissing"
                   value="unselect missing samples" style="width: 250px; margin-left: 5px;"/>
        </div>
    </span>

</div>

<g:each in="${studie.classes}" var="clazz">

    <div class="element">
        <g:javascript>
                    callbacks[${clazz.id}] = $.Callbacks();
        </g:javascript>

        <div id="classSelectionTable_${clazz.id}_div">

            <div class="description">class: ${clazz.id}</div>
            <table id="classSelectionTable_${clazz.id}" class="colored" style='table-layout:fixed'>

                <thead>
                <tr>
                    <th>include</th>
                    <th>file name</th>
                    <th>class</th>
                    <th>species</th>
                    <th>organ</th>
                    <th>label</th>
                    <th>comment</th>

                    <th>has textfile</th>
                    <th>has netcdf file</th>
                </tr>
                </thead>

                <tfoot></tfoot>
                <tbody>

                <g:each in="${clazz.samples}" var="sample">

                    <%

                        //is this row checked
                        def checked = myParams?."checked_${sample.id}"

                        if (checked == null) {
                            checked = false
                        } else if (checked.toString().size() == 0) {
                            checked = false
                        } else {
                            checked = true
                        }

                        //is it a breaker
                        def breaker = myParams?."breaker_${sample.id}"

                        //is it included in the export
                        def export = myParams?."export_${sample.id}"

                        //has it a cdf file
                        def cdf = myParams?."hasCdf_${sample.id}"

                        //has it a txt file
                        def txt = myParams?."hasTXT_${sample.id}"
                    %>

                    <tr class="breaker_${breaker} export_${export}">
                        <td>
                            <g:checkBox name="checked_${sample.id}" checked="${checked}"/>
                        </td>
                        <td>
                            <g:hiddenField name="sample" value="${sample.id}"/>
                            <g:hiddenField name="sampleFileName" value="${sample.toString()}"/>

                            ${sample.toString()}</td>

                        <td><g:hiddenField name="class_${sample.id}" value="${clazz.id}"/>${clazz.id}</td>
                        <td>${clazz.species}</td>
                        <td>${clazz.organ}</td>
                        <td>${sample.label}</td>
                        <td>${sample.comment}</td>

                        <td>
                            <span id="txt_sample_${sample.id}" class="file_${txt}">
                                <g:if test="${txt instanceof java.lang.Boolean}">
                                    ${txt}
                                </g:if>
                                <g:else>
                                    <g:javascript>


                                         callbacks[${clazz.id}].add( function(){
                                         $.ajax({
                                                                                                                     async: true,

                                            method: 'get',
                                            url : "${g.createLink(controller: 'binbaseScheduling', action: 'ajaxHasTxtFile', id: sample.id)}",
                                            dataType : 'text',
                                            success: function (text) {
                                                $("#txt_sample_${sample.id}").text(text).removeClass("file_").removeClass("file_false").removeClass("file_true").addClass("file_"+text);
                                                 }
                                                });
                                            });
                                    </g:javascript>
                                </g:else>
                            </span>
                        </td>
                        <td>
                            <span id="cdf_sample_${sample.id}" class="file_${cdf}">
                                <g:if test="${cdf instanceof java.lang.Boolean}">
                                    ${cdf}
                                </g:if>
                                <g:else>
                                    <g:javascript>

                                                                         callbacks[${clazz.id}].add( function(){

                                                                         $.ajax({
                                                                            async: true,
                                                                            method: 'get',
                                                                            url : "${g.createLink(controller: 'binbaseScheduling', action: 'ajaxHasCDFFile', id: sample.id)}",
                                                                            dataType : 'text',
                                                                            success: function (text) {
                                                                                $("#cdf_sample_${sample.id}").text(text).removeClass("file_").removeClass("file_false").removeClass("file_true").addClass("file_"+text);
                                                                            }

                                                                            });

                                                                            });

                                    </g:javascript>
                                </g:else>
                            </span></td>

                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
    </div>


    <g:javascript>
                    callbacks[${clazz.id}].fire();
    </g:javascript>

</g:each>

<g:hiddenField name="column" value="${column}"/>
<g:hiddenField name="studie" value="${studie.id}"/>

<g:hiddenField name="sessionId" value="${sessionId}"/>


<div class="element">

    <g:if test="${ShiroUser.hasAdminRights() && studie.databaseColumn != null}">

        <g:checkBox name="exportOnly" checked="false"/>
        <span class="description"><label for="exportOnly">only export the data, do not import them again</label>
        </span>

    </g:if>
</div>


<g:if test="${referenceStudy}">

    <div class="element">

        <div class="description">reference study: ${referenceStudy.id}</div>

        <table>
            <thead>
            <tr>
                <th>id</th>
                <th>title</th>
                <th>class count</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><g:link controller="studie" action="show" id="${referenceStudy.id}" target="_blank">${referenceStudy.id}</g:link></td>
                <td>${referenceStudy.title}</td>
                <td>${referenceStudy.classes.size()}</td>
            </tr>
            </tbody>
        </table>

        <g:hiddenField name="referenceStudy" value="${referenceStudy.id}"/>

    </div>

</g:if>
</g:form>
</div>
