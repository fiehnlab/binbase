<g:setProvider library="jquery"/>


<!-- display the scheduled experiemtn -->
<div class="element">

    <g:if test="${exportOnly == true}">
        <div class="message">
            congratulations, your study is scheduled for an export!
        </div>

    </g:if>
    <g:else>
        congratulations, your study is scheduled for an import and export.

    </g:else>
    <div class="header">
        Study Details
    </div>

    <div>
        <span>Study id:</span>
        <span><g:link controller="studie" action="show" id="${experiment.id}">${experiment.id}</g:link></span>
    </div>

    <div>

        <span>Database:</span>
        <span>${experiment.column}</span>
    </div>

    <div>

        <g:each in="${experiment.classes}" var="clazz">
            <div class="element">
                <table class="colored">
                    <thead>
                    <tr>
                        <th>class id</th>
                        <th>sample id</th>
                        <th>sample name</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${clazz.samples}" var="sample">
                        <tr>
                            <td>${clazz.id}</td>
                            <td>${sample.id}</td>
                            <td>${sample.name}</td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </g:each>
    </div>

</div>