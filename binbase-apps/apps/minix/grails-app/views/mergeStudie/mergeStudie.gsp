<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Oct 22, 2010
  Time: 3:38:15 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

    <title>Combine Studie Tool</title>
</head>

<body>
<g:render template="/shared/navigation"/>

<div class="body">

    <div class="design">
        <div class="left">
            <div class="header">

                Information

            </div>


            <div class="left_information_box">

                <p>
                    please select all the studies you would like to combine.
                </p>

                <p>
                    The result will be a new study, which can not be modified.
                </p>
            </div>

        </div>

        <div class="left-center-column">
            <div class="header">
                Welcome to the study merge tool
            </div>

            <div class="textDescription">

                This tool enables you to combine 2 or more studies with each other. The resulting study cannot be modified, but otherwise be used like any other study in the system.

            </div>


            <g:form name="mergeStudy">

                <div id="selectStudyDiv" class="textDescription">
                    <g:render template="selectStudy" model="[studies:studies"/>
                </div>



            </g:form>

        </div>
    </div>

</div>
</body>
</html>
