<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: 2/19/13
  Time: 3:01 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'showClass.css')}"/>

    <title>Create a sub-study</title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">
    <div id="next">
        <g:render template="createStudy" model="[studieInstance:studieInstance]"/>
    </div>
</div>
</body>
</html>