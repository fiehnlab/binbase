<g:setProvider library="jquery"/>



<script type="text/javascript">
    function countdown() {
        if (typeof countdown.counter == 'undefined') {
            countdown.counter = 5; // initial count
        }
        if (countdown.counter > 0) {
            document.getElementById('count').innerHTML = countdown.counter--;
            setTimeout(countdown, 1000);
        }
        else {
            location.href = "${g.createLink(controller: "studie",action: "show",id:result.id)}";
        }
    }
    countdown();

</script>

<div>

    congratulations, your sub study is done and you will
    be forwarded in <span id="count"></span> seconds to your study.

    <p>
If this page doesn't change, please press
<g:link controller="studie" action="show" id="${result.id}">this link</g:link> to continue.

               </p>