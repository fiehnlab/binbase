<table>
    <thead>
    <tr>
        <th>Task</th>
        <th>Id</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${queue}" var="job">
        <g:if test="${job instanceof edu.ucdavis.genomics.metabolomics.binbase.bci.job.SchedulerJob}">
            <tr>
                <td>${job.task}</td>
                <td>${job.id}</td>

            </tr>
        </g:if>
    </g:each>
    </tbody>
</table>