<table>
    <thead>
    <tr>
        <th>Database</th>
        <th>Class</th>
        <th>Sample Count</th>
    </tr>
    </thead>
    <tbody>
        <g:each in="${queue}" var="job">
            <tr>
                <td>${job.column}</td>
                <td>${job.id}</td>
                <td>${job.samples.length}</td>

            </tr>
        </g:each>
    </tbody>
</table>