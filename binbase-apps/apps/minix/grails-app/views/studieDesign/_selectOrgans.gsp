<g:setProvider library="jquery"/>

<%

    assert design != null, "you need to provide a 'design' variable for this template!"
    assert specie != null, "you need to provide a 'specie' variable for this template!"

    //contains our already assigned organs
    Set organs = new TreeSet()

    //build the organs for this species, ugly but doesn't work any other way
    design.designSpeciesConnections.each { def designSpeciesConnections ->

        if (designSpeciesConnections.species.equals(specie)) {

            organs = designSpeciesConnections.organs
        }

    }


%>

<g:javascript>
  $(document).ready(function() {

    $(function() {

      $("input")
      .filter(function() {
        return this.id.match(/organ.*/);
      })
      .autocomplete({
              source: '${g.createLink(action: "findOrgan", controller: "autoCompletion")}', dataType:"json"
              }
      );
    })
  });
</g:javascript>

<div id="selectOrgansDiv_${specie.id}">

    <div class="textField">

        <g:textField name="organ_${specie.id}_0"/>

    </div>

    <div class="button-margin-left">
        <g:submitToRemote class="add" action="ajaxRenderOrgans" name="addOrgan_${specie.id}" id="addOrgan_${specie.id}"
                          value="define another organ" update="selectOrgansDiv_${specie.id}"/>
    </div>
    <g:each var="organ" in="${organs}" status="i">

        <div class="textField">
            <g:textField name="organ_${specie.id}_${i+1}" value="${organ.name}"/>
        </div>
    </g:each>

</div>
