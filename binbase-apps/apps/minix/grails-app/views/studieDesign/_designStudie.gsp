<g:setProvider library="jquery"/>
<link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>

<div class="design">
    <div class="left">
        <g:render template="stepView" model="[design:design]"/>
    </div>

    <div class="left-center-column">
        <g:form name="designStudy">

            <div class="step-dialog">
                <div class="header">
                    step 1 - species design and title definition
                </div>

                <div class="description">
                    please define the title of our study and all the different species, which are included in this studie
                </div>

                <br>

                <g:render template="/shared/validation_error" model="[errorBean:design]"/>

                <g:if test="${flash.message != null &&flash.message.toString().length() > 0}">
                    <div class="fillField">

                        <div class="message">
                            ${flash.message}
                        </div>
                    </div>
                </g:if>

                <div class="textDescription">
                    please select the used architecture for your study. This defines how your data will be processed at a later point
                </div>

                <div class="textField">
                    <g:select from="${architectures}" name="architecture" id="architecture"
                              value="${design?.architecture}"/>
                </div>

                <div class="textDescription">
                    please enter the title of your studie into the textfield below
                </div>


                <div class="textField">
                    <div class="${hasErrors(bean: design, field: 'title', 'errors')}">

                        <g:textField name="title" value="${design?.title}"/>
                    </div>
                </div>

                <div class="textDescription">
                    please define all the different species your experiment will contain. You can add another species to this design, using the 'add' button.
                </div>

                <div id="selectSpeciesDiv">
                    <div class="${hasErrors(bean: design, field: 'speciees', 'errors')}">
                        <g:render template="selectSpecies" model="[species:design?.speciees]"/>
                    </div>
                </div>

                <div class="textDescription">
                    after you filled out all the fields please press the next button to define your organs
                </div>

            </div>

            <div class="button-margin-top">
                <g:submitToRemote class="next" action="ajaxSaveStudie" name="submitToOrgan" id="submitToOrgan"
                                  value="continue with step 2" update="next"/>
            </div>

            <!-- store the hidden id-->
            <g:hiddenField name="designId" id="designId" value="${design?.id}"/>
        </g:form>

    </div>
</div>