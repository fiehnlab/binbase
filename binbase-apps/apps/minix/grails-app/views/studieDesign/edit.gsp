
<%@ page import="minix.StudieDesign" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'studieDesign.label', default: 'StudieDesign')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${createLink(uri: '/')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></span>
            <span class="menuButton"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></span>
        </div>
        <div class="body">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${studieDesignInstance}">
            <div class="errors">
                <g:renderErrors bean="${studieDesignInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <g:hiddenField name="id" value="${studieDesignInstance?.id}" />
                <g:hiddenField name="version" value="${studieDesignInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="title"><g:message code="studieDesign.title.label" default="Title" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studieDesignInstance, field: 'title', 'errors')}">
                                    <g:textField name="title" value="${studieDesignInstance?.title}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="generatedStudie"><g:message code="studieDesign.generatedStudie.label" default="Generated Studie" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studieDesignInstance, field: 'generatedStudie', 'errors')}">
                                    <g:select name="generatedStudie.id" from="${minix.Studie.list()}" optionKey="id" value="${studieDesignInstance?.generatedStudie?.id}" noSelection="['null': '']" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="step"><g:message code="studieDesign.step.label" default="Step" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studieDesignInstance, field: 'step', 'errors')}">
                                    <g:select name="step" from="${minix.step.Step?.values()}" value="${studieDesignInstance?.step}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="architecture"><g:message code="studieDesign.architecture.label" default="Architecture" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studieDesignInstance, field: 'architecture', 'errors')}">
                                    <g:select name="architecture.id" from="${minix.Architecture.list()}" optionKey="id" value="${studieDesignInstance?.architecture?.id}"  />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="designSpeciesConnections"><g:message code="studieDesign.designSpeciesConnections.label" default="Design Species Connections" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studieDesignInstance, field: 'designSpeciesConnections', 'errors')}">
                                    
<ul>
<g:each in="${studieDesignInstance?.designSpeciesConnections?}" var="d">
    <li><g:link controller="designSpeciesConnection" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="designSpeciesConnection" action="create" params="['studieDesign.id': studieDesignInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'designSpeciesConnection.label', default: 'DesignSpeciesConnection')])}</g:link>

                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="numberOfSamples"><g:message code="studieDesign.numberOfSamples.label" default="Number Of Samples" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studieDesignInstance, field: 'numberOfSamples', 'errors')}">
                                    <g:textField name="numberOfSamples" value="${fieldValue(bean: studieDesignInstance, field: 'numberOfSamples')}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="speciees"><g:message code="studieDesign.speciees.label" default="Speciees" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studieDesignInstance, field: 'speciees', 'errors')}">
                                    <g:select name="speciees" from="${minix.Species.list()}" multiple="yes" optionKey="id" size="5" value="${studieDesignInstance?.speciees}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="treatmentsSpecifics"><g:message code="studieDesign.treatmentsSpecifics.label" default="Treatments Specifics" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studieDesignInstance, field: 'treatmentsSpecifics', 'errors')}">
                                    <g:select name="treatmentsSpecifics" from="${minix.TreatmentSpecific.list()}" multiple="yes" optionKey="id" size="5" value="${studieDesignInstance?.treatmentsSpecifics}" />
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                  <label for="user"><g:message code="studieDesign.user.label" default="User" /></label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean: studieDesignInstance, field: 'user', 'errors')}">
                                    <g:select name="user.id" from="${minix.ShiroUser.list()}" optionKey="id" value="${studieDesignInstance?.user?.id}"  />
                                </td>
                            </tr>
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" /></span>
                    <span class="button"><g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
