<%@ page import="minix.step.Step" %><g:setProvider library="jquery"/>

<!-- needed otherwise the jquery update doesn't work right and render the odd and even cells wrong -->
<div>

  <div id="studieDesignActionResult">

  </div>
  <div class="void">
    <table class="colored">
      <thead>
      <tr>

        <g:sortableColumn property="title" title="Title"/>

        <g:sortableColumn property="step" title="Step"/>

        <th>Architecture</th>

      </tr>
      </thead>
      <tbody>
      <g:each in="${studieDesignInstanceList}" status="i" var="studieDesignInstance">
        <tr id="studieDesignInstance_id_${studieDesignInstance?.id}">

          <td>${studieDesignInstance.title?.encodeAsHTML()}</td>

          <td>${studieDesignInstance.step?.getMessage()?.encodeAsHTML()}</td>

          <td>${studieDesignInstance.architecture?.encodeAsHTML()}</td>

        </tr>
      </g:each>
      </tbody>
    </table>

    <g:paginate total="${studieDesignCount}"/>
  </div>
</div>