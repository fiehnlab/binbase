<%@ page import="minix.ShiroUser; minix.StudieDesign" %>


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <title>StudieDesign List</title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">

    <div class="design">

        <div class="left">
            <div class="header">

                Information

            </div>

            <div class="left_information_box">
                <p><g:message code="description.design"/></p>
            </div>

        </div>
        <div class="left-center-column">
            <div class="header">StudieDesign List</div>
            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>
            <div class="element">
                <ul class="none-horizontal">
                    <!-- here we are registering our gctofs systems -->
                    <li><g:link class="add" controller="studieDesign" action="create"><g:message code="new.studie.design"/></g:link></li>
                </ul>
            </div>

            <div class="element">
<g:render template="table/designTable" model="[studieDesignCount:studieDesignCount,studieDesignInstanceList:studieDesignInstanceList]"/>


<g:if test="${ShiroUser.hasAdminRights()}">
    <div class="element">
        <ul class="none-horizontal">
            <!-- here we are registering our gctofs systems -->
            <li>
                <g:link class="list" controller="studieDesign" action="listAll">list all accesible designs</g:link>
            </li>
        </ul>
    </div>
</g:if>
            </div>
        </div>

    </div>
</div>
</body>
</html>

