<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: 10/16/13
  Time: 2:13 PM
  To change this template use File | Settings | File Templates.
--%>


<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>

    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="Retention Index Markers"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>

    <export:resource/>
</head>

<body>


<g:javascript>

function loadRetentionIndexMarkerTable( database,  destinationField){

    $(document).ready(function() {
        var selector = "#" + destinationField;
        $(selector).load(
            "${g.createLink(controller: 'binBaseRetentionIndexMarker', action: 'ajaxLoadMarkers')}?db=" + database
        );
    });

}
</g:javascript>


<div class="body">
    <g:render template="/shared/navigation"/>

    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <div class="design">
        <div class="center-full-page">

            <div class="header">
                Configured retention index markers
            </div>

            <g:each in="${columns}" var="db">
                <div class="description">
                    Markers for ${db}
                </div>

                <div id="${db}">
                    <div class="message">please wait, we are loading the required data...</div>
                </div>
                <g:javascript>
                    loadRetentionIndexMarkerTable("${db}","${db}");
                </g:javascript>
            </g:each>
        </div>
    </div>
</body>
</html>
