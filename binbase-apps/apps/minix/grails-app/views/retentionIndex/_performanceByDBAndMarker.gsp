<div class="topSpacer">

    <g:each in="${standards}" var="standard">
        <div class="header">Performance for ${standard.name}</div>

        <g:each in="${instruments}" var="instrument">

            <div id="${instrument.id}_${standard.id}"></div>

            <g:javascript>
                    var selector = "#${instrument.id}_${standard.id}";
                    $(selector).load("${g.createLink(action: "ajaxShowRetentionIndexMarkerPerformanceByDBAndInstrumentAndStandard", controller: "retentionIndex", params: [instrument: instrument.id, standard: standard.id, standard_name: standard.name, column: column, from: from, to: to, mode: params.mode])}");
            </g:javascript>
        </g:each>

    </g:each>
</div>