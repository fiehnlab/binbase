
<%@ page import="minix.BaseClass" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'baseClass.label', default: 'BaseClass')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>
<body>
<g:render template="/shared/navigation"/>

<div class="body">
    <div class="design">

        <div class="left">
            <div class="header">

                Information

            </div>


            <div class="left_information_box">
                please provide the required informations to create this object
            </div>

        </div>
        <div class="left-center-column">

            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${baseClassInstance}">
                <div class="errors">
                    <g:renderErrors bean="${baseClassInstance}" as="list"/>
                </div>
            </g:hasErrors>
        <div class="element">

            <g:form method="post" >
                <g:hiddenField name="id" value="${baseClassInstance?.id}"/>
                <g:hiddenField name="version" value="${baseClassInstance?.version}"/>
                <div class="scaffoldingProperties">

                    
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="organ"><g:message code="baseClass.organ.label" default="Organ"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: baseClassInstance, field: 'organ', 'errors')}">
                            <g:select name="organ.id" from="${binbase.web.core.BBOrgan.list()}" optionKey="id" value="${baseClassInstance?.organ?.id}" noSelection="['null': '']" />
                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="species"><g:message code="baseClass.species.label" default="Species"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: baseClassInstance, field: 'species', 'errors')}">
                            <g:select name="species.id" from="${binbase.web.core.BBSpecies.list()}" optionKey="id" value="${baseClassInstance?.species?.id}" noSelection="['null': '']" />
                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="metadata"><g:message code="baseClass.metadata.label" default="Metadata"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: baseClassInstance, field: 'metadata', 'errors')}">
                            <g:select name="metadata" from="${binbase.web.core.BBMetadata.list()}" multiple="yes" optionKey="id" size="5" value="${baseClassInstance?.metadata}" />
                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="experiment"><g:message code="baseClass.experiment.label" default="Experiment"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: baseClassInstance, field: 'experiment', 'errors')}">
                            <g:select name="experiment.id" from="${binbase.web.core.BBExperiment.list()}" optionKey="id" value="${baseClassInstance?.experiment?.id}" noSelection="['null': '']" />
                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="samples"><g:message code="baseClass.samples.label" default="Samples"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: baseClassInstance, field: 'samples', 'errors')}">
                            
<ul>
<g:each in="${baseClassInstance?.samples?}" var="s">
    <li><g:link controller="BBExperimentSample" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="BBExperimentSample" action="create" params="['baseClass.id': baseClassInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'BBExperimentSample.label', default: 'BBExperimentSample')])}</g:link>

                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="name"><g:message code="baseClass.name.label" default="Name"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: baseClassInstance, field: 'name', 'errors')}">
                            <g:textField name="name" value="${baseClassInstance?.name}" />
                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="comments"><g:message code="baseClass.comments.label" default="Comments"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: baseClassInstance, field: 'comments', 'errors')}">
                            <g:select name="comments" from="${minix.Comment.list()}" multiple="yes" optionKey="id" size="5" value="${baseClassInstance?.comments}" />
                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="externalIds"><g:message code="baseClass.externalIds.label" default="External Ids"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: baseClassInstance, field: 'externalIds', 'errors')}">
                            
<ul>
<g:each in="${baseClassInstance?.externalIds?}" var="e">
    <li><g:link controller="externalId" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>
<g:link controller="externalId" action="create" params="['baseClass.id': baseClassInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'externalId.label', default: 'ExternalId')])}</g:link>

                        </span>
                    </div>
                        
                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="studie"><g:message code="baseClass.studie.label" default="Studie"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: baseClassInstance, field: 'studie', 'errors')}">
                            <g:select name="studie.id" from="${minix.Studie.list()}" optionKey="id" value="${baseClassInstance?.studie?.id}"  />
                        </span>
                    </div>
                        

                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}"/></span>
                </div>
                </div>
            </g:form>
        </div>
    </div>
</div>
</body>
</html>
