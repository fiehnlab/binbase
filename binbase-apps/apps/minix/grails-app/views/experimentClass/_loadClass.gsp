<!-- dedicated template to load a class using ajax to reduce the rendering time -->
<div class="content-box">
    <div class="content-header">

        <g:if test="${includeCheck}">
            <g:javascript>

                /**
                 * checks all the samples or unchecks them depending on the given clazz, which has to be a clazz id
                 * @param clazz
                 */
                function selectAllSamplesOfClass(clazz) {

                    var origin = "#include_sample_" + clazz;
                    var checked = $(origin).is(':checked');

                    $('input[id*="include_sample_' + clazz + '_"]').each(function () {
                        var id = '#' + $(this).attr('id');
                        $(id).attr('checked', checked);
                    });

                }

            </g:javascript>
            <!-- possible to select all associated samples -->
            <g:checkBox name="include_sample_${clazz.id}" onclick="selectAllSamplesOfClass(${clazz.id});"/>
            <label for="include_sample_${clazz.id}">
                class: ${clazz.id} <g:if
                        test="${clazz.externalIds != null && clazz.externalIds.size() > 0}">- ${clazz.externalIds}</g:if>
            </label>
        </g:if>
        <g:else>
            class: ${clazz.id}<g:if
                test="${clazz.externalIds != null && clazz.externalIds.size() > 0}">- ${clazz.externalIds}</g:if>
        </g:else>
    </div>

    <div class="classProperties">

        <div id="action_result_${clazz.id}" class="classResult">
        </div>

        <g:render template="/shared/validation_error" model="[errorBean: clazz]"/>

        <div id="error_message_${clazz.id}">
        </div>

        <div class="metaData">
            <g:render template="/experimentClass/renderMetaData"
                      model="[clazz: clazz]"/>
        </div>

        <div class="classData">
            <div id="class_${clazz.id}_content">
                <g:render template="/experimentClass/displayClassTable"
                          model="[clazz: clazz, locked: locked, aquisitionTable: clazz.studie.canModify(), includeCheck: includeCheck]"/>
            </div>

        </div>

        <ul class="none-horizontal">
            <li>
                <g:remoteLink class="paste" update="dialog_${clazz.id}" controller="pasteDialog"
                              action="ajaxShow"
                              id="${clazz.id}">paste metadata</g:remoteLink>
            </li>
            <g:if test="${clazz.studie.canModify()}">

                <li>
                    <g:remoteLink class="add" update="class_${clazz.id}_content" controller="experimentClass"
                                  action="ajaxAddSample" id="${clazz.id}"
                                  onFailure="alert('error');">add sample</g:remoteLink>
                </li>

                <g:if test="${clazz.studie.classes.size() > 1}">
                    <li>
                        <g:remoteLink class="remove" update="deleteDialog_${clazz.id}"
                                      controller="experimentClass"
                                      action="showDeleteDialog" id="${clazz.id}">remove class</g:remoteLink>
                    </li>
                </g:if>

            </g:if>
        </ul>

        <div id="dialog_${clazz.id}"></div>
    </div>


    <div id="deleteDialog_${clazz.id}"></div>


    <div class="topSpacer"></div>
</div>