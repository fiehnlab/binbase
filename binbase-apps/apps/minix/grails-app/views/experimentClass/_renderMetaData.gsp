<!-- template used to eaasily modify the class after generation -->
<%@ page import="minix.Organ; minix.Species; minix.ShiroUser" %>

<div id="render_class_${clazz.id}_meta_data">

    <table id="meta_class_${clazz.id}_table" class="colored contentTable">
        <thead>
        <tr>
            <th>class name</th>
            <th>species</th>
            <th>organ</th>
            <th>treatment</th>
        </tr>
        </thead>
        <tbody>

        <tr>
            <g:if test="${ShiroUser.hasAdminRights()}">
                <td>${clazz.name}</td>
                <td>
                    <g:select id="render_class_${clazz.id}_meta_data_species" from="${Species.list()}"
                              value="${clazz.species.id}" name="speciesId" optionKey="id" optionValue="name"/>
                </td>
                <td>
                    <g:select id="render_class_${clazz.id}_meta_data_organ" from="${clazz.species.organs}"
                              value="${clazz.organ.id}"
                              name="organId" optionKey="id" optionValue="name"/>
                </td>
                <td>${clazz.treatmentSpecific.toParentString()}</td>
            </g:if>
            <g:else>
                <td>${clazz.name}</td>
                <td>${clazz.species}</td>
                <td>${clazz.organ}</td>
                <td>${clazz.treatmentSpecific.toParentString()}</td>
            </g:else>
        </tr>
        </tbody>
    </table>

</div>


<g:javascript>
    /**
    * watches species for change
    */

    $('#render_class_${clazz.id}_meta_data_species').change(function() {

        var speciesId = $(this).val();

        if(speciesId == ''){
            return false;
        }

        //update the serverside
        jQuery.ajax({type:'POST',data:{'value': this.value}, url: "${g.createLink(controller: 'experimentClass',action: 'ajaxChangeSpecies', id:clazz.id)}?speciesId="+speciesId,success:function(data,textStatus){jQuery('#render_class_${clazz.id}_meta_data').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
    });


    $('#render_class_${clazz.id}_meta_data_organ').change(function() {

        var organId = $(this).val();

        if(organId == ''){
            return false;
        }
        jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'experimentClass',action: 'ajaxChangeOrgan', id:clazz.id)}?organId="+organId,success:function(data,textStatus){jQuery('#render_class_${clazz.id}_meta_data').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;

    });

</g:javascript>