<g:setProvider library="jquery"/>

<%
    assert sample != null, "you need to provide a sample instance to this template"
    assert clazz != null, "you need to provide a class!"
%>

<!-- renderes a single row of a sample in the table -->
<tr id="row_sample_${sample.id}">

    <g:if test="${includeCheck}">
        <td><g:checkBox name="include_sample_${sample.experimentClass.id}_${sample.id}"/></td>
    </g:if>

    <g:if test="${sample.experimentClass.studie.canModify()}">

        <td>
            <g:remoteLink class="remove" action="ajaxRemoveSample" controller="experimentClass" name="ajaxRemoveSample"
                          id="${sample.id}" value="remove" update="action_result_${clazz.id}"
                          params="[sortBy: 'name', offset: '12']"><span class="hideText">remove</span></g:remoteLink>

        </td>
    </g:if>
    <td class="fileName-row">

        <div class="${hasErrors(bean: sample, field: 'fileName', 'errors')} version_row">

            <g:if test="${!sample.experimentClass.studie.canModify()}">
                <label class="version_select">${sample.fileName}_</label>
                <g:select class="version_select" from="${1..200}" name="sample_version_${sample.id}"
                          value="${sample.fileVersion}"/>


                <!-- sets the fileversion dynamicaly -->
                <g:javascript>
              $(function() {
              $("#sample_version_${sample.id}").change(function( ) {
                      jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'sample', action: 'ajaxSetFileVersion', id: sample.id)}",success:function(data,textStatus){jQuery('#action_result_${clazz.id}').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
                  });
                });

                </g:javascript>

            </g:if>
            <g:else>

                <g:textField class="shrink_to_fit" name="sample_${sample.id}" value="${sample.fileName}"/>

                <!-- sets the filename dynamicaly -->
                <g:javascript>

              $(function() {
              $("#sample_${sample.id}").observe_field(1, function( ) {
              jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'sample', action: 'ajaxSetFileName', id: sample.id)}",success:function(data,textStatus){jQuery('#action_result_${clazz.id}').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
              });
              });
                </g:javascript>

            </g:else>

        </div>
    </td>

    <td class="comment-row">
        <div class="${hasErrors(bean: sample, field: 'comment', 'errors')}">
            <g:textField class="shrink_to_fit" name="sample_comment_${sample.id}" value="${sample.comment}"/>

            <g:javascript>
              $(function() {
              $("#sample_comment_${sample.id}").observe_field(1, function( ) {
                      jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'sample', action: 'ajaxSetComment', id: sample.id)}",success:function(data,textStatus){jQuery('#action_result_${clazz.id}').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
                  });
                });
            </g:javascript>
        </div>
    </td>
    <td class="label-row">
        <div class="${hasErrors(bean: sample, field: 'label', 'errors')}">
            <g:textField class="shrink_to_fit" name="sample_label_${sample.id}" value="${sample.label}"/>


            <g:javascript>
              $(function() {
              $("#sample_label_${sample.id}").observe_field(1, function( ) {
                      jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'sample', action: 'ajaxSetLabel', id: sample.id)}",success:function(data,textStatus){jQuery('#action_result_${clazz.id}').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
                  });
                });
            </g:javascript>
        </div>
    </td>
</tr>