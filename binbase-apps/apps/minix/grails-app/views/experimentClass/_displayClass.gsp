<%@ page import="minix.Organ; minix.Species; minix.ShiroUser" %>
<%
    assert clazz != null, "sorry you need to provide a  variable with the name 'clazz'"
%>

<g:setProvider library="jquery"/>

<g:if test="${clazz instanceof minix.ExperimentClass}">
    <div id="class_${clazz.id}_description">

        <div id="class_${clazz.id}_content">
            <div class="message">please wait loading class ${clazz.id} from server</div>
        </div>


        <g:javascript>

           $(document).ready(function() {
                //render class to speed up the intial load of very very large studies...
                var selector = "#class_${clazz.id}_content";
                $(selector).load(
                    "${g.createLink(controller: 'experimentClass', action: 'ajaxLoadClass', id: clazz.id, params: [locked: locked, includeCheck: includeCheck])}"
                );
           });

        </g:javascript>
    </div>
</g:if>