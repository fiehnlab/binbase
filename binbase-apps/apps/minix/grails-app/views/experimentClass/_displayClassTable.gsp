<g:setProvider library="jquery"/>

<table id="class_${clazz.id}_table" class="colored contentTable">
    <thead>
    <tr class="headerRow_${clazz.id}">
        <g:if test="${includeCheck}">
            <th>include</th>
        </g:if>
        <g:if test="${clazz.studie.canModify()}">
            <th></th>
        </g:if>
        <th>file name</th>
        <th>comment</th>
        <th>label</th>
    </tr>
    </thead>
    <tbody id="class_body_${clazz.id}_table">

    <!-- render all rows -->
    <g:each var="sample" in="${clazz.samples}">
        <g:render template="/experimentClass/sample/renderRow"
                  model="[sample: sample, clazz: clazz, locked: locked, includeCheck: includeCheck]"/>
    </g:each>

    <g:if test="${!clazz.studie.canModify()}">

        <tr>
            <g:if test="${includeCheck}">
                <td></td>
            </g:if>
            <g:if test="${clazz.studie.canModify()}">
                <td></td>
            </g:if>
            <td class="version_row">
                <label class="version_select">set version for all files _</label>
                <g:select class="version_select" from="${1..9}" name="clazz_version_${clazz.id}"
                          noSelection="['': '?']"/>


                <!-- sets the fileversion dynamicaly -->
                <g:javascript>
              $(function() {
                  $("#clazz_version_${clazz.id}").change(function( ) {
                      jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'experimentClass', action: 'ajaxSetFileVersion ', id: clazz.id)}",success:function(data,textStatus){jQuery('#class_${clazz.id}_content').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
                  });
                });

                </g:javascript>
            </td>
            <td></td>
            <td></td>
        </tr>
    </g:if>
    </tbody>

</table>




