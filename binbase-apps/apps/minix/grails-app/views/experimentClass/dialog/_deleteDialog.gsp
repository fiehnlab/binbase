<g:setProvider library="jquery"/>

<g:javascript>
    $(document).ready(function() {
        // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
        $("#dialog-class-modal-${clazz.id}").dialog("destroy");

        $("#dialog-class-modal-${clazz.id}").dialog({
            height: 190,
            width: 450,
            modal: true
        });
    });
</g:javascript>

<g:javascript>

/**
* closes the dialog
*/
function closeDialog(){

  $(document).ready(function() {
    //close the dialog
    $("#dialog-class-modal-${clazz.id}").dialog('close');
  });
}

/**
 *removes the table if the delete was successful
 */
    function removeTable(){
         $(document).ready(function() {
    //close the dialog
    $("#class_${clazz.id}_description").remove();
  });
    }

</g:javascript>
<%
    def pageId = System.currentTimeMillis()
%>
<div id="dialog-class-modal-${clazz.id}" title="Delete class dialog">

    <div id="page_${pageId}">
        <g:form controller="experimentClass" action="ajaxRemoveClass">

            <div class="element">
                <div class="description">are you sure you want to delete this class?</div>
            </div>

            <div class="element">
                <div class="myButton">
                    <div class="buttons">
                        <g:submitToRemote before="closeDialog();" controller="experimentClass" action="ajaxRemoveClass" name="submit" value="yes" class="remove" update="class_${clazz.id}_description" onSuccess="removeTable();"/>
                    </div>
                </div>
            </div>

            <g:hiddenField name="pageId" value="${pageId}"/>

            <g:hiddenField name="id" value="${clazz.id}"/>
        </g:form>
    </div>
</div>