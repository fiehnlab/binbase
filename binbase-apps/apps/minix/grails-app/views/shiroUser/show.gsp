<%@ page import="minix.ShiroUser" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'shiroUser.label', default: 'ShiroUser')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>
<body>
<g:render template="/shared/navigation"/>



<div class="body">

    <div class="design">

        <div class="left-thrirty">

            <div class="scaffoldingProperties">

                <span class="scaffoldingPropertyLabel">
                    <g:message code="shiroUser.username.label" default="Username"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    ${fieldValue(bean: shiroUserInstance, field: "username")}

                </span>


                <span class="scaffoldingPropertyLabel">
                    <g:message code="shiroUser.firstName.label" default="First Name"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    ${fieldValue(bean: shiroUserInstance, field: "firstName")}

                </span>




                <span class="scaffoldingPropertyLabel">
                    <g:message code="shiroUser.lastName.label" default="Last Name"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    ${fieldValue(bean: shiroUserInstance, field: "lastName")}

                </span>




                <span class="scaffoldingPropertyLabel">
                    <g:message code="shiroUser.email.label" default="Email"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    ${fieldValue(bean: shiroUserInstance, field: "email")}

                </span>




                <span class="scaffoldingPropertyLabel">
                    <g:message code="shiroUser.samplePrefix.label" default="Sample Prefix"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    ${fieldValue(bean: shiroUserInstance, field: "samplePrefix")}

                </span>

                <span class="scaffoldingPropertyValue">

                    <ul class="scaffoldingList">
                        <g:each in="${shiroUserInstance.comments}" var="c">
                            <li class="scaffoldingItem">
                                <g:link class="showDetails" controller="comment" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link>
                            </li>
                        </g:each>
                    </ul>

                </span>


                <span class="scaffoldingPropertyLabel">
                    Role
                </span>

                <span class="scaffoldingPropertyValue">
                    <g:render template="renderRole" model="[shiroUserInstance:shiroUserInstance]"></g:render>

                </span>


                <span class="scaffoldingPropertyLabel">
                    <g:message code="shiroUser.guest.label" default="Guest account"/>
                </span>

                <span class="scaffoldingPropertyValue">

                    <g:checkBox name="guest" checked="${shiroUserInstance.guest}" id="guest_account"/>

                </span>

                <span class="scaffoldingPropertyLabel">
                    Schedule Users Studies
                </span>

                <span class="scaffoldingPropertyValue">
                    <g:link controller="scheduleStudy" action="schedule" params="[userId:shiroUserInstance.id]">schedule</g:link>
                </span>




            </div>
        </div>
        <div class="right-seventy left-side-border">

            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>

            <div class="header">
                Actions
            </div>

            <div class="element">
                <ul class="none-horizontal">
                    <li><g:link class="add" controller="shiroUser" action="create">new user</g:link></li>
                    <li><g:link class="edit" controller="shiroUser" action="edit" id="${shiroUserInstance.id}">edit user</g:link></li>

                <!-- these options should not be possible for admins -->
                    <g:if test="${!ShiroUser.hasAdminRights(shiroUserInstance)}">
                        <li><g:link class="remove" controller="shiroUser" action="delete" id="${shiroUserInstance.id}">delete user</g:link></li>
                        <li><g:link class="release" controller="shiroUser" action="releaseDesigns" id="${shiroUserInstance.id}">release designs</g:link></li>
                        <li><g:link class="release" controller="shiroUser" action="releaseStudies" id="${shiroUserInstance.id}">release studies</g:link></li>
                    </g:if>


                    <li><g:link class="list" controller="shiroUser" action="list" id="${shiroUserInstance.id}">browse users</g:link></li>
                </ul>
            </div>

            <div class="header">
                Associated Studies
            </div>

            <div class="element">
                <table>
                    <thead>
                    <tr>
                        <g:sortableColumn property="id" title="Id"/>
                        <g:sortableColumn property="description" title="Description"/>
                        <th>Architecture</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${studieInstanceList}" status="i" var="studieInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                            <td><g:link controller="studie" action="show" id="${studieInstance.id}">${studieInstance.id?.encodeAsHTML()}</g:link></td>

                            <td>${studieInstance.description?.encodeAsHTML()}</td>

                            <td>${studieInstance.architecture?.encodeAsHTML()}</td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>
                <div class="paginateButtons">
                    <g:paginate id="${shiroUserInstance.id}" action="show" controller="shiroUser" total="${studieTotal}" max="10" offset="${session.studieUserPagination?.offset}" params="${[paginate:'Studie']}"/>
                </div>
            </div>
            <div class="header">
                Associated Designs
            </div>

            <div class="element">
                <table>
                    <thead>
                    <tr>

                        <g:sortableColumn property="id" title="Id"/>

                        <g:sortableColumn property="title" title="Title"/>

                        <th>Generated Studie</th>

                        <g:sortableColumn property="step" title="Step"/>

                        <th>Architecture</th>

                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${studieDesignInstanceList}" status="i" var="studieDesignInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                            <td><g:link controller="studieDesign" action="design" id="${studieDesignInstance.id}" params="[designId:studieDesignInstance.id]">${studieDesignInstance.id?.encodeAsHTML()}</g:link></td>

                            <td>${studieDesignInstance.title?.encodeAsHTML()}</td>

                            <td>${studieDesignInstance.generatedStudie?.encodeAsHTML()}</td>

                            <td>${studieDesignInstance.step?.encodeAsHTML()}</td>

                            <td>${studieDesignInstance.architecture?.encodeAsHTML()}</td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>
                <div class="paginateButtons">
                    <g:paginate id="${shiroUserInstance.id}" action="show" controller="shiroUser" total="${studieDesignCount}" max="10" offset="${session.designUserPagination?.offset}" params="${[paginate:'Design']}"/>
                </div>
            </div>
        </div>
    </div>
</div>
<g:javascript>
    $("#guest_account").on('click',function(){
        jQuery.ajax({type:'POST',data:{'guest': this.checked}, url:"${g.createLink(controller: 'shiroUser', action: 'ajaxSetGuest', id: shiroUserInstance.id)}",success:function(data,textStatus){}});
    });
</g:javascript>
</body>

</html>
