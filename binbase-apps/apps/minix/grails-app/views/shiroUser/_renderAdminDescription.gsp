<div class="left_information_box">
  the role admin gives you complete access to the system including the possibility to

  <div class="element">
    <ol>
      <g:render template="userRights"/>
      <g:render template="managerRights"/>
      <g:render template="adminRights"/>
    </ol>
  </div>
</div>