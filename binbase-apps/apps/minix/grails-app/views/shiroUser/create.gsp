<%@ page import="minix.roles.RoleUtil; minix.ShiroRole; minix.ShiroUser" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'shiroUser.label', default: 'ShiroUser')}"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

    <title>Create new User</title>
</head>

<body>
<g:render template="/shared/navigation"/>


<g:javascript>

    $(document).ready(function() {
        $(':radio').change(function () {

            jQuery.ajax({data: ({value:this.value}),dataType: "html",type:'POST', url:"${g.createLink(controller: 'shiroUser',action: 'ajaxEvaluateRole')}",success:function(data, textStatus) {
                jQuery('#roleDescription').html(data);
            },error:function(XMLHttpRequest, textStatus, errorThrown) {
            }});

        })
    })

</g:javascript>
<div class="body">
    <div class="design">
        <div class="left">
            <div class="header">

                Information

            </div>


            <div class="left_information_box">
                please provide the properties for your new user
            </div>

            <div class="topSpacer"></div>

            <div class="header">

                What are users

            </div>

            <div class="left_information_box">

                <p><g:message code="description.user"/></p>
            </div>

            <div id="roleDescription">

            </div>
        </div>

        <div class="left-center-column">
            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>

            <div class="element">
                <g:hasErrors bean="${shiroUserInstance}">
                    <div class="errors">
                        <g:renderErrors bean="${shiroUserInstance}" as="list"/>
                    </div>
                </g:hasErrors>

                <g:form action="save" method="post">
                    <div class="scaffoldingProperties">

                        <div>
                            <span class="scaffoldingPropertyLabel">

                                <label for="username">User name</label>

                            </span>
                            <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'username', 'errors')}">

                                <g:textField name="username" value="${shiroUserInstance?.username}"/>
                            </span>
                        </div>


                        <div>
                            <span class="scaffoldingPropertyLabel">

                                <label for="passwordHash">Password</label>

                            </span>

                            <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'passwordHash', 'errors')}">

                                <g:passwordField name="passwordHash" value="${shiroUserInstance?.passwordHash}"/>
                            </span>
                        </div>


                        <div>
                            <span class="scaffoldingPropertyLabel">

                                <label for="firstName">First Name</label>

                            </span>
                            <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'firstName', 'errors')}">

                                <g:textField name="firstName" value="${shiroUserInstance?.firstName}"/>
                            </span>
                        </div>


                        <div>
                            <span class="scaffoldingPropertyLabel">

                                <label for="lastName">Last Name</label>

                            </span>
                            <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'lastName', 'errors')}">

                                <g:textField name="lastName" value="${shiroUserInstance?.lastName}"/>
                            </span>
                        </div>


                        <div>
                            <span class="scaffoldingPropertyLabel">

                                <label for="email">Email address</label>

                            </span>
                            <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'email', 'errors')}">

                                <g:textField name="email" value="${shiroUserInstance?.email}"/>
                            </span>
                        </div>


                        <div>
                            <span class="scaffoldingPropertyLabel">

                                <label for="samplePrefix">Sample Prefix</label>

                            </span>
                            <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'samplePrefix', 'errors')}">

                                <g:textField name="samplePrefix" maxlength="2"
                                             value="${shiroUserInstance?.samplePrefix}"/>
                            </span>
                        </div>


                        <div>
                            <span class="scaffoldingPropertyLabel">

                                <label for="samplePrefix">User Role</label>

                            </span>
                            <span class="scaffoldingPropertyValue">
                                <ul class="none-horizontal">
                                    <%
                                        def role = null

                                        if (shiroUserInstance != null) {
                                            if (shiroUserInstance.roles?.size() > 0) {
                                                role = shiroUserInstance.roles.iterator().next()
                                            }
                                            else {
                                                role = RoleUtil.getUserRole()
                                            }
                                        }
                                        else {
                                            role = RoleUtil.getUserRole()
                                        }
                                    %>
                                    <g:radioGroup name="role" labels="${ShiroRole.findAll()}"
                                                  values="${ShiroRole.findAll()}" value="${role}">
                                        <li>${it.radio} ${it.label}</li>

                                    </g:radioGroup>
                                </ul>
                            </span>

                        </div>

                    </div>

                    <div class="button-margin-top">
                        <g:submitButton name="create" class="next" value="Save User"/>
                    </div>
                </g:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
