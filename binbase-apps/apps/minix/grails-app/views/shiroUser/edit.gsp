<%@ page import="minix.roles.RoleUtil; minix.ShiroRole; minix.ShiroUser" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'shiroUser.label', default: 'ShiroUser')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'scaffolding.css')}"/>

</head>
<body>
<g:render template="/shared/navigation"/>



<g:javascript>

    $(document).ready(function() {
        $(':radio').change(function () {

            jQuery.ajax({data: ({value:this.value}),dataType: "html",type:'POST', url:"${g.createLink(controller: 'shiroUser',action: 'ajaxEvaluateRole')}",success:function(data, textStatus) {
                jQuery('#roleDescription').html(data);
            },error:function(XMLHttpRequest, textStatus, errorThrown) {
            }});

        })
    })

</g:javascript>

<div class="body">
    <div class="design">

        <div class="left">
            <div class="header">

                Information

            </div>


            <div class="left_information_box">
                please provide the required informations to create this object
            </div>



            <div id="roleDescription">

            </div>

        </div>
        <div class="left-center-column">
            <g:if test="${flash.message}">
                <div class="message">${flash.message}</div>
            </g:if>

        <div class="element">
            <g:hasErrors bean="${shiroUserInstance}">
                <div class="errors">
                    <g:renderErrors bean="${shiroUserInstance}" as="list"/>
                </div>
            </g:hasErrors>

            <g:form method="post">
                <g:hiddenField name="id" value="${shiroUserInstance?.id}"/>
                <g:hiddenField name="version" value="${shiroUserInstance?.version}"/>

                <div class="scaffoldingProperties">

                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <g:message code="shiroUser.username.label" default="Username"/>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'username', 'errors')}">
                            ${shiroUserInstance?.username}
                        </span>
                    </div>


                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="firstName"><g:message code="shiroUser.firstName.label" default="First Name"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'firstName', 'errors')}">
                            <g:textField name="firstName" value="${shiroUserInstance?.firstName}"/>
                        </span>
                    </div>

                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="lastName"><g:message code="shiroUser.lastName.label" default="Last Name"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'lastName', 'errors')}">
                            <g:textField name="lastName" value="${shiroUserInstance?.lastName}"/>
                        </span>
                    </div>

                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="email"><g:message code="shiroUser.email.label" default="Email"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'email', 'errors')}">
                            <g:textField name="email" value="${shiroUserInstance?.email}"/>
                        </span>
                    </div>

                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="samplePrefix"><g:message code="shiroUser.samplePrefix.label" default="Sample Prefix"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'samplePrefix', 'errors')}">
                            <g:textField name="samplePrefix" maxlength="2" value="${shiroUserInstance?.samplePrefix}"/>
                        </span>
                    </div>

                    <div>
                        <span class="scaffoldingPropertyLabel">

                            <label for="samplePrefix">User Role</label>

                        </span>
                        <span class="scaffoldingPropertyValue">
                            <ul class="none-horizontal">
                                <%
                                    def role = null

                                    if (shiroUserInstance != null) {
                                        if (shiroUserInstance.roles?.size() > 0) {
                                            role = shiroUserInstance.roles.iterator().next()
                                        }
                                        else {
                                            role = RoleUtil.getUserRole()
                                        }
                                    }
                                    else {
                                        role = RoleUtil.getUserRole()
                                    }
                                %>
                                <g:radioGroup name="role" labels="${ShiroRole.findAll()}" values="${ShiroRole.findAll()}" value="${role}">
                                    <li>${it.radio} ${it.label}</li>

                                </g:radioGroup>
                            </ul>
                        </span>

                    </div>

                    <div>
                        <span class="scaffoldingPropertyLabel">
                            <label for="locked"><g:message code="shiroUser.locked.label" default="Locked"/></label>
                        </span>
                        <span class="scaffoldingPropertyValue ${hasErrors(bean: shiroUserInstance, field: 'locked', 'errors')}">
                            <g:checkBox name="locked" value="${shiroUserInstance?.locked}"/>
                        </span>
                    </div>

                </div>

                    <div class="button-margin-top">

                        <span class="button-margin-right"><g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}"/></span>

                        <span><g:actionSubmit class="save" action="resetPassword" value="${message(code: 'default.button.resetPassword.label', default: 'Reset Password')}"/></span>
                    </div>
                </div>
            </g:form>
        </div>
    </div>
</div>
</body>
</html>
