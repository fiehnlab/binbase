<g:setProvider library="jquery"/>

<link rel="stylesheet" href="${resource(dir: 'css', file: 'studieDesign.css')}"/>

<!-- simple tool to define and select species -->

<g:javascript>
  $(document).ready(function() {

    $(function() {

      $("input")
      .filter(function() {
        return this.id.match(/value.*/);
      })
      .autocomplete({
              source: '${g.createLink(action: "findOrganValues", controller: "autoCompletion")}',
              dataType:"json"
    });


      $("input")
      .filter(function() {
        return this.id.match(/name.*/);
      })
      .autocomplete({
              source: '${g.createLink(action: "findOrgan", controller: "autoCompletion")}',
              dataType:"json"
    });


    })
  });
</g:javascript>


<g:if test="${flash.message != null}">
    <div class="message">${flash.message}</div>
</g:if>

<g:if test="${values}">

    <table class="contentTable">
        <thead>
        <th>organ id</th>
        <th>name</th>
        <th>species</th>

        </thead>
        <tbody>
        <g:each var="value" in="${values}" status="i">
            <tr>

                <td>${value.id}</td>
                <td>${value.name}</td>
                <td>${value.speciees}</td>



                <div class="textField">
                    <g:hiddenField name="value_name" value="${value.name}"/>
                    <g:hiddenField name="value_id" value="${value.id}"/>
                </div>
            </tr>

        </g:each>

        </tbody>
    </table>

</g:if>

<div class="textDescription">
    please enter your next id or name into the textfield below and press 'add value'.
</div>


<div>

    <div class="textField">
        <g:textField name="value"/>

    </div>

    <div class="button-margin-left">
        <g:submitToRemote class="add" action="ajaxViewInMerge" name="addValue" id="addValue"
                          value="add value" update="selectDiv"/>
    </div>

</div>


<div class="textDescription">
    please enter your new name for the combined organ
</div>


<div>

    <div class="textField">
        <g:textField name="name" value="${name}"/>

    </div>

</div>

<g:hiddenField name="progressId" value="${progressId}"/>

<div class="element">
    <g:jprogress message="please wait for your calculation..." progressId="${progressId}" trigger="executeMerge"/>
</div>

<div class="button-margin-top">
    <g:submitToRemote class="next" action="ajaxExecuteMerge" name="executeMerge" id="executeMerge"
                      value="start merging" update="selectDiv"/>
</div>



