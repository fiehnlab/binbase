<g:form action="signIn" controller="auth">
  <input type="hidden" name="targetUri" value="${targetUri}"/>
  <div>
    <div>
      <span class="property-title">Username:</span>
      <span class="property-value"><g:textField name="username" value="${username}"/></span>
    </div>
    <div>
      <span class="property-title">Password:</span>
      <span class="property-value"><g:passwordField name="password" value=""/></span>
    </div>

    <div class="property-value">
      <div class="buttons">
        <g:submitButton class="next"  name="submit" value="Sign in"/>
      </div>

    </div>

  </div>

</g:form>