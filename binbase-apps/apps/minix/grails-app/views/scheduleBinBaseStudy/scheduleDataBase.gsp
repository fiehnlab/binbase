<%--
  Created by IntelliJ IDEA.
  User: wohlgemuth
  Date: Oct 22, 2010
  Time: 3:38:15 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main"/>

    <title>Schedule study tool</title>
</head>

<body>
<g:render template="/shared/navigation"/>

<div class="body">

    <div class="design">
        <div class="left">
            <div class="header">

                Information

            </div>


            <div class="left_information_box">

                <p>
                    This tool allows you to schedule all studies, which have been calculated on a specific database.
                </p>


            </div>

        </div>

        <div class="left-center-column">
            <div class="header">
                Welcome to the database schedule tool
            </div>

            <div class="textDescription">

                Here you can select a database, which you would like to reschedule
            </div>


            <g:form name="scheduleStudy" action="scheduleDataBase" method="POST">

                <div id="database" class="textDescription">
                    <g:select from="${databases}" name="database" id="column"/>

                </div>


                <div class="element">
                    <div class="button-margin-top">
                        <g:submitButton class="next" action="scheduleDataBase" name="selectProcessingInstructions"
                                        value="next"/>
                    </div>

                </div>


            </g:form>

        </div>
    </div>

</div>
</body>
</html>
