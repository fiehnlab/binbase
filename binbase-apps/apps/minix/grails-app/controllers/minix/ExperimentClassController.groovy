package minix

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample
import binbase.web.core.BBOrgan

class ExperimentClassController {

    ModifyClassService modifyClassService
    def scaffold = true

    /**
     * loads the class for us
     */
    def ajaxLoadClass = {
        render template: "loadClass", model: [clazz: ExperimentClass.get(params.id),locked: params.locked,includeCheck:params.includeCheck?.toString().toBoolean()]
    }
    /**
     * deletes a sample
     */
    def ajaxRemoveSample = {

        Sample sample = Sample.get(params.id)

        if (sample != null) {

            ExperimentClass clazz = sample.experimentClass
            if (clazz.samples.size() > 1) {
                clazz.removeFromSamples sample
                sample.delete(flush: true)
                clazz.save(flush: true)

                render(template: "sample/removeSample", model: [sampleId: sample.id, classId: clazz.id])
            }
            else {
                render """<div class="errors">sorry this class has only one sample and so it can't be removed!</div>"""
            }
        }
        else {
            println "sample was null!!! "
        }
        render """ """
    }

    /**
     * renders the class table
     */
    def ajaxRenderClassTable = {
        ExperimentClass clazz = ExperimentClass.get(params.id)

        render template: "displayClassTable", model: [clazz: clazz, locked: true]
    }

    /**
     * adds a sample to a class and renders the table again
     */
    def ajaxAddSample = {
        ExperimentClass clazz = ExperimentClass.get(params.id)

        if (clazz != null) {
            clazz = modifyClassService.addSample(clazz)

            render template: "displayClassTable", model: [clazz: clazz]
        }
        else {
            throw new RuntimeException("there was no class found with the given id: ${params.id}")
        }
    }

    /**
     * removes a class from the design/study
     */
    def ajaxRemoveClass = {
        println params
        BaseClass clazz = BaseClass.get(params.id)

        if (clazz != null) {
            modifyClassService.removeClass(clazz, clazz.studie)

        }
        else {
            throw new RuntimeException("there was no class found with the given id: ${params.id}")
        }
        render ""
    }

    /**
     * the dialog to delete a studie
     */
    def showDeleteDialog = {

        def clazzInstance = BaseClass.get(params.id)

        if (clazzInstance != null) {
            render template: "dialog/deleteDialog", model: [clazz: clazzInstance]
        }
        else {
            flash.message = "sorry no class found!"
            redirect(action: "list")

        }
    }

    def delete = {
        def clazzInstance = ExperimentClass.get(params.id)
        if (clazzInstance) {
            try {

                modifyClassService.removeClass(clazzInstance, clazzInstance.studie)

                flash.message = "${message(code: 'default.deleted.message', args: [message(code: 'experimentClass.label', default: 'ExperimentClass'), params.id])}"
                redirect(action: "list", controller: "experimentClass")
            }
            catch (org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "${message(code: 'default.not.deleted.message', args: [message(code: 'experimentClass.label', default: 'ExperimentClass'), params.id])}"
                redirect(action: "show", id: params.id, controller: "experimentClass")
            }
        }
        else {
            flash.message = "${message(code: 'default.not.found.message', args: [message(code: 'experimentClass.label', default: 'ExperimentClass'), params.id])}"
            redirect(action: "list", controller: "experimentClass")
        }
    }

    /**
     * sets the file version for all samples in this class
     */
    def ajaxSetFileVersion = {

        ExperimentClass clazz = ExperimentClass.get(params.id)

        boolean success = false
        clazz.samples.each { Sample sample ->
            sample.fileVersion = params.value.toString().toInteger()

            if (sample.validate()) {
                sample.save(flush: true)
                success = true
            }
            else {
                render(template: "/shared/validation_error", model: [errorBean: sample, message: """there was something wrong with your provided value: '${params.value}' for sample: ${sample.id} - ${sample.toString()}"""])
                return
            }
        }

        if (success) {

            render template: "/experimentClass/displayClassTable", model: [clazz: clazz, aquisitionTable: clazz.studie.canModify()]

        }
    }

    /**
     * used to change the species
     */
    def ajaxChangeSpecies = {

        ExperimentClass clazz = ExperimentClass.get(params.id)
        Species species = Species.get(params.speciesId)
        clazz.species = species
        clazz.save()

        if (species.organs.size() == 0) {
            addNoneOrgan(species)
        }
        if (species.organs.contains(clazz.organ) == false) {
            addNoneOrgan(species)
            clazz.organ = BBOrgan.findByName('none')
            clazz.save()
        }
        render template: "/experimentClass/renderMetaData", model: [clazz: clazz]
    }

    private def addNoneOrgan(Species species) {
        BBOrgan organ = BBOrgan.findByName('none')
        if (organ == null) {
            organ = new BBOrgan()
            organ.name = "none"
            organ.save()
        }
        if (species.organs.contains(organ) == false) {
            species.addToOrgans(organ)
        }
        species.save()
    }

    /**
     * used to change the organ
     */
    def ajaxChangeOrgan = {
        ExperimentClass clazz = ExperimentClass.get(params.id)

        Organ organ = Organ.get(params.organId)

        if (clazz.species.organs.contains(organ) == false) {
            clazz.species.organs.add(organ)
            clazz.species.save()
        }
        clazz.organ = organ
        clazz.save(flush: true)

        render template: "/experimentClass/renderMetaData", model: [clazz: clazz]
    }
}
