package minix

class UserTrackingController {

    def index = {


        if (ShiroUser.hasAdminRights() == false) {
            flash.message = "sorry you have no permission to access this data!"

            if (ShiroUser.hasManagmentRights()) {
                redirect(controller: "home", action: "showManagerHome")

            }
            else {
                redirect(controller: "home", action: "showMemberHome")
            }

        }

        redirect(action: list, params: params)
    }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete: 'POST', save: 'POST', update: 'POST']

    /**
     * does the actual listing and ordering
     */
    def list = {
        if (!params.max) params.max = 25
        if (!params.sort) params.sort = "dateCreated"
        if (!params.order) params.order = "desc"

        //the list of users which is in the audit log
        def users = UserTracking.executeQuery("select distinct a.username from UserTracking a where a.username is not null")

        def selectedUser = params.selectedUser

        def result = []
        def count = 0

        if (selectedUser != null && selectedUser.toString().size() != 0) {

            count = UserTracking.executeQuery("select count(a.username) from UserTracking a where a.username is not null").get(0)
            result = UserTracking.findAllByUsername(selectedUser, params)
        }
        else {

            count = UserTracking.count()
            result = UserTracking.list(params)

        }


        [auditLogEventInstanceList: result, auditLogEventInstanceTotal: count, selectedUser: selectedUser, users: users]

    }
}
