package minix
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.StatusJMXFacade
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtil
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ClusterUtilFactory
import edu.ucdavis.genomics.metabolomics.binbase.cluster.ejb.delegate.ClusterConfigService
/**
 * binbase cluster information
 */
class BinBaseClusterController {

    StatusJMXFacade binbaseStatus

    ClusterConfigService clusterConfigService

    def index = {

    }

    def queue = {
    }


    def ajaxRenderConfiguredNodes = {

        ClusterUtil util = createUtil()

        render """
            <span class="scaffoldingPropertyLabel">Configured Nodes</span>
        """

        render """
                        <span class="scaffoldingPropertyValue">

                        ${util.getNodeCount()}

                    </span>
        """

    }


    def ajaxRenderBrokenNodes = {

        ClusterUtil util = createUtil()


        render """
            <span class="scaffoldingPropertyLabel">Broken Nodes</span>
        """

        render """
                        <span class="scaffoldingPropertyValue">

                        ${util.getDeadNodeCount()}

                    </span>
        """

    }

    def ajaxRenderAvailableNodes = {

        ClusterUtil util = createUtil()


        render """
            <span class="scaffoldingPropertyLabel">Available Nodes</span>
        """

        render """
                        <span class="scaffoldingPropertyValue">

                        ${util.getFreeNodeCount()}

                    </span>
        """

    }
    def ajaxRenderRunningNodes = {

        ClusterUtil util = createUtil()

        render """
            <span class="scaffoldingPropertyLabel">Running Nodes</span>
        """

        render """
                        <span class="scaffoldingPropertyValue">

                        ${util.getUsedNodeCount()}

                    </span>
        """


    }


    def ajaxRenderAutostart = {


        render """
            <span class="scaffoldingPropertyLabel">Autostart</span>
        """

        render """
                        <span class="scaffoldingPropertyValue">

                        ${clusterConfigService.isAutoStart()}

                    </span>
        """


    }


    def ajaxRenderAutostartWarning = {

        if(clusterConfigService.isAutoStart() == false){
            render """
                    <div class="message">no calculation possible - autostart is disabled - nodes need to be manually started</div>
                """
        }
        else{
            render ""
        }


    }



    def ajaxRenderBinBaseNodes = {


        render """
            <span class="scaffoldingPropertyLabel">Max Nodes for BinBase</span>
        """

        render """
                        <span class="scaffoldingPropertyValue">

                        ${clusterConfigService.getMaxNodes()}

                    </span>
        """


    }



    def ajaxRenderClusterQueue = {
        ClusterUtil util = createUtil()

        render template: "clusterqueue", model: [queue: util.getQueue()]
    }


    def ajaxRenderPendingClusterQueue = {
        ClusterUtil util = createUtil()

        render template: "clusterqueue", model: [queue: util.getPendingQueue()]
    }

    def ajaxRenderImportQueue = {

        render template: "importqueue", model: [queue: binbaseStatus.listImportJobs()]
    }

    def ajaxRenderScheduleQueue = {

        render template: "schedulequeue", model: [queue: binbaseStatus.listAllJobs()]
    }

    def ajaxRenderDSLScheduleQueue = {

        render template: "dslqueue", model: [queue: binbaseStatus.listDSLJobs()]

    }


    def ajaxRenderExportQueue = {

        render template: "exportqueue", model: [queue: binbaseStatus.listExportJobs()]
    }

    def ajaxRenderLogFileForId = {
        try {
            ClusterUtil util = createUtil()
            String log = util.getInfoLogForJob(params.id)

            if (log.trim().isEmpty()) {
                log = "no content was generated yet, try again later"
            }
            render """<div class="message">${log}</div>"""

        }
        catch (Exception e) {
            render "no logfile found for this job"
        }
    }

    def ajaxStartNode = {
        try {
            ClusterUtil util = createUtil()
            String log = util.startNode()

            render """<div class="message">${log}</div>"""

        }
        catch (Exception e) {
            render "starting of node failed: ${e.getMessage()}"
        }
    }

    def ajaxClearQueue = {
        try {
            binbaseStatus.clearQueue()

            render """<div class="message">queue should be deleted, you might need to refresh this page</div>"""

        }
        catch (Exception e) {
            render "clearing of queue failed: ${e.getMessage()}"
        }
    }
    /**
     * creates the actual utility
     * @return
     */
    private ClusterUtil createUtil() {
        ClusterUtilFactory factory = ClusterUtilFactory.newInstance(clusterConfigService.getClusterFactory())
        ClusterUtil util = factory.createUtil([username: clusterConfigService.getUsername(), password: clusterConfigService.getPassword(), server: clusterConfigService.getCluster()])
        util
    }
}
