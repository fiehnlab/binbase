package minix

import binbase.web.core.BBExperimentSample

class BaseClassController {

    def scaffold = true

    /**
     * sets the file version for all samples in this class
     */
    def ajaxSetFileVersion = {

        BaseClass clazz = BaseClass.executeQuery("select a from ${params.clazz} as a where id = ?", [params.id.toString().toLong()])[0]

        boolean success = false
        clazz.samples.each { def sample ->
            sample.fileVersion = params.value.toString().toInteger()

            if (sample.validate()) {
                sample.save(flush: true)
                success = true
            }
            else {
                render(template: "/shared/validation_error", model: [errorBean: sample, message: """there was something wrong with your provided value: '${params.value}' for sample: ${sample.id} - ${sample.toString()}"""])
                return
            }
        }

        if (success) {

            render template: "/baseClass/displayClassTable", model: [clazz: clazz, aquisitionTable: clazz.studie.canModify()]

        }
    }
}
