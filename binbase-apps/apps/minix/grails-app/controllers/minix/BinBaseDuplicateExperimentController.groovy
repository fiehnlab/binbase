package minix
import core.BinBaseDataAccessService
import core.BinBaseSystemService
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment

class BinBaseDuplicateExperimentController {

  BinBaseDataAccessService binBaseDataAccessService

  BinBaseSystemService binBaseSystemService

  BinBaseImportService binBaseImportService

  /**
   * displays the select dialog for the experiment
   */
  def selectExperiment_ajax = {
    render(template: "selectExperiment", model: [columns: binBaseSystemService.availableColumns])
  }

  /**
   * query all the ids for the database and updates the related field
   */
  def updateExperimentsByDatabase_ajax = {

    def value = binBaseDataAccessService.getExperimentIds(params.database)

    def result = []

    value.each {String val ->
      if (val != null) {
        result.add(val)
      }
    }

    result.sort()

    render(template: "selectExperimentField", model: [experimentIds: result])
  }

  /**
   * imports the actual experiment
   */
  def importExperiment_ajax = {

    if (params.column == null | params.column.toString().size() == 0) {
      flash.errorMessage = "please select a database first!"
      render(template: "selectExperiment", model: [columns: binBaseSystemService.availableColumns])

    }

    else if (params.experiment == null | params.experiment.toString().size() == 0) {
      flash.errorMessage = "please select an experiment first!"
      render(template: "selectExperiment", model: [columns: binBaseSystemService.availableColumns, column: params.column])

    }
    else {

      String database = params.column
      String experimentId = params.experiment

      Experiment experiment = binBaseDataAccessService.getExperiment(experimentId, database)

      //start the import, TODO should show a progress bar
      Studie studie = binBaseImportService.importExperiment(experiment)

      //render the template that it's done
      render (template: "importDone", model: [studie:studie])
    }
  }
}
