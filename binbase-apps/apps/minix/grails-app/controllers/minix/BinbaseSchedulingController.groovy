package minix

import binbase.web.core.BBExperimentClass
import core.BinBaseSchedulingService
import core.SopService
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentClass
import binbase.core.BinBaseConfigReader
import core.BinBaseSystemService
import edu.ucdavis.genomics.metabolomics.util.config.xml.XmlHandling
import grails.converters.JSON
import org.apache.shiro.SecurityUtils
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.Experiment
import edu.ucdavis.genomics.metabolomics.binbase.bci.server.types.ExperimentSample
import binbase.web.core.BBExperimentSample
import org.jdom.Element
import org.jdom.output.Format
import org.jdom.output.XMLOutputter

/**
 * does all the schedulig of the controller
 */
class BinbaseSchedulingController {

    /**
     * access to the binbase scheduling service
     */

    BinBaseSchedulingService binBaseSchedulingService

    BinBaseSystemService binBaseSystemService

    /**
     * access to our sop's
     */
    SopService sopService

    /**
     * renders the view to select the data for the scheduling
     */
    def schedule = {

        def studies = []

        if (params.id == null) {
            ShiroUser user = ShiroUser.findByUsername(SecurityUtils.subject.principal)

            studies = user.studies
        }
        else {
            studies.add(Studie.get(params.id))
        }

        if (studies.size() == 0) {
            flash.message = "sorry we didn't find any studies, please define a study"
            redirect(controller: "studieDesign", action: "design")
        }

        def columns = new HashSet(binBaseSystemService.availableColumns)

        def column = ""
        if (studies.size() == 1){

            Studie studie = studies[0]

            if (studie.databaseColumn != null){
                column = studie.databaseColumn
            }
        }

        return [columns: columns, studies: studies, column: column]
    }

/**
 * select our samples for the calculation
 */
    def ajaxSamplesForCalculation = {

        def studie = Studie.get(params.studie)
        def column = params.column

        if(params.referenceStudy && params.referenceStudy.toString().toLowerCase() == "on"){
                render(template: "selectReferenceStudy", model: [studie: studie, column: column,referenceStudy:"on", myParams: [:]])
        }
        else{
            //a reference study was assigned and was an actual id
            if (params.referenceStudy){

                if (params.referenceStudy.toString() != "") {
                    params.referenceStudy = Studie.get(Long.parseLong(params.referenceStudy.toString().split(":")[0].toString()))
                }

                render(template: "selectSamples", model: [studie: studie, column: column, referenceStudy: params.referenceStudy, myParams: [:]])
            }

            //no reference study assigned
            else{
                render(template: "selectSamples", model: [studie: studie, column: column, myParams: [:]])
            }
        }
    }

        /**
     * validates the export and post status updates
     */
    def ajaxValidateSampleForExport = {
        def studie = Studie.get(params.studie)
        def column = params.column

        //read our samples
        def samples = params.list("sample")
        def names = params.list("sampleFileName")

        //required to repopulate the view
        def model = [:]

        //reaasign params to render the same model
        model.myParams = params
        model.studie = studie

        model.column = column



        //tells us that all data are found
        model.success = true

        def experimentModel = [:]
        experimentModel.database = params.column
        experimentModel.studie = studie
        experimentModel.classes = [:]

        if (params.referenceStudy) {
            experimentModel.referenceStudy = Studie.get(params.referenceStudy)
        }
        //calcualte the properties
        for (int i = 0; i < samples.size(); i++) {

            //calcualte the progress
            double v = (double) i / (double) samples.size() * 100;

            session[params.sessionId] = v

            //actual file name
            String sampleName = names.get(i).toString().trim()

            //ensure the txt file exist
            boolean text = binBaseSystemService.txtFileExist(sampleName)

            //ensure that the cdf file exist
            boolean netcdf = binBaseSystemService.cdfFileExist(sampleName)

            println "cdf: ${sampleName} - ${netcdf}"
            //check if the file is checked
            def checkedVar = "checked_${samples.get(i).toString().trim()}".toString().trim()

            //assign it to the model   `
            model.myParams.put("hasCdf_${samples.get(i)}".toString(), netcdf)
            model.myParams.put("hasTXT_${samples.get(i)}".toString(), text)

            //set that this file is not a breaker
            model.myParams.put("breaker_${samples.get(i)}".toString(), false)
            model.myParams.put("export_${samples.get(i)}".toString(), false)

            //these samples are required, so they break the export
            if (model.myParams.get(checkedVar) != null) {

                //define that this sample will be exported
                model.myParams.put("export_${samples.get(i)}".toString(), true)

                //break == no text
                if (!text) {
                    model.success = false
                    model.myParams.put("breaker_${samples.get(i)}".toString(), true)

                }

                //break == no cdf
                if (!netcdf) {
                    // model.success = false
                    model.myParams.put("breaker_${samples.get(i)}".toString(), true)
                }

                //extract class data for this sample
                String clazzKey = "class_${samples.get(i).toString().trim()}"

                //clazz for this sample
                String clazz = params.get(clazzKey)

                if (experimentModel.classes."${clazz}" == null) {
                    experimentModel.classes."${clazz}" = []
                }

                experimentModel.classes."${clazz}".add([sampleName: sampleName, id: samples.get(i).toString()])
            }
        }

        //finish the progress

        if (model.success) {
            session[params.sessionId] = 100

            def exportOnly = false

            if (params.exportOnly != null && params.exportOnly.toString().toLowerCase() == "on"){
                exportOnly = true
            }
            //schedule the calculation and render the next view
            scheduleCalculation(experimentModel, params.sessionId,exportOnly)

            //assign column
            studie.databaseColumn = column
            studie.save(flush: true)
        }
        else {
            session[params.sessionId] = -1

            //render the template again
            render(template: "selectSamples", model: model)
        }

    }

    /**
     * does the actual scheduling
     * @param model
     */
    private void scheduleCalculation(Map model, def sessionId, def exportOnly = true) {

        try {

            session[sessionId] = 0

            //generate our actual experiment
            Experiment exp = buildExperimentFromModel(model)

            int steps = exp.classes.size() + 1

            if (exportOnly == false){
                //schedule the imports
                exp.classes.eachWithIndex {ExperimentClass clazz, int index ->
                    binBaseSchedulingService.scheduleImport(clazz)

                    double v = (double) index / (double) steps * 100;

                    session[sessionId] = v
                }
            }


            exp = referencedBased(model, exp)

            //schedule the exports
            binBaseSchedulingService.scheduleExport(exp)


            session[sessionId] = 100

            render(template: "schedulingIsDone", model: [experiment: exp,exportOnly:exportOnly])


        }
        catch (Exception e) {
            log.error(e.getMessage(), e)

            throw new RuntimeException(e)
        }
    }

    private Experiment referencedBased(Map model, Experiment exp) {
//we need to modify and generate a new sop
        if (model.referenceStudy) {

            Element sopXML = XmlHandling.readXml(new ByteArrayInputStream(sopService.getDefaultSopXML().bytes))

            sopXML.getChildren("transform").each { Element child ->
                if (child.getChildren("reference") != null && child.getChildren("reference").size() > 0) {
                    child.removeChildren("reference")
                }

                Element reference = new Element("reference")
                reference.setAttribute("experiment", model.referenceStudy.id.toString())

                child.addContent(reference)
            }

            String newSop = new XMLOutputter(Format.getPrettyFormat()).outputString(sopXML)
            String id = exp.getId() + "_ref_" + model.referenceStudy.id + "_" + System.currentTimeMillis() + ".stat"

            sopService.uploadSopFile(id, newSop)

            log.info("uploaded new sop file with name: ${id} and content \n\n ${newSop}")
            exp.setSopUrl(id)
        }

        return exp;
    }

    /**
     * creates the complete experiment from the given model
     * @param model
     * @return
     */
    private Experiment buildExperimentFromModel(Map model) {
        Experiment exp = new Experiment()
        exp.column = model.database
        exp.id = model.studie.id

        Map classes = model.classes
        def keys = classes.keySet()

        ExperimentClass[] experimentClasses = new ExperimentClass[keys.size()]

        int counter = 0
        keys.each {String key ->

            experimentClasses[counter] = new ExperimentClass()
            experimentClasses[counter].id = key

            List samples = classes."${key}"

            ExperimentSample[] experimentSamples = new ExperimentSample[samples.size()]

            for (int i = 0; i < experimentSamples.length; i++) {
                experimentSamples[i] = new ExperimentSample()
                experimentSamples[i].name = samples.get(i).sampleName
                experimentSamples[i].id = samples.get(i).id
            }

            experimentClasses[counter].samples = experimentSamples
            experimentClasses[counter].column = model.database

            counter++
        }

        exp.classes = experimentClasses

        return exp;
    }
    /**
     * fires an update for the given session
     */
    def ajaxRenderProgress = {
        try {
            render "${session[params.id]}"
        }
        catch (Exception e) {
            log.error(e.getMessage(), e)
            render "-1"
        }
    }

    def ajaxHasTxtFile = {

        BBExperimentSample sample = BBExperimentSample.get(params.id)
        boolean result = binBaseSystemService.txtFileExist(sample.toString())

        render "${result}"
    }

    def ajaxHasCDFFile = {

        BBExperimentSample sample = BBExperimentSample.get(params.id)
        boolean result = binBaseSystemService.cdfFileExist(sample.toString())

        render "${result}"
    }

    /**
     * check for all the cdf files for a given lcass
     */
    def ajaxHasCDFFileForClass = {

        Map result = [:]
        BBExperimentClass.get(params.id).samples.each {
            result.put(it.id,binBaseSystemService.cdfFileExist(it.toString()))
        }

        render result as JSON
    }


    /**
     * check for all the cdf files for a given lcass
     */
    def ajaxHasTextFileForClass = {

        Map result = [:]
        BBExperimentClass.get(params.id).samples.each {
            result.put(it.id,binBaseSystemService.txtFileExist(it.toString()))
        }

        render result as JSON
    }


    /**
     * simple check if the study was already calculated on the binbase side of the system
     */
    def ajaxStudieExistInDatabase = {

        if (params.referenceStudy.toString() != "") {
            params.referenceStudy = params.referenceStudy.toString().split(":")[0].toString()
        }

        def database = params.column
        def study = params.referenceStudy

        Experiment exp = binBaseSystemService.getBinbaseEjbService().getExperiment(database,study,BinBaseConfigReader.key)

        if (exp.classes.size() == 0){
            render text: """<script type="text/javascript">\$("#nextStep").hide();</script>""",
                    contentType: 'text/javascript'
            render """<div class="element"><div class="errors">sorry the study <a href="${g.createLink(controller:"studie", action:"show", id:"${study}")}" target="_blank">${study}</a> was not yet calculated in the ${database} database. Please select another study or calculate this study first and try again when the calculation finished</div><div>"""
        }
        else{
            render text: """<script type="text/javascript">\$("#nextStep").show();</script>""",
                    contentType: 'text/javascript'
        }
    }


}
