package minix

import binbase.web.core.BBExperimentSample

class SampleController {
  def scaffold = true

  /**
   * sets a comment for a sample
   */
  def ajaxSetComment = {
    Sample sample = Sample.get(params.id)
    sample.comment = params.value

    if (sample.validate()) {
      sample.save(flush: true)
      render """<div class="message">defined '${params.value}' as comment for sample: ${sample.id} - ${sample.toString()}</div> """
    }
    else {
      render(template: "/shared/validation_error", model: [errorBean: sample, message: """there was something wrong with your provided value: '${params.value}' for sample: ${sample.id} - ${sample.toString()}"""])
    }
  }

  /**
   * sets a label for a sample
   */
  def ajaxSetLabel = {
    Sample sample = Sample.get(params.id)
    sample.label = params.value

    if (sample.validate()) {
      sample.save(flush: true)
      render """<div class="message">defined '${params.value}' as label for sample: ${sample.id} - ${sample.toString()}</div> """
    }
    else {
      render(template: "/shared/validation_error", model: [errorBean: sample, message: """there was something wrong with your provided value: '${params.value}' for sample: ${sample.id} - ${sample.toString()}"""])
    }
  }

  /**
   * sets a filename for a sample
   */
  def ajaxSetFileName = {
    Sample sample = Sample.get(params.id)
    sample.fileName = params.value

    if (sample.validate()) {
      sample.save(flush: true)
      render """<div class="message">defined '${params.value}' as file name for sample: ${sample.id} - ${sample.toString()}</div> """
    }
    else {
      render(template: "/shared/validation_error", model: [errorBean: sample, message: """there was something wrong with your provided value: '${params.value}' for sample: ${sample.id} - ${sample.toString()}"""])
    }
  }

  /**
   * sets a filename for a sample
   */
  def ajaxSetFileVersion = {
    def sample = BBExperimentSample.get(params.id)
    sample.fileVersion = params.value.toString().toInteger()

    if (sample.validate()) {
      sample.save(flush: true)
      render """<div class="message">defined '${params.value}' as file version for sample: ${sample.id} - ${sample.toString()}</div>

                    <script type="text/javascript">
                        \$(function() {
                      jQuery.ajax({type:'POST',data:{'value': this.value}, url:"${g.createLink(controller: 'sample',action: 'ajaxRefreshDataFiles',id:sample.id)}",success:function(data,textStatus){jQuery('#sample_datafile_${sample.id}').html(data);},error:function(XMLHttpRequest,textStatus,errorThrown){}});return false;
                        });
                    </script>


        """
    }
    else {
      render(template: "/shared/validation_error", model: [errorBean: sample, message: """there was something wrong with your provided value: '${params.value}' for sample: ${sample.id} - ${sample.toString()}"""])
    }
  }

    /**
     * refreshes the datafiles field
     */
   def ajaxRefreshDataFiles = {
       def sample = BBExperimentSample.get(params.id)

       render template: "/experimentClass/sample/renderDataFiles", model: [sample:sample,clazz:sample.experimentClass]
   }

}
