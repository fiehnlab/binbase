package minix

import core.MetaDataService

/**
 * used for communications
 */
class CommunicationsController {

    def scaffold = true

    CommunicationsService communicationsService

    /**
     * show the studie data as xml
     */
    def studieDataAsXML = {
        Studie studie = Studie.get(params.id)
        if (params.id) {
            if (studie != null) {
                response.setContentType("text/xml")

                render(communicationsService.getMetaDateAsXML(studie.id))
            }
            else {
                response.setStatus(500)
                response.setContentType("text/xml")

                render "<error><errorMessage>sorry study not found with id ${params.id}</errorMessage></error>"
            }
        }
        else {
            response.setStatus(500)
            response.setContentType("text/xml")

            render "<error><errorMessage>sorry you need to provide an id for this url</errorMessage></error>"

        }
    }

    /**
     * is this studie public
     */
    def studieIsPublicAsXML = {
        Studie studie = Studie.get(params.id)
        if (params.id) {
            if (studie != null) {
                response.setContentType("text/xml")

                render(communicationsService.studieIsPublic(studie.id))
            }
            else {
                response.setStatus(500)
                response.setContentType("text/xml")

                render "<error><errorMessage>sorry study not found with id ${params.id}</errorMessage></error>"
            }
        }
        else {
            response.setStatus(500)
            response.setContentType("text/xml")

            render "<error><errorMessage>sorry you need to provide an id for this url</errorMessage></error>"

        }
    }

}
