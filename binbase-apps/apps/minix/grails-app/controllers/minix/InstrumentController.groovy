package minix

class InstrumentController {
  def scaffold = false

  /**
   * checks that the user has management rights, no everybody should be able
   * to create and delete instruments
   */
  def beforeInterceptor = {
    if (ShiroUser.hasManagmentRights() == false) {
      flash.message = "sorry you have no permission to access this data!"
      redirect(controller: "home", action: "showMemberHome")
    }

  }

  /**
   * list the instruments, should be the only possible method,
   * since the rest is done by implementations of instruments
   */
  def list = {

    params.max = Math.min(params.max ? params.int('max') : 10, 100)
    [instrumentInstanceList: Instrument.list(params), instrumentInstanceTotal: Instrument.count()]
  }
}
