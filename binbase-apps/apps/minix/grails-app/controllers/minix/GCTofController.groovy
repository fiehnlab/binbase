package minix

class GCTofController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def scaffold = true

    /**
     * checks that the user has management rights, no everybody should be able
     * to create and delete instruments
     */
    def beforeInterceptor = {
        if (ShiroUser.hasManagmentRights() == false) {
            flash.message = "sorry you have no permission to access this data!"
            redirect(controller: "home", action: "showMemberHome")
        }

    }

    /**
     * renders the method dialog
     */
    def ajaxMethodDialog = {
        render template: "methodDialog", model: params

    }

    /**
     * removes a method
     */
    def ajaxRemoveMethod = {
        GCTof gctof = GCTof.get(params.id)
        String template = ""

        if (params.type.toString() == "ms") {

            gctof.removeFromMsMethods(params.methodName.toString())
            template = "renderMSMethods"
        }
        else if (params.type.toString() == "gc") {

            gctof.removeFromGcMethods(params.methodName.toString())
            template = "renderGCMethods"

        }
        else if (params.type.toString() == "dp") {

            gctof.removeFromDpMethods(params.methodName.toString())
            template = "renderDpMethods"
        }
        else {
            flash.message = "sorry unknown method specifified: ${params.type}"
            render("sorry you specified an unknown method: ${params.type}")
            return
        }

        render(template: template, model: [instance: gctof])
    }

    /**
     * adds a method
     */
    def ajaxSaveMethod = {

        GCTof gctof = GCTof.get(params.id)

        if (gctof == null) {
            flash.message = "sorry we did not find a GC-Tof with the specified id"
            redirect(action: "list")
        }

        String template = ""

        if (params.methodName.toString().size() > 0) {
            if (params.type.toString() == "ms") {
                if (params.methodName.toString().startsWith("\\Mass Spectrometer (MS) Methods\\") == false) {
                    params.methodName = "\\Mass Spectrometer (MS) Methods\\${params.methodName}"
                }
                gctof.addToMsMethods(params.methodName.toString())
                template = "renderMSMethods"
            }
            else if (params.type.toString() == "gc") {
                if (params.methodName.toString().startsWith("\\Gas Chromatograph (GC) Methods\\") == false) {
                    params.methodName = "\\Gas Chromatograph (GC) Methods\\${params.methodName}"
                }
                gctof.addToGcMethods(params.methodName.toString())
                template = "renderGCMethods"

            }
            else if (params.type.toString() == "dp") {
                if (params.methodName.toString().startsWith("\\Data Processing (DP) Methods\\") == false) {
                    params.methodName = "\\Data Processing (DP) Methods\\${params.methodName}"
                }
                gctof.addToDpMethods(params.methodName.toString())
                template = "renderDpMethods"
            }
            else {
                flash.message = "sorry unknown method specifified: ${params.type}"
            }
        }

        if (gctof.validate()) {
            log.info "operation was successful, rendering: ${template}"
            gctof.save(flush: true)
            render(template: template, model: [instance: gctof])

        }
        else {
            log.error(gctof.errors)
            params.instance = gctof
            render template: "methodDialog", model: params
        }


    }

    def list = {
        redirect(controller: "instrument", action: "list", params: params)
    }
}
