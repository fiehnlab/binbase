package minix
class CommentController {

    def scaffold = true

    /**
     * creates a new comment the ajax way
     */
    def createAjax = {
        render template: "renderCreateDialog", model: params
    }

    /**
     * saves a comment the ajax way
     */
    def saveAjax = {

        try {
            if (params.type.toString() == "studie") {
                Studie studie = Studie.get(params.id)
                if (studie != null) {
                    Comment comment = new Comment()
                    comment.text = params.text.toString().trim()

                    if (comment.validate()) {
                        comment = comment.save(flush: true)

                        studie.addToComments(comment)
                        studie.save(flush: true)
                        render template: "/studie/renderComments", model: [studieInstance: studie]
                    }
                    else {
                        params.comment = comment

                        render template: "renderCreateDialog", model: params

                    }
                }
                else {
                    render "no studie found with id ${params.id}"
                }
            }

            else {
                render "sorry not supported comment type!"
            }
        }
        catch (Error e) {
            e.printStackTrace()
        }
    }

}