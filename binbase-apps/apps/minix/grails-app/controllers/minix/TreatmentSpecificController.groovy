package minix
class TreatmentSpecificController {
  def scaffold = true


  def ajaxSetValue = {
    TreatmentSpecific s = TreatmentSpecific.get(params.id)
    s.value = params.value.toString()

    if (s.validate()) {
      s.save(flush: true)
      render """<div class="message">defined '${params.value}' set as value</div> """
    }
    else {
      render(template: "/shared/validation_error", model: [errorBean: s, message: """there was something wrong with your provided value: '${params.value}' for the object: ${s.id}"""])
    }
  }
}
