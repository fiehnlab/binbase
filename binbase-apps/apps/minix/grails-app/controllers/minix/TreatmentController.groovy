package minix
class TreatmentController {
  def scaffold = true

  def ajaxSetDescription = {
    Treatment s = Treatment.get(params.id)
    s.description = params.value.toString()

    if (s.validate()) {
      s.save(flush: true)
      render """<div class="message">defined '${params.value}' set as description</div> """
    }
    else {
      render(template: "/shared/validation_error", model: [errorBean: s, message: """there was something wrong with your provided value: '${params.value}' for the object: ${s.id}"""])
    }  }
}
