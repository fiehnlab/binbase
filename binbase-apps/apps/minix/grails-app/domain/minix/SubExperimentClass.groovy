package minix

/**
 * a sub experiment class
 */
class SubExperimentClass extends ExperimentClass{

    Set<SubSample> associatedSamples

    static constraints = {
    }

    static hasMany = [associatedSamples:SubSample]

    /**
     * samples of this class
     * @return
     */
    @Override
    SortedSet getSamples() {
        //set can't be modified!
        SortedSet set = new TreeSet()

        associatedSamples.each {
            set.add( it.relatesToSample )
        }
        return Collections.unmodifiableSortedSet(set)
    }


    @Override
    void setSamples(SortedSet<Sample> samples){

    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        tablePerHierarchy false

    }
}
