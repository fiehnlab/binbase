package minix
class Treatment implements Comparable {

    static auditable = true

    static searchable = {
        only: ['description', 'specifics']
        mapping {
            supportUnmarshall false
        }
    }

    /**
     * relations
     */
    static hasMany = [
            specifics: TreatmentSpecific
    ]

    static constraints = {
        description(blank: false, unique: true)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false

    }

    /**
     * specifics
     */
    SortedSet<TreatmentSpecific> specifics

    /**
     * description of the treatment
     *
     */
    String description

    /**
     * displaying the description
     * @return
     */
    String toString() {
        return description
    }

    int compareTo(Object t) {
        return toString().compareTo(t.toString())
    }
}
