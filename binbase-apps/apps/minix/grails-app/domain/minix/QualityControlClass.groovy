package minix

/**
 * specific class for quality control samples
 */
class QualityControlClass extends CalibrationClass {


    static auditable = true

    static searchable = {
        mapping {
            supportUnmarshall false
            only: ['name']
        }
    }

    static hasMany = [samples: QualityControlSample]

    static constraints = {
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        tablePerHierarchy false

    }
}
