package minix
/**
 * defines an architectures
 */
class Architecture {

    static auditable = true

    static searchable = {
        mapping {
            supportUnmarshall false
        }

    }

    static hasMany = [studies: Studie, designs: StudieDesign, instruments: Instrument]
    /**
     * general constraints
     */

    static constraints = {
        name(size: 3..25, blank: false, unique: true)
        description(size: 3..255, blank: false)
        relatedController(unique: false, blank: false, nullable: false)
        relatedAction(unique: false, blank: false, nullable: false)
        rawDataFile(nullable: true)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
    }

    /**
     * related studies
     */
    SortedSet<Studie> studies

    /**
     * designs
     */
    SortedSet<StudieDesign> designs

    /**
     * all instruments for this architecture
     */
    SortedSet<Instrument> instruments
    /**
     * name of the architecture
     */
    String name

    /**
     * description of the archtecture
     */
    String description

    /**
     * related controller for scheduling
     */
    String relatedController = "generic"

    /**
     * related action for scheduling
     */
    String relatedAction = "scheduleGeneric"

    String toString() {
        return name
    }

    /**
     * rawDataFile for architectures
     */
    FileType rawDataFile = FileType.BINBASE_TXT

}
