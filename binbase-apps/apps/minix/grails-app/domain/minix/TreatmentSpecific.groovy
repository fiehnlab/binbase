package minix
/**
 * specific treatment
 */
class TreatmentSpecific implements Comparable {

    static auditable = true

    static searchable = {
        only: ['treatment', 'parent', 'value']

        mapping {
            supportUnmarshall false
        }
    }

    /**
     * belongs to
     */
    static belongsTo = Treatment

    static hasMany = [subTreatmentSpecifics: TreatmentSpecific]

    static constraints = {
        treatment(nullable: false, blank: false)
        value(nullable: false, blank: false)
        subTreatmentSpecifics(nullable: true)
        parent(nullable: true)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false

    }

    /**
     * the related treatment
     */
    Treatment treatment

    /**
     * is's possible to have to each specific several sub treatment
     */
    SortedSet<TreatmentSpecific> subTreatmentSpecifics

    /**
     * a parent
     */
    TreatmentSpecific parent

    /**
     * the value
     */
    String value

    String toString() {
        return "${treatment?.description} - ${value}"
    }

    String toParentString() {
        String result = toString()
        if (parent != null) {
            result = parent.toParentString() + " > " + result
        }

        return result

    }

    int compareTo(Object t) {
        return toString().compareTo(t.toString())
    }
}
