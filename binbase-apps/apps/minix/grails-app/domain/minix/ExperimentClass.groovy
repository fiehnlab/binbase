package minix
/**
 * defines an experiment class
 */
class ExperimentClass extends BaseClass implements Serializable, Comparable {

    static auditable = true

    static searchable = {
        mapping {
            supportUnmarshall false
            only:['name']
        }
    }

    static hasMany = [samples: Sample]

    static constraints = {
        treatmentSpecific(nullable: false)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        tablePerHierarchy false

    }

    /**
     * associated treatment with specification
     */
    TreatmentSpecific treatmentSpecific

}
