package minix

import binbase.web.core.BBExperimentSample
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.date.DateUtil

/**
 * specific sample for calibration curves
 */
class CalibrationSample extends BBExperimentSample implements Serializable, Comparable {

    static auditable = true


    static searchable = {
        only: ["fileName", "comment", "label", 'experimentClass']
        mapping {
            supportUnmarshall false
        }

    }

    static belongsTo = [experimentClass: CalibrationClass, concentration: QualityControlSampleConcentration]

    static hasMany = [comments: Comment]

    static constraints = {

        //range defines the current version
        fileVersion(range: 1..200)
        concentration(nullable: true)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        tablePerHierarchy false

    }

    /**
     * associated concentration
     */
    QualityControlSampleConcentration concentration

    /**
     * the scheduled date for this sample
     */
    Date scheduleDate = DateUtil.stripTime(new Date())

    /**
     * associated comments
     */
    SortedSet<Comment> comments

    int fileVersion = 1

    int compareTo(Object t) {
        if (t instanceof CalibrationSample) {
            CalibrationSample s = t


            if (s.concentration != null && concentration != null) {
                return concentration.concentration.compareTo(s.concentration.concentration)
            }
            if (s.fileName != null && fileName != null) {
                return fileName.compareTo(s.fileName)
            }
            if (s.id != null && id != null) {
                return id.compareTo(s.id)
            }

        }
        return toString().compareTo(t.toString())
    }


    String toString() {
        if (concentration != null) {
            return "${fileName}_${fileVersion}"
        }
        else {
            return "${fileName}_${fileVersion}"
        }
    }

    /**
     * generates the file name to be used with metadata and so
     * @return
     */
    public String generateName() {
        return "${fileName}_${fileVersion}"
    }
}
