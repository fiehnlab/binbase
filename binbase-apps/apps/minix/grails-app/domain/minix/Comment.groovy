package minix
/**
 * a basic comment needed to supply additional informations to data
 */
class Comment implements Comparable {

    static auditable = true

    static searchable = {
        only: ['name']
        supportUnmarshall false
        root false
    }

    static constraints = {
        text minSize: 3, nullable: false, size: 3..300000
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        text type: "text"
    }

    /**
     * the actual text of the comment
     */
    String text

    Date dateCreated

    Date lastUpdated

    int compareTo(Object t) {
        if (t != null) {
            return lastUpdated.compareTo(t.lastUpdated)
        }
        return 0
    }

    String toString(){
        return text
    }

}
