package minix
/**
 * required for designs
 */
class DesignSpeciesConnection implements Comparable {

    static auditable = true

    static searchable = {
        mapping {
            supportUnmarshall false
        }

    }

    /**
     * belongs to a design
     */
    static belongsTo = [design: StudieDesign]

    /**
     * associatiosn
     */
    static hasMany = [organs: Organ]

    /**
     * defined constrains
     */
    static constraints = {
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']

        version false
    }

    /**
     * related species
     */
    Species species

    /**
     * all organs for this set
     */
    SortedSet<Organ> organs

    /**
     * the design it belongs to
     */
    StudieDesign design

    String toString() {
        return "${species} - ${organs}"
    }

    int compareTo(Object t) {
        return toString().compareTo(t.toString())
    }
}
