package minix

class Platform {

    static belongsTo = [Vendor]

    static hasMany = [instruments: Instrument]

    static constraints = {
        description(nullable: true)
    }

    Vendor vendor

    String name

    String description

    String toString() {
        return name
    }

    /**
     * related instruments
     */
    Set<Instrument> instruments
}
