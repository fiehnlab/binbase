package minix

/**
 * defines the concentration for a set of samples
 */
class QualityControlSampleConcentration {

    static constraints = {
        concentration(unique: true, nullable: false)
    }

    /**
     * mapping
     */
    static mapping = {
        id generator: 'sequence', params: [sequence: 'MINIX_ID']
        version false
        tablePerHierarchy false

    }

    /**
     * the concentration in m/ml
     */
    double concentration

    /**
     * related samples
     */
    static hasMany = [samples: CalibrationSample]

}
