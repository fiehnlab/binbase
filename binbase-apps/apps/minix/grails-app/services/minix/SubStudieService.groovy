package minix

import grails.validation.ValidationException
import minix.progress.ProgressHandler
import org.apache.ivy.core.search.OrganisationEntry
import org.apache.shiro.SecurityUtils

/**
 * used to generate substudies based on samples of an existing studie
 */
class SubStudieService {

    UserService userService

    boolean transactional = false

    /**
     * creates the actual studie based on the given information
     * @param studie
     * @param List
     * @return
     */
    SubStudie createSubStudie(Studie studie, List<Sample> samples, String name, String description, boolean singleClass = true, ProgressHandler progressHandler = null, String progressId = null) {

        int counter = 0
        if (progressHandler != null) {
            double percent = 0
            progressHandler.handleProgress(progressId, percent, [])
            counter = counter + 1
        }

        SubStudie result = new SubStudie()
        result.name = name
        result.title = name
        result.description = description
        result.architecture = studie.architecture
        result.origin = studie
        result.canModify()
        studie.addToSubStudies(result)
        studie.save()

        log.info("generated substudie...")
        if (result.validate() == false) {
            log.error(result.errors)

            throw new ValidationException("sorry there was a validation error for the creation of this substudy, based on: ${studie}", result.errors);
        }
        result.save(flush: true)

        counter = increaseProgress(progressHandler, counter, samples,progressId)

        //we are generating one big class
        if (singleClass) {
            //we combine all the samples to one simple big class
            SubExperimentClass sub = new SubExperimentClass()

            sub.organ = Organ.findByName("None")
            sub.species = Species.findByName("None")
            sub.studie = result
            sub.experiment = result
            sub.treatmentSpecific = generateTreatmentSpecific()
            sub.name = "sub class - ${new Date()}"
            sub.studie = result
            sub.experiment = result
            result.addToClasses(sub)
            sub.save(flush: true)

            samples.each { Sample s ->
                SubSample subSample = new SubSample()
                subSample.clazz = sub
                subSample.relatesToSample = s

                sub.addToAssociatedSamples(subSample)
                subSample.save()

                counter = increaseProgress(progressHandler, counter, samples,progressId)

            }

            sub.save(flush: true)


            if (sub.validate() == false) {
                throw new ValidationException("sorry there was a validation error for the creation of this substudy, based on: ${studie}", sub.errors);

            }
        }
        //we are generating subclasses based on the existing class
        else {
            def classes = new HashMap<Long, SubExperimentClass>()

            samples.each { Sample sample ->
                if (classes.containsKey(sample.experimentClass.id) == false) {

                    SubExperimentClass sub = new SubExperimentClass()
                    ExperimentClass c = sample.experimentClass
                    sub.name = "sub class - ${new Date()} of ${c.name}"
                    sub.organ = c.organ
                    sub.species = c.species
                    sub.treatmentSpecific = generateTreatmentSpecific(c.treatmentSpecific.treatment.description, c.treatmentSpecific.value)
                    sub.studie = result
                    sub.experiment = result
                    sub.save(flush: true)

                    result.addToClasses(sub)
                    classes.put(c.id, sub)
                }

                SubExperimentClass sub = classes.get(sample.experimentClass.id)
                SubSample subSample = new SubSample()
                subSample.clazz = sub
                subSample.relatesToSample = sample

                sub.addToAssociatedSamples(subSample)
                subSample.save()
                sub.save(flush: true)

                classes.put(sample.experimentClass.id, sub)

                if (sub.validate() == false) {
                    throw new ValidationException("sorry there was a validation error for the creation of this substudy, based on: ${studie}", sub.errors);

                }

                counter = increaseProgress(progressHandler, counter, samples,progressId)

            }
        }

        result.forceLock()
        result.save()

        //assign the study to all users who had rights to the original studie
        studie.users.each { ShiroUser user ->
            userService.assignStudy(user, result)
        }

        if (progressHandler != null) {
            double percent = 100
            progressHandler.handleProgress(progressId, percent, [])
            counter = counter + 1
        }
        return result

    }

    /**
     * used to handle our progress listener
     * @param progressHandler
     * @param counter
     * @param samples
     * @return
     */
    private int increaseProgress(ProgressHandler progressHandler, int counter, List<Sample> samples,String progressId) {
        if (progressHandler != null) {
            double percent = ((double) counter / ((double) samples.size() + 4) * 100)
            progressHandler.handleProgress(progressId, percent, [])
            counter = counter + 1
        }
        counter
    }

    /**
     * just creates a treatment specific
     * @param treatmentDesciption
     * @param treatmentValue
     * @return
     */
    private TreatmentSpecific generateTreatmentSpecific(String treatmentDesciption = "None", String treatmentValue = "None") {
        Treatment treatment = Treatment.findByDescription(treatmentDesciption)

        if (treatment == null) {
            treatment = new Treatment()
            treatment.description = treatmentDesciption
            treatment.save()
        }

        TreatmentSpecific treat = new TreatmentSpecific()
        treat.setTreatment(treatment)
        treat.setValue(treatmentValue)
        treat.save()
        treat
    }
}
