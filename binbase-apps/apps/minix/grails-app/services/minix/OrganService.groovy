package minix

import binbase.web.core.BBExperimentClass
import minix.progress.ProgressHandler
import binbase.web.core.BBOrgan
import binbase.web.core.BBSpecies
import org.hibernate.exception.ConstraintViolationException

class OrganService {

    boolean transactional = true

    /**
     * combines the listed organs and updates the related studies and designs
     * finaly the existing organs will be deleted from the system
     * @param kingdom
     * @param species
     * @return
     */
    Organ combineOrgan(String name, Collection<Organ> organs, ProgressHandler progressHandler = null, String progressId = null) {

        int counter = 0

        if (progressHandler != null) {
            double percent = 0
            progressHandler.handleProgress(progressId, percent, [])
            counter = counter + 1
        }
        else {
            println "no progress handler assigned..."
        }


        def temp = Organ.findByName(name)

        if (temp) {
            if (!organs.contains(temp)) {
                organs.add(temp)
            }
        }
        def species = new HashSet<BBSpecies>()

        Organ combined = new Organ()

        def organName = "TEMP_ORGAN_${System.currentTimeMillis()}"

        combined.name = organName

        combined.save(flush: true)

        //association the species of each organ with the new organ
        organs.each {Organ organ ->
            if (organ) {
                try {

                    if (progressHandler != null) {
                        double percent = ((double) counter / ((double) organs.size() * 2) * 100)
                        progressHandler.handleProgress(progressId, percent, [])
                        counter = counter + 1
                    }

                    def speciesToRemove = new HashSet<BBSpecies>()

                    organ.speciees.each {BBSpecies sp ->
                        if (sp) {
                            if (!speciesToRemove.contains(sp)) {
                                speciesToRemove.add(sp)
                            }

                            if (!species.contains(sp)) {
                                species.add(sp)
                            }
                            if (combined.speciees == null) {
                                combined.addToSpeciees(sp)
                                sp.addToOrgans(combined)
                                sp.save(flush: true)
                                combined.save(flush: true)
                            }
                            else if (!combined.speciees.contains(sp)) {
                                combined.addToSpeciees(sp)
                                sp.addToOrgans(combined)
                                sp.save(flush: true)
                                combined.save(flush: true)
                            }
                        }
                    }

                    //detach species from this organ
                    speciesToRemove.each {BBSpecies sp ->
                        organ.removeFromSpeciees(sp)
                        sp.save(flush: true)
                    }

                    //assign new experiment classes
                    def classes = []
                    BBExperimentClass.findAllByOrgan(organ).each {
                        classes.add(it)
                    }

                    classes.each {BBExperimentClass c ->
                        c.organ = combined
                        c.save(flush: true)
                    }

                    //find all study designs
                    DesignSpeciesConnection.executeQuery("select a from DesignSpeciesConnection a join a.organs b where b.id = ? ", [organ.id]).each {DesignSpeciesConnection design ->
                        design.removeFromOrgans(organ)
                        design.addToOrgans(combined)
                        design.save(flush: true)
                    }

                    organ.save(flush: true)

                    // organ.delete(flush: true)
                } catch (Exception e) {
                    e.printStackTrace()
                    log.error(e)
                }

                if (progressHandler != null) {
                    double percent = ((double) counter / ((double) organs.size() * 2) * 100)
                    progressHandler.handleProgress(progressId, percent, [])
                    counter = counter + 1
                }
            }
        }

        organs.each {
            if (it) {
                it.delete(flush: true)
            }
        }
        combined.save(flush: true)


        combined.name = name

        if (!combined.validate()) {
            log.info(combined.errors)
        }
        combined.save(flush: true)

        if (progressHandler != null) {
            double percent = 100
            progressHandler.handleProgress(progressId, percent, [])
            counter = counter + 1
        }

        return combined

    }

}
