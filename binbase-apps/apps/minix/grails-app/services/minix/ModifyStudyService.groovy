package minix

/**
 * used to modify studies
 */
class ModifyStudyService {

    boolean transactional = false

    /**
     * used to change the architecture of a study
     * @param studie
     * @param architecture
     * @return
     */
    Studie changeArchitecture(Studie study, Architecture architecture) {

        Architecture old = study.architecture
        old.removeFromStudies(study)
        old.save()

        study.architecture = architecture
        study.save()

        architecture.addToStudies(study)
        architecture.save()

        return study
    }
}
