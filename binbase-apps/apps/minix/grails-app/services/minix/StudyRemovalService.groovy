package minix

import binbase.web.core.BBContent
import binbase.web.core.BBResult
import minix.progress.ProgressHandler

/**
 * used to delete studies
 */
class StudyRemovalService {

	boolean transactional = false

	AttachmentService attachmentService

	UserService userService

	ModifyClassService modifyClassService

	/**
	 * deletes the given study
	 * @param studieInstance
	 * @return
	 */
	def delete(Studie studieInstance, ProgressHandler progressHandler = null, String progressId = null) {
		Studie.withTransaction {

			int total = 3 + studieInstance.results.size() + studieInstance.attachments.size() + studieInstance.classes.size() + studieInstance.users.size()
			int counter = 0
			if (progressHandler != null) {
				double percent = 0
				progressHandler.handleProgress(progressId, percent, [])
				counter = counter + 1
			}

			studieInstance.aquisitionTable = null

			StudieDesign design = StudieDesign.findByGeneratedStudie(studieInstance)

			if (design != null) {
				//delete the associated design
				design.delete(flush: true)
			}

			counter = increaseProgress(progressHandler, counter, total, progressId)


			def results = []

			studieInstance.results.each { BBResult result ->
				results.add(result)
			}
			results.each { BBResult result ->

				result.experiment.removeFromResults(result)
				result.experiment = null
				BBContent.findAllByResult(result).each { it.delete() }

				result.delete()
				counter = increaseProgress(progressHandler, counter, total, progressId)

			}

			studieInstance.save(flush: true)

			def attachments = FileAttachment.findAllByStudie(studieInstance)

			attachments.each { FileAttachment fileAttachmentInstance ->
				attachmentService.deleteFileFromStudie(fileAttachmentInstance)
				counter = increaseProgress(progressHandler, counter, total, progressId)

			}

			studieInstance.save(flush: true)

			if (studieInstance instanceof MergedStudie) { //can't delete classes from merged studies
				log.info("detaching member studies...")
				studieInstance.members = null
				studieInstance.save(flush: true)
				studieInstance.refresh()

			} else {
				def classes = []
				studieInstance.classes.each {
					classes.add(it)
				}

				classes.each {
					modifyClassService.removeClass(it, studieInstance)
					counter = increaseProgress(progressHandler, counter, total, progressId)
				}
			}
			def users = []

			studieInstance.users.each { users.add(it) }

			users.each { ShiroUser user ->
				userService.revokeStudyFromUser(user, studieInstance)
				counter = increaseProgress(progressHandler, counter, total, progressId)
			}

			//if it's a substudie, let's remove it from the associated parents
			if (studieInstance instanceof SubStudie) {
				SubStudie sub = studieInstance
				sub.origin.removeFromSubStudies(sub)
				sub.origin = null
			}

			// removing from architecture
			def arch = studieInstance.architecture
			studieInstance.architecture = null
			arch.removeFromStudies(studieInstance)

			if (progressHandler != null) {
				double percent = 100
				progressHandler.handleProgress(progressId, percent, [])
				counter = counter + 1
			}
			studieInstance.delete(flush: true)
		}
	}

	private int increaseProgress(ProgressHandler progressHandler, int counter, int total, String progressId) {
		if (progressHandler != null) {
			double percent = ((double) counter / ((double) total) * 100)
			progressHandler.handleProgress(progressId, percent, [])
			counter = counter + 1
		}
		counter
	}
}
