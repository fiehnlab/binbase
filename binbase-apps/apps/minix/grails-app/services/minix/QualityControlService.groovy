package minix

import edu.ucdavis.genomics.metabolomics.binbase.bci.server.jmx.DatabaseJMXFacade
import groovy.sql.Sql

class QualityControlService {

    DatabaseJMXFacade binbaseDatabaseAccess

    boolean transactional = false

    /**
     *
     * returns the last failed samples for the selected database
     * @param db
     *
     * @param count
     * @return
     */
    List<Sample> getLastFailedSamplesForDB(String db, int count = 50) {
        Sql sql = createBinBaseConnection(db)

        Set<Sample> result = new HashSet<Sample>()

        sql.eachRow("select sample_name from samples where sample_id in (select sample_id from samples order by sample_id DESC limit ?) and correction_failed = 'TRUE' and visible = 'TRUE' order by date DESC",[count]) {
            String name = it.sample_name

            try{
                String[] s = name.split("_")

                Sample sample = Sample.findByFileNameAndFileVersion(s[0],s[1])

                if (sample){
                    sample.save()
                    result.add(sample)
                }
            }
            catch (Exception e){
                log.error(e)
            }
        }

        sql.close()

        return result.toList()
    }

    /**
     * needs some form of pooling
     * @param databaseName
     * @return
     */
     Sql createBinBaseConnection(String databaseName) {

        def sql = Sql.newInstance("jdbc:postgresql://${binbaseDatabaseAccess.databaseServer}/${binbaseDatabaseAccess.database}", databaseName,
                binbaseDatabaseAccess.databaseServerPassword, "org.postgresql.Driver")

        return sql
    }

    /**
     * did this sample fail correction
     * @param sample
     * @param database
     * @return
     */
    boolean sampleFailed(Sample sample, String db){
        Sql sql = createBinBaseConnection(db)

        def result = sql.firstRow("select count(*) as c from samples where visible = 'TRUE' and sample_name = ? and correction_failed = 'TRUE' ",[sample.toString()])

        //if the result equals one, that means it failed
        boolean res = result.c == 1

        sql.close()
        return res
    }

    /**
     * was the sample calculated in the system
     * @param sample
     * @param db
     * @return
     */
    boolean sampleCalculated(Sample sample, String db){
        Sql sql = createBinBaseConnection(db)

        def result = sql.firstRow("select count(*) as c from samples where visible = 'TRUE' and sample_name = ?",[sample.toString()])

        //if the result equals one, that means it was calculated
        boolean res = result.c == 1

        sql.close()
        return res
    }

    /**
     * returns all the id's for the registered standards
     * @param db
     * @return
     */
    Collection<Map> getStandardIdsForDatabase(String db){

        Sql sql = createBinBaseConnection(db)

        List<Map> set = new Vector<Map>()

        sql.eachRow("select a.bin_id, b.name from standard a, bin b where a.bin_id = b.bin_id order by b.retention_index"){
            set.add([id:it.bin_id,name:it.name])
        }

        sql.close()

        return set
    }

    /**
     * returns the qualifier performance for the last n samples
     * @param database
     * @param instrumentIdentifier
     * @param standard
     * @param sampleCount
     * @return
     */
    List getRetentionIndexMarkerPerformanceForLastNSamples(String database, String instrumentIdentifier, Integer standard, Integer sampleCount = 100){

        Sql sql = createBinBaseConnection(database)

        List set = []

        String query = """

 select date, (massspec[cast(qualifier as int4)] / (select max(unnest) from (select unnest(massspec)) b)) as ratio,min_ratio,max_ratio,min_apex_sn,apex_sn ,  (massspec[cast(quantmass as int4)]) as intensity  from (

    select correction_failed, a.bin_id, a.retention_time,b.name,c.qualifier, (convertspectra(a.spectra)) as "massspec",sample_name,date,min_ratio,max_ratio,min_apex_sn,a.apex_sn,quantmass from spectra a, bin b, standard c,samples d where a.bin_id = b.bin_id and c.bin_id = b.bin_id and a.sample_id = d.sample_id
        and d.sample_id in (
             select sample_id from samples where visible = 'TRUE' and machine = ? order by date desc limit ?
        )
        and a.bin_id = ?  and sample_name  not like '%bl%'
    )
a

order by date


"""
        sql.eachRow(query,[instrumentIdentifier,sampleCount,standard]){def cursor ->

            set.add (createEntry(cursor) )
        }

        sql.close()

        return set

    }

    /**
     * gest all the values for the last n days
     */
    List getRetentionIndexMarkerPerformanceForLastNDays(String database, String instrumentIdentifier, Integer standard, Integer days = 30){

        Sql sql = createBinBaseConnection(database)

        List set = []

        String query = """

 select date, (massspec[cast(qualifier as int4)] / (select max(unnest) from (select unnest(massspec)) b)) as ratio,min_ratio,max_ratio ,min_apex_sn,apex_sn ,  (massspec[cast(quantmass as int4)]) as intensity from (

    select correction_failed, a.bin_id, a.retention_time,b.name,c.qualifier, (convertspectra(a.spectra)) as "massspec",sample_name,date,min_ratio,max_ratio,min_apex_sn,a.apex_sn,quantmass from spectra a, bin b, standard c,samples d where a.bin_id = b.bin_id and c.bin_id = b.bin_id and a.sample_id = d.sample_id
        and d.sample_id in (
             select sample_id from samples where visible = 'TRUE' and machine = '${instrumentIdentifier}' and date > now()::date - ${days}
        )
        and a.bin_id = ${standard}  and sample_name  not like '%bl%'
    )
a

order by date


"""
        sql.eachRow(query){def cursor ->

            set.add (createEntry(cursor) )
        }

        sql.close()

        return set

    }


    /**
     * gest all the values for the last n days
     */
    List getRetentionIndexMarkerPerformanceForDates(String database, String instrumentIdentifier, Integer standard, Long from, Long to){


        Sql sql = createBinBaseConnection(database)

        List set = []

        String query = """

 select date, (massspec[cast(qualifier as int4)] / (select max(unnest) from (select unnest(massspec)) b)) as ratio,min_ratio,max_ratio,min_apex_sn,apex_sn ,  (massspec[cast(quantmass as int4)]) as intensity  from (

    select correction_failed, a.bin_id, a.retention_time,b.name,c.qualifier, (convertspectra(a.spectra)) as "massspec",sample_name,date,min_ratio,max_ratio,min_apex_sn,a.apex_sn,quantmass from spectra a, bin b, standard c,samples d where a.bin_id = b.bin_id and c.bin_id = b.bin_id and a.sample_id = d.sample_id
        and d.sample_id in (
             select sample_id from samples where visible = 'TRUE' and machine = '${instrumentIdentifier}' and date between ? and ?
        )
        and a.bin_id = ${standard}  and sample_name  not like '%bl%'
    )
a

order by date


"""

        log.info("query for instrument: ${database} - ${instrumentIdentifier} - ${standard} - from: ${new java.sql.Date(from)} to: ${new java.sql.Date(to)}")
        log.info("sql: ${query}")

        sql.eachRow(query,[new java.sql.Date(from),new java.sql.Date(to)]){def cursor ->

            set.add (createEntry(cursor) )

        }

        sql.close()

        return set

    }

    /**
     * return the qualifier retention index marker performance report for the given study and standard
     * @param studie
     * @param standard
     * @return
     */
    List getRetentionIndexMarkerPerformanceForStudy(Studie studie, Integer standard) {


        Sql sql = createBinBaseConnection(studie.databaseColumn)

        List set = []

        String query = """

 select date, (massspec[cast(qualifier as int4)] / (select max(unnest) from (select unnest(massspec)) b)) as ratio,min_ratio,max_ratio,min_apex_sn,apex_sn,  (massspec[cast(quantmass as int4)]) as intensity   from (

    select correction_failed, a.bin_id, a.retention_time,b.name,c.qualifier, (convertspectra(a.spectra)) as "massspec",sample_name,date,min_ratio,max_ratio,min_apex_sn,a.apex_sn,quantmass from spectra a, bin b, standard c,samples d where a.bin_id = b.bin_id and c.bin_id = b.bin_id and a.sample_id = d.sample_id
        and d.sample_id in (
             select sample_id from result a, result_link b where a.setupx = ? and b.result_id = a.result_id
        )
        and a.bin_id = ?  and sample_name  not like '%bl%'
    )
a

order by date


"""

        log.info("query for study: ${studie} - ${standard}")
        log.info("sql: ${query}")

        sql.eachRow(query,[studie.id.toString(),standard]){  def cursor ->

            set.add (createEntry(cursor) )
        }

        sql.close()

        return set

    }

    /**
     * creates a dataset entry
     * @param cursor
     * @return
     */
    private Map createEntry(cursor) {
        def entry = [:]

        entry.date = cursor.date
        entry.ratio = cursor.ratio
        entry.min = cursor.min_ratio
        entry.max = cursor.max_ratio
        entry.sn = cursor.apex_sn
        entry.min_sn = cursor.min_apex_sn
        entry.intensity = cursor.intensity


        entry
    }

    List getBinValuesForStudy(Integer binId, Integer ion, Long study){

        Studie studie1 = Studie.get(study)

        Sql sql = createBinBaseConnection(studie1.databaseColumn)

        List set = []

        String query = """

        select (convertspectra(a.spectra))[?] as "intensity",date,a.sample_id from spectra a, samples b where a.sample_id = b.sample_id and a.sample_id in (
             select sample_id from result a, result_link b where a.result_id = b.result_id and a.setupx = ?
        )
        and a.bin_id = ${binId} order by date


"""

        sql.eachRow(query,[ion,study.toString()]){def cursor ->

            def entry = [:]

            entry.date = cursor.date
            entry.intensity = cursor.intensity
            entry.sample = cursor.sample_id


            set.add (
                    entry
            )

        }

        sql.close()

        return set

    }
    /**
     * gest all the values for the last n days
     */
    List getBinValuesForDates(String database, String instrumentIdentifier, Integer binId, Long from, Long to,Integer ion,String samplePattern = ".*"){


        Sql sql = createBinBaseConnection(database)

        List set = []

        String query = """

        select (convertspectra(a.spectra))[?] as "intensity",date,a.sample_id from spectra a, samples b where a.sample_id = b.sample_id and a.sample_id in (
             select sample_id from samples where visible = 'TRUE' and machine = '${instrumentIdentifier}' and sample_name ~ '${samplePattern}' and date between ? and ?
        )
        and a.bin_id = ${binId} order by date


"""

        sql.eachRow(query,[ion,new java.sql.Date(from),new java.sql.Date(to)]){def cursor ->

            def entry = [:]

            entry.date = cursor.date
            entry.intensity = cursor.intensity
            entry.sample = cursor.sample_id


            set.add (
                    entry
            )

        }

        sql.close()

        return set

    }
    List getBinRatioValuesForDates(String database, String instrumentIdentifier, Integer first,Integer second, Long from, Long to,String samplePattern = ".*"){


        Sql sql = createBinBaseConnection(database)

        List set = []

        String query = """

 select a.intensity/b.intensity as ratio, a.sample_id,date from
(
select (convertspectra(a.spectra))[quantmass] as "intensity",date,a.sample_id
    from
            spectra a,
            samples b,
            bin c

    where a.sample_id = b.sample_id and a.bin_id = c.bin_id and visible = 'TRUE' and machine = '${instrumentIdentifier}' and sample_name ~ '${samplePattern}'  and a.bin_id = ${first}  and date between ? and ?
) a,
(
select (convertspectra(a.spectra))[quantmass] as "intensity",a.sample_id
    from
            spectra a,
            samples b,
            bin c

    where a.sample_id = b.sample_id and a.bin_id = c.bin_id and visible = 'TRUE' and machine = '${instrumentIdentifier}' and sample_name ~ '${samplePattern}'  and a.bin_id = ${second}  and date between ? and ?
) b

where a.sample_id = b.sample_id

"""

        sql.eachRow(query,[new java.sql.Date(from),new java.sql.Date(to),new java.sql.Date(from),new java.sql.Date(to)]){def cursor ->

            def entry = [:]

            entry.date = cursor.date
            entry.ratio = cursor.ratio
            entry.sample = cursor.sample_id


            set.add (
                    entry
            )

        }

        sql.close()

        return set

    }

}
