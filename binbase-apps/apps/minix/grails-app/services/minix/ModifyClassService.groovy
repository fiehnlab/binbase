package minix

import edu.ucdavis.genomics.metabolomics.exception.BinBaseException
import binbase.web.core.BBExperimentSample
import edu.ucdavis.genomics.metabolomics.binbase.bdi.types.experiment.sample.bin.QualityControl
import org.codehaus.groovy.grails.commons.ConfigurationHolder
import binbase.web.core.BBExperimentClass

class ModifyClassService {

    boolean transactional = false

    def searchableService

    /**
     * adds additional samples
     * @param clazz
     * @param count
     * @return
     */
    def addSamples(ExperimentClass clazz, int increaseBy) {
        log.info "adding ${increaseBy} samples to class: ${clazz.id}"

        int count = 0

        if (clazz.samples != null) {

            log.info "class has already: ${clazz.samples.size()} samples"
            count = clazz.samples.size()
        }

        for (int x = count; x < (count + increaseBy); x++) {
            Sample sample = new Sample()

            sample.experimentClass = clazz
            clazz.addToSamples(sample)
            if (!sample.validate()) {
                log.error(sample.errors)
            } else {
                sample.save()
                sample.setFileName("please_change_me_${sample.id}")
                sample.save()
            }
        }
        if (!clazz.validate()) {
            log.error clazz.errors
        }


        clazz.save()

        log.info "class has now: ${clazz.samples.size()} samples"


        return ExperimentClass.get(clazz.id)
    }

    /**
     * increases the class size or keeps it as it is
     * @param clazz
     * @param newSize
     * @return
     */
    def increaseClassSize(ExperimentClass clazz, int newSize) {
        def count = newSize - clazz.samples.size()

        if (count > 0) {
            clazz = addSamples(clazz, count)
        }

        return clazz
    }

    /**
     * adds a single sample to this class
     * @param clazz
     * @return
     */
    def addSample(ExperimentClass clazz) {
        addSamples(clazz, 1)
    }

    /**
     * remmoves the clazz from the associated studie
     * @param clazz
     * @param studie
     * @return
     */
    def removeClass(BaseClass clazz, Studie studie) {

        log.info """deleting class: ${clazz}"""

        Studie.withTransaction {

            if (clazz instanceof SubExperimentClass) {
                SubExperimentClass sub = clazz

                def samples = SubSample.findAllByClazz(sub)

                samples.each {
                    it.relatesToSample = null
                    sub.removeFromAssociatedSamples(it)
                    it.delete(flush: true)
                }

                sub.save(flush: true)
                studie.removeFromClasses(sub)

            } else {
                def samples = BBExperimentSample.findAllByExperimentClass(clazz)

                samples.each { sample ->

                    if (sample instanceof CalibrationSample) {

                        log.info("removing concentration associations...")
                        QualityControlSampleConcentration conc = sample.concentration
                        conc.removeFromSamples(sample)
                        conc.save(flush: true)

                        sample.concentration = null
                        sample.save(flush: true)
                    }

                    clazz.removeFromSamples(sample)
                }

                clazz.save(flush: true)
                studie.removeFromClasses(clazz)
            }
            studie.save(flush: true)
        }
        return studie
    }

    /**
     * merges several classes together, but doesn't save them
     * @param studie
     * @param classes
     * @return
     */
    MergeClass mergeTo(BaseClass... classes) {

        MergeClass result = new MergeClass()

        classes.each { BaseClass clazz ->
            result.addToClasses(clazz)
        }

        return result
    }

    /**
     * adds a calibration class to the studie
     * @param studie
     * @return
     */
    CalibrationClass addCalibrationClass(Studie studie) {

        def classes = []
        studie.classes.each {
            if (it instanceof QualityControlClass) {

            } else if (it instanceof ReactionBlankClass) {
                //ignore
            } else if (it instanceof CalibrationClass) {
                classes.add(it)
            }
        }
        classes.each {
            removeClass(it, studie)
        }

        CalibrationClass clazz = new CalibrationClass()

        clazz.name = "None"

        //organ/species needs to be generate dynamic
        clazz.organ = Organ.findByName("None")
        clazz.species = Species.findByName("None")

        clazz.studie = studie
        clazz.experiment = studie

        studie.addToClasses(clazz)
        def config = ConfigurationHolder.config
        def concentrations = config.minix.standard.concentrations.sort()

        for (int i = 0; i < config.minix.standard.concentrations.size(); i++) {
            CalibrationSample sample = new CalibrationSample()
            addSample(clazz, sample)
        }

        clazz.samples.eachWithIndex { def sample, def position ->
            //we always use the largest concentration for out quality control sample
            registerConcentration(concentrations, position, sample)
        }
        studie.save(flush: true)

        return clazz
    }

    /**
     * adds a calibration class to the studie
     * @param studie
     * @return
     */
    QualityControlClass addQualityClass(Studie studie, int interval = 10) {
        int sampleCount = 0
        def classes = []

        studie.classes.each {
            if (it instanceof QualityControlClass) {
                classes.add(it)
            } else if (it instanceof CalibrationClass) {
                //ignore
            } else if (it instanceof ReactionBlankClass) {
                //ignore
            } else {
                //use to calculate the sample count
                sampleCount = sampleCount + it.samples.size()
            }
        }
        classes.each {
            removeClass(it, studie)
        }


        sampleCount = sampleCount / interval

        log.info "need to generate: ${sampleCount } samples"
        QualityControlClass clazz = new QualityControlClass()

        clazz.name = "None"

        //organ/species needs to be generate dynamic
        clazz.organ = Organ.findByName("None")
        clazz.species = Species.findByName("None")

        clazz.studie = studie
        clazz.experiment = studie

        studie.addToClasses(clazz)

        //if we dont have enough samples to begin with, we will add another sample to have at least one quality control
        if (sampleCount == 0) {
            sampleCount = 1
        }

        def config = ConfigurationHolder.config
        def concentrations = config.minix.standard.concentrations.sort()

        //building the class
        for (int i = 0; i < sampleCount; i++) {
            QualityControlSample sample = new QualityControlSample()
            addSample(clazz, sample)
        }

        clazz.samples.eachWithIndex { def sample, def position ->

            //we always use the largest concentration for out quality control sample
            registerConcentration(concentrations, concentrations.size() - 1, sample)
        }
        log.info "generated samples: ${clazz.samples.size() } "

        studie.save(flush: true)
        return clazz
    }
    /**
     * adds a reaction blank class
     * @param studie
     * @param i
     * @return
     */
    ReactionBlankClass addReactionBlankClass(Studie studie, int interval = 10) {
        int sampleCount = 0
        def classes = []

        studie.classes.each {
            if (it instanceof ReactionBlankClass) {
                classes.add(it)
            } else if (it instanceof QualityControlClass) {
            } else if (it instanceof CalibrationClass) {
                //ignore
            } else {
                //use to calculate the sample count
                sampleCount = sampleCount + it.samples.size()
            }

        }
        classes.each {
            removeClass(it, studie)
        }


        sampleCount = sampleCount / interval

        log.info "need to generate: ${sampleCount } samples"
        ReactionBlankClass clazz = new ReactionBlankClass()

        clazz.name = "None"

        //organ/species needs to be generate dynamic
        clazz.organ = Organ.findByName("None")
        clazz.species = Species.findByName("None")

        clazz.studie = studie
        clazz.experiment = studie

        studie.addToClasses(clazz)

        //if we dont have enough samples to begin with, we will add another sample to have at least one quality control
        if (sampleCount == 0) {
            sampleCount = 1
        }

        //we need one additional sample always to measure first befire quality controls and actual samples
        sampleCount = sampleCount + 1

        for (int i = 0; i < sampleCount; i++) {
            ReactionBlankSample sample = new ReactionBlankSample()
            addSample(clazz, sample)
        }
        log.info "generated samples: ${clazz.samples.size() } "

        studie.save(flush: true)
        return clazz
    }

    /**
     * simple method to add a sample to a class
     * @param clazz
     * @param sample
     * @return
     */
    private def addSample(BaseClass clazz, BBExperimentSample sample) {
        sample.experimentClass = clazz
        clazz.addToSamples(sample)

        if (!sample.validate()) {
            log.error(sample.errors)
        } else {
            sample.save(flush: true)
            sample.setFileName("please_change_me_${sample.id}")
            sample.save(flush: true)
        }
        clazz.save(flush: true)
    }

    /**
     * registers a concentration
     * @param concentrations
     * @param i
     * @param sample
     * @return
     */
    private def registerConcentration(def concentrations, int i, CalibrationSample sample) {
        if (concentrations[i]) {
            def standard = QualityControlSampleConcentration.findByConcentration(concentrations[i])

            if (!standard) {
                standard = new QualityControlSampleConcentration()
                standard.concentration = concentrations[i]
                standard.save(flush: true)
            }

            sample.concentration = standard
            standard.addToSamples(sample)
        }
    }

}
