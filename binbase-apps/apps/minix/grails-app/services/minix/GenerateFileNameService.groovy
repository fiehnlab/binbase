package minix

import java.text.DecimalFormat
import java.text.NumberFormat
import edu.ucdavis.genomics.metabolomics.binbase.algorythm.date.DateUtil

class GenerateFileNameService {
    NumberFormat format = new DecimalFormat("00");

    boolean transactional = true

    def progressService

    def modifyClassService
    /**
     * generates and assignes the file names for the given studie
     *
     * @params studie - the studie which provides all informatiosn
     * @param instrument - related instrument
     * @param date - the date when the measurment should start
     */
    Studie generateFileNames(Studie studie, Instrument instrument, Date date, ShiroUser user, def progressId = false, def progressFactor = 1, boolean randomize = true) {

        int i = 0

        def samples = []

        //get over all classes
        studie.classes.each {BaseClass clazz ->
            if (clazz instanceof ExperimentClass) {

                //get over all samples
                clazz.samples.each {Sample s ->
                    samples.add(s)
                }
            }
        }

        //if the file should be randomized
        if(randomize){
            //here we shuffle all the samples to ensure random data generation
            Collections.shuffle(samples)
        }

        samples.eachWithIndex {Sample s, int index ->

            //generate the name
            def result = generate(date, instrument, user)

            i = i + 1
            s.fileName = result.name
            s.scheduleDate = result.date
            s.fileVersion = 1
            s.save()

            if (progressId) {
                progressService.setProgressBarValue(progressId, index * 100 / (samples.size() * progressFactor + 5))
            }

        }

        //tell the studie that the files were generated
        studie.sampleFilesGenerated = true

        return studie
    }

/**
 * does the actual generation of a name for an instrument
 * @param date
 * @param instrument
 * @return
 */
    private def generate(Date date, Instrument instrument, ShiroUser user) {

        Calendar current = Calendar.getInstance()
        current.setTime date

        SampleNameCounter counter = getCounter(current, instrument, user)


        boolean rollover = true

        if (instrument instanceof GCTof) {
            rollover = ((GCTof) instrument).rollOver
        }

        if (rollover) {

            log.debug "rollover is enabled..."
            //if the max capciaty for this instrument is to high, roll over
            //it's just an aproximiation and not perfect
            while (counter.counter >= instrument.getDailyCapacity()) {
                current.add(Calendar.DAY_OF_MONTH, 1)
                counter = getCounter(current, instrument, user)
            }
        }
        else {
            log.debug "rollover is disabled..."
        }

        counter.counter = counter.counter + 1

        //counter should be valid so lets save it
        counter.save()


        def name = "${counter.prefix}${format.format(counter.counter)}"


        return [name: name, date: DateUtil.stripTime(current.time)]
    }

    private def generateQC(Date date, Instrument instrument, ShiroUser user, String postfix) {

        Calendar current = Calendar.getInstance()
        current.setTime date

        SampleNameCounter counter = getCounterQC(current, instrument, user,postfix)


        boolean rollover = true

        if (instrument instanceof GCTof) {
            rollover = ((GCTof) instrument).rollOver
        }

        if (rollover) {

            log.debug "rollover is enabled..."
            //if the max capciaty for this instrument is to high, roll over
            //it's just an aproximiation and not perfect
            while (counter.counter >= instrument.getDailyCapacity()) {
                current.add(Calendar.DAY_OF_MONTH, 1)
                counter = getCounterQC(current, instrument, user, postfix)
            }
        }
        else {
            log.debug "rollover is disabled..."
        }

        counter.counter = counter.counter + 1

        //counter should be valid so lets save it
        counter.save()


        def name = "${counter.prefix}${format.format(counter.counter)}"


        return [name: name, date: DateUtil.stripTime(current.time)]
    }

    /**
     * retrieves the current sample name counter for this date or generates a new one
     * @param current
     * @param instrument
     * @param user
     * @return
     */
    public synchronized SampleNameCounter getCounter(Calendar current, Instrument instrument, ShiroUser user) {

        String prefix = ""

        prefix = minix.util.FileNameGenerator.generateSampleName(current, instrument, user)

        return getCounter(prefix, current, instrument, user)

    }

    /**
     * retrieves the current sample name counter for this date or generates a new one
     * @param current
     * @param instrument
     * @param user
     * @return
     */
    public synchronized SampleNameCounter getCounterQC(Calendar current, Instrument instrument, ShiroUser user, String postfix) {

        String prefix = ""

        prefix = "${minix.util.FileNameGenerator.generateCalibrationSampleName(current, instrument, user)}${postfix}"

        return getCounter(prefix, current, instrument, user)
    }
    /**
     * retrieves the current sample name counter for this date or generates a new one
     * @param current
     * @param instrument
     * @param user
     * @return
     */
    public synchronized SampleNameCounter getCounterBlank(Calendar current, Instrument instrument, ShiroUser user) {

        String prefix = ""

        prefix = "${minix.util.FileNameGenerator.generateReactionBlankSampleName(current, instrument, user)}"

        return getCounter(prefix, current, instrument, user)
    }

    /**
     * returns the unique counter
     * @param prefix
     * @param current
     * @param instrument
     * @param user
     * @return
     */
    private SampleNameCounter getCounter(String prefix, Calendar current, Instrument instrument, ShiroUser user) {
        SampleNameCounter counter = SampleNameCounter.findByPrefix(prefix)

        if (counter == null) {
            counter = new SampleNameCounter()
            counter.prefix = prefix
            counter.counter = 0
            counter.save()
        }

        //counter is no longer valid, we need to generate a new one
        if (counter.validate() == false) {
            current.add(Calendar.DAY_OF_MONTH, 1)
            return getCounter(current, instrument, user)
        }
        else {
            return counter
        }
    }

    /**
     * adds the calibration standards to the study
     * @param studie
     * @param gcTof
     * @param date
     * @param shiroUser
     * @return
     */
    Studie addCalibrationSamples(Studie studie, GCTof gcTof, Date date, ShiroUser shiroUser) {

        CalibrationClass clazz = modifyClassService.addCalibrationClass(studie)

        clazz.samples.eachWithIndex {CalibrationSample s, int index ->

            int pos = index + 1
            //generate the name
            def result = generateQC(date, gcTof, shiroUser, "${format.format(pos)}-")

            s.fileName = result.name
            s.scheduleDate = result.date
            s.fileVersion = 1
            s.save()
        }

        return studie

    }

    /**
     * adds the calibration standards to the study
     * @param studie
     * @param gcTof
     * @param date
     * @param shiroUser
     * @return
     */
    Studie addQualityControlSamples(Studie studie, GCTof gcTof, Date date, ShiroUser shiroUser, int interval = 10) {

        QualityControlClass clazz = modifyClassService.addQualityClass(studie, interval)

        clazz.samples.eachWithIndex {CalibrationSample s, int index ->

            int pos = index + 1
            //generate the name and all quality control samples have to end with 6
            def result = generateQC(date, gcTof, shiroUser, "${format.format(6)}-")

            s.fileName = result.name
            s.scheduleDate = result.date
            s.fileVersion = 1
            s.save()
        }

        return studie

    }

    /**
     * adds the calibration standards to the study
     * @param studie
     * @param gcTof
     * @param date
     * @param shiroUser
     * @return
     */
    Studie addReactionBlankSamples(Studie studie, GCTof gcTof, Date date, ShiroUser shiroUser, int interval = 10) {

        ReactionBlankClass clazz = modifyClassService.addReactionBlankClass(studie, interval)

        clazz.samples.eachWithIndex {ReactionBlankSample s, int index ->

            int pos = index + 1
            def result = generateReactionBlank(date, gcTof, shiroUser)

            s.fileName = result.name
            s.scheduleDate = result.date
            s.fileVersion = 1
            s.save()
        }

        return studie

    }

    private def generateReactionBlank(Date date, GCTof instrument, ShiroUser user) {

        Calendar current = Calendar.getInstance()
        current.setTime date

        SampleNameCounter counter = getCounterBlank(current, instrument, user)


        boolean rollover = true

        if (instrument instanceof GCTof) {
            rollover = ((GCTof) instrument).rollOver
        }

        if (rollover) {

            log.debug "rollover is enabled..."
            //if the max capciaty for this instrument is to high, roll over
            //it's just an aproximiation and not perfect
            while (counter.counter >= instrument.getDailyCapacity()) {
                current.add(Calendar.DAY_OF_MONTH, 1)
                counter = getCounterBlank(current, instrument, user)
            }
        }
        else {
            log.debug "rollover is disabled..."
        }

        counter.counter = counter.counter + 1

        //counter should be valid so lets save it
        counter.save()


        def name = "${counter.prefix}${format.format(counter.counter)}"


        return [name: name, date: DateUtil.stripTime(current.time)]
    }


}

