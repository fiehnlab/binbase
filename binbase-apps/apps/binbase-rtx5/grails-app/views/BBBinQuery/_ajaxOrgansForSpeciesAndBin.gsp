<table>
    <thead>
    <tr>
        <th class="ui-state-default">organ name</th>
        <th class="ui-state-default">found in samples (abs)</th>
        <th class="ui-state-default">found in samples (rel)</th>
        <th class="ui-state-default">total sample count</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${data}" var="map" status="i">
        <tr>
            <td><g:link controller="BBOrgan" action="show" id="${map.organ_id}" params="[organId:map.organ_id,speciesId:map.species_id]">${map.organ}</g:link></td>
            <td>${map.foundInSamples}</td>
            <td><g:formatNumber number="${map.foundInSamples/map.totalSamples*100}" maxFractionDigits="2"/>%</td>

            <td>${map.totalSamples}</td>
        </tr>
    </g:each>
    </tbody>
</table>