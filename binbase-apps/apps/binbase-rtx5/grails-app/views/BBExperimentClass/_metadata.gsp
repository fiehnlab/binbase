<table>
    <thead>
    <tr>

        <th class="ui-state-default" >
            name
        </th>
        <th class="ui-state-default" >
            value
        </th>

    </tr>
    </thead>
    <tbody>

    <g:each in="${c.metadata}" var="m">
        <tr>
            <td>${m.name}</td>
            <td>${m.value}</td>
        </tr>
    </g:each>
    </tbody>
</table>
