# Welcome #

The BinBase database system is an automated peak annotation and database system developed for the analysis of GC-TOF-MS data for volatile and none volatile data. BinBase was developed to track and identify derivatized metabolites. The BinBase algorithm uses deconvoluted spectra and peak metadata (retention index, unique ion, spectral similarity, peak signal-to-noise ratio, and peak purity) from the Leco ChromaTOF software, and annotates peaks using a multi-tiered filtering system with stringent thresholds.

Utilizing BinBase, users will obtain fully annotated data sheets with quantitative information for all compounds for studies that may consist of thousands of chromatograms.

BinBase has been used to process several hundreds of thousands of samples since 2002 and supports very different matrixes and column configurations.

## Support ##

Commercial support for installation, maintenance of a local BinBase or hosting remote BinBase instances is provided by the company coding and more. You can contact us at contact@codingandmore.org